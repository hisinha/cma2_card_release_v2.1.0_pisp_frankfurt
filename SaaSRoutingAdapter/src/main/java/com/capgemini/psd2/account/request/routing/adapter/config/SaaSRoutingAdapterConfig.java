package com.capgemini.psd2.account.request.routing.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.account.request.routing.adapter.impl.SaaSRoutingAdapter;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;

@Configuration
public class SaaSRoutingAdapterConfig {

	@Bean
	public AuthenticationAdapter saaSRoutingAdapter(){
		return new SaaSRoutingAdapter();
	}
}

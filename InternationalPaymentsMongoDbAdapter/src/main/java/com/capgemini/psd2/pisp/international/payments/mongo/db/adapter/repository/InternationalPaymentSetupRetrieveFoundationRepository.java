package com.capgemini.psd2.pisp.international.payments.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;

public interface InternationalPaymentSetupRetrieveFoundationRepository extends MongoRepository<CustomIPaymentConsentsPOSTResponse, String>{

	  public CustomIPaymentConsentsPOSTResponse findOneByDataConsentId(String consentId );	
}

package com.capgemini.psd2.pisp.international.payments.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.IPaymentsFoundationResource;

public interface InternationalPaymentsFoundationRepository
		extends MongoRepository<IPaymentsFoundationResource, String> {
	
	public IPaymentsFoundationResource findOneByDataInternationalPaymentId(String submissionId);
}

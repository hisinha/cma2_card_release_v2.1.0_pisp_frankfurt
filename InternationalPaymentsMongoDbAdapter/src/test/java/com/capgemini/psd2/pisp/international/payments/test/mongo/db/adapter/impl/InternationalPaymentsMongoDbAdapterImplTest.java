package com.capgemini.psd2.pisp.international.payments.test.mongo.db.adapter.impl;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.IPaymentsFoundationResource;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternational1;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.international.payments.mongo.db.adapter.impl.InternationalPaymentsMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.international.payments.mongo.db.adapter.repository.InternationalPaymentsFoundationRepository;
import com.capgemini.psd2.pisp.international.payments.test.mongo.db.adapter.impl.mockdata.IPaymentsMongoDbAdapterMockData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentsMongoDbAdapterImplTest {

	@Mock
	private InternationalPaymentsFoundationRepository repository;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@Mock
	private SandboxValidationUtility utility;

	@InjectMocks
	private List<OBCharge1> chargesList = new ArrayList<OBCharge1>();

	@InjectMocks
	private InternationalPaymentsMongoDbAdapterImpl adapter;

	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testProcessPaymentsMultiAuthorised() {
		CustomIPaymentsPOSTRequest request = IPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);

		IPaymentsFoundationResource resource = new IPaymentsFoundationResource();
		OBExchangeRate2 information = new OBExchangeRate2();
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(IPaymentsFoundationResource.class))).thenReturn(resource);
		adapter.processPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessPaymentsAwaitingFurtherAuthorisation() {
		CustomIPaymentsPOSTRequest request = IPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;
		
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);

		IPaymentsFoundationResource resource = new IPaymentsFoundationResource();
		OBExchangeRate2 information = new OBExchangeRate2();
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(IPaymentsFoundationResource.class))).thenReturn(resource);
		adapter.processPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessPaymentsRejectedAuthorisation() {
		CustomIPaymentsPOSTRequest request = IPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		IPaymentsFoundationResource resource = new IPaymentsFoundationResource();
		OBExchangeRate2 information = new OBExchangeRate2();
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(IPaymentsFoundationResource.class))).thenReturn(resource);
		adapter.processPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessPaymentsNullAuthorisation() {
		CustomIPaymentsPOSTRequest request = IPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.FAIL;
		
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		
		OBMultiAuthorisation1 multiAuthorisation = null;

		OBExchangeRate2 information = new OBExchangeRate2();
		IPaymentsFoundationResource resource = new IPaymentsFoundationResource();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(IPaymentsFoundationResource.class))).thenReturn(resource);
		adapter.processPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testProcessPaymentsNullSubmissionId() {
		CustomIPaymentsPOSTRequest request = IPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = null;
		
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		IPaymentsFoundationResource resource = new IPaymentsFoundationResource();
		OBExchangeRate2 information = new OBExchangeRate2();
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(IPaymentsFoundationResource.class))).thenReturn(resource);
		adapter.processPayments(request, new HashMap<>(), new HashMap<>());
		assertNull(processStatus);
	}
	

	@Test
	public void testProcessPaymentsValildContractIdentification() {
		CustomIPaymentsPOSTRequest request = IPaymentsMongoDbAdapterMockData.getRequest();
		request.getData().getInitiation().getExchangeRateInformation().setContractIdentification("Test");
		
		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = null;
		
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		IPaymentsFoundationResource resource = new IPaymentsFoundationResource();
		OBExchangeRate2 information = new OBExchangeRate2();
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(IPaymentsFoundationResource.class))).thenReturn(resource);
		adapter.processPayments(request, new HashMap<>(), new HashMap<>());
		assertNull(processStatus);
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessPayments_DataAccessResourceFailureException() {
		CustomIPaymentsPOSTRequest request = IPaymentsMongoDbAdapterMockData.getRequest();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);
		
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");

		OBExchangeRate2 information = new OBExchangeRate2();
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedExchangeRateInformation(any(), anyString())).thenReturn(information);
		when(repository.save(any(IPaymentsFoundationResource.class)))
				.thenThrow(new DataAccessResourceFailureException("Test"));
		adapter.processPayments(request, new HashMap<>(), new HashMap<>());
	}

	@Test
	public void testRetrieve() {

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		IPaymentsFoundationResource resource = IPaymentsMongoDbAdapterMockData.getResource();

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);

		when(repository.findOneByDataInternationalPaymentId(anyString())).thenReturn(resource);

		adapter.retrieveStagedPaymentsResponse(stageIdentifiers, new HashMap<>());

	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveException() {
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		IPaymentsFoundationResource resource = IPaymentsMongoDbAdapterMockData.getResource();

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);

		when(repository.findOneByDataInternationalPaymentId(anyString())).thenReturn(resource);

		adapter.retrieveStagedPaymentsResponse(stageIdentifiers, new HashMap<>());
	}

	
	@Test
	public void createstage_Test_CIExRate() {

		CustomIPaymentsPOSTRequest request = new CustomIPaymentsPOSTRequest();
		OBWriteDataInternational1 data = new OBWriteDataInternational1();
		request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("USD");
		initiation.setInstructedAmount(instructedAmount);

		OBCharge1 charge = new OBCharge1();
		OBCharge1Amount x = new OBCharge1Amount();
		x.setAmount("11.11");
		x.setCurrency("USD");
		charge.setAmount(x);
		charge.setChargeBearer(OBChargeBearerType1Code.SHARED);
		adapter.processPayments(request, new HashMap<>(), new HashMap<>());

	}
	
	@Test
	public void createstage_Test_CIExRate_NonMatching() {

		CustomIPaymentsPOSTRequest Request = new CustomIPaymentsPOSTRequest();
		OBWriteDataInternational1 data = new OBWriteDataInternational1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		ReflectionTestUtils.setField(adapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("USD");
		initiation.setInstructedAmount(instructedAmount);
		OBCharge1 charge = new OBCharge1();
		OBCharge1Amount x = new OBCharge1Amount();
		x.setAmount("11.11");
		x.setCurrency("USD");
		charge.setAmount(x);
		charge.setChargeBearer(OBChargeBearerType1Code.SHARED);
		adapter.processPayments(Request, new HashMap<>(), new HashMap<>());
	}

	
	@After
	public void tearDown() {
		utility = null;
		reqAttributes = null;
		repository = null;
		adapter = null;
	}
}

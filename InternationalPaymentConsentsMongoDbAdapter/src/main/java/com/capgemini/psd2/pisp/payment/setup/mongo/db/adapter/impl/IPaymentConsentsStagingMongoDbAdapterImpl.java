package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.IPaymentConsentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("iPaymentConsentsStagingMongoDbAdapter")
public class IPaymentConsentsStagingMongoDbAdapterImpl implements InternationalPaymentStagingAdapter {

	@Autowired
	private IPaymentConsentsFoundationRepository iPaymentConsentsBankRepository;

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private RequestHeaderAttributes reqAttributes;

	@Value("${app.sandboxValidationPolicies.contractIdentificationPattern:^Test[a-zA-Z0-9]*}")
	private String contractIdentificationPattern;

	@Override
	public CustomIPaymentConsentsPOSTResponse processInternationalPaymentConsents(
			CustomIPaymentConsentsPOSTRequest customRequest, CustomPaymentStageIdentifiers customStageIdentifiers,
			Map<String, String> params, OBExternalConsentStatus1Code successStatus,
			OBExternalConsentStatus1Code failureStatus) {

		CustomIPaymentConsentsPOSTResponse paymentConsentsBankResource = transformInternationalPaymentConsentsToBankResource(
				customRequest);

		String currency = customRequest.getData().getInitiation().getInstructedAmount().getCurrency();
		String initiationAmount = customRequest.getData().getInitiation().getInstructedAmount().getAmount();

		if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_POST_MOCKING)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}

		// Sandbox Secondary Identification Validation
		String secondaryIdentification = customRequest.getData().getInitiation().getCreditorAccount()
				.getSecondaryIdentification();
		if (utility.isValidSecIdentification(secondaryIdentification)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTSECONDARYIDENTIFIER,
							ErrorMapKeys.ACCOUNT_SECONDARYIDENTIFIER_INVALID));
		}
		OBChargeBearerType1Code reqChargeBearer = customRequest.getData().getInitiation().getChargeBearer();

		String consentId = null;
		String expectedExecutionDateTime = utility.getMockedExpectedExecDtTm();
		String expectedSettlementDateTime = utility.getMockedExpectedSettlementDtTm();
		String cutOffDateTime = utility.getMockedCutOffDtTm();
		List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, reqChargeBearer);
		OBExchangeRate2 exchangeRateInformation = utility.getMockedExchangeRateInformation(
				customRequest.getData().getInitiation().getExchangeRateInformation(), initiationAmount);
		ProcessConsentStatusEnum processConsentStatusEnum = utility.getMockedConsentProcessExecStatus(currency,
				initiationAmount);

		/*
		 * Failure mocked condition for sandbox - Invalid Contract Identification and
		 * Exchange Rate
		 */
		if (null != customRequest.getData().getInitiation().getExchangeRateInformation()
				&& null != customRequest.getData().getInitiation().getExchangeRateInformation()
						.getContractIdentification()
				&& isRejectionContractIdentification(customRequest.getData().getInitiation()
						.getExchangeRateInformation().getContractIdentification())) {
			processConsentStatusEnum = ProcessConsentStatusEnum.FAIL;
		}

		if (processConsentStatusEnum == ProcessConsentStatusEnum.PASS
				|| processConsentStatusEnum == ProcessConsentStatusEnum.FAIL)
			consentId = UUID.randomUUID().toString();

		paymentConsentsBankResource.getData().setCreationDateTime(customRequest.getCreatedOn());
		paymentConsentsBankResource.getData().setConsentId(consentId);
		paymentConsentsBankResource.getData().setCharges(charges);
		paymentConsentsBankResource.getData().setExpectedExecutionDateTime(expectedExecutionDateTime);
		paymentConsentsBankResource.getData().setExpectedSettlementDateTime(expectedSettlementDateTime);
		paymentConsentsBankResource.getData().setCutOffDateTime(cutOffDateTime);
		paymentConsentsBankResource.setConsentPorcessStatus(processConsentStatusEnum);
		paymentConsentsBankResource.getData().setExchangeRateInformation(exchangeRateInformation);

		if (consentId != null) {
			OBExternalConsentStatus1Code status = calculateCMAStatus(processConsentStatusEnum, successStatus,
					failureStatus);
			paymentConsentsBankResource.getData().setStatus(status);
			paymentConsentsBankResource.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}

		try {
			iPaymentConsentsBankRepository.save(paymentConsentsBankResource);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_TECHNICAL_ERROR_FS_PRE_STAGE_INSERTION));
		}
		return paymentConsentsBankResource;
	}

	@Override
	public CustomIPaymentConsentsPOSTResponse retrieveStagedInternationalPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		try {
			String paymentConsentId = customPaymentStageIdentifiers.getPaymentConsentId();

			CustomIPaymentConsentsPOSTResponse paymentBankResource = iPaymentConsentsBankRepository
					.findOneByDataConsentId(paymentConsentId);

			if (paymentBankResource == null || paymentBankResource.getData() == null
					|| paymentBankResource.getData().getInitiation() == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));

			String initiationAmount = null;
			if (paymentBankResource != null && paymentBankResource.getData() != null
					&& paymentBankResource.getData().getInitiation() != null
					&& paymentBankResource.getData().getInitiation().getInstructedAmount() != null) {
				initiationAmount = paymentBankResource.getData().getInitiation().getInstructedAmount().getAmount();
			}
			
			if ("GET".equals(reqAttributes.getMethodType())
					&& (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING))) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
			paymentBankResource.setId(null);

			return paymentBankResource;

		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_TECHNICAL_ERROR_FS_PAYMENT_RETRIEVE));
		}
	}

	private CustomIPaymentConsentsPOSTResponse transformInternationalPaymentConsentsToBankResource(
			CustomIPaymentConsentsPOSTRequest paymentConsentsRequest) {
		String paymentFoundationRequest = JSONUtilities.getJSONOutPutFromObject(paymentConsentsRequest);
		return JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(), paymentFoundationRequest,
				CustomIPaymentConsentsPOSTResponse.class);
	}

	private OBExternalConsentStatus1Code calculateCMAStatus(ProcessConsentStatusEnum processConsentStatusEnum,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return (processConsentStatusEnum == ProcessConsentStatusEnum.PASS) ? successStatus : failureStatus;
	}

	private boolean isRejectionContractIdentification(String contractIdentification) {
		if (contractIdentification.matches(contractIdentificationPattern)) {
			return true;
		}
		// Default Case
		return false;
	}
}
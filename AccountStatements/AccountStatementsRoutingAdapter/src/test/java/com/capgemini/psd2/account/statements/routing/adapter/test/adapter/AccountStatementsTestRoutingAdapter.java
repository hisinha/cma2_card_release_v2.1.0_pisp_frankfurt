/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.statements.routing.adapter.test.adapter;

import java.util.Map;

import com.capgemini.psd2.account.statements.routing.adapter.test.mock.data.AccountStatementsRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountStatementTestRoutingAdapter.
 */
public class AccountStatementsTestRoutingAdapter implements AccountStatementsAdapter{


	@Override
	public PlatformAccountStatementsResponse retrieveAccountStatements(AccountMapping accountMapping,
			Map<String, String> params) {
		// TODO Auto-generated method stub
		return AccountStatementsRoutingAdapterTestMockData.getMockStatementGETResponse();
	}

	@Override
	public PlatformAccountStatementsResponse retrieveAccountStatementsByStatementId(AccountMapping accountMapping,
			Map<String, String> params) {
		// TODO Auto-generated method stub
		return AccountStatementsRoutingAdapterTestMockData.getMockStatementGETResponse();
	}

	@Override
	public PlatformAccountTransactionResponse retrieveAccountTransactionsByStatementsId(AccountMapping accountMapping,
			Map<String, String> params) {
		// TODO Auto-generated method stub
		return AccountStatementsRoutingAdapterTestMockData.getMockTransactionGETResponse();
	}

	@Override
	public PlatformAccountSatementsFileResponse downloadStatementFileByStatementsId(AccountMapping accountMapping,
			Map<String, String> params) {
		// TODO Auto-generated method stub
		return AccountStatementsRoutingAdapterTestMockData.getMockFileGETResponse();
	}

	
}


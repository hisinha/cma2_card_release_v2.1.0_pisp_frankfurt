package com.capgemini.psd2.account.statements.mongo.db.adapter.test.mock.data;

import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStatementFileCMA2;

public class AccountStatementFileTestData {
	
	public static AccountStatementFileCMA2 getAccountStatementsFileData(){
		AccountStatementFileCMA2 fileCMA2 = new AccountStatementFileCMA2();
		fileCMA2.setStatementFileName("local.pdf");
		return fileCMA2;
	}
}

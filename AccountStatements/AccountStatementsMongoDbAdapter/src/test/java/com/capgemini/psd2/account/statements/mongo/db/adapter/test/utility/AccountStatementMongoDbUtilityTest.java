package com.capgemini.psd2.account.statements.mongo.db.adapter.test.utility;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.statements.mongo.db.adapter.constants.AccountStatementMongoDbAdapterConstants;
import com.capgemini.psd2.account.statements.mongo.db.adapter.domain.DateRange;
import com.capgemini.psd2.account.statements.mongo.db.adapter.test.mock.data.AccountStatementTestData;
import com.capgemini.psd2.account.statements.mongo.db.adapter.utility.AccountStatementsMongoDbAdapterUtility;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementMongoDbUtilityTest {

	
	@InjectMocks
	private AccountStatementsMongoDbAdapterUtility utility;
	
	Map<String, String> params = new HashMap<>();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}
	
	@Test
	public void createTransactionDateRangeTestWithAllData() {
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2017-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2018-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2020-01-01T00:00:00");
		
		utility.createTransactionDateRange(params);
	}
	
	@Test
	public void createTransactionDateRangeTestWithNullData() {
		
		utility.createTransactionDateRange(params);
	}
	
	@Test
	public void createTransactionDateRangeTestWithTogreaterConsent() {
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2021-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2021-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2020-01-01T00:00:00");
		
		utility.createTransactionDateRange(params);
	}
	
	@Test(expected = PSD2Exception.class)
	public void fsCallFilterExpiredConsent () {
		
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2021-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2021-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2018-01-01T00:00:00");
		
		DateRange transactionDateRange = utility.createTransactionDateRange(params);
		utility.fsCallFilter(transactionDateRange);
	}
	
	/*@Test
	public void fsCallFilterFromAfterConsent() {
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2020-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2021-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2020-01-01T00:00:00" );
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2021-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2019-01-01T00:00:00");
		
		DateRange transactionDateRange = utility.createTransactionDateRange(params);
		utility.fsCallFilter(transactionDateRange);
	}*/
	
	@Test
	public void fsCallFilterFromBeforeFromFilter() {
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2020-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2021-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2019-01-01T00:00:00" );
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2021-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2022-01-01T00:00:00");
		
		DateRange transactionDateRange = utility.createTransactionDateRange(params);
		utility.fsCallFilter(transactionDateRange);
	}
	
	@Test
	public void fsCallFilterToBeforeToFilter() {
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2019-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2020-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2018-01-01T00:00:00" );
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2022-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2023-01-01T00:00:00");
		
		DateRange transactionDateRange = utility.createTransactionDateRange(params);
		utility.fsCallFilter(transactionDateRange);
	}
	
	@Test
	public void fsCallFilterToBeforeTransactionDate() {
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2019-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2020-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2019-02-01T00:00:00" );
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2020-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2023-01-01T00:00:00");
		
		DateRange transactionDateRange = utility.createTransactionDateRange(params);
		utility.fsCallFilter(transactionDateRange);
	}

	@Test
    public void populateStamentIdAndLinksTest() {
     utility.populateStamentIdAndLinks(AccountStatementTestData.getAccountStatementsDataPage(), null);

    }
	
}

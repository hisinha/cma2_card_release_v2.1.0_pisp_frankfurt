package com.capgemini.psd2.account.statements.mongo.db.adapter.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountTransactionCMA2;

public interface AccountStatementTransactionRepository extends MongoRepository<AccountTransactionCMA2, String> {

Page<AccountTransactionCMA2> findByAccountNumberAndAccountNSCAndStatementId(String accountNumber, String accountNsc, String statementId, Pageable pageable);

Page<AccountTransactionCMA2> findByAccountNumberAndAccountNSCAndStatementIdAndCreditDebitIndicator(
		String accountNumber, String accountNsc, String statementId,  String indicator,  Pageable pageable);

}

package com.capgemini.psd2.account.statements.mongo.db.adapter.domain;

import java.time.LocalDateTime;

public class DateRange {

	private LocalDateTime consentExpiryDateTime;
	private LocalDateTime transactionFromDateTime;
	private LocalDateTime transactionToDateTime;
	private LocalDateTime requestDateTime;
	private LocalDateTime filterFromDate;
	private LocalDateTime filterToDate;
	private LocalDateTime newFilterFromDate;
	private LocalDateTime newFilterToDate;
	private boolean emptyResponse;

	public LocalDateTime getNewFilterFromDate() {
		return newFilterFromDate;
	}

	public void setNewFilterFromDate(LocalDateTime newFilterFromDate) {
		this.newFilterFromDate = newFilterFromDate;
	}

	public LocalDateTime getNewFilterToDate() {
		return newFilterToDate;
	}

	public void setNewFilterToDate(LocalDateTime newFilterToDate) {
		this.newFilterToDate = newFilterToDate;
	}

	public LocalDateTime getConsentExpiryDateTime() {
		return consentExpiryDateTime;
	}

	public void setConsentExpiryDateTime(LocalDateTime consentExpiryDateTime) {
		this.consentExpiryDateTime = consentExpiryDateTime;
	}

	public LocalDateTime getTransactionFromDateTime() {
		return transactionFromDateTime;
	}

	public void setTransactionFromDateTime(LocalDateTime transactionFromDateTime) {
		this.transactionFromDateTime = transactionFromDateTime;
	}

	public LocalDateTime getTransactionToDateTime() {
		return transactionToDateTime;
	}

	public void setTransactionToDateTime(LocalDateTime transactionToDateTime) {
		this.transactionToDateTime = transactionToDateTime;
	}

	public LocalDateTime getRequestDateTime() {
		return requestDateTime;
	}

	public void setRequestDateTime(LocalDateTime requestDateTime) {
		this.requestDateTime = requestDateTime;
	}

	public LocalDateTime getFilterFromDate() {
		return filterFromDate;
	}

	public void setFilterFromDate(LocalDateTime filterFromDate) {
		this.filterFromDate = filterFromDate;
	}

	public LocalDateTime getFilterToDate() {
		return filterToDate;
	}

	public void setFilterToDate(LocalDateTime filterToDate) {
		this.filterToDate = filterToDate;
	}

	public boolean isEmptyResponse() {
		return emptyResponse;
	}

	public void setEmptyResponse(boolean emptyResponse) {
		this.emptyResponse = emptyResponse;
	}


}

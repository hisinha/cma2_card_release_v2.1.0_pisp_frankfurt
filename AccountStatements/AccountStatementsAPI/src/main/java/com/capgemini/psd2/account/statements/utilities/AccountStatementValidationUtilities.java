package com.capgemini.psd2.account.statements.utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AccountStatementValidationUtilities {

	@Value("${app.minPageSize}")
	private String minPageSize;

	@Value("${app.maxPageSize}")
	private String maxPageSize;

	public void validateParams(Integer pageNumber, Integer pageSize) {
		
		if ((pageNumber != null) && (pageNumber.intValue() < 0)) {

			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
		}
		if ((pageSize != null) && ((pageSize.intValue() < Integer.valueOf(minPageSize))
				|| (pageSize.intValue() > Integer.valueOf(maxPageSize)))) {

			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
		}
	}
	
	public void validateDate(String fromStatementDateTime, String toStatementDateTime) {

		if (!NullCheckUtils.isNullOrEmpty(fromStatementDateTime))
			DateUtilites.validateStringInISODateTimeWithoutTimeZone(fromStatementDateTime);

		if (!NullCheckUtils.isNullOrEmpty(toStatementDateTime))
			DateUtilites.validateStringInISODateTimeWithoutTimeZone(toStatementDateTime);

		if (!NullCheckUtils.isNullOrEmpty(toStatementDateTime) && !NullCheckUtils.isNullOrEmpty(fromStatementDateTime)
				&& !isDateComparisonPassed(fromStatementDateTime, toStatementDateTime)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));

		}

	}
	
	public boolean isDateComparisonPassed(String inputfromDateTime, String inputtoDateTime) {

		boolean dateTimeComparison = false;
		LocalDateTime fromDateTime = null;
		LocalDateTime toDateTime = null;

		try {
			fromDateTime = LocalDateTime.parse(inputfromDateTime, DateTimeFormatter.ISO_DATE_TIME);
			toDateTime = LocalDateTime.parse(inputtoDateTime, DateTimeFormatter.ISO_DATE_TIME);
		} catch (DateTimeParseException e) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
		}

		if (fromDateTime.isBefore(toDateTime) || fromDateTime.isEqual(toDateTime)) {
			dateTimeComparison = true;
		}

		return dateTimeComparison;
	}
}

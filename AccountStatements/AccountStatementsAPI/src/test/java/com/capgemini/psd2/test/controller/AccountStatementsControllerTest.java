package com.capgemini.psd2.test.controller;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.account.statements.controller.AccountStatementsController;
import com.capgemini.psd2.account.statements.service.AccountStatementsService;
import com.capgemini.psd2.account.statements.utilities.AccountStatementValidationUtilities;
import com.capgemini.psd2.aisp.domain.OBReadStatement1;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.test.mock.data.AccountStatementsApiTestData;


@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsControllerTest 
{

	/** The service. */
	@Mock
	private AccountStatementsService service;
	
	@Mock
	private AccountStatementValidationUtilities accountStatementValidationUtilities;

	/** The mock mvc. */
	private MockMvc mockMvc;

	/** The controller. */
	@InjectMocks
	private AccountStatementsController controller;

	/** The statement response. */
	OBReadStatement1 statementsGETResponse = null;
	
	/** The statement response. */
	OBReadTransaction3 transactionsGETResponse = null;
	
	PlatformAccountSatementsFileResponse fileResponse = null;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}
//statements
	/**
	 * Test null account id. Failure test
	 */
	/*@Test(expected = PSD2Exception.class)
	public void testNullAccountId() {
		controller.retrieveAccountStatements(null, "2012-01-01T00:00:00+00:00", "2016-01-01T00:00:00+00:00", 3, 4, "dsd");
	}*/
	
	/**
	 * Test invalid account id length.
	 */
	/*@Test(expected = PSD2Exception.class)
	public void testInvalidAccountIdLength() {
		controller.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a-qwer23454c51326fdr34-12345123smcge36dg337b", "2012-01-01T00:00:00+00:00", "2016-01-01T00:00:00+00:00", 3, 4, "dsd");
	}*/

	/**
	 * Test.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void successTest() {
		when(service.retrieveAccountStatements(anyString(), anyString(), anyString(), anyInt(), anyInt(), anyString())).thenReturn(statementsGETResponse);
		try {
			this.mockMvc.perform(get("/accounts/{accountId}/statements", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2012-01-01T00:00:00+00:00", "2016-01-01T00:00:00+00:00", 1, 1, "dsd")
					.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	
	//statements/statementId
		/**
		 * Test null account id. Failure test
		 */
	
	
		/*@Test(expected = PSD2Exception.class)
		public void testNullAccountIdForStatementIdUrl() {
			doNothing(accountStatementValidationUtilities.validateParams(anyString(), pageNumber, pageSize);
			controller.retrieveAccountStatementByStatementsId(null , "essdt", 3, 4, "dsd");
		}*/
		
		/**
		 * Test invalid account id length.
		 */
		/*@Test(expected = PSD2Exception.class)
		public void testInvalidAccountIdLengthForStatementIdUrl() {
			controller.retrieveAccountStatementByStatementsId("269c3ff5d7f8-4", "20erweruyewruiewyruewiriwerhuweirhiewhfewuiry789322648926347238647234y674yuierhjdfdsfhghewfguiweryewuiryiewury8923748972348927443iryhuieshjferifheruiy934875983475893454", 3, 4, "dsd");
		}
*/
	
	
		/**
		 * Test.
		 *
		 * @throws Exception the exception
		 */
		@Test
		public void successTestForStatementIdUrl() {
			when(service.retrieveAccountStatementsByStatementsId(anyString(), anyString(), anyInt(), anyInt(), anyString())).thenReturn(statementsGETResponse);
			try {
				this.mockMvc.perform(get("/accounts/{accountId}/statements/{statementId}", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2016", 1, 1, "dsd")
						.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		/**
		 * Tear down.
		 *
		 * @throws Exception the exception
		 */
		
		//statements/statementId/transactions
		/**
		 * Test null account id. Failure test
		 */
		/*@Test(expected = PSD2Exception.class)
		public void testNullAccountIdForTransactionsUrl() {
			controller.retrieveAccountTransactionsByStatementsId(null, "2012-dhdw", 3, 4, "dsd");
		}
		*/
		/**
		 * Test null statement id. Failure test
		 */
		/*@Test(expected = PSD2Exception.class)
		public void testNullStatementIdForTransactionsUrl() {
			controller.retrieveAccountTransactionsByStatementsId("2012-dhdw", null, 3, 4, "dsd");
		}*/
		
		/**
		 * Test invalid account id length.
		 */
		/*@Test(expected = PSD2Exception.class)
		public void testInvalidAccountIdLengthForTransactionsUrl() {
			controller.retrieveAccountTransactionsByStatementsId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a-qwer23454c51326fdr34-12345123smcge36dg337b", "2012uhfiwef", 3, 4, "dsd");
		}*/
		
		/**
		 * Test invalid statementId id length.
		 */
		/*@Test(expected = PSD2Exception.class)
		public void testInvalidStatementtIdLengthForTransactionsUrl() {
			controller.retrieveAccountTransactionsByStatementsId("269c3ff5-d7f8-419b-a", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a-qwer23454c51326fdr34-12345123smcge36dg337b", 3, 4, "dsd");
		}*/

		/**
		 * Test.
		 *
		 * @throws Exception the exception
		 */
		@Test
		public void successTestForTransactionsUrl() {
			when(service.retrieveAccountTransactionsByStatementsId(anyString(), anyString(), anyInt(), anyInt(), anyString())).thenReturn(transactionsGETResponse);
			try {
				this.mockMvc.perform(get("/accounts/{accountId}/statements/{statementId}/transactions", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2012dhwj", 1, 1, "dsd")
						.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
		
		//statements/statementId/file
				/**
				 * Test null account id. Failure test
				 */
				/*@Test(expected = PSD2Exception.class)
				public void testNullAccountIdForFileUrl() {
					try {
						controller.downloadStatementFileByStatementsId(null, "2012-dhdw", null);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}*/
				
				/**
				 * Test null statement id. Failure test
				 */
				/*@Test(expected = PSD2Exception.class)
				public void testNullStatementIdForFileDownload() {
					try {
						controller.downloadStatementFileByStatementsId("2012-dhdw", null, null);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}*/
				
				/**
				 * Test invalid account id length.
				 */
				/*@Test(expected = PSD2Exception.class)
				public void testInvalidAccountIdLengthForFileDownload() {
					try {
						controller.downloadStatementFileByStatementsId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a-qwer23454c51326fdr34-12345123smcge36dg337b", null, null);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}*/
				
				/**
				 * Test invalid statementId id length.
				 */
				/*@Test(expected = PSD2Exception.class)
				public void testInvalidStatementtIdLengthForFileDownload() {
					try {
						controller.downloadStatementFileByStatementsId("269c3ff5-d7f8-419b-a", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a-qwer23454c51326fdr34-12345123smcge36dg337b", null);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
*/
				/**
				 * Test.
				 *
				 * @throws Exception the exception
				 */
				@Test
				public void successTestForFileDownload() {
					when(service.downloadStatementFileByStatementsId(anyString(), anyString())).thenReturn(AccountStatementsApiTestData.getMockFileResponse());
					try {
						this.mockMvc.perform(get("/accounts/{accountId}/statements/{statementId}/file", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2012dhwj", null)
								.contentType(MediaType.APPLICATION_PDF_VALUE)).andExpect(status().isOk());
					} catch (Exception e) {
						
						e.printStackTrace();
					}
				}
	
	@After
	public void tearDown() throws Exception {
		controller = null;
	}
}

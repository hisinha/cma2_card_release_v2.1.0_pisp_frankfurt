package com.capgemini.psd2.test.service.impl;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadStatement1;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.domain.OBTransaction3;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.response.validator.ResponseValidator;

@Component
public class AccountStatementsResponseValidator extends ResponseValidator {

	@Override
	public void performPlatformValidation(Object tppProductsResponse) {
		
		OBReadTransaction3 input = (OBReadTransaction3)tppProductsResponse;
		OBReadStatement1 input1 = (OBReadStatement1)tppProductsResponse;

		
		for(OBTransaction3 obTransaction2:input.getData().getTransaction()) {
			if(obTransaction2.getStatementReference()!=null) {
				for(String reference:obTransaction2.getStatementReference()) {
					if(reference.length()>35) {
						throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
					}
				}
			}
		}

		
		
	}
	
	
}

/*************************************************************************
* 
 * CAPGEMINI CONFIDENTIAL
* __________________
* 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
* 
 * NOTICE:  All information contained herein is, and remains
* the property of CAPGEMINI GROUP.
* The intellectual and technical concepts contained herein
* are proprietary to CAPGEMINI GROUP and may be covered
* by patents, patents in process, and are protected by trade secret
* or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from CAPGEMINI GROUP.
*/

/*************************************************************************
* 
 * CAPGEMINI CONFIDENTIAL
* __________________
* 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
* 
 * NOTICE:  All information contained herein is, and remains
* the property of CAPGEMINI GROUP.
* The intellectual and technical concepts contained herein
* are proprietary to CAPGEMINI GROUP and may be covered
* by patents, patents in process, and are protected by trade secret
* or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from CAPGEMINI GROUP.
*/

package com.capgemini.psd2.security.saas.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter;
import com.capgemini.psd2.scaconsenthelper.filters.PSD2SecurityFilter;
import com.capgemini.psd2.security.saas.handlers.CustomAuthenticationFailureHandler;
import com.capgemini.psd2.security.saas.handlers.CustomAuthenticationProvider;
import com.capgemini.psd2.security.saas.handlers.SimpleUrlAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomAuthenticationProvider authenticationProvider;

	@Autowired
	private SimpleUrlAuthenticationSuccessHandler successHandler;

	@Autowired
	private CustomAuthenticationFailureHandler failureHandler;
	
	@Autowired
	private PSD2SecurityFilter psd2SecurityFilter;
	
	@Autowired
	private CookieHandlerFilter cookieHandlerFilter;

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	/**
	 * Configured the CustomAuthenticationProvider to perform Multi Factor
	 * Authentication with the help of Ping Directory (LDAP) & Ping ID
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider);
	}

	/*@Bean
	public ContentNegotiatingViewResolver contentViewResolver() throws Exception {
		ContentNegotiationManagerFactoryBean contentNegotiationManager = new ContentNegotiationManagerFactoryBean();
		contentNegotiationManager.addMediaType("json", MediaType.APPLICATION_JSON);


		MappingJackson2JsonView defaultView = new MappingJackson2JsonView();
		defaultView.setExtractValueFromSingleKeyModel(true);

		ContentNegotiatingViewResolver contentViewResolver = new ContentNegotiatingViewResolver();
		contentViewResolver.setContentNegotiationManager(contentNegotiationManager.getObject());
		contentViewResolver.setDefaultViews(Arrays.<View>asList(defaultView));
		return contentViewResolver;
	}*/
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**","/sca-ui/**","/ext-libs/**","/securityLogout","/localization/**","/js/**","/css/**","/img/**","/fonts/**","/WEB-INF/**","/errors","/sessionErrors","/restart","/ui/staticContentUpdate","/ui/staticContentGet");
	}

	/**
	 * Security configuration done.. to protect some resources and to define
	 * login action & configure success & failure handler.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/restart").permitAll();
		http.authorizeRequests().antMatchers("/ui/staticContentUpdate").permitAll();
		http.authorizeRequests().antMatchers("/ui/staticContentGet").permitAll();
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http.authorizeRequests().anyRequest()
		.permitAll()
		.antMatchers("/console/**").permitAll()
		.and()
		.logout().logoutUrl("/logout").logoutSuccessUrl("/login")
		.and()
		.formLogin().loginProcessingUrl("/login")
		.successHandler(successHandler).failureHandler(failureHandler).loginPage("/login")
		.and()
		.csrf().disable()
		.addFilterBefore(cookieHandlerFilter, AbstractPreAuthenticatedProcessingFilter.class)
		.addFilterBefore(psd2SecurityFilter, AbstractPreAuthenticatedProcessingFilter.class);

	}
	
}
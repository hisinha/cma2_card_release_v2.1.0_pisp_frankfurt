package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.delegate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import com.capgemini.psd2.adapter.datetime.utility.DateAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementsResponse;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.transformer.AccountStatementsFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AccountStatementsFoundationServiceDelegate {

	@Value("${foundationService.correlationIdInReqHeader:#{X-API-TRANSACTION-ID}}")
	private String correlationIdInReqHeader;

	@Value("${foundationService.sourceSystemReqHeader:#{X-API-SOURCE-SYSTEM}}")
	private String sourceSystemReqHeader;

	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;

	@Value("${foundationService.channelInReqHeader:#{X-API-CHANNEL-CODE}}")
	private String channelInReqHeader;

	@Value("${foundationService.maskedPan:#{X-MASKED-PAN}}")
	private String maskedPan;

	@Value("${foundationService.singleAccountStatementsFileBaseURL}")
	private String singleAccountStatementsFileBaseURL;

	@Value("${foundationService.singleAccountStatementsCreditCardFileBaseURL}")
	private String singleAccountStatementsCreditCardFileBaseURL;

	@Autowired
	private AccountStatementsFoundationServiceTransformer accountStatementsFSTransformer;

	public HttpHeaders createRequestHeadersFile(RequestInfo requestInfo, AccountMapping accountMapping,
			Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();

		if (!((singleAccountStatementsFileBaseURL.contains("mockservices"))
				|| (singleAccountStatementsCreditCardFileBaseURL.contains("mockservices")))) {
			httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_PDF));
		}
		httpHeaders.add(correlationIdInReqHeader, accountMapping.getCorrelationId());
		httpHeaders.add(sourceSystemReqHeader, AccountStatementsFoundationServiceConstants.SOURCE_SYSTEM_PSD2API);
		httpHeaders.add(sourceUserReqHeader, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeader,
				params.get((AccountStatementsFoundationServiceConstants.CHANNEL_ID)).toUpperCase());

		if (params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE)
				.equals(AccountStatementsFoundationServiceConstants.CREDIT_CARD)) {
			httpHeaders.add(maskedPan, params.get("maskedPan"));
		}
		return httpHeaders;
	}

	public HttpHeaders createRequestHeadersStmt(RequestInfo requestInfo, AccountMapping accountMapping,
			Map<String, String> params) {

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(correlationIdInReqHeader, accountMapping.getCorrelationId());
		httpHeaders.add(sourceSystemReqHeader, AccountStatementsFoundationServiceConstants.SOURCE_SYSTEM_PSD2API);
		httpHeaders.add(sourceUserReqHeader, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeader,
				params.get((AccountStatementsFoundationServiceConstants.CHANNEL_ID)).toUpperCase());
		if (params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE)
				.equals(AccountStatementsFoundationServiceConstants.CREDIT_CARD)) {
			httpHeaders.add(maskedPan, params.get("maskedPan"));
		}
		return httpHeaders;

	}

	public MultiValueMap<String, String> getFromAndToDate(Map<String, String> params) {
		String fromDate = null;
		String toDate = null;
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();

		if (!NullCheckUtils
				.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_DATETIME))
				&& !NullCheckUtils
						.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_DATETIME))) {
			fromDate = params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_DATETIME);
			toDate = params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_DATETIME);

		} else if (!NullCheckUtils
				.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME))
				&& !NullCheckUtils.isNullOrEmpty(
						params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME))) {
			fromDate = params.get(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME);
			toDate = params.get(AccountStatementsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME);

		}

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		DateAdapter dateAdapter = new DateAdapter();
		if (null != fromDate && null != toDate) {
			try {
				fromDate = formatter.format(dateAdapter.parseDate(fromDate));
				toDate = formatter.format(dateAdapter.parseDate(toDate));
				queryParams.add(AccountStatementsFoundationServiceConstants.DATEFROM, fromDate);
				queryParams.add(AccountStatementsFoundationServiceConstants.DATETO, toDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
		return queryParams;

	}

	public String getFoundationServiceCardURLFile(Map<String, String> params,
			String singleAccountStatementsCreditCardFileBaseURL) {

		String finalURL = null;

		if (NullCheckUtils.isNullOrEmpty(singleAccountStatementsCreditCardFileBaseURL))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = singleAccountStatementsCreditCardFileBaseURL;

		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER)))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = finalURL + "/" + params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER);

		finalURL = finalURL + "/statements";

		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.STATEMENT_ID)))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = finalURL + "/" + params.get(AccountStatementsFoundationServiceConstants.STATEMENT_ID);

		return finalURL + "/file";
	}

	public String getFoundationServiceURLFile(Map<String, String> params, String singleAccountStatementsFileBaseURL) {

		String finalURL = null;

		if (NullCheckUtils.isNullOrEmpty(singleAccountStatementsFileBaseURL))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = singleAccountStatementsFileBaseURL;

		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NSC)))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = finalURL + "/" + params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NSC);

		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER)))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = finalURL + "/" + params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER);

		finalURL = finalURL + "/statements";

		if (NullCheckUtils.isNullOrEmpty(params.get(AccountStatementsFoundationServiceConstants.STATEMENT_ID)))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		else
			finalURL = finalURL + "/" + params.get(AccountStatementsFoundationServiceConstants.STATEMENT_ID);

		return finalURL + "/file";
	}

	/**
	 * Gets the foundation service URL.
	 *
	 * @param accountNSC
	 *            the account NSC
	 * @param accountNumber
	 *            the account number
	 * @param baseURL
	 *            the base URL
	 * @return the foundation service URL
	 */
	public String getFoundationServiceURL(String accountNSC, String accountNumber, String baseURL) {
		if (NullCheckUtils.isNullOrEmpty(accountNSC)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if (NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if (NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + accountNSC + "/" + accountNumber + "/" + "statements";
	}

	/**
	 * Gets the foundation service CreditCardURL.
	 *
	 * @param accountNSC
	 *            the account NSC
	 * @param accountNumber
	 *            the account number
	 * @param baseURL
	 *            the base URL
	 * @return the foundation service CreditCardURL
	 */

	public String getFoundationServiceCreditcardURL(String accountNumber, String baseURL) {

		if (NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if (NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + accountNumber + "/" + "statements";

	}

	public PlatformAccountStatementsResponse transformAccountStatementResponse(StatementsResponse statementsResponse,
			Map<String, String> params) {
		return accountStatementsFSTransformer.transformAccountStatementRes(statementsResponse, params);
	}

}

package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementsResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Component
public class AccountStatementsFoundationServiceClient {

	/** The rest client. */
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	/**
	 * Rest transport for account statements file.
	 *
	 * @param reqInfo the req info
	 * @param responseType the response type
	 * @param headers the headers
	 * @return the accounts
	 */
	public byte[] restTransportForAccountStatementsFile(RequestInfo reqInfo, Class<byte[]> statementPDF, HttpHeaders headers) {
		return restClient.callForGet(reqInfo, statementPDF, headers);
		
	}
	public StatementsResponse restTransportForAccountStatementsStmt(RequestInfo reqInfo, Class<StatementsResponse> responseType, MultiValueMap<String, String> params, HttpHeaders headers) {
		if(params.isEmpty())
		{
			reqInfo.setUrl(UriComponentsBuilder.fromHttpUrl(reqInfo.getUrl()).queryParams(params).build().toString());
		}
		return restClient.callForGet(reqInfo, responseType, headers);
	}
}

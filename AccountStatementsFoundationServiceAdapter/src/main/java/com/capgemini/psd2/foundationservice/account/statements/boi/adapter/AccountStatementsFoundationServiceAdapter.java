package com.capgemini.psd2.foundationservice.account.statements.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.client.AccountStatementsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.delegate.AccountStatementsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementsResponse;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class AccountStatementsFileFoundationServiceAdapter.
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
public class AccountStatementsFoundationServiceAdapter implements AccountStatementsAdapter {
	/** The single account balance base URL. */
	@Value("${foundationService.singleAccountStatementsFileBaseURL}")
	private String singleAccountStatementsFileBaseURL;
	
	/** The single account statements credit card file base URL. */
	@Value("${foundationService.singleAccountStatementsCreditCardFileBaseURL}")
	private String singleAccountStatementsCreditCardFileBaseURL;

	/** The statement base URL for account. */
	@Value("${foundationService.singleAccountStatementsFileBaseStmtURL}")
	private String singleAccountStatementsFileBaseStmtURL;
	/** The statement base URL for Creditcardaccount. */
	@Value("${foundationService.singleAccountStatementsCreditCardFileBaseStmtURL}")
	private String singleAccountStatementsCreditCardFileBaseStmtURL;

	/** The account statements file foundation service delegate. */
	@Autowired
	private AccountStatementsFoundationServiceDelegate accountStatementsFileFoundationServiceDelegate;

	/** The account balance foundation service client. */
	@Autowired
	private AccountStatementsFoundationServiceClient accountStatementsFileFoundationServiceClient;

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter#downloadStatementFileByStatementsId(com.capgemini.psd2.consent.domain.AccountMapping, java.util.Map)
	 */
	public PlatformAccountSatementsFileResponse downloadStatementFileByStatementsId(AccountMapping accountMapping,
			Map<String, String> params) {

		PlatformAccountSatementsFileResponse accountSatementsFileResponse = new PlatformAccountSatementsFileResponse();

		if (accountMapping == null || accountMapping.getPsuId() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();

		AccountDetails accountDetails;
		if (accountMapping.getAccountDetails() != null && !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,
					accountDetails.getAccountSubType().toString());
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER,
					accountDetails.getAccountNumber());

		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);

		String finalURL = null;
		 if((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType()) && params.get(PSD2Constants.CMAVERSION) == null)
					|| accountDetails.getAccountSubType().toString().contentEquals(AccountStatementsFoundationServiceConstants.CURRENT_ACCOUNT)
					|| accountDetails.getAccountSubType().toString().contentEquals(AccountStatementsFoundationServiceConstants.SAVINGS)) {
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NSC, accountDetails.getAccountNSC());
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER,
					accountDetails.getAccountNumber());
			httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(requestInfo,accountMapping, params);

			finalURL = accountStatementsFileFoundationServiceDelegate.getFoundationServiceURLFile(params,singleAccountStatementsFileBaseURL);
		}
		else if (accountDetails.getAccountSubType().toString()
				.equals(AccountStatementsFoundationServiceConstants.CREDIT_CARD)) {

			params.put("maskedPan", accountDetails.getAccount().getIdentification().substring(accountDetails.getAccount().getIdentification().length() - 4));

			httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(requestInfo,	accountMapping, params);

			finalURL = accountStatementsFileFoundationServiceDelegate.getFoundationServiceCardURLFile(params,singleAccountStatementsCreditCardFileBaseURL);

		}

		requestInfo.setUrl(finalURL);
		byte[] byteArray = null;
		
			byteArray = accountStatementsFileFoundationServiceClient
					.restTransportForAccountStatementsFile(requestInfo, byte[].class, httpHeaders);
					accountSatementsFileResponse.setFileName("E-Statements.pdf");
					accountSatementsFileResponse.setFileByteArray(byteArray);
			
					if (NullCheckUtils.isNullOrEmpty(byteArray)) {
						throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE);
					}
		
		return accountSatementsFileResponse;
	}
	
	
	/* (non-Javadoc)
	 * @see com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter#
	 * retrieveAccountStatements(com.capgemini.psd2.consent.domain.AccountMapping, java.util.Map)
	 * To retrieve Account Statements for Current/Saving accounts and creditcard 
	 */
	public PlatformAccountStatementsResponse retrieveAccountStatements(AccountMapping accountMapping,Map<String, String> params) {
		if (accountMapping == null || accountMapping.getPsuId() == null) {

			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (params == null) {

			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();
		/*HttpHeaders httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersStmt(requestInfo,
				accountMapping, params);*/
		AccountDetails accountDetails;

		if (accountMapping.getAccountDetails() != null && !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,
					String.valueOf(accountDetails.getAccountSubType()));

		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		if ((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType()) && params.get(PSD2Constants.CMAVERSION) == null)
				|| accountDetails.getAccountSubType().toString().equalsIgnoreCase(AccountStatementsFoundationServiceConstants.CURRENT_ACCOUNT)
				|| accountDetails.getAccountSubType().toString().equalsIgnoreCase(AccountStatementsFoundationServiceConstants.SAVINGS)) {
			
			httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersStmt(requestInfo, accountMapping, params);
			
			String finalURL = accountStatementsFileFoundationServiceDelegate.getFoundationServiceURL(
					accountDetails.getAccountNSC(), accountDetails.getAccountNumber(), singleAccountStatementsFileBaseStmtURL);
			requestInfo.setUrl(finalURL);
		} else if (accountDetails.getAccountSubType().toString()
				.equalsIgnoreCase(AccountStatementsFoundationServiceConstants.CREDIT_CARD)) {
			params.put("maskedPan", accountDetails.getAccount().getIdentification().substring(accountDetails.getAccount().getIdentification().length() - 4));
			httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersStmt(requestInfo, accountMapping, params);
			
			String finalURL = accountStatementsFileFoundationServiceDelegate.getFoundationServiceCreditcardURL(
					accountDetails.getAccountNumber(), singleAccountStatementsCreditCardFileBaseStmtURL);
			requestInfo.setUrl(finalURL);
		}
		
		MultiValueMap<String, String> queryParams = accountStatementsFileFoundationServiceDelegate.getFromAndToDate(params);
		
		StatementsResponse statementsResponse = accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(requestInfo, StatementsResponse.class, queryParams, httpHeaders);
		
		if (statementsResponse.getStatementsList().isEmpty()) {
			statementsResponse = new StatementsResponse();
		}
		return accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(statementsResponse, params);
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter#retrieveAccountStatementsByStatementId(com.capgemini.psd2.consent.domain.AccountMapping, java.util.Map)
	 */
	public PlatformAccountStatementsResponse retrieveAccountStatementsByStatementId(AccountMapping accountMapping,
			Map<String, String> params) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter#retrieveAccountTransactionsByStatementsId(com.capgemini.psd2.consent.domain.AccountMapping, java.util.Map)
	 */
	public PlatformAccountTransactionResponse retrieveAccountTransactionsByStatementsId(AccountMapping accountMapping,
			Map<String, String> params) {
		return null;
	}
}

package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.test;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.client.AccountStatementsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.delegate.AccountStatementsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementObject;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementsResponse;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.transformer.AccountStatementsFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsFoundationServiceDelegateRestCallTest {
	/** The account statements file foundation service adapter. */

	@Mock
	private AccountStatementsFoundationServiceDelegate delegate;

	/** The account statements file foundation service client. */

	@InjectMocks
	private AccountStatementsFoundationServiceClient accountStatementsFileFoundationServiceClient;



	/** The rest client. */

	@Mock
	private RestClientSync restClient;

	/** The account statements file foundation service delegate. */

	@Mock
	private AccountStatementsFoundationServiceTransformer accountStatementsFileFSTransformer;

	/**

	 * Sets the up.

	 */

	@Before
	public void setUp(){
MockitoAnnotations.initMocks(this);
}

	/**

	 * Context loads.

	 */

	@Test
	public void contextLoads() {

	}



	/**

	 * Test account statements file FS.

	 */

	@Test
	public void testrestTransportForAccountStatementsFile(){
		RequestInfo reqInfo=new RequestInfo();
		HttpHeaders headers=new HttpHeaders();
		headers.add("X-API-TRANSACTION-ID", "correlationIdInReqHeader");
		headers.add("X-API-SOURCE-SYSTEM", "sourceSystemReqHeader");
		headers.add("X-API-SOURCE-USER", "sourceUserReqHeader");
		headers.add("X-API-CHANNEL-CODE","channelInReqHeader" );
		headers.add("X-MASKED-PAN","xMaskedPan" );
		reqInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
		accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsFile(reqInfo, byte[].class, headers);
	}

	@Test
	public void testrestTransportForAccountStatementsStmt(){
		RequestInfo reqInfo=new RequestInfo();
		HttpHeaders headers=new HttpHeaders();
		StatementsResponse statements = new StatementsResponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		stmt.setDocumentIdentificationNumber("abcd");
		
		headers.add("X-API-TRANSACTION-ID", "correlationIdInReqHeader");
		headers.add("X-API-SOURCE-SYSTEM", "sourceSystemReqHeader");
		headers.add("X-API-SOURCE-USER", "sourceUserReqHeader");
		headers.add("X-API-CHANNEL-CODE","channelInReqHeader" );
		headers.add("X-MASKED-PAN","xMaskedPan" );
		reqInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
		
	}
	
	@Test
	public void testrestTransportForAccountStatementsList(){
		RequestInfo reqInfo=new RequestInfo();
		HttpHeaders headers=new HttpHeaders();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		headers.add("X-API-TRANSACTION-ID", "correlationIdInReqHeader");
		headers.add("X-API-SOURCE-SYSTEM", "sourceSystemReqHeader");
		headers.add("X-API-SOURCE-USER", "sourceUserReqHeader");
		headers.add("X-API-CHANNEL-CODE","channelInReqHeader" );
		headers.add("X-MASKED-PAN","xMaskedPan" );
		reqInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
		accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(reqInfo, StatementsResponse.class, params, headers);
	}

}
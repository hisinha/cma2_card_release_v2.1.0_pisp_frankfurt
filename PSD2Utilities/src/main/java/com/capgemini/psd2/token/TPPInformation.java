package com.capgemini.psd2.token;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TPPInformation extends TppInformationTokenData{
	
	@JsonIgnore
	private String tppBlock;
	
	public String getTppBlock() {
		return tppBlock;
	}
	public void setTppBlock(String tppBlock) {
		this.tppBlock = tppBlock;
	}
}
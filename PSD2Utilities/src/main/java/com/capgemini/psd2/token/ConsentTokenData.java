package com.capgemini.psd2.token;

import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class ConsentTokenData {

	private String consentId;
	private String consentExpiry;
	private ConsentStatusEnum Status;
	public String getConsentId() {
		return consentId;
	}
	public void setConsentId(String consentId) {
		this.consentId = consentId;
	}
	public String getConsentExpiry() {
		return consentExpiry;
	}
	public void setConsentExpiry(String consentExpiry) {
		this.consentExpiry = consentExpiry;
	}
	public ConsentStatusEnum getStatus() {
		return Status;
	}
	public void setStatus(ConsentStatusEnum status) {
		Status = status;
	}
	
}

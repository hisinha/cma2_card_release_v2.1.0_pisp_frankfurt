package com.capgemini.psd2.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("app.sandboxValidationPolicies")
public class SandboxValidationConfig {

	private String chargeAmount;
	private String chargeCurrency;
	
	private String chargeAmountLimit;

	private List<String> allowedCurrencies = new ArrayList<>();

	private Map<String, String> amountValidation = new HashMap<>();

	private List<String> unsupportedCurrencies = new ArrayList<>();

	private List<String> statementIdList = new ArrayList<>();
	
	private List<String> rejectedPSUIds = new ArrayList<>(); 

	private Map<String, List<String>> charges = new HashMap<>();

	@Value("${app.sandboxValidationPolicies.psuId:#{'18070751'}}")
	private String psuId;


	private String secondaryIdentificationRule;

	public List<String> getAllowedCurrencies() {
		return allowedCurrencies;
	}

	public void setAllowedCurrencies(List<String> allowedCurrencies) {
		this.allowedCurrencies = allowedCurrencies;
	}

	public Map<String, String> getAmountValidation() {
		return amountValidation;
	}

	public void setAmountValidation(Map<String, String> amountValidation) {
		this.amountValidation = amountValidation;
	}

	public List<String> getUnsupportedCurrencies() {
		return unsupportedCurrencies;
	}

	public void setUnsupportedCurrencies(List<String> unsupportedCurrencies) {
		this.unsupportedCurrencies = unsupportedCurrencies;
	}

	public List<String> getStatementIdList() {
		return statementIdList;
	}

	public void setStatementIdList(List<String> statementIdList) {
		this.statementIdList = statementIdList;
	}

	public Map<String, List<String>> getCharges() {
		return charges;
	}

	public void setCharges(Map<String, List<String>> charges) {
		this.charges = charges; 
	}

	public String getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(String chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getChargeCurrency() {
		return chargeCurrency;
	}

	public void setChargeCurrency(String chargeCurrency) {
		this.chargeCurrency = chargeCurrency;
	}

	public String getPsuId() {
		return psuId;
	}

	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	public String getSecondaryIdentificationRule() {
		return secondaryIdentificationRule;
	}

	public void setSecondaryIdentificationRule(String secondaryIdentificationRule) {
		this.secondaryIdentificationRule = secondaryIdentificationRule;
	}

	public List<String> getRejectedPSUIds() {
		return rejectedPSUIds;
	}

	public void setRejectedPSUIds(List<String> rejectedPSUIds) {
		this.rejectedPSUIds = rejectedPSUIds;
	}

	public String getChargeAmountLimit() {
		return chargeAmountLimit;
	}

	public void setChargeAmountLimit(String chargeAmountLimit) {
		this.chargeAmountLimit = chargeAmountLimit;
	}
}

package com.capgemini.psd2.utilities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

public abstract class DateUtilitiesCMA2 {

	/**
	 * @param dateTime
	 * @return valid dateTime with offset
	 * 
	 *         Use this method to validate the format of the dateTime passed in
	 *         the request and add an offset of +00:00 if offset is not present
	 *         or mentioned as 'Z'
	 */
	public String transformDateTimeInRequest(String dateTime) {
		if (!Offsetcheck(dateTime)) {
			try {
				dateTime = addOffset(dateTime);
			} catch (DateTimeParseException e) {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.INVALID_DATE_FORMAT));
			}
		}
		return dateTime;
	}

	/**
	 * @param dateTime
	 * @return boolean indicator
	 * 
	 *         This method validates the dateTime against valid patterns
	 */
	public void validateDateTimeInRequest(String dateTime) {
		if (!Offsetcheck(dateTime)) {
			try {
				dateTime = addOffset(dateTime);
			} catch (DateTimeParseException e) {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.INVALID_DATE_FORMAT));
			}
		}

		if (dateTime != null && !validateDateTime(dateTime)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.INVALID_DATE_FORMAT));
		}
	}

	public void validateDateTimeInResponse(String dateTime) {
		if (!validateDateTime(dateTime)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, ErrorMapKeys.INVALID_DATE_FORMAT));
		}
	}

	private boolean Offsetcheck(String dateTime) {
		boolean flag = false;
		try {
			OffsetDateTime.parse(dateTime);
			flag = true;
		} catch (DateTimeParseException e) {
			flag = false;
		}
		return flag;

	}

	private String addOffset(String dateTimeWithoutOffset) throws DateTimeParseException {
		if (!NullCheckUtils.isNullOrEmpty(dateTimeWithoutOffset)) {
			/*
			 * LocalDateTime parsedLocalDateTime =
			 * LocalDateTime.parse(dateTimeWithoutOffset); OffsetDateTime
			 * parsedOffsetDateTime = OffsetDateTime.of(parsedLocalDateTime,
			 * ZoneOffset.ofHoursMinutes(0, 00));
			 */

			// String temp = parsedOffsetDateTime.toString();
			// String array[] = temp.split("Z");
			StringBuilder builder = new StringBuilder();
			builder.append(dateTimeWithoutOffset);
			builder.append("+00:00");
			return builder.toString();
		} else {
			return null;
		}
	}

	private boolean validateDateTime(String dateTime) {

		boolean isValid = false;
		String dateTimeCopy = dateTime;

		if (dateTimeCopy.contains("Z"))
			dateTimeCopy = dateTimeCopy.split("Z")[0] + "+00:00";

		DateTimeFormatter[] formatters = { DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mmXXX"),
				DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX"),
				DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"),
				DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SXXX") };

		for (DateTimeFormatter formatter : formatters) {
			try {
				LocalDate.parse(dateTimeCopy, formatter);
				isValid = true;
			} catch (DateTimeParseException | NullPointerException e) {
				continue;
			}
			break;
		}
		return isValid;
	}

}

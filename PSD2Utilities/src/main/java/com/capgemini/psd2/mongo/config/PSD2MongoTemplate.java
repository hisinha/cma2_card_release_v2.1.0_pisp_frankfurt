package com.capgemini.psd2.mongo.config;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.stereotype.Component;

@Component
@Primary
public class PSD2MongoTemplate extends MongoTemplate {

	protected static final Logger LOGGER = LoggerFactory.getLogger(PSD2MongoTemplate.class);

	@Value("${spring.data.mongodb.ignoredError:null}")
	private String suppressedError;

	public PSD2MongoTemplate(MongoDbFactory mongoDbFactory, MongoConverter mongoConverter) {
		super(mongoDbFactory, mongoConverter);
	}

	@Override
	public void save(Object objectToSave) {
		try {
			super.save(objectToSave);
		} catch (DataIntegrityViolationException e) {
			String message = e.getLocalizedMessage();
			if (!message.contains(suppressedError)) {
				throw e;
			}
			LOGGER.info(e.getMessage());
		}
	}

	@Override
	public void save(Object objectToSave, String collectionName) {
		try {
			super.save(objectToSave, collectionName);
		} catch (DataIntegrityViolationException e) {
			String message = e.getLocalizedMessage();
			if (!message.contains(suppressedError)) {
				throw e;
			}
			LOGGER.info(e.getMessage());
		}
	}

	@Override
	public void insert(Object objectToSave) {
		try {
			super.insert(objectToSave);
		} catch (DataIntegrityViolationException e) {
			String message = e.getLocalizedMessage();
			if (!message.contains(suppressedError)) {
				throw e;
			}
			LOGGER.info(e.getMessage());
		}
	}

	@Override
	public void insert(Collection<? extends Object> batchToSave, Class<?> entityClass) {
		try {
			super.insert(batchToSave, entityClass);
		} catch (DataIntegrityViolationException e) {
			String message = e.getLocalizedMessage();
			if (!message.contains(suppressedError)) {
				throw e;
			}
			LOGGER.info(e.getMessage());
		}

	}
}

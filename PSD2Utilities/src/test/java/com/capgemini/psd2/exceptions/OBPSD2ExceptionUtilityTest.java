package com.capgemini.psd2.exceptions;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

public class OBPSD2ExceptionUtilityTest {

	@InjectMocks
	private OBPSD2ExceptionUtility utility;
	
	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	@Ignore
	public void initTest(){
		ReflectionTestUtils.setField(utility, "configServerUrl", "http://localhost:8087/accountbeneficiariesv3");
		ReflectionTestUtils.setField(utility, "profile", "local");
		utility.init();
	}
}

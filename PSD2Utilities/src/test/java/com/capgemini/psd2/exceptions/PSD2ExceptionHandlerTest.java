/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exceptions;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;


/**
 * The Class PSD2ExceptionHandlerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PSD2ExceptionHandlerTest {
		
	/** The web request. */
	@Mock
	private WebRequest webRequest;
	
	/** The http client error exception. */
	@Mock
	private HttpClientErrorException httpClientErrorException;
	
	/** The psd 2 exception. */
	@Mock
	private PSD2Exception psd2Exception;
	
	/** The handler. */
	@InjectMocks
	private PSD2ExceptionHandler handler;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		when(httpClientErrorException.getStatusCode()).thenReturn(HttpStatus.ACCEPTED);
	}

	/**
	 * Test.
	 */
	@Test
	public void test() {
		handler.handleInvalidRequest(httpClientErrorException, webRequest);
		
		ErrorInfo errorInfo = new ErrorInfo("500");
		/*errorInfo.setStatusCode("500");*/
		Mockito.when(psd2Exception.getErrorInfo()).thenReturn(errorInfo);
		handler.handleInvalidRequest(psd2Exception, webRequest);
		
		handler.handleInvalidRequest(new Exception(), webRequest);
		
		handler.handleInvalidRequest(null, webRequest);
	}

}

package com.capgemini.psd2.utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;

import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;

public class StringUtilsTest {

	@Test
	public void test(){
		Set<String> set = new HashSet<>();
		set.add("StringValue");
		set.add("SecondValue");
		assertNotNull(StringUtils.getCommaSeperatedString(set) );
	}
	
	@Test
	public void stringToHashNullTest(){
		assertNull(StringUtils.generateHashedValue(null));
	}
	
	@Test
	public void stringToHashTest(){
		assertEquals("9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",StringUtils.generateHashedValue("test"));
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=RuntimeException.class)
	public void stringToHashExceptionTest(){
		Mockito.when(StringUtils.generateHashedValue(anyString())).thenThrow(NoSuchAlgorithmException.class);
	}
	
	@Test
	public void defaultValTest(){
		StringUtils.defaultVal(null);
		StringUtils.defaultVal("abcd");
	}
}

/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * The Class JWTUtilityTest.
 */
public class JWTUtilityTest {

	/**
	 * Test.
	 */
	@Test
	public void test() {
		assertNotNull(JWTUtility.decodeToken("Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmFua0FjY291bnRUcmFuc2FjdGlvbnMiXSwiZXhwIjoxNDg1NTkxODY1LCJ1c2VyX25hbWUiOiJkZW1vdXNlciIsImp0aSI6ImI1ZmFiOTI3LWQ3NDItNDc1MC05ZTE5LWFkMjQ5ODY4ZTBlZSIsImNsaWVudF9pZCI6Im1vbmV5d2lzZSIsInNjb3BlIjpbIntcInJlc291cmNlTmFtZVwiOlwiYmFsYW5jZVwiLFwiY2xhaW1zXCI6W3tcImFjY291bnROdW1iZXJcIjpcIkVFMDYyMjAwMjIyMDEwMTkyMjM4XCIsXCJiYWxcIjp0cnVlLFwiYWNjdFwiOntcImNjeVwiOnRydWUsXCJvd25yXCI6dHJ1ZSxcImlkXCI6dHJ1ZX19XX0iLCJ7XCJyZXNvdXJjZU5hbWVcIjpcInRyYW5zYWN0aW9uXCIsXCJjbGFpbXNcIjpbe1wiYWNjb3VudE51bWJlclwiOlwiRUUwNjIyMDAyMjIwMTAxOTIyMzhcIixcInN0YXRlbWVudE1vbnRoc1wiOlwiNlwifV19Il19.SMNi5VkmXCbAjgQXdpPQM-WKb2oVp2eAlPLe_xh29tZamwLbjhsOksb_G-V6redvk-zwU_nBeO6vyMU1LCYjX5cip30fxA6ZA3_LAddKtesSNNbmNVqpwBnD_1wjcVaY9g7fmp-jbzF10CmI417hSb6x5HXPSAZcVioBWGGDclDjLzR4FcdQ-k58nFN3nOWqG-l29UPT4Aj9t305L2D4JiltILSaBNcIVKyTsSlJpELyx2qkDNwTHYQUzJc-fZoPD2E4-_Oweg6VAyRgtUGp7l-yd14rTk7ZX6Sfz4jX0eZZfd9VEqIbyRlrR9pJNyIsfVDPhZi4xHKH9cv5u5anhA"));
	}

	@Test
	public void testAccessToken() {
		assertNull(JWTUtility.decodeToken(null));
		assertNull(JWTUtility.decodeToken(""));
		assertNotNull(JWTUtility.decodeToken("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmFua0FjY291bnRUcmFuc2FjdGlvbnMiXSwiZXhwIjoxNDg1NTkxODY1LCJ1c2VyX25hbWUiOiJkZW1vdXNlciIsImp0aSI6ImI1ZmFiOTI3LWQ3NDItNDc1MC05ZTE5LWFkMjQ5ODY4ZTBlZSIsImNsaWVudF9pZCI6Im1vbmV5d2lzZSIsInNjb3BlIjpbIntcInJlc291cmNlTmFtZVwiOlwiYmFsYW5jZVwiLFwiY2xhaW1zXCI6W3tcImFjY291bnROdW1iZXJcIjpcIkVFMDYyMjAwMjIyMDEwMTkyMjM4XCIsXCJiYWxcIjp0cnVlLFwiYWNjdFwiOntcImNjeVwiOnRydWUsXCJvd25yXCI6dHJ1ZSxcImlkXCI6dHJ1ZX19XX0iLCJ7XCJyZXNvdXJjZU5hbWVcIjpcInRyYW5zYWN0aW9uXCIsXCJjbGFpbXNcIjpbe1wiYWNjb3VudE51bWJlclwiOlwiRUUwNjIyMDAyMjIwMTAxOTIyMzhcIixcInN0YXRlbWVudE1vbnRoc1wiOlwiNlwifV19Il19.SMNi5VkmXCbAjgQXdpPQM-WKb2oVp2eAlPLe_xh29tZamwLbjhsOksb_G-V6redvk-zwU_nBeO6vyMU1LCYjX5cip30fxA6ZA3_LAddKtesSNNbmNVqpwBnD_1wjcVaY9g7fmp-jbzF10CmI417hSb6x5HXPSAZcVioBWGGDclDjLzR4FcdQ-k58nFN3nOWqG-l29UPT4Aj9t305L2D4JiltILSaBNcIVKyTsSlJpELyx2qkDNwTHYQUzJc-fZoPD2E4-_Oweg6VAyRgtUGp7l-yd14rTk7ZX6Sfz4jX0eZZfd9VEqIbyRlrR9pJNyIsfVDPhZi4xHKH9cv5u5anhA"));		
	}

}

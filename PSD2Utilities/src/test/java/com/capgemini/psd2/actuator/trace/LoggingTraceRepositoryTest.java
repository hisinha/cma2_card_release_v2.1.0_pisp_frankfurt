package com.capgemini.psd2.actuator.trace;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.actuate.trace.InMemoryTraceRepository;
import org.springframework.boot.actuate.trace.TraceRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class LoggingTraceRepositoryTest {
	
	@Mock
	private final TraceRepository delegate = new InMemoryTraceRepository();
	
	@InjectMocks
	private LoggingTraceRepository repository;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void addTest(){
		Map<String, Object> traceInfo = new HashMap<>();
		repository.add(traceInfo);
	}
	
	@Test
	public void findAllTest(){
		repository.findAll();
	}
}

package com.capgemini.psd2.token;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class TPPInformationTest {
	
	@InjectMocks
	TPPInformation tppInformation;
	
	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		tppInformation.setTppLegalEntityName("tppLegalEntityName");
		tppInformation.setTppRegisteredId("tppRegisteredId");
		Set<String> tppRoles = new HashSet<>();
		tppRoles.add("AISP");
		tppInformation.setTppRoles(tppRoles );
		tppInformation.setTppBlock("tppBlock");
	}

	@Test
	public void errorInfoTest(){
		
		assertTrue(tppInformation.getTppBlock()== "tppBlock");
	}
}

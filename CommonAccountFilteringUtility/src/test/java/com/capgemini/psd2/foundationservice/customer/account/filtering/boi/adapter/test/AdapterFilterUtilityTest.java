package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.CreditCardAccnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Brand;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.BrandCode;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;




@RunWith(SpringJUnit4ClassRunner.class)
public class AdapterFilterUtilityTest {

	@InjectMocks
	AdapterFilterUtility adapterFilterUtility;


	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}




	@Test (expected= AdapterException.class)
	public void testPrevalidateAccounts1(){

		DigitalUserProfile accGetResponse = new DigitalUserProfile();
		Accnt accnt1 = new Accnt();
		accnt1.setAccountNSC("253");
		accnt1.setAccountNumber("123");
		CreditCardAccnt ccAccount = new CreditCardAccnt();
		ccAccount.setPlApplId("123");
		ccAccount.setCreditCardNumber("111111");
		//accGetResponse.getCreditCardAccount().add(ccAccount);
		AccountDetails accountDetails1 = new AccountDetails();
		accountDetails1.setAccountNSC("567");
		accountDetails1.setAccountNumber("123");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.getAccountDetails().add(accountDetails1);
		List<AccountDetails> accDetails1=new ArrayList<AccountDetails>();
		accDetails1.add(accountDetails1);
		OBCashAccount3 obAcc=new OBCashAccount3();
		obAcc.setIdentification("111111");
		accountDetails1.setAccount(obAcc);
		List<CreditCardAccnt> list1=new ArrayList<CreditCardAccnt>();
		list1.add(ccAccount);
		AccountMapping filteredAccounts;
		filteredAccounts =	adapterFilterUtility.prevalidateAccounts(accGetResponse, accountMapping);
		assertNotNull(filteredAccounts); 
	}
	@Test (expected= AdapterException.class)
	public void testPrevalidateAccounts(){

        DigitalUserProfile accGetResponse = new DigitalUserProfile();
		Accnt accnt1 = new Accnt();
		accnt1.setAccountNSC("test");
		accnt1.setAccountNumber("123");
		//accGetResponse.getAccount().add(accnt1);
		AccountDetails accountDetails1 = new AccountDetails();
		accountDetails1.setAccountNSC("test");
		accountDetails1.setAccountNumber("123");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.getAccountDetails().add(accountDetails1);
		OBCashAccount3 obAcc=new OBCashAccount3();
		obAcc.setIdentification("gkg");
		accountDetails1.setAccount(obAcc);
		List<Accnt> list =new ArrayList<Accnt>();
		list.add(accnt1);
		AccountMapping mapping = adapterFilterUtility.prevalidateAccounts(accGetResponse, accountMapping);
		assertNotNull(mapping);

	}

	@Test (expected= AdapterException.class)
	public void testPrevalidateAccounts1forException(){

		DigitalUserProfile accGetResponse = new DigitalUserProfile();
		AccountDetails accountDetails1 = new AccountDetails();
		accountDetails1.setAccountNSC("test");
		accountDetails1.setAccountNumber("123");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.getAccountDetails().add(accountDetails1);
		OBCashAccount3 obAcc=new OBCashAccount3();
		obAcc.setIdentification("gkg");
		accountDetails1.setAccount(obAcc);
		AccountMapping mapping = adapterFilterUtility.prevalidateAccounts(accGetResponse, accountMapping);


	}

	@Test
	public void testPrevalidateJurisdiction(){
		DigitalUserProfile  digiUserProfile = new DigitalUserProfile();

		Brand brand = new Brand();
		brand.setBrandCode(BrandCode.AA);
		Account acc=new Account();
		acc.setAccountBrand(brand);
		AccountEntitlements accnt =new AccountEntitlements();
		accnt.setAccount(acc);
		List<AccountEntitlements> list=new ArrayList<AccountEntitlements>();
		digiUserProfile.setAccountEntitlements(list);
		List<String> listString = new ArrayList<String>();
		accnt.setEntitlements(listString);
		list.add(accnt);
		String jurisdictionType="brandCode";
		digiUserProfile=adapterFilterUtility.prevalidateJurisdiction(digiUserProfile, jurisdictionType);
		assertNotNull(digiUserProfile);

	}


	@Test
	public void testPrevalidateJurisdictionForNull(){
		DigitalUserProfile  digiUserProfile = new DigitalUserProfile();
		Brand brand = new Brand();
		brand.setBrandCode(BrandCode.AA);
		Account acc=new Account();
		acc.setAccountBrand(brand);
		AccountEntitlements accnt =new AccountEntitlements();
		accnt.setAccount(acc);
		List<AccountEntitlements> list=new ArrayList<AccountEntitlements>();
		digiUserProfile.setAccountEntitlements(list);
		List<String> listString = new ArrayList<String>();
		accnt.setEntitlements(listString);
		list.add(accnt);
		String jurisdictionType="brandCode";
		digiUserProfile=adapterFilterUtility.prevalidateJurisdiction(digiUserProfile, jurisdictionType);
		assertNotNull(digiUserProfile);

	}


	@Test
	public void testPrevalidateJurisdictionForNotContain(){
		DigitalUserProfile  digiUserProfile = new DigitalUserProfile();
		Brand brand = new Brand();
		brand.setBrandCode(BrandCode.BOI_GB);
		Account acc=new Account();
		acc.setAccountBrand(brand);
		AccountEntitlements accnt =new AccountEntitlements();
		accnt.setAccount(acc);
		List<AccountEntitlements> list=new ArrayList<AccountEntitlements>();
		digiUserProfile.setAccountEntitlements(list);
		List<String> listString = new ArrayList<String>();
		accnt.setEntitlements(listString);
		list.add(accnt);
		String jurisdictionType="brandCode";
		digiUserProfile=adapterFilterUtility.prevalidateJurisdiction(digiUserProfile, jurisdictionType);
		assertNotNull(digiUserProfile);

	}
}
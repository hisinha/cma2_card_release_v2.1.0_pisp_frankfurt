package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter;


import java.util.Collections;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.client.FraudnetViaMulesoftProxyClient;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FraudServiceRequest;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.utility.FraudnetViaMulesoftProxyUtility;
import com.capgemini.psd2.fraudsystem.adapter.FraudSystemAdapter;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component
public class FraudnetViaMulesoftProxyAdapter implements FraudSystemAdapter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FraudnetViaMulesoftProxyAdapter.class);

	@Value("${foundationService.fraudnetMulesoftEndpointURL}")
	private String fraudnetMulesoftEndpointURL;

	@Autowired
	private FraudnetViaMulesoftProxyUtility fraudnetViaMulesoftProxyUtility;

	@Autowired
	private FraudnetViaMulesoftProxyClient fraudnetViaMulesoftProxyClient;

	@Override
	public <T> T retrieveFraudScore(Map<String, Map<String, Object>> fraudSystemRequest) {
		
		FraudServiceResponse fraudServiceResponse = null;
		RequestInfo requestInfo = new RequestInfo();

		HttpHeaders httpHeaders = fraudnetViaMulesoftProxyUtility.createRequestHeaders(requestInfo, fraudSystemRequest);
		requestInfo.setUrl(fraudnetMulesoftEndpointURL);
		FraudServiceRequest fraudServiceRequest = fraudnetViaMulesoftProxyUtility.transformRequestFromAPIToFN(fraudSystemRequest);

		try {
			httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			fraudServiceResponse = fraudnetViaMulesoftProxyClient.fraudnetViaMulesoftCall(requestInfo, fraudServiceRequest, FraudServiceResponse.class, httpHeaders);
		} catch (AdapterException e) {
			throw e;
		} catch (Exception e) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return fraudnetViaMulesoftProxyUtility.transformResponseFromFNToAPI(fraudServiceResponse);
	}


}

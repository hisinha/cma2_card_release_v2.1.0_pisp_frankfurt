package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

/**
 * ContactInformation
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactInformation {
	@SerializedName("emailAddress")
	private String emailAddress = null;

	@SerializedName("faxNumber")
	private String faxNumber = null;

	@SerializedName("homePhoneNumber")
	private String homePhoneNumber = null;

	@SerializedName("mobilePhoneNumber")
	private String mobilePhoneNumber = null;

	@SerializedName("workPhoneNumber")
	private String workPhoneNumber = null;

	@SerializedName("otherPhoneNumber")
	private String otherPhoneNumber = null;

	public ContactInformation emailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
		return this;
	}

	/**
	 * Get emailAddress
	 * 
	 * @return emailAddress
	 **/
	@ApiModelProperty(value = "")
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public ContactInformation faxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
		return this;
	}

	/**
	 * Get faxNumber
	 * 
	 * @return faxNumber
	 **/
	@ApiModelProperty(value = "")
	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public ContactInformation homePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
		return this;
	}

	/**
	 * Get homePhoneNumber
	 * 
	 * @return homePhoneNumber
	 **/
	@ApiModelProperty(value = "")
	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}

	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	public ContactInformation mobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
		return this;
	}

	/**
	 * Get mobilePhoneNumber
	 * 
	 * @return mobilePhoneNumber
	 **/
	@ApiModelProperty(value = "")
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public ContactInformation workPhoneNumber(String workPhoneNumber) {
		this.workPhoneNumber = workPhoneNumber;
		return this;
	}

	/**
	 * Get workPhoneNumber
	 * 
	 * @return workPhoneNumber
	 **/
	@ApiModelProperty(value = "")
	public String getWorkPhoneNumber() {
		return workPhoneNumber;
	}

	public void setWorkPhoneNumber(String workPhoneNumber) {
		this.workPhoneNumber = workPhoneNumber;
	}

	public ContactInformation otherPhoneNumber(String otherPhoneNumber) {
		this.otherPhoneNumber = otherPhoneNumber;
		return this;
	}

	/**
	 * Get otherPhoneNumber
	 * 
	 * @return otherPhoneNumber
	 **/
	@ApiModelProperty(value = "")
	public String getOtherPhoneNumber() {
		return otherPhoneNumber;
	}

	public void setOtherPhoneNumber(String otherPhoneNumber) {
		this.otherPhoneNumber = otherPhoneNumber;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ContactInformation contactInformation = (ContactInformation) o;
		return Objects.equals(this.emailAddress, contactInformation.emailAddress)
				&& Objects.equals(this.faxNumber, contactInformation.faxNumber)
				&& Objects.equals(this.homePhoneNumber, contactInformation.homePhoneNumber)
				&& Objects.equals(this.mobilePhoneNumber, contactInformation.mobilePhoneNumber)
				&& Objects.equals(this.workPhoneNumber, contactInformation.workPhoneNumber)
				&& Objects.equals(this.otherPhoneNumber, contactInformation.otherPhoneNumber);
	}

	@Override
	public int hashCode() {
		return Objects.hash(emailAddress, faxNumber, homePhoneNumber, mobilePhoneNumber, workPhoneNumber,
				otherPhoneNumber);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ContactInformation {\n");

		sb.append("    emailAddress: ").append(toIndentedString(emailAddress)).append("\n");
		sb.append("    faxNumber: ").append(toIndentedString(faxNumber)).append("\n");
		sb.append("    homePhoneNumber: ").append(toIndentedString(homePhoneNumber)).append("\n");
		sb.append("    mobilePhoneNumber: ").append(toIndentedString(mobilePhoneNumber)).append("\n");
		sb.append("    workPhoneNumber: ").append(toIndentedString(workPhoneNumber)).append("\n");
		sb.append("    otherPhoneNumber: ").append(toIndentedString(otherPhoneNumber)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}

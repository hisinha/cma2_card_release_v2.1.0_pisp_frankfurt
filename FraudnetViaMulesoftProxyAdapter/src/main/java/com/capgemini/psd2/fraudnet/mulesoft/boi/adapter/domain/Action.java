package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The action on the entity or resource which resulted in the event originating
 * from the source / source sub system
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum Action {

	AUTHENTICATE("Authenticate"),

	AUTHORIZE("Authorize"),

	CREATE("Create"),

	UPDATE("Update"),

	DELETE("Delete");

	private String value;

	Action(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static Action fromValue(String text) {
		for (Action b : Action.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}

}

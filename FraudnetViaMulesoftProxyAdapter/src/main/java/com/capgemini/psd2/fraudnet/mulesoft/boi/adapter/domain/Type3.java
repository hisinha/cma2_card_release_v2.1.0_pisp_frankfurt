package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * Gets or Sets type3
 */
@JsonAdapter(Type3.Adapter.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public enum Type3 {

	CRITICAL("CRITICAL"),

	WARNING("WARNING"),

	INFORMATION("INFORMATION"),

	SUCCESSFUL("SUCCESSFUL");

	private String value;

	Type3(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	public static Type3 fromValue(String text) {
		for (Type3 b : Type3.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}

	public static class Adapter extends TypeAdapter<Type3> {
		@Override
		public void write(final JsonWriter jsonWriter, final Type3 enumeration) throws IOException {
			jsonWriter.value(enumeration.getValue());
		}

		@Override
		public Type3 read(final JsonReader jsonReader) throws IOException {
			String value = jsonReader.nextString();
			return Type3.fromValue(String.valueOf(value));
		}
	}
}

package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The user session details are optional but if present must contain
 */
@ApiModel(description = "The user session details are optional but if present must contain")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OnlineUserSession {
	@SerializedName("sessionId")
	private String sessionId;

	@SerializedName("sessionDuration")
	private Long sessionDuration;

	@SerializedName("userSessionActive")
	private Boolean userSessionActive;

	@SerializedName("twoFactorAuthentication")
	private Boolean twoFactorAuthentication;

	@SerializedName("failedAuthentication")
	private Double failedAuthentication;

	public OnlineUserSession sessionId(String sessionId) {
		this.sessionId = sessionId;
		return this;
	}

	/**
	 * A unique identifier of the user session
	 * 
	 * @return sessionId
	 **/
	@ApiModelProperty(required = true, value = "A unique identifier of the user session")
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public OnlineUserSession sessionDuration(Long sessionDuration) {
		this.sessionDuration = sessionDuration;
		return this;
	}

	/**
	 * The duration for which the session is active in millisecond
	 * 
	 * @return sessionDuration
	 **/
	@ApiModelProperty(required = true, value = "The duration for which the session is active in millisecond")
	public Long getSessionDuration() {
		return sessionDuration;
	}

	public void setSessionDuration(Long sessionDuration) {
		this.sessionDuration = sessionDuration;
	}

	public OnlineUserSession userSessionActive(Boolean userSessionActive) {
		this.userSessionActive = userSessionActive;
		return this;
	}

	/**
	 * An indicator to depict whether the user has an active authenticated
	 * session or not
	 * 
	 * @return userSessionActive
	 **/
	@ApiModelProperty(required = true, value = "An indicator to depict whether the user has an active authenticated session or not")
	public Boolean isUserSessionActive() {
		return userSessionActive;
	}

	public void setUserSessionActive(Boolean userSessionActive) {
		this.userSessionActive = userSessionActive;
	}

	public OnlineUserSession twoFactorAuthentication(Boolean twoFactorAuthentication) {
		this.twoFactorAuthentication = twoFactorAuthentication;
		return this;
	}

	/**
	 * An indicator to depict whether the user has been authenticated via 2FA or
	 * not
	 * 
	 * @return twoFactorAuthentication
	 **/
	@ApiModelProperty(required = true, value = "An indicator to depict whether the user has been authenticated via 2FA or not")
	public Boolean isTwoFactorAuthentication() {
		return twoFactorAuthentication;
	}

	public void setTwoFactorAuthentication(Boolean twoFactorAuthentication) {
		this.twoFactorAuthentication = twoFactorAuthentication;
	}

	public OnlineUserSession failedAuthentication(Double failedAuthentication) {
		this.failedAuthentication = failedAuthentication;
		return this;
	}

	/**
	 * The number of times the user failed to authenticate due to invalid
	 * credentials
	 * 
	 * @return failedAuthentication
	 **/
	@ApiModelProperty(value = "The number of times the user failed to authenticate due to invalid credentials")
	public Double getFailedAuthentication() {
		return failedAuthentication;
	}

	public void setFailedAuthentication(Double failedAuthentication) {
		this.failedAuthentication = failedAuthentication;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OnlineUserSession onlineUserSession = (OnlineUserSession) o;
		return Objects.equals(this.sessionId, onlineUserSession.sessionId)
				&& Objects.equals(this.sessionDuration, onlineUserSession.sessionDuration)
				&& Objects.equals(this.userSessionActive, onlineUserSession.userSessionActive)
				&& Objects.equals(this.twoFactorAuthentication, onlineUserSession.twoFactorAuthentication)
				&& Objects.equals(this.failedAuthentication, onlineUserSession.failedAuthentication);
	}

	@Override
	public int hashCode() {
		return Objects.hash(sessionId, sessionDuration, userSessionActive, twoFactorAuthentication,
				failedAuthentication);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class OnlineUserSession {\n");

		sb.append("    sessionId: ").append(toIndentedString(sessionId)).append("\n");
		sb.append("    sessionDuration: ").append(toIndentedString(sessionDuration)).append("\n");
		sb.append("    userSessionActive: ").append(toIndentedString(userSessionActive)).append("\n");
		sb.append("    twoFactorAuthentication: ").append(toIndentedString(twoFactorAuthentication)).append("\n");
		sb.append("    failedAuthentication: ").append(toIndentedString(failedAuthentication)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}

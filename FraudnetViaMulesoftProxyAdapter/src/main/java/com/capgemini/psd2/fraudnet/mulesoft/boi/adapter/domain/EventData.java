package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

/**
 * EventData
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventData {
	@SerializedName("electronicDeviceSession")
	private ElectronicDeviceSession electronicDeviceSession;

	@SerializedName("onlineUserSession")
	private OnlineUserSession onlineUserSession;

	@SerializedName("onlineUser")
	private OnlineUser onlineUser;

	@SerializedName("account")
	private List<Account> account;

	@SerializedName("transaction")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Transaction transaction;

	public EventData electronicDeviceSession(ElectronicDeviceSession electronicDeviceSession) {
		this.electronicDeviceSession = electronicDeviceSession;
		return this;
	}

	/**
	 * Get electronicDeviceSession
	 * 
	 * @return electronicDeviceSession
	 **/
	@ApiModelProperty(required = true, value = "")
	public ElectronicDeviceSession getElectronicDeviceSession() {
		return electronicDeviceSession;
	}

	public void setElectronicDeviceSession(ElectronicDeviceSession electronicDeviceSession) {
		this.electronicDeviceSession = electronicDeviceSession;
	}

	public EventData onlineUserSession(OnlineUserSession onlineUserSession) {
		this.onlineUserSession = onlineUserSession;
		return this;
	}

	/**
	 * Get onlineUserSession
	 * 
	 * @return onlineUserSession
	 **/
	@ApiModelProperty(required = true, value = "")
	public OnlineUserSession getOnlineUserSession() {
		return onlineUserSession;
	}

	public void setOnlineUserSession(OnlineUserSession onlineUserSession) {
		this.onlineUserSession = onlineUserSession;
	}

	public EventData onlineUser(OnlineUser onlineUser) {
		this.onlineUser = onlineUser;
		return this;
	}

	/**
	 * Get onlineUser
	 * 
	 * @return onlineUser
	 **/
	@ApiModelProperty(required = true, value = "")
	public OnlineUser getOnlineUser() {
		return onlineUser;
	}

	public void setOnlineUser(OnlineUser onlineUser) {
		this.onlineUser = onlineUser;
	}

	public EventData account(List<Account> account) {
		this.account = account;
		return this;
	}

	public EventData addAccountItem(Account accountItem) {
		if (this.account == null) {
			this.account = new ArrayList<Account>();
		}
		this.account.add(accountItem);
		return this;
	}

	/**
	 * The account information of the end user used in the originating event.
	 * Multiple account information for Account - Authorize\&quot;
	 * 
	 * @return account
	 **/
	@ApiModelProperty(value = "The account information of the end user used in the originating event. Multiple account information for Account - Authorize\"")
	public List<Account> getAccount() {
		return account;
	}

	public void setAccount(List<Account> account) {
		this.account = account;
	}

	public EventData transaction(Transaction transaction) {
		this.transaction = transaction;
		return this;
	}

	/**
	 * Get transaction
	 * 
	 * @return transaction
	 **/
	@ApiModelProperty(value = "")
	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		EventData eventData = (EventData) o;
		return Objects.equals(this.electronicDeviceSession, eventData.electronicDeviceSession)
				&& Objects.equals(this.onlineUserSession, eventData.onlineUserSession)
				&& Objects.equals(this.onlineUser, eventData.onlineUser)
				&& Objects.equals(this.account, eventData.account)
				&& Objects.equals(this.transaction, eventData.transaction);
	}

	@Override
	public int hashCode() {
		return Objects.hash(electronicDeviceSession, onlineUserSession, onlineUser, account, transaction);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class EventData {\n");

		sb.append("    electronicDeviceSession: ").append(toIndentedString(electronicDeviceSession)).append("\n");
		sb.append("    onlineUserSession: ").append(toIndentedString(onlineUserSession)).append("\n");
		sb.append("    onlineUser: ").append(toIndentedString(onlineUser)).append("\n");
		sb.append("    account: ").append(toIndentedString(account)).append("\n");
		sb.append("    transaction: ").append(toIndentedString(transaction)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}

package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

/**
 * AccountInfoType
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountInfo {
  @SerializedName("accountName")
  private String accountName = null;

  @SerializedName("accountNumber")
  private String accountNumber = null;

  @SerializedName("branchSubNationalSortCode")
  private String branchSubNationalSortCode = null;

  public AccountInfo accountName(String accountName) {
    this.accountName = accountName;
    return this;
  }

   /**
   * Name of the account
   * @return accountName
  **/
  @ApiModelProperty(value = "Name of the account")
  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public AccountInfo accountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
    return this;
  }

   /**
   * The identifier of the Account. This will be unique within its Parent NSC, but will not be globally unique.
   * @return accountNumber
  **/
  @ApiModelProperty(required = true, value = "The identifier of the Account. This will be unique within its Parent NSC, but will not be globally unique.")
  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public AccountInfo branchSubNationalSortCode(String branchSubNationalSortCode) {
    this.branchSubNationalSortCode = branchSubNationalSortCode;
    return this;
  }

   /**
   * Where a Branch is organised under a higher level Parent National Sort Code (NSC), a specific NSC allocated to that Branch.
   * @return branchSubNationalSortCode
  **/
  @ApiModelProperty(value = "Where a Branch is organised under a higher level Parent National Sort Code (NSC), a specific NSC allocated to that Branch.")
  public String getBranchSubNationalSortCode() {
    return branchSubNationalSortCode;
  }

  public void setBranchSubNationalSortCode(String branchSubNationalSortCode) {
    this.branchSubNationalSortCode = branchSubNationalSortCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountInfo accountInfoType = (AccountInfo) o;
    return Objects.equals(this.accountName, accountInfoType.accountName) &&
        Objects.equals(this.accountNumber, accountInfoType.accountNumber) &&
        Objects.equals(this.branchSubNationalSortCode, accountInfoType.branchSubNationalSortCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountName, accountNumber, branchSubNationalSortCode);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccountInfoType {\n");
    
    sb.append("    accountName: ").append(toIndentedString(accountName)).append("\n");
    sb.append("    accountNumber: ").append(toIndentedString(accountNumber)).append("\n");
    sb.append("    branchSubNationalSortCode: ").append(toIndentedString(branchSubNationalSortCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}


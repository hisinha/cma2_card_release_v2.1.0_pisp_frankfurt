package com.capgemini.psd2.domestic.standing.order.mock.foundationservice.constants;

public class DomesticStandingOrderFoundationConstants {
	
	public static final String RECORD_NOT_FOUND = "Standing Order Instruction Proposal Record Not Found";
	
	public static final String REFERENCE = "PISPreference";
	
}

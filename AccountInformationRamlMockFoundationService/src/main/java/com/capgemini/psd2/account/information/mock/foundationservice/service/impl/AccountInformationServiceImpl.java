/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.information.mock.foundationservice.raml.domain.Account;
import com.capgemini.psd2.account.information.mock.foundationservice.raml.domain.RetailNonRetailCode;
import com.capgemini.psd2.account.information.mock.foundationservice.repository.AccountInformationRepository;
import com.capgemini.psd2.account.information.mock.foundationservice.service.AccountInformationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;

/**
 * The Class AccountInformationServiceImpl.
 */
@Service
public class AccountInformationServiceImpl implements AccountInformationService {

	/** The repository. */
	@Autowired
	private AccountInformationRepository repository;

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.account.information.mock.foundationservice.service.AccountInformationService#retrieveAccountInformation(java.lang.String, java.lang.String)
	 */
	@Override
	public Account retrieveAccountInformation(String accountNumber,String accountNSC) throws Exception  {

		Account accountBaseType= repository.findByAccountNumber(accountNumber);
//		Account accountBaseType = new Account();
//		
//		accountBaseType.setAccountName("Abc");
//		accountBaseType.setAccountNumber("123");
		//abt.setRetailNonRetailCode(RetailNonRetailCode.RETAIL);
		
	//	repository.save(accountBaseType);
		
    if (accountBaseType == null) {
    	throw MockFoundationServiceException
		.populateMockFoundationServiceException(ErrorCodeEnum.NO_ACOUNT_FOUND_ACC_PAC);
	}
		return accountBaseType;

	}

	@Override
	public Account retrieveAccountInformationRaml(String creditCardNumber, String accountNumber) throws Exception {
		
		Account accnt = repository.findByAccountNumber(accountNumber);
		
		if (accnt == null) {
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.NO_ACOUNT_FOUND_ACC_PAC);
		}
		

		return accnt;
	}

}

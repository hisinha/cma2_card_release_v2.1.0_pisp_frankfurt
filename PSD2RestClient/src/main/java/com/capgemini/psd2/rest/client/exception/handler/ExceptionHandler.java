/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.rest.client.exception.handler;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

/**
 * The Interface ExceptionHandler.
 */
public interface ExceptionHandler {
	
	/**
	 * Handle exception.
	 *
	 * @param e the e
	 */
	public void handleException(HttpServerErrorException e);
	
	/**
	 * Handle exception.
	 *
	 * @param e the e
	 */
	public void handleException(HttpClientErrorException e);
	
	/**
	 * Handle exception.
	 *
	 * @param e the e
	 */
	public void handleException(ResourceAccessException e);
}

package com.capgemini.psd2.foundationservice.insertprestagepayment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.client.InsertPreStagePaymentFoundationClientImpl;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PayeeInformation;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PayerInformation;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.Payment;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInformation;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;


public class InsertPreStagePaymentFoundationServiceTest {

	@InjectMocks
	private InsertPreStagePaymentFoundationClientImpl insertPreStagePaymentFoundationClient;
	
	@Mock
	private RestClientSync restClientSync;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testrestTransportForPaymentRetrieval() {
		
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("/fs-payment-web-service/services/ValidatePreStagePayment");
		
		Payment payment= new Payment();
		
		payment.setPaymentId("FSPID_0000000000226");
		payment.setRequestType("Q");
		Date createdOn = new Date();
		payment.setCreatedOn(createdOn); 
		payment.setChannelUserID("59000037");		
		PayeeInformation payee= new PayeeInformation();
		payee.setIban("IE06BOFI90256597810260");
		payee.setBic("1");
		payee.setBeneficiaryAccountNumber("97810260");
		payee.setBeneficiaryName("Mark Rogers");
		payee.setAccountSecondaryID("83445202");
		payee.setBeneficiaryAddressline1("No1 Dublin Road");
		payee.setBeneficiaryAddressline2("Dublin 2");
		payee.setBeneficiaryStreetname("Chennai");
		payee.setBeneficiaryBuildingNumber("Chennai 1234");
		payee.setBeneficiaryPostCode("Dublin 101");
		payee.setBeneficiaryNsc("902565");
		payee.setBeneficiaryTownName("Dublin");
		payee.setBenCountrySubDivision1("Ranchi");
		payee.setBenCountrySubDivision2("Patna");
		payee.setBeneficiaryCountry("ROI");
		payee.setBeneficiaryCurrency("EUR");
		payee.setMerchentCategoryCode("123456");
		
		PaymentInformation paymentInfo= new PaymentInformation();
		paymentInfo.setPaymentContextCode("6789456");
		paymentInfo.setAmount(new BigDecimal ("200.0"));
		paymentInfo.setInstructionIdentification("987654432");
		Date paymentDate = new Date();
		paymentInfo.setPaymentDate(paymentDate); 
		
		paymentInfo.setInitiatingPartyName("gergaerg");
		paymentInfo.setBeneficiaryReference("gergeargerg");
		paymentInfo.setJurisdiction("ROI");
		Date createdDateTime = new Date();
		paymentInfo.setCreatedDateTime(createdDateTime);; 
		paymentInfo.setUnstructured("ngf12345");
		paymentInfo.setStatus("fefv123445");
		
		PayerInformation payer= new PayerInformation();
		payer.setAccountName("Savings Account");
		//payer.setCifLinkID("8434623");
		payer.setIban("IE65BOFI90570710059532");
		payer.setBic("BOFIIE2DXXX");
		payer.setNsc("905707");
		payer.setAccountNumber("10059532");
		payer.setAccountSecondaryID("835181213");
		payer.setPayerCurrency("EUR");
		payer.setMerchantCustomerIdentification("12345");
		
		payment.setPayeeInformation(payee);
		payment.setPayerInformation(payer);
		payment.setPaymentInformation(paymentInfo);
		
		PaymentInstruction paymentInst = new PaymentInstruction();
		paymentInst.setPayment(payment);
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");
		
		ValidationPassed validationPassed = new ValidationPassed();
		validationPassed.setSuccessCode("test");
		validationPassed.setSuccessMessage("test");
		
		Mockito.when(restClientSync.callForGet(any(), any(), any())).thenReturn(paymentInst);
		PaymentInstruction paymentList = insertPreStagePaymentFoundationClient.restTransportForPaymentRetrieval(requestInfo, PaymentInstruction.class, httpHeaders);
		assertNotNull(paymentList);
		
		
		
	}

	@Test
    public void restTransportForInsertPreStagePaymentTest(){
           
           ValidationPassed validationPassed = new ValidationPassed();
           HttpHeaders headers = new HttpHeaders();
           RequestInfo reqInfo = new RequestInfo();
           PaymentInstruction paymentInstruction = new PaymentInstruction();
           
           Mockito.when(restClientSync.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
           
           validationPassed = insertPreStagePaymentFoundationClient.restTransportForInsertPreStagePayment(reqInfo, paymentInstruction, ValidationPassed.class, headers);
           assertNotNull(validationPassed);
    }
    
    @Test
    public void restTransportForUpdatePreStagePaymentTest(){
           
           ValidationPassed validationPassed = new ValidationPassed();
           HttpHeaders headers = new HttpHeaders();
           RequestInfo reqInfo = new RequestInfo();
           PaymentInstruction paymentInstruction = new PaymentInstruction();
           
           Mockito.when(restClientSync.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
           
           validationPassed = insertPreStagePaymentFoundationClient.restTransportForUpdatePreStagePayment(reqInfo, paymentInstruction, ValidationPassed.class, headers);
           assertNotNull(validationPassed);
    }

	
}

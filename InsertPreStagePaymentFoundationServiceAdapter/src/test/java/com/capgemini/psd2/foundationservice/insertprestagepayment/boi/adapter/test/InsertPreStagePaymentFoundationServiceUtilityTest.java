package com.capgemini.psd2.foundationservice.insertprestagepayment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.transformer.InsertPreStagePaymentFoundationServiceTransformer;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.utility.InsertPreStagePaymentFoundationServiceUtility;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class InsertPreStagePaymentFoundationServiceUtilityTest {
	
	@InjectMocks
	private InsertPreStagePaymentFoundationServiceUtility insertPreStagePaymentFoundationServiceUtility;
	
	@Mock
	private InsertPreStagePaymentFoundationServiceTransformer transformer;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void transformResponseFromAPIToFDForInsertTest(){
		
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		Map<String, String> params = new HashMap<String, String>();
		
		Mockito.when(transformer.transformPaymentInformationAPIToFDForInsert(anyObject(), anyObject())).thenReturn(paymentInstruction);
		
		paymentInstruction = insertPreStagePaymentFoundationServiceUtility.transformResponseFromAPIToFDForInsert(paymentSetupPOSTRequest, params);
		assertNotNull(paymentInstruction);
	}
	
	@Test
	public void transformResponseFromFDToAPIForInsertTest(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		PaymentSetupStagingResponse paymentSetupStagingResponse = new PaymentSetupStagingResponse();
		
		Mockito.when(transformer.transformPaymentInformationFDToAPIForInsert(anyObject())).thenReturn(paymentSetupStagingResponse);
		
		paymentSetupStagingResponse = insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPIForInsert(validationPassed);
		assertNotNull(paymentSetupStagingResponse);
	}
	
	@Test
	public void transformResponseFromAPIToFDForUpdateTest(){
		
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSetupPOSTResponse paymentSetupPostResp = new CustomPaymentSetupPOSTResponse();
		
		Mockito.when(transformer.transformPaymentInformationAPIToFDForUpdate(anyObject(), anyObject())).thenReturn(paymentInstruction);
		
		paymentInstruction = insertPreStagePaymentFoundationServiceUtility.transformResponseFromAPIToFDForUpdate(paymentSetupPostResp, null);
		assertNotNull(paymentInstruction);
	}
	
	@Test
	public void transformResponseFromFDToAPIForUpdateTest(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		PaymentSetupStagingResponse paymentSetupStagingResponse = new PaymentSetupStagingResponse();
		
		Mockito.when(transformer.transformPaymentInformationFDToAPIForUpdate(anyObject())).thenReturn(paymentSetupStagingResponse);
		
		paymentSetupStagingResponse = insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPIForUpdate(validationPassed);
		assertNotNull(paymentSetupStagingResponse);
	}
	
	@Test
	public void getFoundationServiceURLTest(){
		
		String paymentId = "123test";
		String baseURL = "test";
		String endURL = "test";
		
		String finalURL = insertPreStagePaymentFoundationServiceUtility.getFoundationServiceURL(paymentId, baseURL, endURL);
		assertNotNull(finalURL);
	}
	
	@Test(expected = AdapterException.class)
	public void getFoundationServiceURL_PaymentIdNullTest(){
		
		insertPreStagePaymentFoundationServiceUtility.getFoundationServiceURL(null, "baseURL", "endURL");
	}
	
	@Test(expected = AdapterException.class)
	public void getFoundationServiceURL_BaseURLNullTest(){
		
		insertPreStagePaymentFoundationServiceUtility.getFoundationServiceURL("paymentId", null, "endURL");
	}
	
	@Test(expected = AdapterException.class)
	public void getFoundationServiceURL_EndURLNullTest(){
		
		insertPreStagePaymentFoundationServiceUtility.getFoundationServiceURL("paymentId", "baseURL", null);
	}
	
	@Test
	public void transformResponseFromFDToAPITest(){
		
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		
		Mockito.when(transformer.transformPaymentRetrieval(anyObject())).thenReturn(paymentSetupPOSTResponse);
		
		paymentSetupPOSTResponse = insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPI(paymentInstruction);
		assertNotNull(paymentSetupPOSTResponse);
	}
	
	@Test
	public void createRequestHeadersUserHeaderNull(){
		
		ReflectionTestUtils.setField(insertPreStagePaymentFoundationServiceUtility, "userInReqHeader", "test");
		ReflectionTestUtils.setField(insertPreStagePaymentFoundationServiceUtility, "channelInReqHeader", "test");
		ReflectionTestUtils.setField(insertPreStagePaymentFoundationServiceUtility, "platformInReqHeader", "test");
		ReflectionTestUtils.setField(insertPreStagePaymentFoundationServiceUtility, "correlationIdInReqHeader", "test");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, null);
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "BOL");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, null);
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "testPlatform");
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		
		headers = insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(requestInfo, params);
		assertNotNull(headers);
		
	}
	
	@Test
	public void createRequestHeadersChannelHeaderNull(){
		
		ReflectionTestUtils.setField(insertPreStagePaymentFoundationServiceUtility, "userInReqHeader", "test");
		ReflectionTestUtils.setField(insertPreStagePaymentFoundationServiceUtility, "channelInReqHeader", "test");
		ReflectionTestUtils.setField(insertPreStagePaymentFoundationServiceUtility, "platformInReqHeader", "test");
		ReflectionTestUtils.setField(insertPreStagePaymentFoundationServiceUtility, "correlationIdInReqHeader", "test");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "User");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, null);
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "testCorrelation");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, null);
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		
		headers = insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(requestInfo, params);
		assertNotNull(headers);
		
	}
	
	

}

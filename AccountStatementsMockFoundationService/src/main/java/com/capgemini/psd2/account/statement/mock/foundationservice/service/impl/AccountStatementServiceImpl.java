/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.statement.mock.foundationservice.service.impl;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.statement.mock.foundationservice.raml.domain.StatementsResponse;
import com.capgemini.psd2.account.statement.mock.foundationservice.repository.AccountStatementRepository;
import com.capgemini.psd2.account.statement.mock.foundationservice.service.AccountStatementService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;

/**
 * The Class AccountStatementsServiceImpl.
 */
@Service
public class AccountStatementServiceImpl implements AccountStatementService {

	/** The repository. */
	@Autowired
	private AccountStatementRepository repository;

	@Override
	public StatementsResponse retrieveAccountStatements(String accountId) throws Exception {
		StatementsResponse statementsResponse = repository.findByAccountId(accountId);
		
		if(null == statementsResponse)
		{
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.STATEMENT_NOT_FOUND_CS_STL);
		}
		return statementsResponse;

	}

	public byte[] retrivePdf(String accountId, String statementId) throws IOException {
		InputStream in = null;
		byte[] byteArray = null;
		try{
			in = this.getClass().getResourceAsStream("/Statements.pdf");
			byteArray = IOUtils.toByteArray(in);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
            //release resource, if any
			in.close();
        }
		
		return byteArray;
	}

}

package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.ISOConsentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("iStandingOrderConsentsStagingMongoDbAdapter")

public class ISOConsentsStagingMongoDbAdapterImpl implements InternationalStandingOrdersPaymentStagingAdapter {

	@Autowired
	private ISOConsentsFoundationRepository iStandingOrderConsentsBankRepository;

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private RequestHeaderAttributes reqAttributes;

	@Value("${app.sandboxValidationPolicies.contractIdentificationPattern:^Test[a-zA-Z0-9]*}")
	private String contractIdentificationPattern;

	@Override
	public CustomIStandingOrderConsentsPOSTResponse processStandingOrderConsents(
			CustomIStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {

		CustomIStandingOrderConsentsPOSTResponse paymentConsentsBankResource = transformInternationalStandingOrderConsentsToBankResource(
				standingOrderConsentsRequest);

		String currency = standingOrderConsentsRequest.getData().getInitiation().getInstructedAmount().getCurrency();
		String initiationAmount = standingOrderConsentsRequest.getData().getInitiation().getInstructedAmount()
				.getAmount();

		if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_POST_MOCKING)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}

		// Sandbox Secondary Identification Validation
		String secondaryIdentification = standingOrderConsentsRequest.getData().getInitiation().getCreditorAccount()
				.getSecondaryIdentification();
		if (utility.isValidSecIdentification(secondaryIdentification)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTSECONDARYIDENTIFIER,
							ErrorMapKeys.ACCOUNT_SECONDARYIDENTIFIER_INVALID));
		}

		OBChargeBearerType1Code reqChargeBearer = standingOrderConsentsRequest.getData().getInitiation()
				.getChargeBearer();

		String consentId = null;
		String cutOffDateTime = utility.getMockedCutOffDtTm();
		List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, reqChargeBearer);
		ProcessConsentStatusEnum processConsentStatusEnum = utility.getMockedConsentProcessExecStatus(currency,
				initiationAmount);

		if (processConsentStatusEnum == ProcessConsentStatusEnum.PASS
				|| processConsentStatusEnum == ProcessConsentStatusEnum.FAIL)
			consentId = UUID.randomUUID().toString();

		paymentConsentsBankResource.getData().setCreationDateTime(standingOrderConsentsRequest.getCreatedOn());
		paymentConsentsBankResource.getData().setConsentId(consentId);
		paymentConsentsBankResource.getData().setCharges(charges);
		paymentConsentsBankResource.getData().setCutOffDateTime(cutOffDateTime);
		paymentConsentsBankResource.setConsentProcessStatus(processConsentStatusEnum);

		if (consentId != null) {
			OBExternalConsentStatus1Code status = calculateCMAStatus(processConsentStatusEnum, successStatus,
					failureStatus);
			paymentConsentsBankResource.getData().setStatus(status);
			paymentConsentsBankResource.getData().setStatusUpdateDateTime(standingOrderConsentsRequest.getCreatedOn());
		}

		try {
			iStandingOrderConsentsBankRepository.save(paymentConsentsBankResource);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_TECHNICAL_ERROR_FS_PRE_STAGE_INSERTION));
		}
		return paymentConsentsBankResource;
	}

	private CustomIStandingOrderConsentsPOSTResponse transformInternationalStandingOrderConsentsToBankResource(
			CustomIStandingOrderConsentsPOSTRequest paymentConsentsRequest) {
		String paymentFoundationRequest = JSONUtilities.getJSONOutPutFromObject(paymentConsentsRequest);
		return JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(), paymentFoundationRequest,
				CustomIStandingOrderConsentsPOSTResponse.class);

	}

	private OBExternalConsentStatus1Code calculateCMAStatus(ProcessConsentStatusEnum processConsentStatusEnum,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return (processConsentStatusEnum == ProcessConsentStatusEnum.PASS) ? successStatus : failureStatus;
	}

	@Override
	public CustomIStandingOrderConsentsPOSTResponse retrieveStagedInternationalStandingOrdersConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		try {
			String paymentConsentId = customPaymentStageIdentifiers.getPaymentConsentId();

			CustomIStandingOrderConsentsPOSTResponse paymentBankResource = null;

			paymentBankResource = iStandingOrderConsentsBankRepository.findOneByDataConsentId(paymentConsentId);

			String initiationAmount = paymentBankResource.getData().getInitiation().getInstructedAmount().getAmount();

			if (reqAttributes.getMethodType().equals("GET")) {
				if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING)) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
				}
			}

			return paymentBankResource;

		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

}

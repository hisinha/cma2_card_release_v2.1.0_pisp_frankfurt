package com.capgemini.psd2.customer.account.list.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.aisp.domain.OBReadAccount2Data;
import com.capgemini.psd2.cisp.mock.domain.MockChannelProfile;
import com.capgemini.psd2.customer.account.list.mongo.db.adapter.repository.CustomerAccountListMongoDBAdapterRepository;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.StringUtils;

@ConfigurationProperties("app")
public class CustomerAccountListMongoDBAdapterImpl implements CustomerAccountListAdapter {

	@Autowired
	private CustomerAccountListMongoDBAdapterRepository customerAccountListMongoDBAdapterRepository;

	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();

	private boolean allowCreditCardInPISPConsent;
	
	public Map<String, String> getConsentSupportedSchemeMap() {
		return consentSupportedSchemeMap;
	}

	@Override
	public OBReadAccount2 retrieveCustomerAccountList(String userId, Map<String, String> params) {
		OBReadAccount2 accountGETResponse = new OBReadAccount2();
		try {
			List<OBAccount2> accountsNew = new ArrayList<>();
			List<OBAccount2> accounts = customerAccountListMongoDBAdapterRepository.findByPsuId(userId);

			for (OBAccount2 account : accounts) {

				MockChannelProfile mock = (MockChannelProfile) account;
				List<OBCashAccount3> accts = mock.getAccount();
				// assuming we can only have 2 schema representations of any
				// account (IBAN-SORTCODE)
				OBCashAccount3 acct = accts.get(0);
				if ((PSD2Constants.AISP).equals(params.get(PSD2Constants.CONSENT_FLOW_TYPE))) {
					mock.setHashedValue(StringUtils.generateHashedValue(acct.getIdentification()));
					accountsNew.add(mock);
				} else {

					Set<String> additionalInfoKeySet = mock.getAdditionalInformation().keySet();
					String schemeKey = additionalInfoKeySet.stream()
							.filter(key -> consentSupportedSchemeMap.containsValue(key)).findFirst().orElse(null);
					if (NullCheckUtils.isNullOrEmpty(schemeKey)) {
						// throw error ?
					} else {
						String schemeIdentification = mock.getAdditionalInformation().get(schemeKey);
						if (!allowCreditCardInPISPConsent && (PSD2Constants.PISP).equals(params.get(PSD2Constants.CONSENT_FLOW_TYPE))
								&& schemeKey.contains(PSD2Constants.PAN)) {

						} else {
							mock.setHashedValue(StringUtils.generateHashedValue(schemeIdentification));
							accountsNew.add(mock);
						}
					}
				}
				

			}

			OBReadAccount2Data data2 = new OBReadAccount2Data();
			data2.setAccount(accountsNew);
			accountGETResponse.setData(data2);

		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		return accountGETResponse;
	}

	@Override
	public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params) {
		return null;
	}

}

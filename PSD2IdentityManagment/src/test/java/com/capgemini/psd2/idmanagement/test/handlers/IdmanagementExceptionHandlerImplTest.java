package com.capgemini.psd2.idmanagement.test.handlers;

import java.nio.charset.Charset;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.provider.token.store.jwk.JwkException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import com.capgemini.psd2.idmanagement.handlers.IdmanagementExceptionHandlerImpl;

public class IdmanagementExceptionHandlerImplTest {

	private IdmanagementExceptionHandlerImpl idMgtImpl;
	
	
	@Before
	public void setUp() throws Exception {
		idMgtImpl = new IdmanagementExceptionHandlerImpl();
	}
	
	
	@Test(expected=JwkException.class)
	public void test(){
		HttpServerErrorException e = new HttpServerErrorException(HttpStatus.BAD_REQUEST);
		idMgtImpl.handleException(e );
	}
	
	@Test(expected=JwkException.class)
	public void testHttpClientErrorException(){
		HttpClientErrorException e = new HttpClientErrorException(HttpStatus.NOT_FOUND);
		idMgtImpl.handleException(e);
	}
	
	@Test(expected=InvalidClientException.class)
	public void testHttpClientError(){
		String str = new String("Client not found");
		byte[] responseBody = str.getBytes();
		HttpClientErrorException httpClientErrorException = new HttpClientErrorException(HttpStatus.NOT_FOUND, "Client not found", responseBody,
				Charset.forName("UTF-8"));
		idMgtImpl.handleException(httpClientErrorException);
	}
	
	
	@Test(expected=JwkException.class)
	public void testResourceAccessException(){
		ResourceAccessException e = new ResourceAccessException("Data_Not_Found");
		idMgtImpl.handleException(e );
	}
	
	
	
}

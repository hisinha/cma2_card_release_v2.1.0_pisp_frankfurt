package com.capgemini.psd2.identitymanagement.test.service.impl;

import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.List;

import javax.naming.directory.BasicAttributes;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.identitymanagement.services.impl.TppInfoMgmtServiceImpl;


public class TppInfoMgmtServiceImplTest {

	
	@Mock
	private LdapTemplate ldapTemplate;

	
	@InjectMocks
	private TppInfoMgmtServiceImpl tppInfoMgmtServiceImpl;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(expected =OAuth2Exception.class)
	public void testFindTppInfoFailure(){
		tppInfoMgmtServiceImpl.findTppInfo("Moneywise_Wealth");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testFindTppInfo(){
		List<BasicAttributes> applicationDetail = new ArrayList<>();
		BasicAttributes value = new BasicAttributes();
		value.put("x-roles", "x-roles: AISP,PISP,CISP");
		value.put("x-clientid", "x-clientID: Moneywise-Wealth");
		value.put("x-scopes", "x-scopes: AISP");
		value.put("isMemberOf","isMemberOf");
		value.put("x-secret","x-secret: Moneywise-Wealth");
		value.put("x-redirecturl","x-redirecturl: https://www.getpostman.com/oauth2/callback");
		value.put("x-block","x-block: false");
		value.put("o","o: Moneywise.com");
		value.put("cn","Moneywise_Wealth");
		applicationDetail.add(value);
		Mockito.when(ldapTemplate.search(Mockito.any(LdapQuery.class), Mockito.any(AttributesMapper.class))).thenReturn(applicationDetail);
		ReflectionTestUtils.setField(tppInfoMgmtServiceImpl, "ldapTemplate", ldapTemplate);
		Mockito.when(ldapTemplate.lookup(anyString(),  Mockito.any(AttributesMapper.class))).thenReturn(value);
		tppInfoMgmtServiceImpl.findTppInfo("Moneywise_Wealth");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=OAuth2Exception.class)
	public void testFindTppInfoWithLookUpIsNull(){
		List<BasicAttributes> applicationDetail = new ArrayList<>();
		BasicAttributes value = new BasicAttributes();
		value.put("x-roles", "x-roles: AISP,PISP,CISP");
		value.put("x-clientid", "x-clientID: Moneywise-Wealth");
		value.put("x-scopes", "x-scopes: AISP");
		value.put("isMemberOf","isMemberOf");
		value.put("x-secret","x-secret: Moneywise-Wealth");
		value.put("x-redirecturl","x-redirecturl: https://www.getpostman.com/oauth2/callback");
		value.put("x-block","x-block: false");
		value.put("o","o: Moneywise.com");
		value.put("cn","Moneywise_Wealth");
		applicationDetail.add(value);
		Mockito.when(ldapTemplate.search(Mockito.any(LdapQuery.class), Mockito.any(AttributesMapper.class))).thenReturn(applicationDetail);
		ReflectionTestUtils.setField(tppInfoMgmtServiceImpl, "ldapTemplate", ldapTemplate);
		Mockito.when(ldapTemplate.lookup(anyString(),  Mockito.any(AttributesMapper.class))).thenReturn(null);
		tppInfoMgmtServiceImpl.findTppInfo("Moneywise_Wealth");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=OAuth2Exception.class)
	public void testFindTppInfoWithSearchIsNull(){
		Mockito.when(ldapTemplate.search(Mockito.any(LdapQuery.class), Mockito.any(AttributesMapper.class))).thenReturn(null);
		ReflectionTestUtils.setField(tppInfoMgmtServiceImpl, "ldapTemplate", ldapTemplate);
		tppInfoMgmtServiceImpl.findTppInfo("Moneywise_Wealth");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=OAuth2Exception.class)
	public void testFindTppInfoWithSearchIsError(){
		Mockito.when(ldapTemplate.search(Mockito.any(LdapQuery.class), Mockito.any(AttributesMapper.class))).thenThrow(new RuntimeException("Test"));
		ReflectionTestUtils.setField(tppInfoMgmtServiceImpl, "ldapTemplate", ldapTemplate);
		tppInfoMgmtServiceImpl.findTppInfo("Moneywise_Wealth");
	}
	
}

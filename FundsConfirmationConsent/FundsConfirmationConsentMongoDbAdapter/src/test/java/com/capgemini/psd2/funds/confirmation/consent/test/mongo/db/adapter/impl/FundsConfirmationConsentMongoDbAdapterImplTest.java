package com.capgemini.psd2.funds.confirmation.consent.test.mongo.db.adapter.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.el.util.ReflectionUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.impl.FundsConfirmationConsentMongoDbAdapterImpl;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.repository.FundsConfirmationConsentRepository;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.DateUtilites;

@RunWith(SpringJUnit4ClassRunner.class)
public class FundsConfirmationConsentMongoDbAdapterImplTest {

	@Mock
	private FundsConfirmationConsentRepository fundsConfirmationConsentRepository;

	@Mock
	private CispConsentAdapter cispConsentAdapter;
	
	@Mock
	private CompatibleVersionList compatibleVersionList;

	@InjectMocks
	private FundsConfirmationConsentMongoDbAdapterImpl adapter;
	
	@Mock
	private RequestHeaderAttributes req;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		Mockito.when(req.getTenantId()).thenReturn("1");
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateFundsConfirmationConsentPOSTResponse() {
		OBFundsConfirmationConsentDataResponse1 data = new OBFundsConfirmationConsentDataResponse1();
		data.setConsentId("1234");
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		fundsConfirmationConsentPOSTResponse.setData(data);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentDataResponse1.class)))
				.thenReturn(data);
		adapter.createFundsConfirmationConsentPOSTResponse(data);
		assertNotNull(data);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateFundsConfirmationConsentPOSTResponseWithNullData() {
	
		OBFundsConfirmationConsentDataResponse1 data1 = null;
		
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentDataResponse1.class)))
				.thenThrow(DataAccessResourceFailureException.class);
		fundsConfirmationConsentPOSTResponse.setData(getfundData());
		
		adapter.createFundsConfirmationConsentPOSTResponse(getfundData());
		assertNull(data1);
	}

	@Test(expected = PSD2Exception.class)
	public void testGetFundsConfirmationConsentPOSTResponse() {
		OBFundsConfirmationConsentDataResponse1 data = new OBFundsConfirmationConsentDataResponse1();
		data.setTppCID("12345");
		String consentId = "1234";
		data.setConsentId(consentId);
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		
		Mockito.when(req.getTppCID()).thenReturn("1");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		fundsConfirmationConsentPOSTResponse.setData(data);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any())).thenReturn(data);
		adapter.getFundsConfirmationConsentPOSTResponse(consentId);
		assertNotNull(data);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetFundsConfirmationConsentPOSTResponsenull() {
		OBFundsConfirmationConsentDataResponse1 data = new OBFundsConfirmationConsentDataResponse1();
		String consentId = "1234";
		data.setConsentId(consentId);
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		fundsConfirmationConsentPOSTResponse.setData(data);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any())).thenReturn(null);
		adapter.getFundsConfirmationConsentPOSTResponse(consentId);
		assertNull(data);
	}
	
	

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testGetFundsConfirmationConsentPOSTResponseWithNoResourceFound() {
		OBFundsConfirmationConsentDataResponse1 data1 = null;
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		String consentId = "1456";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any()))
				.thenThrow(DataAccessResourceFailureException.class);
		fundsConfirmationConsentPOSTResponse.setData(data1);
		adapter.getFundsConfirmationConsentPOSTResponse(consentId);
		assertEquals(1456, consentId);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testGetFundsConfirmationConsentPOSTResponseWithNullData() {
		OBFundsConfirmationConsentDataResponse1 data1 = null;
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		String consentId = "3333";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any())).thenThrow(PSD2Exception.class);
		fundsConfirmationConsentPOSTResponse.setData(data1);
		adapter.getFundsConfirmationConsentPOSTResponse(null);
		assertEquals(3333, consentId);
	}

	@Test
	public void testRemoveFundsConfirmationConsent() {
		OBFundsConfirmationConsentDataResponse1 data1 = new OBFundsConfirmationConsentDataResponse1();
		String cId = "123456";
		data1.setTppCID(cId);
		data1.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalRequestStatus1Code.REVOKED);
		String consentId = "11113";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenReturn(data1);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentDataResponse1.class)))
		.thenReturn(data1);
		List<CispConsent> cispConsent = new ArrayList<>();
		CispConsent consent = new CispConsent();
		consent.setChannelId("786478");
		cispConsent.add(consent);
		when(cispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(anyString(), any())).thenReturn(cispConsent);
		cispConsentAdapter.updateConsentStatusWithResponse(anyString(), any());
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentNullcispConsent() {
		OBFundsConfirmationConsentDataResponse1 data1 = new OBFundsConfirmationConsentDataResponse1();
		String cId = "123456";
		data1.setTppCID(cId);
		data1.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalRequestStatus1Code.REVOKED);
		String consentId = "11113";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any())).thenReturn(data1);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentDataResponse1.class)))
		.thenThrow(DataAccessResourceFailureException.class);
		when(cispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(anyString(), any())).thenReturn(null);
	    Mockito.doNothing().when(cispConsentAdapter).updateConsentStatusWithResponse(anyString(), any());
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}
	
	
	
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentNullcispConsent1() {
		OBFundsConfirmationConsentDataResponse1 data1 = new OBFundsConfirmationConsentDataResponse1();
		String cId = "123456";
		data1.setTppCID(cId);
		data1.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalRequestStatus1Code.REVOKED);
		String consentId = "11113";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any())).thenReturn(data1);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentDataResponse1.class)))
		.thenThrow(DataAccessResourceFailureException.class);
		CispConsent cis=new CispConsent();
		cis.setConsentId("12345");
		when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(anyString(), any())).thenReturn(cis);
	    Mockito.doNothing().when(cispConsentAdapter).updateConsentStatusWithResponse(anyString(), any());
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithInvalidTppUser() {
		OBFundsConfirmationConsentDataResponse1 data1 = new OBFundsConfirmationConsentDataResponse1();
		data1.setTppLegalEntityName("TPPLegalName");
		String consentId = "11113";
		String cId = "11114";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenReturn(data1);
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithNoResourceFound() {
		OBFundsConfirmationConsentDataResponse1 data1 = null;
		String consentId = "11113";
		String cId = "11114";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any()))
				.thenThrow(DataAccessResourceFailureException.class);
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithNullData() {
		OBFundsConfirmationConsentDataResponse1 data1 = null;
		String consentId = "11113";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenThrow(PSD2Exception.class);
		adapter.removeFundsConfirmationConsent(null, null);
	}

	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithRevokedStatus() {
		OBFundsConfirmationConsentDataResponse1 data1 = new OBFundsConfirmationConsentDataResponse1();
		data1.setStatus(OBExternalRequestStatus1Code.REVOKED);
		data1.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		String consentId = "11113";
		String cId = "11114";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		CispConsent cispConsent = new CispConsent();
		cispConsent.setChannelId("88888");
		when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(anyString(), any())).thenReturn(cispConsent);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentDataResponse1.class)))
				.thenReturn(data1);
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithRevokedStatusException() {
		OBFundsConfirmationConsentDataResponse1 data1 = new OBFundsConfirmationConsentDataResponse1();
		data1.setStatus(OBExternalRequestStatus1Code.REVOKED);
		CispConsent cispConsent = null;
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		cispConsentAdapter.updateConsentStatus(anyString(), any());
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentDataResponse1.class)))
				.thenThrow(DataAccessResourceFailureException.class);
		adapter.removeFundsConfirmationConsent(null, null);

	}
	
	@Test
	public void testUpdateFundsConfirmationConsentResponseWithDataResource() {
		OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentDataResponse1 data = new OBFundsConfirmationConsentDataResponse1();
		data.setTppCID("123456");
		data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setStatus(OBExternalRequestStatus1Code.AUTHORISED);
		String consentId = "786";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenReturn(data);
		try {
			when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentDataResponse1.class)))
					.thenReturn(data);
		} catch (DataAccessResourceFailureException e) {
			assertEquals("CONNECTION_ERROR", e.getMessage());
		}
		updateFundsConfirmationConsentPOSTResponse.setData(data);
		adapter.updateFundsConfirmationConsentResponse(consentId, OBExternalRequestStatus1Code.AUTHORISED);
	}

	@SuppressWarnings("unchecked") // Done Completely
	@Test(expected = PSD2Exception.class)
	public void testUpdateFundsConfirmationConsentResponseAndException() {
		OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentDataResponse1 data = null;
		String consentId = "786";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any()))
		.thenThrow(DataAccessResourceFailureException.class);
		updateFundsConfirmationConsentPOSTResponse.setData(data);
		adapter.updateFundsConfirmationConsentResponse(consentId, OBExternalRequestStatus1Code.AUTHORISED);
	}


	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testUpdateFundsConfirmationConsentResponseWithDataResourceException() {
		OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentDataResponse1 data = new OBFundsConfirmationConsentDataResponse1();
		data.setTppCID("123456");
		data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setStatus(OBExternalRequestStatus1Code.AUTHORISED);
		String consentId = "786";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenReturn(data);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentDataResponse1.class)))
				.thenThrow(DataAccessResourceFailureException.class);
		updateFundsConfirmationConsentPOSTResponse.setData(data);
		adapter.updateFundsConfirmationConsentResponse("dd", OBExternalRequestStatus1Code.AUTHORISED);
	}

	@After
	public void tearDown() throws Exception {
		fundsConfirmationConsentRepository = null;
		cispConsentAdapter = null;
		adapter = null;
	}
	
	
	public static OBFundsConfirmationConsentDataResponse1 getfundData(){
	

	 OBFundsConfirmationConsentDataResponse1 obFundsConfirmationConsentDataResponse1=new OBFundsConfirmationConsentDataResponse1();	
	obFundsConfirmationConsentDataResponse1.setConsentId("123456");
	 obFundsConfirmationConsentDataResponse1.setCreationDateTime("kjkjn");
	 //obFundsConfirmationConsentDataResponse1.setDebtorAccount(debtorAccount);
	 return obFundsConfirmationConsentDataResponse1;
  }

}

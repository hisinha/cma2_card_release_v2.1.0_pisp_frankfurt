package com.capgemini.psd2.funds.confirmation.consent.routing.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.funds.confirmation.consent.routing.adapter.impl.FundsConfirmationConsentRoutingAdapter;

@Configuration
public class FundsConfirmationConsentRoutingAdapterConfig {

	@Bean
	public FundsConfirmationConsentAdapter fundsConfirmationConsentRoutingAdapter(){
		return new FundsConfirmationConsentRoutingAdapter();
	}
	
}

package com.capgemini.psd2.funds.confirmation.consent.test.validations;

import org.junit.Test;

import com.capgemini.psd2.cisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.consent.validations.FundsConfirmationConsentValidationUtility;

public class FundsConfirmationConsentValidationUtilityTest {

	/*@Test(expected=PSD2Exception.class)
	public void testValidateExpirationDateTime() {
		FundsConfirmationConsentValidationUtility.validateExpirationDateTime("2019-05-02T00:00:00+00:00");
		FundsConfirmationConsentValidationUtility.validateExpirationDateTime(null);
		try {
		FundsConfirmationConsentValidationUtility.validateExpirationDateTime("2019-05-02T00:00:00+00:");
		}catch(PSD2Exception e) {
		}
		FundsConfirmationConsentValidationUtility.validateExpirationDateTime("2017-05-02T00:00:00+00:00");
	}*/
	
	@Test(expected=PSD2Exception.class)
	public void testValidateCOnsentId() {
		FundsConfirmationConsentValidationUtility.validateConsentId("1234", "[0-9]*");
		FundsConfirmationConsentValidationUtility.validateConsentId(null,"[0-9]*");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateIban() {
		FundsConfirmationConsentValidationUtility.validateIBAN("FR1420041010050500013M02606");
		try {
		FundsConfirmationConsentValidationUtility.validateIBAN(null);
		}catch(PSD2Exception e) {
		}
		FundsConfirmationConsentValidationUtility.validateIBAN("@szduy");
	}
	
	@Test
	public void testValidateDebtorAccount() {
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setIdentification("FR1420041010050500013M02606");
		FundsConfirmationConsentValidationUtility.validateIdentification(debtorAccount);
	}
	
/*	@Test
	public void testValidateDebtorAccount1() {
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setIdentification("12345678901234");
		FundsConfirmationConsentValidationUtility.validateIdentification(debtorAccount);
	}
*/	
	@Test(expected=PSD2Exception.class)
	public void testValidateDebtorAccount2() {
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setIdentification("12345678901234546");
		FundsConfirmationConsentValidationUtility.validateIdentification(debtorAccount);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateDebtorAccount1Exception() {
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		debtorAccount.setSchemeName(null);
		debtorAccount.setIdentification("12345678901234");
		FundsConfirmationConsentValidationUtility.validateIdentification(debtorAccount);
	}
}

package com.capgemini.psd2.pisp.domestic.standing.order.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.DStandingorderFoundationResource;

public interface DomesticStandingOrderFoundationRepository extends MongoRepository<DStandingorderFoundationResource,String>{
	
	public DStandingorderFoundationResource findOneByDataDomesticStandingOrderId(String standingOrderId);

}

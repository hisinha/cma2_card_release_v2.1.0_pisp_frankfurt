package com.capgemini.psd2.pisp.standing.order.submission.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticStandingOrderAdapter;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.standing.order.submission.adapter.impl.DStandingOrderRoutingAdapterImpl;
import com.capgemini.psd2.pisp.standing.order.submission.adapter.routing.DStandingOrderAdapterFactory;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class DStandingOrderRoutingAdapterImplTest {

	@Mock
	private DStandingOrderAdapterFactory factory;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@InjectMocks
	private DStandingOrderRoutingAdapterImpl routingAdapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testProcessDomesticStandingOrder() {
		CustomDStandingOrderPOSTRequest request = new CustomDStandingOrderPOSTRequest();

		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		token.setSeviceParams(params);

		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		params.put(PSD2Constants.USER_IN_REQ_HEADER,
				reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));

		DomesticStandingOrderAdapter adapter = new DomesticStandingOrderAdapter() {

			@Override
			public DStandingOrderPOST201Response retrieveStagedDomesticStandingOrder(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public DStandingOrderPOST201Response processDomesticStandingOrder(CustomDStandingOrderPOSTRequest request,
					Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
		};

		Mockito.when(factory.getDomesticStandingOrdersInstance(anyString())).thenReturn(adapter);
		routingAdapter.processDomesticStandingOrder(request, new HashMap<>(), params);
	}
	
	@Test
	public void testRetrieveStagedDomesticStandingOrder() {
		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		token.setSeviceParams(params);

		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		DomesticStandingOrderAdapter adapter = new DomesticStandingOrderAdapter() {

			@Override
			public DStandingOrderPOST201Response retrieveStagedDomesticStandingOrder(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public DStandingOrderPOST201Response processDomesticStandingOrder(CustomDStandingOrderPOSTRequest request,
					Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
		};

		Mockito.when(factory.getDomesticStandingOrdersInstance(anyString())).thenReturn(adapter);
		routingAdapter.retrieveStagedDomesticStandingOrder(customPaymentStageIdentifiers, params);
		assertNotNull(params);

	}

	public void testRetrieveStagedDomesticStandingOrderNullParams() {

		Map<String, String> params = null;
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		DomesticStandingOrderAdapter adapter = new DomesticStandingOrderAdapter() {

			@Override
			public DStandingOrderPOST201Response retrieveStagedDomesticStandingOrder(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public DStandingOrderPOST201Response processDomesticStandingOrder(CustomDStandingOrderPOSTRequest request,
					Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
		};

		Mockito.when(factory.getDomesticStandingOrdersInstance(anyString())).thenReturn(adapter);
		routingAdapter.retrieveStagedDomesticStandingOrder(customPaymentStageIdentifiers, params);
		assertNull(params);
	}

	@After
	public void tearDown() throws Exception {
		factory = null;
		reqHeaderAtrributes = null;
		routingAdapter = null;
	}

}

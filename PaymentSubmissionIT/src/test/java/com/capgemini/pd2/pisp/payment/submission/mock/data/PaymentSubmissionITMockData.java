package com.capgemini.pd2.pisp.payment.submission.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSubmission;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;

public class PaymentSubmissionITMockData {

	private static PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData;

	public static PaymentSubmissionPOSTRequest getPaymentSubmissionPOSTRequestMockData() {
		paymentSubmissionPOSTRequestMockData = new PaymentSubmissionPOSTRequest();

		PaymentSubmission paymentSubmission = new PaymentSubmission();

		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("DemoUser");

		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.0");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);

		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setIdentification("12345");
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		initiation.setCreditorAgent(creditorAgent);

		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);

		/*DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setSecondaryIdentification("002");
		initiation.setDebtorAccount(debtorAccount);

		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setIdentification("SC1128010");
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.UKSORTCODE);
		initiation.setDebtorAgent(debtorAgent);*/

		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		paymentSubmission.setInitiation(initiation);
		paymentSubmission.setPaymentId("e5bcd9f127144ca99e07bbf0335b920b");
		paymentSubmissionPOSTRequestMockData.setData(paymentSubmission);

		Risk risk = new Risk();

		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		List<String> addressLine = new ArrayList();
		addressLine.add("Flat 7 ");
		addressLine.add(" Acacia Lodge ");
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setCountry("GB");
		deliveryAddress.setBuildingNumber("27");
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		risk.setDeliveryAddress(deliveryAddress);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		paymentSubmissionPOSTRequestMockData.setRisk(risk);
		return paymentSubmissionPOSTRequestMockData;
	}
}
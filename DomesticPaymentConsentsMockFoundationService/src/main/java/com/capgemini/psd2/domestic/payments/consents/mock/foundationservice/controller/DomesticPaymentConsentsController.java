/*******************************************************************************
f * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.service.DomesticPaymentConsentsService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;


// TODO: Auto-generated Javadoc
/**
 * The Class DomesticPaymentConsentsController.
 */

@RestController
@RequestMapping("/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/")
public class DomesticPaymentConsentsController {

	/** The account information service. */
	@Autowired
	private DomesticPaymentConsentsService domesticpaymentService;

	/** The validation utility. */
	@Autowired
	private ValidationUtility validationUtility;

	/**
	 * Channel A DomesticPaymentConsents information.
	 *
	 * @param paymentInstructionProposalId
	 *            the payment instruction proposal id
	 * @param channelcode
	 *            the channelcode
	 * @param sourceuse
	 *            the sourceuse
	 * @param transactionid
	 *            the transactionid
	 * @param sourcesystem
	 *            the sourcesystem
	 * @param correlationID
	 *            the correlation ID
	 * @return the accounts
	 * @throws Exception
	 *             the exception
	 */
	
	@RequestMapping(value = "/v{version}/domestic/payment-instruction-proposals/{paymentInstructionProposalId}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public PaymentInstructionProposal domesticpayment(
			@PathVariable("paymentInstructionProposalId") String paymentInstructionProposalId,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelcode,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String transactionId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-PARTY-SOURCE-ID-NUMBER") String partysourceReqHeader,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand) throws Exception {

		if (null == sourceSystemReqHeader || null == channelBrand) {
		
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
		}

		if (paymentInstructionProposalId == null) {

			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);

		}
		validationUtility.validateErrorCode(transactionId);
		
	
		return domesticpaymentService.retrieveAccountInformation(paymentInstructionProposalId);

	}

	/**
	 * Domestic payment consent post.
	 *
	 * @param paymentInstProposalReq
	 *            the payment inst proposal req
	 * @param channelcode
	 *            the channelcode
	 * @param correlationId
	 *            the correlation id
	 * @param sourceSystemReqHeader
	 *            the source system req header
	 * @param sourceUserReqHeader
	 *            the source user req header
	 * @param channelInReqHeader
	 *            the channel in req header
	 * @return the response entity
	 */
	@RequestMapping(value = "/v{version}/domestic/payment-instruction-proposals", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<PaymentInstructionProposal> domesticPaymentConsentPost(
			@RequestBody PaymentInstructionProposal paymentInstProposalReq,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelcode,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelInReqHeader,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand) throws Exception {

		if (null == sourceSystemReqHeader || null == channelBrand) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
		}
		
			
			validationUtility.validateMockBusinessValidationsForSchedulePayment(paymentInstProposalReq.getInstructionReference());
			
		PaymentInstructionProposal paymentInstructionProposalResponse = null;

		try {
			paymentInstructionProposalResponse = domesticpaymentService
					.createDomesticPaymentConsentsResource(paymentInstProposalReq);
			
			

		} catch (RecordNotFoundException e) {
			e.printStackTrace();
		}

		if (null == paymentInstructionProposalResponse) {

			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);
		}
		
		
		return new ResponseEntity<>(paymentInstructionProposalResponse, HttpStatus.CREATED);

	}
	
	@RequestMapping(value = "/v{version}/domestic/payment-instruction-proposals/validate", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<PaymentInstructionProposal> domesticPaymentConsentValidatePost(@RequestBody PaymentInstructionProposal paymentInstProposalReq,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelInReqHeader,
			@RequestHeader(required = false, value = "X-API-PARTY-SOURCE-ID-NUMBER") String partysourceReqHeader,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationMuleReqHeader,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand) throws Exception{
		
		if (null == sourceSystemReqHeader || null == channelBrand) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
		}
		
		if (null == paymentInstProposalReq) {

			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);
		}
		if (paymentInstProposalReq.getInstructionReference().contains("601")
				|| paymentInstProposalReq.getInstructionReference().contains("629")) {
			validationUtility.validateExecutionErrorCodeForMuleOutOfBoxPolicy(
					paymentInstProposalReq.getInstructionReference());
		}
		if (paymentInstProposalReq.getInstructionReference().contains("PPA_DPIPVP") 
			 || paymentInstProposalReq.getInstructionReference().contains("FS_PMV")) {
			
				 validationUtility.validateExecutionErrorCode(paymentInstProposalReq.getInstructionReference());

				}
		
		PaymentInstructionProposal paymentInstructionProposalResponse = null;

		try {
			paymentInstructionProposalResponse = domesticpaymentService.validateDomesticPaymentConsentsResource(paymentInstProposalReq);

		} catch (RecordNotFoundException e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>(paymentInstructionProposalResponse, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/v{version}/domestic/payment-instruction-proposals/{paymentInstructionProposalId}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public PaymentInstructionProposal domesticPaymentConsentPut(
			@PathVariable("paymentInstructionProposalId") String paymentInstructionProposalId,
			@RequestBody PaymentInstructionProposal paymentInstProposalReq,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelcode,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String transactionId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-PARTY-SOURCE-ID-NUMBER") String partysourceReqHeader,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand) throws Exception {
 
		if (null == sourceSystemReqHeader || null == channelBrand) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
		} 
		if (paymentInstProposalReq.getInstructionReference().contains("801")
				|| paymentInstProposalReq.getInstructionReference().contains("829")) {
			validationUtility.validateExecutionErrorCodeForMuleOutOfBoxPolicyForUpdate(
					paymentInstProposalReq.getInstructionReference());
		}
		if (paymentInstProposalReq.getInstructionReference().contains("PPA_DPIPR")) {
			
				 validationUtility.validateExecutionErrorCode(paymentInstProposalReq.getInstructionReference());

				}

			System.out.println("Inside Domestic payment Consent Update");
		 
		return domesticpaymentService.updateDomesticPaymentConsentsResource(paymentInstructionProposalId,paymentInstProposalReq);

		
	}

}


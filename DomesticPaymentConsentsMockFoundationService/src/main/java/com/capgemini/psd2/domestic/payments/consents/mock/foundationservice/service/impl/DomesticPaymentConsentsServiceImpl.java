/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.constants.DomesticPaymentConsentsFoundationConstants;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.repository.DomesticPaymentConsentsRepository;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.repository.PaymentRetrievalRepository;
import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.service.DomesticPaymentConsentsService;
import com.capgemini.psd2.foundationservice.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.pisp3.domain.Channel;
import com.capgemini.psd2.foundationservice.pisp3.domain.ChannelCode;
import com.capgemini.psd2.foundationservice.pisp.transformer.PaymentFoundationServiceTransformer;
import com.capgemini.psd2.foundationservice.pisp1.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.pisp3.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.pisp3.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.pisp3.domain.FraudSystemResponse;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp3.domain.ProposalStatus;



// TODO: Auto-generated Javadoc
/**
 * The Class DomesticPaymentConsentsServiceImpl.
 */
@Service
public class DomesticPaymentConsentsServiceImpl implements DomesticPaymentConsentsService {

	/** The repository. */
	@Autowired
	private DomesticPaymentConsentsRepository repository;

	@Autowired
	private PaymentRetrievalRepository paymentRepository;
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	@Autowired
	private PaymentFoundationServiceTransformer paymentFoundationServiceTransformer;
	
	AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.
	 * service.DomesticPaymentConsentsService#retrieveAccountInformation(java.
	 * lang.String)
	 */
	@Override
	public PaymentInstructionProposal retrieveAccountInformation(String paymentInstructionProposalId) throws Exception {

		PaymentInstructionProposal paymentInstProposal = repository.findByPaymentInstructionProposalId(paymentInstructionProposalId);
		
		if (null == paymentInstProposal) {
			PaymentInstruction paymentInstruction = paymentRepository.findByPaymentPaymentId(paymentInstructionProposalId);

			if (null != paymentInstruction) {
				paymentInstProposal = paymentFoundationServiceTransformer.transformPaymentInstructionConsent(paymentInstruction);
			}
		}
		if (null == paymentInstProposal) {			
			throw new RecordNotFoundException(DomesticPaymentConsentsFoundationConstants.RECORD_NOT_FOUND);
		}

		return paymentInstProposal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.
	 * service.DomesticPaymentConsentsService#
	 * createDomesticPaymentConsentsResource(com.capgemini.psd2.domestic.
	 * payments.consents.mock.foundationservice.domain.
	 * PaymentInstructionProposal)
	 */
	@Override
	public PaymentInstructionProposal createDomesticPaymentConsentsResource(
			PaymentInstructionProposal paymentInstProposalReq) throws RecordNotFoundException {
		String consentID = null;
		if (null == paymentInstProposalReq) {

			throw new RecordNotFoundException(DomesticPaymentConsentsFoundationConstants.RECORD_NOT_FOUND);
		}
		if (!((paymentInstProposalReq.getInstructionEndToEndReference())
				.equalsIgnoreCase(DomesticPaymentConsentsFoundationConstants.INSTRUCTION_ENDTOEND_REF))) {

			paymentInstProposalReq.setProposalStatus(ProposalStatus.AwaitingAuthorisation);
			
			if (null != paymentInstProposalReq.getChannel()
					&& null != paymentInstProposalReq.getChannel().getBrandCode()) {

				if (paymentInstProposalReq.getChannel().getBrandCode().toString().equalsIgnoreCase("BOI-ROI")) {
					if (paymentInstProposalReq.getProposingPartyAccount().getSchemeName()
							.equalsIgnoreCase("UK.OBIE.SortCodeAccountNumber")
							|| paymentInstProposalReq.getProposingPartyAccount().getSchemeName()
									.equalsIgnoreCase("SortCodeAccountNumber")
							) {
						paymentInstProposalReq.setProposalStatus(ProposalStatus.Rejected);
					}
					else if (null != paymentInstProposalReq.getAuthorisingPartyAccount() && null != paymentInstProposalReq.getAuthorisingPartyAccount().getSchemeName()) {
						if(paymentInstProposalReq.getAuthorisingPartyAccount().getSchemeName()
									.equalsIgnoreCase("UK.OBIE.SortCodeAccountNumber")
							|| paymentInstProposalReq.getAuthorisingPartyAccount().getSchemeName()
									.equalsIgnoreCase("SortCodeAccountNumber")) {
							paymentInstProposalReq.setProposalStatus(ProposalStatus.Rejected);
						}
						
					}
				}

			}

		} else {

			paymentInstProposalReq.setProposalStatus(ProposalStatus.Rejected);
		}
		consentID = UUID.randomUUID().toString();
		paymentInstProposalReq.setPaymentInstructionProposalId(consentID);
		try {
			Date date = Calendar.getInstance().getTime();		
			String strDate = timeZoneDateTimeAdapter.parseDateTimeCMA(date);
			paymentInstProposalReq.setProposalCreationDatetime(strDate);
			paymentInstProposalReq.setProposalStatusUpdateDatetime(strDate);
		}catch(Exception e) {}
		repository.save(paymentInstProposalReq);
		return paymentInstProposalReq;

	}
	
	@Override
	public PaymentInstructionProposal validateDomesticPaymentConsentsResource(
			PaymentInstructionProposal paymentInstProposalReq) throws RecordNotFoundException {
		
		
		repository.save(paymentInstProposalReq);
		return paymentInstProposalReq;
	}
	
	@Override
	public PaymentInstructionProposal updateDomesticPaymentConsentsResource(String paymentInstructionProposalId,PaymentInstructionProposal paymentInstProposalReq) throws Exception {
		
		System.out.println("Inside Domestic serviceIMPL");

		System.out.println("paymentInstructionProposalId: " + paymentInstructionProposalId);
		// repository.save(paymentInstProposalReq);
		PaymentInstruction paymentInstruction = paymentRepository.findByPaymentPaymentId(paymentInstructionProposalId);
		PaymentInstructionProposal paymentInstructionpropsal = repository
				.findByPaymentInstructionProposalId(paymentInstProposalReq.getPaymentInstructionProposalId());
		if (null != paymentInstruction) {
			PaymentInstruction paymentInstructionreponce = paymentFoundationServiceTransformer
					.transformPaymentInstructionUpdate(paymentInstruction, paymentInstProposalReq);
			paymentRepository.save(paymentInstructionreponce);
		} else {

			// Update Proposal Status
			if (paymentInstProposalReq.getProposalStatus().equals(ProposalStatus.AcceptedCustomerProfile)) {
				paymentInstructionpropsal.setProposalStatus(ProposalStatus.AcceptedCustomerProfile);
			}else {
				paymentInstructionpropsal.setProposalStatus(paymentInstProposalReq.getProposalStatus());}

			// Update Channel Code 
			Channel channel = new Channel();
			if (paymentInstProposalReq.getChannel().getChannelCode().equals(ChannelCode.BOL)) {
				channel.setChannelCode(ChannelCode.BOL);
			} else if (paymentInstProposalReq.getChannel().getChannelCode().equals(ChannelCode.B365)) {
				channel.setChannelCode(ChannelCode.BOL);
			} else if (paymentInstProposalReq.getChannel().getChannelCode().equals(ChannelCode.API)) {
				channel.setChannelCode(ChannelCode.API);
			}
			
			// Update Channel Brand 
			if (paymentInstProposalReq.getChannel().getBrandCode().equals(BrandCode3.NIGB)) {
				channel.setBrandCode(BrandCode3.NIGB);
			} else if (paymentInstProposalReq.getChannel().getBrandCode().equals(BrandCode3.ROI)) {
				channel.setBrandCode(BrandCode3.ROI);
			} 
			
			paymentInstructionpropsal.setChannel(channel);
			if (null != paymentInstProposalReq.getFraudSystemResponse()) {
			// Update FraudScore
			if (paymentInstProposalReq.getFraudSystemResponse().equals(FraudSystemResponse.Approve)) {
				paymentInstructionpropsal.setFraudSystemResponse(FraudSystemResponse.Approve);
			} else if (paymentInstProposalReq.getFraudSystemResponse().equals(FraudSystemResponse.Investigate)) {
				paymentInstructionpropsal.setFraudSystemResponse(FraudSystemResponse.Investigate);
			} else if (paymentInstProposalReq.getFraudSystemResponse().equals(FraudSystemResponse.Reject)) {
				paymentInstructionpropsal.setFraudSystemResponse(FraudSystemResponse.Reject);
			}
            }
			// Update Debtors Details
			if (null != paymentInstProposalReq.getAuthorisingPartyAccount()) {
			authorisingPartyAccount.setSchemeName(paymentInstProposalReq.getAuthorisingPartyAccount().getSchemeName());
			authorisingPartyAccount.setAccountIdentification(
					paymentInstProposalReq.getAuthorisingPartyAccount().getAccountIdentification());
			authorisingPartyAccount
					.setAccountName(paymentInstProposalReq.getAuthorisingPartyAccount().getAccountName());
			authorisingPartyAccount
					.setAccountNumber(paymentInstProposalReq.getAuthorisingPartyAccount().getAccountNumber());
			authorisingPartyAccount.setSecondaryIdentification(
					paymentInstProposalReq.getAuthorisingPartyAccount().getSecondaryIdentification());
			paymentInstructionpropsal.setAuthorisingPartyAccount(authorisingPartyAccount);
			}
			paymentInstructionpropsal
					.setProposalStatusUpdateDatetime(paymentInstProposalReq.getProposalStatusUpdateDatetime());

			repository.save(paymentInstructionpropsal);
		}
		return paymentInstructionpropsal;
	/*	System.out.println("Inside Domestic serviceIMPL");
		
		System.out.println("paymentInstructionProposalId: "+paymentInstructionProposalId);
		
		PaymentInstruction paymentInstruction = paymentRepository.findByPaymentPaymentId(paymentInstructionProposalId);
		
		if (null != paymentInstruction) {
			PaymentInstruction paymentInstructionreponce = paymentFoundationServiceTransformer.transformPaymentInstructionUpdate(paymentInstruction, paymentInstProposalReq);
			paymentRepository.save(paymentInstructionreponce);
		} else {
			repository.save(paymentInstProposalReq);
		}	
		return paymentInstProposalReq;*/
		
	}

	
	public PaymentInstruction retrievePayment(String paymentId) throws Exception {
		PaymentInstruction paymentInstruction = paymentRepository.findByPaymentPaymentId(paymentId);

		return paymentInstruction;

	}
}

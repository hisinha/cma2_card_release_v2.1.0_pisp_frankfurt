package com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.constants;

public class DomesticPaymentConsentsFoundationConstants {
	
	public static final String RECORD_NOT_FOUND = "Payment Instruction Proposal Record Not Found";
	
	public static final String INSTRUCTION_ENDTOEND_REF="FRESCO.21302.GFX.10";
	
	

}

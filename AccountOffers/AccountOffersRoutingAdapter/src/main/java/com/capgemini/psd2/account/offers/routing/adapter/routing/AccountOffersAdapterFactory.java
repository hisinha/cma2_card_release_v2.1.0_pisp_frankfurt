package com.capgemini.psd2.account.offers.routing.adapter.routing;

import com.capgemini.psd2.aisp.adapter.AccountOffersAdapter;

@FunctionalInterface
public interface AccountOffersAdapterFactory {

	public AccountOffersAdapter getAdapterInstance(String coreSystemName);

}

package com.capgemini.psd2.pisp.isp.service;

import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;

public interface ISPaymentsService {

	public CustomISPaymentsPOSTResponse createInternationalScheduledPaymentsResource(
			CustomISPaymentsPOSTRequest customRequest);

	public CustomISPaymentsPOSTResponse retrieveInternationalScheduledPaymentsResource(String internationalScheduledPaymentId);

}

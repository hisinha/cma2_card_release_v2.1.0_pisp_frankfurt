package com.capgemini.psd2.pisp.isp.test.transformer.impl.Mock;

import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledResponse1;

public class ISPaymentsResponseTransformerImplMockData {

	public OBWriteDataInternationalScheduledResponse1 getOBInternationalScheduled1(){
		OBWriteDataInternationalScheduledResponse1 data = new OBWriteDataInternationalScheduledResponse1();
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		 
		  
				//Setting Creditor Agent
				OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
				OBPostalAddress6 postalAddress = new OBPostalAddress6();
				creditorAgent.setSchemeName("BICFI");
				creditorAgent.setIdentification("Testing Scheme");
				creditorAgent.setName("Testing Creditor Agent Block");
				creditorAgent.setPostalAddress(postalAddress);
						
				//Setting Creditor Account
				OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
				creditorAccount.setSchemeName("IBAN");
				creditorAccount.setIdentification("GB29NWBK60161331926819");
				creditorAccount.setName("Creditor Account");
				creditorAccount.setSecondaryIdentification("123");
				
				OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
				creditorAccount.setSchemeName("PAN");
				creditorAccount.setIdentification("123456789");
				creditorAccount.setName("Debtor Account");
				creditorAccount.setSecondaryIdentification("123");
				
				initiation.setCreditorAccount(creditorAccount);
				initiation.setDebtorAccount(debtorAccount);
				initiation.setCreditorAgent(creditorAgent);
				
				data.setInitiation(initiation);
				data.setCreationDateTime("2018-05-09T11:10:12.064Z");
				data.setStatusUpdateDateTime("2018-05-09T11:10:12.064Z");
				
				return data;
	}
	
}

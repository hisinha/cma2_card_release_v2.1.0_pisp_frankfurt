package com.capgemini.psd2.scaconsenthelper.services;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.helpers.HeaderHelper;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;


@Service
public class SCAConsentHelperService {

	@Autowired
	private PFConfig pfConfig;
	
	@Autowired
	private RestClientSync restClientSyncImpl;
	
	@Autowired
	private RequestHeaderAttributes headerAttributes;
	
	
	public String cancelJourney(String oAuthUrl,PFInstanceData pfInstanceData) {
		DropOffResponse dropOffResponse = dropOffOnCancel(pfInstanceData.getPfInstanceId(),pfInstanceData.getPfInstanceUserName(),pfInstanceData.getPfInstanceUserPwd());

		String redirectURI = UriComponentsBuilder.fromHttpUrl(oAuthUrl).queryParam(PFConstants.REF, dropOffResponse != null ? dropOffResponse.getRef() : null)
				.toUriString();

		return redirectURI;
	}
	
	public void revokeAllPreviousGrants(String intentId,String instanceUserId,String instancePwd){
		//String url = String.format(pfConfig.getGrantsrevocationURL(), intentId);
		System.out.println("tenant id in revoke previous grants test: "+headerAttributes.getTenantId());
		System.out.println("Grant Revocation URL is - "+pfConfig.getTenantSpecificGrantsRevocationUrl(headerAttributes.getTenantId()));
		String url = String.format(pfConfig.getTenantSpecificGrantsRevocationUrl(headerAttributes.getTenantId()), intentId);
		RequestInfo requestInfo = new RequestInfo();

		requestInfo.setUrl(url);

		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();

		String credentials = instanceUserId.concat(":").concat(instancePwd);

		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());

		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER, SCAConsentHelperConstants.BASIC+" ".concat(encoderString));

		httpHeaders.add("X-XSRF-Header", GenerateUniqueIdUtilities.generateRandomUniqueID());

		restClientSyncImpl.callForDelete(requestInfo, String.class, httpHeaders);
	}
	
	// Case of PF access_denied error
	private DropOffResponse dropOffOnCancel(String instanceId,String instanceUserId,String instancePwd) {

		RequestInfo requestInfo = new RequestInfo();

		//requestInfo.setUrl(pfConfig.getDropOffURL());
		System.out.println("tenant id in drop off on cancel test: "+headerAttributes.getTenantId());
		requestInfo.setUrl(pfConfig.getTenantSpecificDropOffUrl(headerAttributes.getTenantId()));

		DropOffRequest dropOffDataModel = new DropOffRequest();

		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();

		String credentials = instanceUserId.concat(":").concat(instancePwd);

		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());

		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER, SCAConsentHelperConstants.BASIC+" ".concat(encoderString));

		httpHeaders.add(PFConstants.PING_INSTANCE_ID, instanceId);

		String jsonResponse = restClientSyncImpl.callForPost(requestInfo, dropOffDataModel, String.class, httpHeaders);
		
		if(jsonResponse == null || jsonResponse.trim().length() == 0 || jsonResponse.contains("Authentication Required")){
			return null;
		}

		DropOffResponse dropOffResponse = JSONUtilities.getObjectFromJSONString(jsonResponse, DropOffResponse.class);
		return dropOffResponse;
	}	
	
	public DropOffResponse dropOffOnConsentSubmission(PickupDataModel pickupDataModel,String instanceId,String instanceUserName,String instancePwd){
		RequestInfo requestInfo = new RequestInfo();

		//requestInfo.setUrl(pfConfig.getDropOffURL());
		System.out.println("tenant id in drop off on consent submission: "+headerAttributes.getTenantId());
		requestInfo.setUrl(pfConfig.getTenantSpecificDropOffUrl(headerAttributes.getTenantId()));
		
		DropOffRequest dropOffRequest = new DropOffRequest();
		
		dropOffRequest.setSubject(pickupDataModel.getIntentId());
		
		dropOffRequest.setIntent_type(pickupDataModel.getPaymentType());
		
		dropOffRequest.setTenant_id(headerAttributes.getTenantId());
		
		dropOffRequest.setConsent_expiry(pickupDataModel.getConsentExpiryInMinutes() != null ? pickupDataModel.getConsentExpiryInMinutes().toString() : "-1");
		
		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();
		
		String credentials = instanceUserName.concat(":").concat(instancePwd);

		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());

		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER, SCAConsentHelperConstants.BASIC+" ".concat(encoderString));

		httpHeaders.add(PFConstants.PING_INSTANCE_ID, instanceId);
		
		String jsonResponse = restClientSyncImpl.callForPost(requestInfo, dropOffRequest, String.class, httpHeaders);
		
		if(jsonResponse == null || jsonResponse.trim().length() == 0 || jsonResponse.contains("Authentication Required")){
			return null;
		}		
		
		DropOffResponse dropOffResponse = JSONUtilities.getObjectFromJSONString(jsonResponse,DropOffResponse.class);
		return dropOffResponse;
	}	
}

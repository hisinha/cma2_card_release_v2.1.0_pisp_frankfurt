package com.capgemini.psd2.scaconsenthelper.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DropOffResponse {
	
	@JsonProperty("REF")
	private String ref;

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}
}

package com.capgemini.psd2.aisp.validation.adapter.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder3;
import com.capgemini.psd2.aisp.domain.OBStandingOrder3;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountStandingOrdersValidator")
@ConfigurationProperties("app")
public class AccountStandingOrdersValidatorImpl
		implements AISPCustomValidator<OBReadStandingOrder3, OBReadStandingOrder3> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Override
	public OBReadStandingOrder3 validateRequestParams(OBReadStandingOrder3 oBReadStandingOrder3) {
		return oBReadStandingOrder3;
	}

	@Override
	public boolean validateResponseParams(OBReadStandingOrder3 oBReadStandingOrder3) {
		if (resValidationEnabled)
			executeAccountScheduledPaymentSwaggerValidations(oBReadStandingOrder3);
		executeAccountResponseCustomValidations(oBReadStandingOrder3);
		return true;
	}

	// swagger validation for AccountStandingOrder
	private void executeAccountScheduledPaymentSwaggerValidations(OBReadStandingOrder3 oBReadStandingOrder3) {
		psd2Validator.validate(oBReadStandingOrder3);
	}

	// custom validation for AccountStandingOrder
	private void executeAccountResponseCustomValidations(OBReadStandingOrder3 oBReadStandingOrder3) {
		if((!NullCheckUtils.isNullOrEmpty(oBReadStandingOrder3)) && (!NullCheckUtils.isNullOrEmpty(oBReadStandingOrder3.getData()))
				&& (!NullCheckUtils.isNullOrEmpty(oBReadStandingOrder3.getData().getStandingOrder()))){
			//Validate Permission
			validatePermissionBasedResponse(oBReadStandingOrder3.getData().getStandingOrder());
			
			for (OBStandingOrder3 oBStandingOrder3 : oBReadStandingOrder3.getData().getStandingOrder()) {
				if(!NullCheckUtils.isNullOrEmpty(oBStandingOrder3)) {	
				// validate FirstPaymentDateTime
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getFirstPaymentDateTime()))
					commonAccountValidations
							.validateAndParseDateTimeFormatForResponse(oBStandingOrder3.getFirstPaymentDateTime());

				// validate NextPaymentDateTime
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getNextPaymentDateTime()))
					commonAccountValidations
							.validateAndParseDateTimeFormatForResponse(oBStandingOrder3.getNextPaymentDateTime());

				// validate FinalPaymentDateTime
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getFinalPaymentDateTime()))
					commonAccountValidations
							.validateAndParseDateTimeFormatForResponse(oBStandingOrder3.getFinalPaymentDateTime());
				
				//validate Creditor Agent
				if(!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getCreditorAgent()))
					commonAccountValidations.validateCreditorAgent(oBStandingOrder3.getCreditorAgent());

				// check CreditorAccount is null or empty
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getCreditorAccount())) {
					// validate SchemeName with Identification for CreditorAccount
					if ((!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getCreditorAccount().getSchemeName()))
							&& !NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getCreditorAccount().getIdentification()))
						commonAccountValidations.validateSchemeNameWithIdentification(
								oBStandingOrder3.getCreditorAccount().getSchemeName(),
								oBStandingOrder3.getCreditorAccount().getIdentification());

					// validate SchemeName with SecondryIdentification for CreditorAccount
					if ((!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getCreditorAccount().getSchemeName()))
							&& !NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getCreditorAccount().getSecondaryIdentification()))
						commonAccountValidations.validateSchemeNameWithSecondaryIdentification(
								oBStandingOrder3.getCreditorAccount().getSchemeName(),
								oBStandingOrder3.getCreditorAccount().getSecondaryIdentification());
				}

				// Validate FirstPaymentAmount
				validateFirstPaymentAmount(oBStandingOrder3);

				// Validate NextPaymentAmount
				validateNextPaymentAmount(oBStandingOrder3);

				// Validate FinalPaymentAmount
				validateFinalPaymentAmount(oBStandingOrder3);
			}
			}
		}
		
	}

	// Validate Amount & Currency for FirstPaymentAmount
	private void validateFirstPaymentAmount(OBStandingOrder3 oBStandingOrder3) {
		if(!NullCheckUtils.isNullOrEmpty(oBStandingOrder3)) {
			if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getFirstPaymentAmount())) {
				// validate amount
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getFirstPaymentAmount().getAmount()))
					commonAccountValidations.validateAmount(oBStandingOrder3.getFirstPaymentAmount().getAmount());
				// validate currency
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getFirstPaymentAmount().getCurrency()))
					commonAccountValidations.isValidCurrency(oBStandingOrder3.getFirstPaymentAmount().getCurrency());
			}
		}
	}

	// Validate Amount & Currency for NextPaymentAmount
	private void validateNextPaymentAmount(OBStandingOrder3 oBStandingOrder3) {
		if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getNextPaymentAmount())) {
			// validate amount
			if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getNextPaymentAmount().getAmount()))
				commonAccountValidations.validateAmount(oBStandingOrder3.getNextPaymentAmount().getAmount());
			// validate currency
			if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getNextPaymentAmount().getCurrency()))
				commonAccountValidations.isValidCurrency(oBStandingOrder3.getNextPaymentAmount().getCurrency());
		}
	}

	// Validate Amount & Currency for FinalPaymentAmount
	private void validateFinalPaymentAmount(OBStandingOrder3 oBStandingOrder3) {
		if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getFinalPaymentAmount())) {
			// validate amount
			if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getFinalPaymentAmount().getAmount()))
				commonAccountValidations.validateAmount(oBStandingOrder3.getFinalPaymentAmount().getAmount());
			// validate currency
			if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getFinalPaymentAmount().getCurrency()))
				commonAccountValidations.isValidCurrency(oBStandingOrder3.getFinalPaymentAmount().getCurrency());
		}
	}
	
	private void validatePermissionBasedResponse(List<OBStandingOrder3> standingOrderList){
		if(!NullCheckUtils.isNullOrEmpty(reqHeaderAtrributes.getClaims())){
			if((reqHeaderAtrributes.getClaims().toString().toUpperCase()).contains(OBExternalPermissions1Code.READSTANDINGORDERSDETAIL.toString().toUpperCase())){
				for(OBStandingOrder3 oBStandingOrder3 : standingOrderList){
					if(!NullCheckUtils.isNullOrEmpty(oBStandingOrder3)){
						if(NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getCreditorAccount())){
							throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
								InternalServerErrorMessage.INSUFFICIENT_DATA_FOR_DETAIL_PERMISSION));
						}
						if(!NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getStandingOrderStatusCode())&&"ACTIVE".equalsIgnoreCase(oBStandingOrder3.getStandingOrderStatusCode().toString())&&(NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getNextPaymentAmount())
							|| NullCheckUtils.isNullOrEmpty(oBStandingOrder3.getNextPaymentDateTime()))) {
								throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
												InternalServerErrorMessage.INSUFFICIENT_DATA_FOR_DETAIL_PERMISSION));
						}
					}
				}
			}
		}
	}

	@Override	
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}
}
package com.capgemini.psd2.aisp.validation.adapter.utilities;

import java.util.ArrayList;
import java.util.List;

public class AccountsConstants {
	public static final String UK_OBIE_IBAN = "UK.OBIE.IBAN";
	public static final String UK_OBIE_SORTCODEACCOUNTNUMBER = "UK.OBIE.SortCodeAccountNumber";
	public static final String UK_OBIE_PAN = "UK.OBIE.PAN";
	public static final String UK_OBIE_PAYM = "UK.OBIE.Paym";
	public static final String UK_OBIE_BBAN = "UK.OBIE.BBAN";
	public static final String UK_OBIE_ABACODEACCOUNTNUMBER="UK.OBIE.ABACodeAccountNumber";
	public static final String UK_BOI_ACCOUNTNUMBER="UK.BOI.AccountNumber";
	
	public static final String IBAN = "IBAN";
	public static final String SORTCODEACCOUNTNUMBER = "SortCodeAccountNumber";
	public static final String PAN = "PAN";
	
	public static final String  UK_OBIE_BICFI="UK.OBIE.BICFI";
	public static final String  BICFI="BICFI";
	
	protected static final List<String> schemeNameList = new ArrayList<>();
	private AccountsConstants(){}
	public static List<String> prefixToAddSchemeNameList(){
		schemeNameList.add(IBAN);
		schemeNameList.add(SORTCODEACCOUNTNUMBER);
		schemeNameList.add(PAN);	
		return schemeNameList;
	}
	
	
}

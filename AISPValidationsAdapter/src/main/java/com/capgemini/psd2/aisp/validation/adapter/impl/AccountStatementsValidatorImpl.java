package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadStatement1;
import com.capgemini.psd2.aisp.domain.OBStatement1;
import com.capgemini.psd2.aisp.domain.OBStatementAmount1;
import com.capgemini.psd2.aisp.domain.OBStatementBenefit1;
import com.capgemini.psd2.aisp.domain.OBStatementDateTime1;
import com.capgemini.psd2.aisp.domain.OBStatementFee1;
import com.capgemini.psd2.aisp.domain.OBStatementInterest1;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountStatementsValidator")
@ConfigurationProperties("app")
public class AccountStatementsValidatorImpl implements AISPCustomValidator<OBReadStatement1, OBReadStatement1> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadStatement1 validateRequestParams(OBReadStatement1 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadStatement1 obReadStatement1) {
		if (resValidationEnabled)
			executeAccountResponseSwaggerValidations(obReadStatement1);
		executeAccountResponseCustomValidations(obReadStatement1);
		return true;
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}
	
	private void executeAccountResponseSwaggerValidations(OBReadStatement1 obReadStatement1) {
		psd2Validator.validate(obReadStatement1);
		
	}

	private void executeAccountResponseCustomValidations(OBReadStatement1 obReadStatement1) {
	
		if(obReadStatement1 != null && obReadStatement1.getData() != null &&  obReadStatement1.getData().getStatement()!=null){

			for (OBStatement1 oBStatement1 : obReadStatement1.getData().getStatement()) {
				if(!NullCheckUtils.isNullOrEmpty(oBStatement1)) {	
					commonAccountValidations.validateAndParseDateTimeFormatForResponse(oBStatement1.getStartDateTime());
					commonAccountValidations.validateAndParseDateTimeFormatForResponse(oBStatement1.getEndDateTime());
					commonAccountValidations.validateAndParseDateTimeFormatForResponse(oBStatement1.getCreationDateTime());
	
					if (!NullCheckUtils.isNullOrEmpty(oBStatement1.getStatementDateTime())) {
						for (OBStatementDateTime1 obStatementDateTime1 : oBStatement1.getStatementDateTime()) {
							commonAccountValidations.validateAndParseDateTimeFormatForResponse(obStatementDateTime1.getDateTime());
						}
					}
	
					if (!NullCheckUtils.isNullOrEmpty(oBStatement1.getStatementBenefit())) {
						for (OBStatementBenefit1 oBStatementBenefit1 : oBStatement1.getStatementBenefit()) {
							if (!NullCheckUtils.isNullOrEmpty(oBStatementBenefit1.getAmount())) {
								commonAccountValidations.validateAmount(oBStatementBenefit1.getAmount().getAmount());
								commonAccountValidations.isValidCurrency(oBStatementBenefit1.getAmount().getCurrency());
							}
	
						}
	
					}
	
					if (!NullCheckUtils.isNullOrEmpty(oBStatement1.getStatementInterest())) {
						for (OBStatementInterest1 OBStatementInterest1 : oBStatement1.getStatementInterest()) {
							if (!NullCheckUtils.isNullOrEmpty(OBStatementInterest1.getAmount())) {
								commonAccountValidations.validateAmount(OBStatementInterest1.getAmount().getAmount());
								commonAccountValidations.isValidCurrency(OBStatementInterest1.getAmount().getCurrency());
							}
	
						}
					}
	
					if (!NullCheckUtils.isNullOrEmpty(oBStatement1.getStatementFee())) {
						for (OBStatementFee1 obtatementFee1 : oBStatement1.getStatementFee()) {
							if (!NullCheckUtils.isNullOrEmpty(obtatementFee1.getAmount())) {
								commonAccountValidations.validateAmount(obtatementFee1.getAmount().getAmount());
								commonAccountValidations.isValidCurrency(obtatementFee1.getAmount().getCurrency());
							}
	
						}
	
					}
	
					if (!NullCheckUtils.isNullOrEmpty(oBStatement1.getStatementAmount())) {
						for (OBStatementAmount1 obStatementAmount1 : oBStatement1.getStatementAmount()) {
							if (!NullCheckUtils.isNullOrEmpty(obStatementAmount1.getAmount())) {
								commonAccountValidations.validateAmount(obStatementAmount1.getAmount().getAmount());
								commonAccountValidations.isValidCurrency(obStatementAmount1.getAmount().getCurrency());
							}
	
						}
	
					}
				}

			}

			if (!NullCheckUtils.isNullOrEmpty(obReadStatement1.getMeta())
					&& !NullCheckUtils.isNullOrEmpty(obReadStatement1.getMeta().getFirstAvailableDateTime())) {
				commonAccountValidations.validateAndParseDateTimeFormatForResponse(
						obReadStatement1.getMeta().getFirstAvailableDateTime());
			}

			if (!NullCheckUtils.isNullOrEmpty(obReadStatement1.getMeta())
					&& !NullCheckUtils.isNullOrEmpty(obReadStatement1.getMeta().getLastAvailableDateTime())) {
				commonAccountValidations.validateAndParseDateTimeFormatForResponse(
						obReadStatement1.getMeta().getLastAvailableDateTime());
			}

		}

	}

}
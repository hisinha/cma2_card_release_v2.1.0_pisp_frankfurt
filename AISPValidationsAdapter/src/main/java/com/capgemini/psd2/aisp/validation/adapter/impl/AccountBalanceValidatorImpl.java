package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBCashBalance1;
import com.capgemini.psd2.aisp.domain.OBCreditLine1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountBalanceValidator")
@ConfigurationProperties("app")
public class AccountBalanceValidatorImpl implements AISPCustomValidator<OBReadBalance1, OBReadBalance1> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadBalance1 validateRequestParams(OBReadBalance1 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadBalance1 oBReadBalance1) {
		if (resValidationEnabled)
			executeAccountBalanceResponseSwaggerValidations(oBReadBalance1);
		executeAccountBalanceResponseCustomValidations(oBReadBalance1);
		return true;
	}

	private void executeAccountBalanceResponseSwaggerValidations(OBReadBalance1 oBReadBalance1) {
		psd2Validator.validate(oBReadBalance1);
	}

	private void executeAccountBalanceResponseCustomValidations(OBReadBalance1 obReadBalance1) {
		if (!NullCheckUtils.isNullOrEmpty(obReadBalance1) && !NullCheckUtils.isNullOrEmpty(obReadBalance1.getData())) {
			for (OBCashBalance1 obCashBalance1 : obReadBalance1.getData().getBalance()) {
				if (!NullCheckUtils.isNullOrEmpty(obCashBalance1)) {
					if (!NullCheckUtils.isNullOrEmpty(obCashBalance1.getDateTime())) {
						commonAccountValidations.validateAndParseDateTimeFormatForResponse(obCashBalance1.getDateTime());
					}
					if (!NullCheckUtils.isNullOrEmpty(obCashBalance1.getAmount())) {
						if (!NullCheckUtils.isNullOrEmpty(obCashBalance1.getAmount().getAmount())) {
							commonAccountValidations.validateAmount(obCashBalance1.getAmount().getAmount());
						}
						if (!NullCheckUtils.isNullOrEmpty(obCashBalance1.getAmount().getCurrency())) {
							commonAccountValidations.isValidCurrency(obCashBalance1.getAmount().getCurrency());
						}
					}
					if(!NullCheckUtils.isNullOrEmpty(obCashBalance1.getCreditLine())) {
						for (OBCreditLine1 obCreditLine1 : obCashBalance1.getCreditLine()) {
							if(NullCheckUtils.isNullOrEmpty(obCreditLine1.isIncluded()))
							{
								throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
										InternalServerErrorMessage.INVALID_VALUE_INCLUDE));
							}
							if(!NullCheckUtils.isNullOrEmpty(obCreditLine1)) {
								if (!NullCheckUtils.isNullOrEmpty(obCreditLine1.getAmount())) {
									if (!NullCheckUtils.isNullOrEmpty(obCreditLine1.getAmount().getAmount())) {
										commonAccountValidations.validateAmount(obCreditLine1.getAmount().getAmount());
									}
									if (!NullCheckUtils.isNullOrEmpty(obCreditLine1.getAmount().getCurrency())) {
										commonAccountValidations.isValidCurrency(obCreditLine1.getAmount().getCurrency());
									}
								}
							}
						}
						
					}
				}
			}
		}
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED, ErrorMapKeys.NO_ACCOUNT_ID_FOUND));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}

}
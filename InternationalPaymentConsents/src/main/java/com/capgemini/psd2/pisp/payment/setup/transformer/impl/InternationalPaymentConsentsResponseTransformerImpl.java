package com.capgemini.psd2.pisp.payment.setup.transformer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsTransformer;
import com.capgemini.psd2.pisp.status.PaymentStatusCompatibilityMapEnum;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("internationalPaymentConsentTransformer")
public class InternationalPaymentConsentsResponseTransformerImpl implements PaymentConsentsTransformer<CustomIPaymentConsentsPOSTResponse, CustomIPaymentConsentsPOSTRequest> {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Autowired
	private PispDateUtility pispDateUtility;

	@Override
	public CustomIPaymentConsentsPOSTResponse paymentConsentsResponseTransformer(
			CustomIPaymentConsentsPOSTResponse internationalConsentsTppResponse,
			PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType) {

		/*
		 * These fields are expected to already be populated in
		 * internationalSetupTppResponse from International Stage Service : Charges,
		 * CutOffDateTime, ExpectedExecutionDateTime, ExpectedSettlementDateTime
		 */

		/* Fields from platform Resource */
		internationalConsentsTppResponse.getData().setCreationDateTime(paymentSetupPlatformResource.getCreatedAt());
		String setupCompatibleStatus = PaymentStatusCompatibilityMapEnum
				.valueOf(paymentSetupPlatformResource.getStatus().toUpperCase()).getCompatibleStatus();
		internationalConsentsTppResponse.getData().setStatus(OBExternalConsentStatus1Code.fromValue(setupCompatibleStatus));
		internationalConsentsTppResponse.getData().setStatusUpdateDateTime(
				NullCheckUtils.isNullOrEmpty(paymentSetupPlatformResource.getStatusUpdateDateTime())
						? paymentSetupPlatformResource.getUpdatedAt()
						: paymentSetupPlatformResource.getStatusUpdateDateTime());

		/*
		 * If debtorFlag is 'False' then DebtorAccount & DebtorAgent details do
		 * not have to send in response. debtorFlag would be set as Null
		 * DebtorAgent is not available in CMA3
		 */
		String creditorAccountScheme=internationalConsentsTppResponse.getData().getInitiation().getCreditorAccount().getSchemeName();
		if(PaymentSetupConstants.prefixToAddSchemeNameList().contains(creditorAccountScheme)) {
			internationalConsentsTppResponse.getData().getInitiation().getCreditorAccount().setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(creditorAccountScheme));
		}
		
		String debtorAccountScheme= internationalConsentsTppResponse.getData().getInitiation().getDebtorAccount()!=null ?
				internationalConsentsTppResponse.getData().getInitiation().getDebtorAccount().getSchemeName():null;
		
		if(debtorAccountScheme!=null && PaymentSetupConstants.prefixToAddSchemeNameList().contains(debtorAccountScheme)) {
			internationalConsentsTppResponse.getData().getInitiation().getDebtorAccount().setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(debtorAccountScheme));
		}
		
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails()))
			internationalConsentsTppResponse.getData().getInitiation().setDebtorAccount(null);

		/*
		 * If tppDebtorNameFlag is 'False' then DebtorAccount.name should not be
		 * sent to TPP.
		 */
		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& !Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
			internationalConsentsTppResponse.getData().getInitiation().getDebtorAccount().setName(null);

		if (internationalConsentsTppResponse.getLinks() == null)
			internationalConsentsTppResponse.setLinks(new PaymentSetupPOSTResponseLinks());
		
		internationalConsentsTppResponse.getLinks().setSelf(PispUtilities.populateLinks(
				paymentSetupPlatformResource.getPaymentConsentId(), methodType, reqHeaderAtrributes.getSelfUrl()));
		
		if (internationalConsentsTppResponse.getMeta() == null)
			internationalConsentsTppResponse.setMeta(new PaymentSetupPOSTResponseMeta());

		return internationalConsentsTppResponse;
	}

	@Override
	public CustomIPaymentConsentsPOSTRequest paymentConsentRequestTransformer(
			CustomIPaymentConsentsPOSTRequest paymentConsentRequest) {
		if (!NullCheckUtils.isNullOrEmpty(paymentConsentRequest.getData().getAuthorisation()) && !NullCheckUtils
				.isNullOrEmpty(paymentConsentRequest.getData().getAuthorisation().getCompletionDateTime())) {
			paymentConsentRequest.getData().getAuthorisation()
					.setCompletionDateTime((pispDateUtility.transformDateTimeInRequest(
							paymentConsentRequest.getData().getAuthorisation().getCompletionDateTime())));
		}
		return paymentConsentRequest;
	}

}
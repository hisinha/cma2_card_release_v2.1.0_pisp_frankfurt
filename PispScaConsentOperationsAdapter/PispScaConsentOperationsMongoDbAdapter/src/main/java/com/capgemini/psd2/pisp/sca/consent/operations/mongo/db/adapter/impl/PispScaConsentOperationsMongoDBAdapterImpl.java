package com.capgemini.psd2.pisp.sca.consent.operations.mongo.db.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.operations.transformer.PaymentStageDataTransformer;
import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticScheduledPaymentStagingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalScheduledPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;

@Conditional(MongoDbMockCondition.class)
@Component("pispStageMongoDBAdapter")
@DependsOn({ "dPaymentConsentsStagingMongoDbAdapter", "dPaymentStageDataTransformer",
		"iPaymentConsentsStagingMongoDbAdapter", "iPaymentStageDataTransformer",
		"dStandingOrderConsentsStagingMongoDbAdapter", "dStandingOrderStageDataTransformer",
		"dScheduledPaymentConsentsStagingMongoDbAdapter", "dsPaymentStageDataTransformer",
		"isPaymentStageDataTransformer" }) // list
// of // beans
public class PispScaConsentOperationsMongoDBAdapterImpl implements PispScaConsentOperationsAdapter {

	@Autowired
	@Qualifier("dPaymentConsentsStagingMongoDbAdapter")
	private DomesticPaymentStagingAdapter dPaymentStagingAdapter;

	@Autowired
	@Qualifier("iPaymentConsentsStagingMongoDbAdapter")
	private InternationalPaymentStagingAdapter iPaymentStagingAdapter;

	@Autowired
	@Qualifier("dStandingOrderConsentsStagingMongoDbAdapter")
	private DomesticStandingOrdersPaymentStagingAdapter dsoStagingAdapter;

	@Autowired
	@Qualifier("dScheduledPaymentConsentsStagingMongoDbAdapter")
	private DomesticScheduledPaymentStagingAdapter dsStagingAdapter;

	@Autowired
	@Qualifier("iScheduledPaymentConsentsStagingMongoDbAdapter")
	private InternationalScheduledPaymentStagingAdapter isStagingAdapter;

	@Autowired
	@Qualifier("filePaymentConsentsStagingMongoDbAdapter")
	private FilePaymentConsentsAdapter fileStagingAdapter;

	@Autowired
	@Qualifier("dPaymentStageDataTransformer")
	private PaymentStageDataTransformer<CustomDPaymentConsentsPOSTResponse> dPaymentStageDataTransformer;

	@Autowired
	@Qualifier("iPaymentStageDataTransformer")
	private PaymentStageDataTransformer<CustomIPaymentConsentsPOSTResponse> iPaymentStageDataTransformer;

	@Autowired
	@Qualifier("dStandingOrderStageDataTransformer")
	private PaymentStageDataTransformer<CustomDStandingOrderConsentsPOSTResponse> dsoPaymentStageDataTransformer;

	@Autowired
	@Qualifier("dsPaymentStageDataTransformer")
	private PaymentStageDataTransformer<CustomDSPConsentsPOSTResponse> dsPaymentStageDataTransformer;

	@Autowired
	@Qualifier("isPaymentStageDataTransformer")
	private PaymentStageDataTransformer<CustomISPConsentsPOSTResponse> isPaymentStageDataTransformer;

	@Autowired
	@Qualifier("filePaymentStageDataTransformer")
	private PaymentStageDataTransformer<CustomFilePaymentConsentsPOSTResponse> filePaymentStageDataTransformer;

	@Override
	public CustomConsentAppViewData retrieveConsentAppStagedViewData(CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {

		CustomConsentAppViewData customConsentAppSetupData = null;
		if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_PAY) {
			CustomDPaymentConsentsPOSTResponse domesticStageResponse = dPaymentStagingAdapter
					.retrieveStagedDomesticPaymentConsents(stageIdentifiers, params);
			customConsentAppSetupData = dPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(domesticStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_PAY) {
			CustomIPaymentConsentsPOSTResponse domesticStageResponse = iPaymentStagingAdapter
					.retrieveStagedInternationalPaymentConsents(stageIdentifiers, params);
			customConsentAppSetupData = iPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(domesticStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_ST_ORD) {
			CustomDStandingOrderConsentsPOSTResponse domesticStandingOrderStageResponse = dsoStagingAdapter
					.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, params);
			customConsentAppSetupData = dsoPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(domesticStandingOrderStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_SCH_PAY) {
			CustomDSPConsentsPOSTResponse domesticStageResponse = dsStagingAdapter
					.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, params);
			customConsentAppSetupData = dsPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(domesticStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_SCH_PAY) {
			CustomISPConsentsPOSTResponse internationalStageResponse = isStagingAdapter
					.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, params);
			customConsentAppSetupData = isPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(internationalStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.FILE_PAY) {
			CustomFilePaymentConsentsPOSTResponse fileStageResponse = fileStagingAdapter
					.fetchFilePaymentMetadata(stageIdentifiers, params);
			customConsentAppSetupData = filePaymentStageDataTransformer
					.transformDStageToConsentAppViewData(fileStageResponse);
		}
		return customConsentAppSetupData;
	}

	@Override
	public CustomFraudSystemPaymentData retrieveFraudSystemPaymentStagedData(
			CustomPaymentStageIdentifiers stageIdentifiers, Map<String, String> params) {

		CustomFraudSystemPaymentData customFraudSystemPaymentData = null;
		if (stageIdentifiers.getPaymentTypeEnum() == (PaymentTypeEnum.DOMESTIC_PAY)) {
			CustomDPaymentConsentsPOSTResponse domesticStageResponse = dPaymentStagingAdapter
					.retrieveStagedDomesticPaymentConsents(stageIdentifiers, params);
			customFraudSystemPaymentData = dPaymentStageDataTransformer
					.transformDStageToFraudData(domesticStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == (PaymentTypeEnum.INTERNATIONAL_PAY)) {
			CustomIPaymentConsentsPOSTResponse internationalStageResponse = iPaymentStagingAdapter
					.retrieveStagedInternationalPaymentConsents(stageIdentifiers, params);
			customFraudSystemPaymentData = iPaymentStageDataTransformer
					.transformDStageToFraudData(internationalStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == (PaymentTypeEnum.DOMESTIC_ST_ORD)) {
			CustomDStandingOrderConsentsPOSTResponse domesticStandingOrderStageResponse = dsoStagingAdapter
					.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, params);
			customFraudSystemPaymentData = dsoPaymentStageDataTransformer
					.transformDStageToFraudData(domesticStandingOrderStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_SCH_PAY) {
			CustomDSPConsentsPOSTResponse domesticStageResponse = dsStagingAdapter
					.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, params);
			customFraudSystemPaymentData = dsPaymentStageDataTransformer
					.transformDStageToFraudData(domesticStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.FILE_PAY) {
			CustomFilePaymentConsentsPOSTResponse fileStageResponse = fileStagingAdapter
					.fetchFilePaymentMetadata(stageIdentifiers, params);
			customFraudSystemPaymentData = filePaymentStageDataTransformer
					.transformDStageToFraudData(fileStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_SCH_PAY) {
			CustomISPConsentsPOSTResponse internationalStageResponse = isStagingAdapter
					.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, params);
			customFraudSystemPaymentData = isPaymentStageDataTransformer
					.transformDStageToFraudData(internationalStageResponse);
		}

		return customFraudSystemPaymentData;
	}

	@Override
	public PaymentConsentsValidationResponse validatePreAuthorisation(OBCashAccountDebtor3 selectedDebtorDetails,
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo, CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {
		PaymentConsentsValidationResponse response = new PaymentConsentsValidationResponse();
		String currency = null;
		if (stageIdentifiers.getPaymentTypeEnum() == (PaymentTypeEnum.DOMESTIC_PAY)) {
			CustomDPaymentConsentsPOSTResponse domesticStageResponse = dPaymentStagingAdapter
					.retrieveStagedDomesticPaymentConsents(stageIdentifiers, params);
			currency = domesticStageResponse.getData().getInitiation().getInstructedAmount().getCurrency();
		} else if (stageIdentifiers.getPaymentTypeEnum() == (PaymentTypeEnum.INTERNATIONAL_PAY)) {
			CustomIPaymentConsentsPOSTResponse internationalStageResponse = iPaymentStagingAdapter
					.retrieveStagedInternationalPaymentConsents(stageIdentifiers, params);
			currency = internationalStageResponse.getData().getInitiation().getInstructedAmount().getCurrency();
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_ST_ORD) {
			CustomDStandingOrderConsentsPOSTResponse domesticStandingOrderStageResponse = dsoStagingAdapter
					.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, params);
			currency = domesticStandingOrderStageResponse.getData().getInitiation().getFirstPaymentAmount()
					.getCurrency();
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_SCH_PAY) {
			CustomDSPConsentsPOSTResponse dspStageResponse = dsStagingAdapter
					.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, params);
			currency = dspStageResponse.getData().getInitiation().getInstructedAmount().getCurrency();

		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.FILE_PAY) {
			//changes once we get from FS
			currency = "GBP";
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_SCH_PAY) {
			CustomISPConsentsPOSTResponse ispStageResponse = isStagingAdapter
					.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, params);
			currency = ispStageResponse.getData().getInitiation().getInstructedAmount().getCurrency();
		}
		if (currency.equals("INR")) {
			response.setPaymentSetupValidationStatus(OBTransactionIndividualStatus1Code.REJECTED);
		} else {
			response.setPaymentSetupValidationStatus(OBTransactionIndividualStatus1Code.PENDING);
		}
		return response;
	}

	@Override
	public void updatePaymentStageData(CustomPaymentStageIdentifiers stageIdentifiers,
			CustomPaymentStageUpdateData updateData, Map<String, String> params) {
		if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_PAY) {
			if (stageIdentifiers.getPaymentSetupVersion().equals(PaymentConstants.CMA_FIRST_VERSION)) {

				/*
				 * Fix Scheme name validation failed. In MongoDB producing
				 * exception: can't have. In field names [UK.OBIE.IBAN] while
				 * creating test data. Hence, in mock data, we used "_" instead
				 * of "." character Replacing "_" to "." to convert valid Scheme
				 * Name into mongo db adapter only. there is no impact on CMA
				 * api flow. Its specifc to Sandbox functionality
				 */
				if (updateData.getDebtorDetails() != null && updateData.getDebtorDetails().getSchemeName() != null) {
					String debtorSchemeName = updateData.getDebtorDetails().getSchemeName().toUpperCase();
					updateData.getDebtorDetails().setSchemeName(debtorSchemeName.replace('_', '.'));
				}
				dPaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_PAY) {
				iPaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_ST_ORD) {
				dsoPaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_SCH_PAY) {
				dsPaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_SCH_PAY) {
				isPaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.FILE_PAY) {
				filePaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			}
		}
	}
}

package com.capgemini.psd2.security.consent.test.service;

import static org.junit.Assert.assertNotNull;

import org.json.simple.parser.ParseException;
import org.junit.Test;

import com.capgemini.psd2.security.consent.service.ConsentPickupDataService;

public class ConsentPickupDataServiceTest {
	
	private ConsentPickupDataService consentPickupDataService = new ConsentPickupDataService();
	
	@Test
	public void testPopulateIntentData() throws ParseException{
		String jsonResponse = "{\"claims\":\"SETUP\",\"scope\":\"openid\",\"username\":\"demo\",\"channel_id\":\"APIChannel\",\"client_id\":\"6443e15975554bce8099e35b88b40465\",\"correlationId\":\"12345\"}";
		assertNotNull(consentPickupDataService.populateIntentData(jsonResponse));
	}
	
}

package com.capgemini.psd2.account.standingorder.mongo.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadStandingOrder3;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder3Data;
import com.capgemini.psd2.aisp.domain.OBStandingOrder3;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStandingOrderCMA2;
import com.capgemini.psd2.logger.LoggerAttribute;

public class AccountStandingOrderMockData {

	 public static List<AccountStandingOrderCMA2> getMockAccountDetails()
	 {
		 return new ArrayList<AccountStandingOrderCMA2>();
	 }
 
	 public static LoggerAttribute getMockLoggerData() {
			LoggerAttribute x = new LoggerAttribute();
			x.setApiId("testApiID");
			return x;
		}
	 
	 public static OBReadStandingOrder3 getMockExpectedStandingOrderResponse()
	 {
		 OBReadStandingOrder3Data obReadStandingOrder2Data = new OBReadStandingOrder3Data();
		 OBReadStandingOrder3 obReadStandingOrder2 = new OBReadStandingOrder3();
		 AccountStandingOrderCMA2 mockStandingOrder = new AccountStandingOrderCMA2();
		 
		 mockStandingOrder.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		 
		 List<OBStandingOrder3> standingOrderList = new ArrayList<>();
		 standingOrderList.add(mockStandingOrder);
		 obReadStandingOrder2Data.setStandingOrder(standingOrderList);
		 obReadStandingOrder2.setData(obReadStandingOrder2Data);
		 return obReadStandingOrder2;
	 }
}

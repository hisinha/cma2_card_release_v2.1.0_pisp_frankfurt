package com.capgemini.psd2.account.standingorder.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.standingorder.service.AccountStandingOrderService;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder3;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStandingOrdersResponse;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@Service
public class AccountStandingOrderServiceImpl implements AccountStandingOrderService {

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account standing order adapter. */
	@Autowired
	@Qualifier("accountStandingOrderRoutingAdapter")
	private AccountStandingOrdersAdapter accountStandingOrderAdapter;

	/** The account mapping adapter. */
	@Autowired
	private AccountMappingAdapter accountMappingAdapter;

	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;
	
	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator accountsCustomValidator;

	@SuppressWarnings("unchecked")
	@Override
	public OBReadStandingOrder3 retrieveAccountStandingOrders(String accountId) {
		//validate accountId
		accountsCustomValidator.validateUniqueId(accountId);
		
		// Get consents by accountId and validate.
		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);

		Map<String,String> params=reqHeaderAtrributes.getToken().getSeviceParams();
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		
		// Retrieve account standing-orders.
		PlatformAccountStandingOrdersResponse platformAccountStandingOrdersResponse = accountStandingOrderAdapter
				.retrieveAccountStandingOrders(accountMapping, params);

		if (platformAccountStandingOrdersResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		// Get CMA2 standing-orders response.
		OBReadStandingOrder3 tppStandingOrdersResponse = platformAccountStandingOrdersResponse
				.getoBReadStandingOrder3();

		if (tppStandingOrdersResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		// update platform response.
		updatePlatformResponse(tppStandingOrdersResponse);

		// Apply platform and CMA2-swagger validations.
		accountsCustomValidator.validateResponseParams(tppStandingOrdersResponse);

		return tppStandingOrdersResponse;
	}

	private void updatePlatformResponse(OBReadStandingOrder3 tppStandingOrdersResponse) {
		if (tppStandingOrdersResponse.getLinks() == null)
			tppStandingOrdersResponse.setLinks(new Links());
		tppStandingOrdersResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());

		if (tppStandingOrdersResponse.getMeta() == null)
			tppStandingOrdersResponse.setMeta(new Meta());
		tppStandingOrdersResponse.getMeta().setTotalPages(1);
	}

}

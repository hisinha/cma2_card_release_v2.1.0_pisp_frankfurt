package com.capgemini.psd2.mock.foundationservice.account.transactions;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.Accnt;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.BalanceType;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.GroupByDate;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.Transaction;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.TransactionStatus;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.TransactionType;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.Transactions;
import com.capgemini.psd2.mock.foundationservice.account.transactions.repository.AccountTransactionsRepository;

@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })
public class AccountTransactionsMockFoundationServiceApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(AccountTransactionsMockFoundationServiceApplication.class, args);
	}
	
	@Autowired
	private AccountTransactionsRepository accountTransactionsRepository;
	 
	@Override
	public void run(String... arg0) throws Exception {
	 
		Accnt accnt = new Accnt();
		Transactions transactions = new Transactions();
		
		GroupByDate groupByDate1 = new GroupByDate();
		Transaction transaction11= new Transaction();
		transaction11.setTransactionType(TransactionType.CR);
		transaction11.setTransactionStatus(TransactionStatus.BOOKED);
		transaction11.setValue(new BigDecimal(60.01));
		transaction11.setTransactionTimestamp(getXMLGregorianCalendarDate("2017-05-12 08:05:46","yyyy-MM-dd HH:mm:ss"));
		transaction11.setCurrency("EUR");
		transaction11.setNarrative1("BDD60.01");
		groupByDate1.setBalanceType(BalanceType.POSTED);
		groupByDate1.setCurrency("EUR");
		groupByDate1.setDatePosted(getXMLGregorianCalendarDate("2017-04-16","yyyy-MM-dd"));
		groupByDate1.setEodBalance(new BigDecimal(2.01));
		groupByDate1.getTransaction().add(transaction11);
		
		GroupByDate groupByDate2 = new GroupByDate();
		Transaction transaction21= new Transaction();
		transaction21.setTransactionType(TransactionType.CR);
		transaction21.setTransactionStatus(TransactionStatus.BOOKED);
		transaction21.setValue(new BigDecimal(60.02));
		transaction21.setTransactionTimestamp(getXMLGregorianCalendarDate("2017-05-12 08:05:46","yyyy-MM-dd HH:mm:ss"));
		transaction21.setCurrency("EUR");
		transaction21.setNarrative1("BDD60.02");
		groupByDate2.setBalanceType(BalanceType.POSTED);
		groupByDate2.setCurrency("EUR");
		groupByDate2.setDatePosted(getXMLGregorianCalendarDate("2017-04-15","yyyy-MM-dd"));
		groupByDate2.setEodBalance(new BigDecimal(2.01));
		groupByDate2.getTransaction().add(transaction21);
		
		GroupByDate groupByDate3 = new GroupByDate();
		Transaction transaction31= new Transaction();
		Transaction transaction32= new Transaction();
		transaction31.setTransactionType(TransactionType.CR);
		transaction31.setTransactionStatus(TransactionStatus.BOOKED);
		transaction31.setValue(new BigDecimal(60.04));
		transaction31.setTransactionTimestamp(getXMLGregorianCalendarDate("2017-05-12 08:05:46","yyyy-MM-dd HH:mm:ss"));
		transaction31.setCurrency("EUR");
		transaction31.setNarrative1("BDD60.04");
		transaction32.setTransactionType(TransactionType.CR);
		transaction32.setTransactionStatus(TransactionStatus.BOOKED);
		transaction32.setValue(new BigDecimal(60.03));
		transaction32.setTransactionTimestamp(getXMLGregorianCalendarDate("2017-05-12 08:05:46","yyyy-MM-dd HH:mm:ss"));
		transaction32.setCurrency("EUR");
		transaction32.setNarrative1("BDD60.03");
		groupByDate3.setBalanceType(BalanceType.POSTED);
		groupByDate3.setCurrency("EUR");
		groupByDate3.setDatePosted(getXMLGregorianCalendarDate("2017-04-14","yyyy-MM-dd"));
		groupByDate3.setEodBalance(new BigDecimal(2.01));
		groupByDate3.getTransaction().add(transaction31);
		groupByDate3.getTransaction().add(transaction32);
		
		GroupByDate groupByDate4 = new GroupByDate();
		Transaction transaction41= new Transaction();
		transaction41.setTransactionType(TransactionType.CR);
		transaction41.setTransactionStatus(TransactionStatus.BOOKED);
		transaction41.setValue(new BigDecimal(60.01));
		transaction41.setTransactionTimestamp(getXMLGregorianCalendarDate("2017-05-12 08:05:46","yyyy-MM-dd HH:mm:ss"));
		transaction41.setCurrency("EUR");
		transaction41.setNarrative1("BDD60.05");
		groupByDate4.setBalanceType(BalanceType.POSTED);
		groupByDate4.setCurrency("EUR");
		groupByDate4.setDatePosted(getXMLGregorianCalendarDate("2017-04-13","yyyy-MM-dd"));
		groupByDate4.setEodBalance(new BigDecimal(2.01));
		groupByDate4.getTransaction().add(transaction41);
		
		GroupByDate groupByDate5 = new GroupByDate();
		Transaction transaction51= new Transaction();
		transaction51.setTransactionType(TransactionType.CR);
		transaction51.setTransactionStatus(TransactionStatus.BOOKED);
		transaction51.setValue(new BigDecimal(60.08));
		transaction51.setTransactionTimestamp(getXMLGregorianCalendarDate("2017-05-25 09:00:00","yyyy-MM-dd HH:mm:ss"));
		transaction51.setCurrency("EUR");
		transaction51.setNarrative1("BDD60.08");
		groupByDate5.setBalanceType(BalanceType.POSTED);
		groupByDate5.setCurrency("EUR");
		groupByDate5.setDatePosted(getXMLGregorianCalendarDate("2017-04-12","yyyy-MM-dd"));
		groupByDate5.setEodBalance(new BigDecimal(3.00));
		groupByDate5.getTransaction().add(transaction51);
		
		transactions.getGroupByDate().add(groupByDate1);
		transactions.getGroupByDate().add(groupByDate2);
		transactions.getGroupByDate().add(groupByDate3);
		transactions.getGroupByDate().add(groupByDate4);
		transactions.getGroupByDate().add(groupByDate5);
		transactions.setHasMoreTxns(true);
		transactions.setPageNumber(1);
		transactions.setPageSize(6);
		transactions.setTxnRetrievalKey("0-170418");
		
		accnt.setAccountNumber("23682876");
		accnt.setNsc("901538");
		accnt.setTransactions(transactions);
		
		//accountTransactionsRepository.save(accnt);
	}
	
	private XMLGregorianCalendar getXMLGregorianCalendarDate(String dateInStr, String dateFormat) {
		XMLGregorianCalendar xmlCalender=null;
	    GregorianCalendar calender = new GregorianCalendar();
	    Date date=null;
	    try {
			date = new SimpleDateFormat(dateFormat).parse(dateInStr);
		} catch (ParseException e) {
		} 
	    calender.setTime(date);
	    try {
			xmlCalender = DatatypeFactory.newInstance().newXMLGregorianCalendar(calender);
		} catch (DatatypeConfigurationException e) {
		}
	    return xmlCalender;
	}
	
}

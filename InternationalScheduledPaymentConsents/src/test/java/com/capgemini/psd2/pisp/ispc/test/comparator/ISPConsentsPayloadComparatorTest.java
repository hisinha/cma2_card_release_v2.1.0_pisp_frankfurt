package com.capgemini.psd2.pisp.ispc.test.comparator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBPartyIdentification43;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.ispc.comparator.ISPConsentsPayloadComparator;

@RunWith(SpringJUnit4ClassRunner.class)
public class ISPConsentsPayloadComparatorTest {

	@InjectMocks
	private ISPConsentsPayloadComparator comparator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCompareValueEquals0() {
		OBWriteInternationalScheduledConsentResponse1 response = new OBWriteInternationalScheduledConsentResponse1();
		OBWriteInternationalScheduledConsentResponse1 adaptedScheduledPaymentConsentsResponse = new OBWriteInternationalScheduledConsentResponse1();

		OBWriteDataInternationalScheduledConsentResponse1 data = new OBWriteDataInternationalScheduledConsentResponse1();
		response.setData(data);
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("823");
		response.getData().setAuthorisation(new OBAuthorisation1());
		response.setRisk(new OBRisk1());


		adaptedScheduledPaymentConsentsResponse.setData(data);
		data.setInitiation(initiation);
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("823");
		adaptedScheduledPaymentConsentsResponse.getData().setAuthorisation(new OBAuthorisation1());
		adaptedScheduledPaymentConsentsResponse.setRisk(new OBRisk1());

		int i = comparator.compare(response, adaptedScheduledPaymentConsentsResponse);
		assertEquals(0, i);
	}

	@Test
	public void testCompareValueEquals1() {
		OBWriteInternationalScheduledConsentResponse1 response = new OBWriteInternationalScheduledConsentResponse1();
		OBWriteInternationalScheduledConsentResponse1 adaptedScheduledPaymentConsentsResponse = new OBWriteInternationalScheduledConsentResponse1();


		OBWriteDataInternationalScheduledConsentResponse1 data = new OBWriteDataInternationalScheduledConsentResponse1();
		response.setData(data);
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("823");
		response.getData().getInitiation().setLocalInstrument("LI");
		response.getData().setAuthorisation(new OBAuthorisation1());
		response.setRisk(new OBRisk1());

		adaptedScheduledPaymentConsentsResponse.setData(data);
		data.setInitiation(initiation);
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("823");
		adaptedScheduledPaymentConsentsResponse.getData().setAuthorisation(new OBAuthorisation1());
		adaptedScheduledPaymentConsentsResponse.getData().getInitiation().setLocalInstrument("LI");
		adaptedScheduledPaymentConsentsResponse.setRisk(new OBRisk1());

		int i = comparator.compare(response, adaptedScheduledPaymentConsentsResponse);
		assertEquals(0, i);
	}

	@Test
	public void testCompareValueEquals1_AuthMisMatch() {
		OBWriteInternationalScheduledConsentResponse1 response = new OBWriteInternationalScheduledConsentResponse1();
		OBWriteInternationalScheduledConsentResponse1 adaptedScheduledPaymentConsentsResponse = new OBWriteInternationalScheduledConsentResponse1();

		OBWriteDataInternationalScheduledConsentResponse1 data = new OBWriteDataInternationalScheduledConsentResponse1();
		response.setData(data);
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		data.setInitiation(initiation );
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		initiation.setInstructedAmount(instructedAmount );
		instructedAmount.setAmount("823");
		response.getData().setAuthorisation(new OBAuthorisation1());
		response.getData().getAuthorisation().setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		response.setRisk(new OBRisk1());


		adaptedScheduledPaymentConsentsResponse.setData(data);
		data.setInitiation(initiation);
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("823");
		adaptedScheduledPaymentConsentsResponse.getData().setAuthorisation(new OBAuthorisation1());
		adaptedScheduledPaymentConsentsResponse.getData().getAuthorisation().setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		adaptedScheduledPaymentConsentsResponse.setRisk(new OBRisk1());

		int i = comparator.compare(response, adaptedScheduledPaymentConsentsResponse);
		assertEquals(0, i);
	}

	@Test
	public void testCompareScheduledPaymentDetails_TppDetails() {
		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		CustomISPConsentsPOSTResponse adaptedScheduledPaymentConsentsResponse = new CustomISPConsentsPOSTResponse();
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();

		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("145.23");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());

		adaptedScheduledPaymentConsentsResponse.setData(response.getData());
		response.getData().setAuthorisation(new OBAuthorisation1());
		response.getData().getAuthorisation().setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		adaptedScheduledPaymentConsentsResponse.getData().getAuthorisation().setAuthorisationType(null);

		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		response.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());

		paymentSetupPlatformResource.setTppDebtorDetails("true");
		comparator.compareScheduledPaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void testCompareScheduledPaymentDetails_TppDetailsFalse() {
		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setBuildingNumber("14");
		deliveryAddress.setCountrySubDivision("Division");
		risk.setDeliveryAddress(deliveryAddress);
		response.setRisk(risk);
		request.setRisk(risk);
		
		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("145");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().setCreditor(new OBPartyIdentification43());
		response.getData().getInitiation().getCreditor().setPostalAddress(new OBPostalAddress6());
		response.getData().getInitiation().getCreditor().getPostalAddress().setAddressLine(null);

		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("145");
		request.getData().getInitiation().setCreditor(new OBPartyIdentification43());
		request.getData().getInitiation().getCreditor().setPostalAddress(new OBPostalAddress6());
		request.getData().getInitiation().getCreditor().getPostalAddress().setAddressLine(addressLine);
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		request.getData().getInitiation().getRemittanceInformation().setReference(null);
		request.getData().getInitiation().getRemittanceInformation().setUnstructured(null);
		response.getData().getInitiation().setRemittanceInformation(null);

		paymentSetupPlatformResource.setTppDebtorDetails("false");
		comparator.compareScheduledPaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
	@Test
	public void testCompareScheduledPaymentDetails_TppDetailsFalse_NullAddressLine() {
		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setBuildingNumber("14");
		deliveryAddress.setCountrySubDivision("Division");
		risk.setDeliveryAddress(deliveryAddress);
		response.setRisk(risk);
		request.setRisk(risk);
		
		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("145");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());

		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("145");
		request.getData().getInitiation().setCreditor(new OBPartyIdentification43());
		request.getData().getInitiation().getCreditor().setPostalAddress(new OBPostalAddress6());
		request.getData().getInitiation().getCreditor().getPostalAddress().setAddressLine(null);
		response.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());

		paymentSetupPlatformResource.setTppDebtorDetails("false");
		comparator.compareScheduledPaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void testCompareScheduledPaymentDetails_TppDetailsFalse_Addresslineempty() {
		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		CustomISPConsentsPOSTResponse adaptedScheduledPaymentConsentsResponse = new CustomISPConsentsPOSTResponse();
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();

		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("145");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().setAuthorisation(new OBAuthorisation1());
		response.getData().getAuthorisation().setAuthorisationType(OBExternalAuthorisation1Code.ANY);

		adaptedScheduledPaymentConsentsResponse.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		adaptedScheduledPaymentConsentsResponse.getData().setInitiation(new OBInternationalScheduled1());
		adaptedScheduledPaymentConsentsResponse.getData().setAuthorisation(null);

		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("145");
		response.getData().getInitiation().setRemittanceInformation(null);
		request.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		request.getData().getInitiation().getDebtorAccount().setName("BOI");
		response.getData().getInitiation().setDebtorAccount(null);

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = null;
		List<String> addressLineList = new ArrayList<>();
		String addressLine = null;
		addressLineList.add(addressLine);
		risk.setDeliveryAddress(deliveryAddress);
		response.setRisk(risk);
		request.setRisk(risk);

		paymentSetupPlatformResource.setTppDebtorDetails("false");
		comparator.compareScheduledPaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@After
	public void tearDown() {
		comparator = null;
	}
}
package com.capgemini.psd2.pisp.ispc.test.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.ispc.controller.ISPConsentsApiController;
import com.capgemini.psd2.pisp.ispc.service.ISPConsentsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class ISPConsentsApiControllerTest {

	@Mock
	private ISPConsentsService service;

	@Mock
	ObjectMapper objectMapper;

	@InjectMocks
	private ISPConsentsApiController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testObjectMapper() {
		controller.getObjectMapper();
	}

	@Test
	public void testRequest() {
		controller.getRequest();
	}

	@Test
	public void testCreateInternationalScheduledPaymentConsents() {
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		@SuppressWarnings("serial")
		DefaultSerializerProvider p = new DefaultSerializerProvider() {

			@Override
			public DefaultSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
				return null;
			}
		};
		objectMapper.setSerializerProvider(p);

		String xFapiFinancialId = "1";
		String authorization = "2";
		String xIdempotencyKey = "3";
		String xJwsSignature = "4";

		Mockito.when(service.createInternationalScheduledPaymentConsentsResource(request)).thenReturn(response);
		controller.createInternationalScheduledPaymentConsents(request, xFapiFinancialId, authorization,
				xIdempotencyKey, xJwsSignature, null, null, null, null);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateInternationalScheduledPaymentConsentsException() {

		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		String xFapiFinancialId = "1";
		String authorization = "2";
		String xIdempotencyKey = "3";
		String xJwsSignature = "4";

		Mockito.when(service.createInternationalScheduledPaymentConsentsResource(request))
				.thenThrow(PSD2Exception.class);
		controller.createInternationalScheduledPaymentConsents(request, xFapiFinancialId, authorization,
				xIdempotencyKey, xJwsSignature, null, null, null, null);

	}

	@Test
	public void testGetInternationalScheduledPaymentConsentsConsentId() {
		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();

		@SuppressWarnings("serial")
		DefaultSerializerProvider p = new DefaultSerializerProvider() {

			@Override
			public DefaultSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
				return null;
			}
		};
		objectMapper.setSerializerProvider(p);

		String xFapiFinancialId = "1";
		String authorization = "2";
		String internationalScheduledPaymentId = "123456";

		Mockito.when(service.retrieveInternationalScheduledPaymentConsentsResource(internationalScheduledPaymentId))
				.thenReturn(response);
		controller.getInternationalScheduledPaymentConsentsConsentId(internationalScheduledPaymentId, xFapiFinancialId,
				authorization, null, null, null, null);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testGetInternationalScheduledPaymentConsentsConsentIdException() {
		String xFapiFinancialId = "1";
		String authorization = "2";
		String internationalScheduledPaymentId = "123456";

		Mockito.when(service.retrieveInternationalScheduledPaymentConsentsResource(internationalScheduledPaymentId))
				.thenThrow(PSD2Exception.class);
		controller.getInternationalScheduledPaymentConsentsConsentId(internationalScheduledPaymentId, xFapiFinancialId,
				authorization, null, null, null, null);
	}

	@After
	public void tearDown() {
		service = null;
		controller = null;
	}

}

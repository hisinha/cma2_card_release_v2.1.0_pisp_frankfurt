package com.capgemini.psd2.pisp.ispc.service;

import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;

public interface ISPConsentsService {

	public CustomISPConsentsPOSTResponse createInternationalScheduledPaymentConsentsResource(
			CustomISPConsentsPOSTRequest scheduledPaymentRequest);	
	 
	public CustomISPConsentsPOSTResponse retrieveInternationalScheduledPaymentConsentsResource(
			String consentId);	
}

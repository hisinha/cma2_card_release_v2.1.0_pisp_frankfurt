package com.capgemini.psd2.pisp.ispc.comparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class ISPConsentsPayloadComparator implements Comparator<OBWriteInternationalScheduledConsentResponse1> {

	@Override
	public int compare(OBWriteInternationalScheduledConsentResponse1 response,
			OBWriteInternationalScheduledConsentResponse1 adaptedScheduledPaymentConsentsResponse) {

		int value = 1;

		if (Double.compare(Double.parseDouble(response.getData().getInitiation().getInstructedAmount().getAmount()), Double
				.parseDouble(adaptedScheduledPaymentConsentsResponse.getData().getInitiation().getInstructedAmount()
						.getAmount()))==0) {
			adaptedScheduledPaymentConsentsResponse.getData().getInitiation().getInstructedAmount()
					.setAmount(response.getData().getInitiation().getInstructedAmount().getAmount());
		}

		if (response.getData().getInitiation().equals(adaptedScheduledPaymentConsentsResponse.getData().getInitiation())
				&& response.getRisk().equals(adaptedScheduledPaymentConsentsResponse.getRisk()))
			value = 0;

		OBAuthorisation1 responseAuth = response.getData().getAuthorisation();
		OBAuthorisation1 adaptedResponseAuth = adaptedScheduledPaymentConsentsResponse.getData().getAuthorisation();

		if (!NullCheckUtils.isNullOrEmpty(responseAuth) && !NullCheckUtils.isNullOrEmpty(adaptedResponseAuth)
				&& value != 1) {
			if (responseAuth.equals(adaptedResponseAuth))
				value = 0;
			else
				value = 1;
		}

		return value;
	}

	public int compareScheduledPaymentDetails(CustomISPConsentsPOSTResponse response,
			CustomISPConsentsPOSTRequest request, PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		String strPaymentConsentsRequest = JSONUtilities.getJSONOutPutFromObject(request);
		CustomISPConsentsPOSTResponse adaptedScheduledPaymentConsentsResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), strPaymentConsentsRequest, CustomISPConsentsPOSTResponse.class);

		if (!validateDebtorDetails(response.getData().getInitiation(),
				adaptedScheduledPaymentConsentsResponse.getData().getInitiation(), paymentSetupPlatformResource))
			return 1;

		compareAmount(response.getData().getInitiation().getInstructedAmount(),
				adaptedScheduledPaymentConsentsResponse.getData().getInitiation().getInstructedAmount());

		/*
		 * Date: 13-Feb-2018 Changes are made for CR-10
		 */
		checkAndModifyEmptyFields(adaptedScheduledPaymentConsentsResponse, response);

		if (!NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation()) && (NullCheckUtils
				.isNullOrEmpty(adaptedScheduledPaymentConsentsResponse.getData().getAuthorisation()))) {
			return 1;
		}

		if (NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation()) && !NullCheckUtils
				.isNullOrEmpty(adaptedScheduledPaymentConsentsResponse.getData().getAuthorisation())) {
			return 1;
		}

		int returnValue = compare(response, adaptedScheduledPaymentConsentsResponse);

		response.getData().getInitiation()
				.setRemittanceInformation(request.getData().getInitiation().getRemittanceInformation());
		response.getRisk().setDeliveryAddress(request.getRisk().getDeliveryAddress());

		// End of CR-10
		return returnValue;

	}

	private void checkAndModifyEmptyFields(CustomISPConsentsPOSTResponse adaptedPaymentResponse,
			CustomISPConsentsPOSTResponse response) {

		checkAndModifyRemittanceInformation(adaptedPaymentResponse.getData().getInitiation(), response);
		if (adaptedPaymentResponse.getRisk().getDeliveryAddress() != null) {
			checkAndModifyAddressLine(adaptedPaymentResponse.getRisk().getDeliveryAddress(), response);
		}

		if (adaptedPaymentResponse.getData().getInitiation().getCreditor()!=null && adaptedPaymentResponse.getData().getInitiation().getCreditor().getPostalAddress() != null
				&& adaptedPaymentResponse.getData().getInitiation().getCreditor().getPostalAddress()
						.getAddressLine() != null) {
			checkAndModifyCreditorPostalAddressLine(
					adaptedPaymentResponse.getData().getInitiation().getCreditor().getPostalAddress(), response);
		}

	}

	private void checkAndModifyRemittanceInformation(OBInternationalScheduled1 obInternationalScheduled1,
			CustomISPConsentsPOSTResponse response) {

		OBRemittanceInformation1 remittanceInformation = obInternationalScheduled1.getRemittanceInformation();
		if (remittanceInformation != null && response.getData().getInitiation().getRemittanceInformation() == null
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference())
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getUnstructured())) {

			obInternationalScheduled1.setRemittanceInformation(null);
		}
	}

	private void checkAndModifyAddressLine(OBRisk1DeliveryAddress riskDeliveryAddress,
			CustomISPConsentsPOSTResponse response) {

		if (riskDeliveryAddress.getAddressLine() != null) {
			List<String> addressLineList = new ArrayList<>();
			for (String addressLine : riskDeliveryAddress.getAddressLine()) {
				if (NullCheckUtils.isNullOrEmpty(addressLine))
					continue;
				addressLineList.add(addressLine);
			}
			if (addressLineList.isEmpty() && response.getRisk().getDeliveryAddress().getAddressLine() == null)
				addressLineList = null;

			riskDeliveryAddress.setAddressLine(addressLineList);

		}
	}

	private void checkAndModifyCreditorPostalAddressLine(OBPostalAddress6 obPostalAddress6,
			CustomISPConsentsPOSTResponse response) {
		List<String> addressLineList = new ArrayList<>();
		for (String addressLine : (obPostalAddress6.getAddressLine())) {
			if (NullCheckUtils.isNullOrEmpty(addressLine))
				continue;
			addressLineList.add(addressLine);
		}
		if (addressLineList.isEmpty()
				&& response.getData().getInitiation().getCreditor().getPostalAddress().getAddressLine() == null)
			addressLineList = null;

		obPostalAddress6.setAddressLine(addressLineList);
	}

	private boolean validateDebtorDetails(OBInternationalScheduled1 oBInternationalScheduled1,
			OBInternationalScheduled1 oBInternationalScheduled12,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {
			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be true, then
			 * name will participate in comparison else product will set foundation response
			 * DebtorAccount.Name as null so that it can be passed in comparison. Both
			 * fields would have the value as 'Null'.
			 */
			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
				oBInternationalScheduled1.getDebtorAccount().setName(null);

			// DebtorAgent is not present in CMA3
			return Objects.equals(oBInternationalScheduled1.getDebtorAccount(),
					oBInternationalScheduled12.getDebtorAccount());
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& oBInternationalScheduled12.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& oBInternationalScheduled12.getDebtorAccount() != null) {
			oBInternationalScheduled1.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;

	}

	/*
	 * Date: 12-Jan-2018, SIT defect#956 Comparing two float values received from FS
	 * and TPP. Both values must be treated equal mathematically if values are in
	 * below format. i.e 1.2 == 1.20, 0.20000 == 0.20, 0.2 == 00.20000, 0.2 ==
	 * 0.20000, 001.00 == 1.00 etc. If values are equal then set the amount received
	 * from TPP to the amount object of FS response. By doing this, comparison would
	 * be passed in swagger's java file(compare method of this class) and same
	 * amount would be available to TPP also in response of idempotent request.
	 */
	private void compareAmount(OBDomestic1InstructedAmount oBDomestic1InstructedAmount,
			OBDomestic1InstructedAmount oBDomestic1InstructedAmount2) {
		if (Double.compare(Double.parseDouble(oBDomestic1InstructedAmount.getAmount()),
				Double.parseDouble(oBDomestic1InstructedAmount2.getAmount())) == 0)
			oBDomestic1InstructedAmount.setAmount(oBDomestic1InstructedAmount2.getAmount());
	}

}

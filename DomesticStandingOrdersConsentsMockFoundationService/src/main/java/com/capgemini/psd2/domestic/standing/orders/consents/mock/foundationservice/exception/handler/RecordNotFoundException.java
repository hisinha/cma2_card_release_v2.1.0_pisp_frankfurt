package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.exception.handler;

public class RecordNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public RecordNotFoundException(String s){
		super(s);
		
	}
	
}

package com.capgemini.psd2.aisp.validation.adapter;

public interface AISPCustomValidator<T> {
	// ValidateAccountsResponse
	public boolean validateAccountsResponse(T t);

}

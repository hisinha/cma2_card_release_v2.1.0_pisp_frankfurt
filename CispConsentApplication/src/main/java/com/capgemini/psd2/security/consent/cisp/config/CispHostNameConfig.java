package com.capgemini.psd2.security.consent.cisp.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
public class CispHostNameConfig {

	private Map<String, String> edgeserverhost;
	
	public String getTenantSpecificEdgeserverhost(String id) {
		return edgeserverhost.get(id);
	}

	public Map<String, String> getEdgeserverhost() {
		return edgeserverhost;
	}

	public void setEdgeserverhost(Map<String, String> edgeserverhost) {
		this.edgeserverhost = edgeserverhost;
	}
}

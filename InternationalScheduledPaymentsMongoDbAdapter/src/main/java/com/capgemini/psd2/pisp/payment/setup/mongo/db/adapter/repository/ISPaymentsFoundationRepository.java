package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;

public interface ISPaymentsFoundationRepository extends MongoRepository<CustomISPaymentsPOSTResponse, String> {

	public CustomISPaymentsPOSTResponse findOneByDataInternationalScheduledPaymentId(String submissionId);

}
package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class DigitalUserRequest {

	private DigitalUser digitalUser;
	

	
	public DigitalUser getDigitalUser() {
		return digitalUser;
	}
	public void setDigitalUser(DigitalUser digitalUser) {
		this.digitalUser = digitalUser;
	}
	
}

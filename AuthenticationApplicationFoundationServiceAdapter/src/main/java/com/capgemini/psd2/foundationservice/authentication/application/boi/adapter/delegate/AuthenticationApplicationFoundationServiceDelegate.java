
package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component
public class AuthenticationApplicationFoundationServiceDelegate {

	@Value("${foundationService.sourceUserInReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserInReqHeader;

	@Value("${foundationService.sourceSystemInReqHeader:#{X-API-SOURCE-SYSTEM}}")
	private String sourceSystemInReqHeader;

	@Value("${foundationService.trasanctionIdInReqHeader:#{X-API-TRANSANCTION-ID}}")
	private String trasanctionIdInReqHeader;
	
	@Value("${foundationService.apiCorrelationIdInReqHeader:#{X-API-CORRELATION-ID}}")
	private String apiCorrelationIdInReqHeader;

	
	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	@Value("${foundationService.platformInReqHeader:#{X-BOI-PLATFORM}}")
	private String platformInReqHeader;

	@Value("${foundationService.correlationIdInReqHeader:#{X-CORRELATION-ID}}")
	private String correlationIdInReqHeader;

	@Value("${app.platform}")
	private String platform;

	public HttpHeaders createRequestHeadersBOL(RequestInfo requestInfo, Map<String, String> params) {

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(sourceUserInReqHeader, params.get(AdapterSecurityConstants.USER_HEADER));
		httpHeaders.add(sourceSystemInReqHeader, AdapterSecurityConstants.SOURCE_SYSTEM_HEADER_VALUE);
		httpHeaders.add(apiCorrelationIdInReqHeader, "");
		httpHeaders.add(trasanctionIdInReqHeader, params.get(PSD2Constants.CORRELATION_ID));
		httpHeaders.add("Content-Type","application/json");
		

		return httpHeaders;
	}

	public HttpHeaders createRequestHeadersB365(RequestInfo requestInfo, Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, params.get(AdapterSecurityConstants.USER_HEADER));
		httpHeaders.add(channelInReqHeader, params.get(AdapterSecurityConstants.CHANNELID_HEADER));
		httpHeaders.add(platformInReqHeader, platform);
		httpHeaders.add(correlationIdInReqHeader, params.get(PSD2Constants.CORRELATION_ID));
		httpHeaders.add("Content-Type","application/xml");
		httpHeaders.add("Accept","application/xml");
	
	return httpHeaders;}
}

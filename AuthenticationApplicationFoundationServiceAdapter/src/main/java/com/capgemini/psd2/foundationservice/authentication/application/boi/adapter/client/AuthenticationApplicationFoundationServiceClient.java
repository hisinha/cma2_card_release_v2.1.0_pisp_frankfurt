
package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.AuthenticationRequest;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.DigitalUserRequest;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.LoginResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Component
public class AuthenticationApplicationFoundationServiceClient {

	@Autowired
	@Qualifier("restClientSecurityFoundation")
	private RestClientSync restClient;
	
	@Autowired
	private RestTemplate restTemplate;
	
/*	@PostConstruct
	private void init() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        converter.setObjectMapper(mapper);
        
        StringHttpMessageConverter converter1 = new StringHttpMessageConverter();
        FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
        
        restTemplate.getMessageConverters().add(0,formHttpMessageConverter);
        restTemplate.getMessageConverters().add(1,converter);
        restTemplate.getMessageConverters().add(2,converter1);
       
	} 
*/
	public String restTransportForAuthenticationApplication(RequestInfo reqInfo, AuthenticationRequest authenticationRequest, Class<String> response, HttpHeaders headers) {
		return restClient.callForPost(reqInfo, authenticationRequest, response, headers);

	}
	public LoginResponse restTransportForAuthenticationApplication(RequestInfo reqInfo, DigitalUserRequest digitalUserRequest, Class<LoginResponse> response, HttpHeaders headers) {
		return restClient.callForPost(reqInfo, digitalUserRequest, response, headers);

	}

}

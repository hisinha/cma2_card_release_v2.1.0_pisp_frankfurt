
package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.adapter.exceptions.security.AdapterAuthenticationException;
import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.client.AuthenticationApplicationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.delegate.AuthenticationApplicationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.AuthenticationMethodCode;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.AuthenticationRequest;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.AuthenticationSystemCode;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.CustomerAuthenticationSession;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.DigitalUserRequest;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.LoginResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class AuthenticationApplicationFoundationServiceAdapter implements AuthenticationAdapter {

	@Autowired
	private AdapterUtility adapterUtility;
	

	@Autowired
	private AuthenticationApplicationFoundationServiceDelegate authenticationApplicationFoundationServiceDelegate;

	@Autowired
	private AuthenticationApplicationFoundationServiceClient authenticationApplicationFoundationServiceClient;

	
	@Override
	public <T> T authenticate(T authentication, Map<String, String> params) {

		RequestInfo requestInfo = new RequestInfo();
		
		Authentication authenticationObject = (Authentication)authentication;

		String channelId = params.get(AdapterSecurityConstants.CHANNELID_HEADER);
		
		if (AuthenticationSystemCode.BOL.getValue().equalsIgnoreCase(channelId)){
			DigitalUserRequest request = this.createBOLRequest(authenticationObject, params);
			
			HttpHeaders httpHeaders = authenticationApplicationFoundationServiceDelegate.createRequestHeadersBOL(requestInfo, params);
			String finalURL = adapterUtility.retriveFoundationServiceURL(channelId);
			if(NullCheckUtils.isNullOrEmpty(finalURL)){
				throw SecurityAdapterException.populatePSD2SecurityException("No valid foundation url found.",SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			requestInfo.setUrl(finalURL);
			LoginResponse loginResponse = authenticationApplicationFoundationServiceClient.restTransportForAuthenticationApplication(requestInfo, request, LoginResponse.class, httpHeaders);
			
			if(loginResponse.getDigitalUserSession().isSessionInitiationFailureIndicator()){
				throw SecurityAdapterException.populatePSD2SecurityException("User authentication failed", SecurityAdapterErrorCodeEnum.LOGON_UNSUCCESSFUL);
			}
			
		}
		else if(AuthenticationSystemCode.B365.getValue().equalsIgnoreCase(channelId)){
			AuthenticationRequest authenticationRequest = new AuthenticationRequest();

			HttpHeaders httpHeaders = authenticationApplicationFoundationServiceDelegate.createRequestHeadersB365(requestInfo, params);
			authenticationRequest.setUserName(authenticationObject.getPrincipal().toString());
			authenticationRequest.setPassword(authenticationObject.getCredentials().toString());
			
			String finalURL = adapterUtility.retriveFoundationServiceURL(channelId);
			if(NullCheckUtils.isNullOrEmpty(finalURL)){
				throw AdapterAuthenticationException.populateAuthenticationFailedException("No valid foundation url found.",SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			requestInfo.setUrl(finalURL);
			authenticationApplicationFoundationServiceClient.restTransportForAuthenticationApplication(requestInfo, authenticationRequest, String.class, httpHeaders);		  
		}
		return authentication;
	}
	
	private DigitalUserRequest createBOLRequest(Authentication authenticationObject, Map<String, String> params)
	{
		DigitalUser digitalUser = new DigitalUser();
		DigitalUserRequest request = new DigitalUserRequest();
		
		digitalUser.setDigitalUserIdentifier(String.valueOf(authenticationObject.getPrincipal()));
		
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.BOL);
		//request.setDigitalId(authenticationObject.getPrincipal().toString());
		List<CustomerAuthenticationSession> customerAuthenticationSession = new ArrayList<>();
		CustomerAuthenticationSession customerAuthSession = new CustomerAuthenticationSession();
		customerAuthSession.setAuthenticationMethodCode(AuthenticationMethodCode.ONE_TIME_PASSWORD);
		customerAuthSession.setSecureAccessKeyUsed(String.valueOf(authenticationObject.getCredentials()));
		
		
		//---Fix for SIT defect 3017, this block is used for other purpose so excluded it for now as FS facing issue to consume and there is no mapping logic in ICD for this block.--- 
		
		List<String> authenticationParameterText = new ArrayList<>();
		
		authenticationParameterText.add("");
		authenticationParameterText.add("");
		customerAuthSession.setAuthenticationParameterText(authenticationParameterText);
		customerAuthenticationSession.add(customerAuthSession);
		
		digitalUser.getCustomerAuthenticationSession().addAll(customerAuthenticationSession);
		request.setDigitalUser(digitalUser);
		request.setDigitalUser(request.getDigitalUser());
		return request;
	}
	
}

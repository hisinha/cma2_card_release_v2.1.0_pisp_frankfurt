package com.capgemini.psd2.pisp.file.payment.setup.utilities;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@Component
public class FilePaymentConsentsUtilities {

	public byte[] generateFileHash(MultipartFile paymentFile) {
		try {
			return DigestUtils.sha256(paymentFile.getBytes());
		}catch(Exception exp) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, "Unable to generate proper file hash"));
		}

	}

	public byte[] base64DecodeHash(String hashValue) {
		return Base64.decodeBase64(hashValue.getBytes());
	}
	
	public String base64EncodeHash(byte[] hash) {

		byte[] bytesEncoded = Base64.encodeBase64(hash);
		return new String(bytesEncoded);

	}

}

package com.capgemini.psd2.pisp.file.payment.setup.test.transformer.impl;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.file.payment.setup.transformer.impl.FPaymentConsentsResponseTransformerImpl;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class FPaymentConsentsResponseTransformerImplTest {

	private MockMvc mockMvc;
	
	@Mock 
	private RequestHeaderAttributes reqHeaderAttributes;
	
	@Mock 
	private PispDateUtility pispDateUtility; 
	
	@InjectMocks
	private FPaymentConsentsResponseTransformerImpl transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(transformer).dispatchOptions(true).build();
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testFilePaymentConsentsResponseTransformer() throws Exception {
	
   		CustomFilePaymentConsentsPOSTResponse fileConsentsTppResponse=new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsentResponse1 data=new OBWriteDataFileConsentResponse1();
		OBFile1 initiation = new OBFile1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("SortCodeAccountNumber");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
	
		fileConsentsTppResponse.setData(data);

		PaymentConsentsPlatformResource paymentSetupPlatformResource=new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails(Boolean.toString(false));
		paymentSetupPlatformResource.setTppDebtorNameDetails(Boolean.toString(false));
		paymentSetupPlatformResource.setStatus("AwaitingAuthorisation");
		String methodType=RequestMethod.POST.toString();
		
		transformer.filePaymentConsentsResponseTransformer(fileConsentsTppResponse, paymentSetupPlatformResource, methodType);
	}
	
	@Test
	public void testFilePaymentConsentsResponseTransformerTpp() throws Exception {
	
   		CustomFilePaymentConsentsPOSTResponse fileConsentsTppResponse=new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsentResponse1 data=new OBWriteDataFileConsentResponse1();
		OBFile1 initiation = new OBFile1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("SortCodeAccountNumber");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
	
		fileConsentsTppResponse.setData(data);

		PaymentConsentsPlatformResource paymentSetupPlatformResource=new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails(Boolean.toString(true));
		paymentSetupPlatformResource.setTppDebtorNameDetails(Boolean.toString(false));
		paymentSetupPlatformResource.setStatus("AwaitingAuthorisation");
		String methodType=RequestMethod.POST.toString();
		
		transformer.filePaymentConsentsResponseTransformer(fileConsentsTppResponse, paymentSetupPlatformResource, methodType);
	}

	@Test
	public void testPaymentConsentRequestTransformer() throws Exception {
	
		CustomFilePaymentSetupPOSTRequest paymentConsentRequest=new CustomFilePaymentSetupPOSTRequest();
		OBWriteDataFileConsent1 data=new OBWriteDataFileConsent1();
		OBAuthorisation1 authorisation=new OBAuthorisation1();
		OBFile1 initiation = new OBFile1();
		
		initiation.setRequestedExecutionDateTime("2019-01-31T00:00:00+00:00");
		authorisation.setCompletionDateTime("2019-01-31T00:00:00+00:00");
		data.setInitiation(initiation);
		data.setAuthorisation(authorisation);
		paymentConsentRequest.setData(data);
		when(pispDateUtility.transformDateTimeInRequest(anyString())).thenReturn("2019-01-31T00:00:00+00:00");
		transformer.paymentConsentRequestTransformer(paymentConsentRequest);
	}
}

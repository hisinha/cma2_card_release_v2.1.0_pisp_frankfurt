package com.capgemini.psd2.pisp.file.payment.setup.test.mongo.db.adapter.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsentResponse1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.config.PaymentConsentAwsObject;
import com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.impl.FPaymentConsentsMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.repository.FPaymentConsentsChargesDataRepository;
import com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.repository.FPaymentConsentsDataRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.SandboxValidationConfig;
import com.capgemini.psd2.utilities.SandboxValidationUtility;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class FPaymentConsentsMongoDbAdapterImplTest {

	private MockMvc mockMvc;
	
	@Mock
	private FPaymentConsentsDataRepository fPaymentConsentsDataRepository;

	@Mock
	private FPaymentConsentsChargesDataRepository fPaymentConsentsChargesRepository;

	@Mock
	private PSD2ValidatorImpl psd2Validator;

	@Mock
	private BasicAWSCredentials awsCredentials;

	@Mock
	private ClientConfiguration clientConfig;

	@Mock
	private SandboxValidationUtility utility;
	
	@Mock
	private PaymentConsentAwsObject s3objectmock;
	
	@Mock
	private RequestHeaderAttributes reqAttributes;

	@Mock
	public AmazonS3 s3client;
	
	@Mock
	private PaymentConsentAwsObject awsObject;
	
	@Mock
	SandboxValidationConfig sandboxConfig;

	
	@InjectMocks
	private FPaymentConsentsMongoDbAdapterImpl adapter;
	
	@Value("${app.awsRegion}")
	private String awsRegion;

	@Value("${app.awsS3BucketName}")
	private String awsS3BucketName;
	
	@Value("${app.awsReportfileName}")
	private String fileName;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(adapter).dispatchOptions(true).build();
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateStagingFilePaymentConsents() throws Exception{
		
		CustomFilePaymentSetupPOSTRequest customRequest=new CustomFilePaymentSetupPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers=new  CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		OBWriteDataFileConsent1 data=new OBWriteDataFileConsent1();
		CustomFilePaymentConsentsPOSTResponse response=new CustomFilePaymentConsentsPOSTResponse();
		OBFile1 initiation = new OBFile1();
		data.setInitiation(initiation);
		BigDecimal sum= new BigDecimal("1110.23");
		initiation.setControlSum(sum);
		customRequest.setData(data);
		when(sandboxConfig.getChargeAmountLimit()).thenReturn("5");
		Mockito.when(utility.getMockedCutOffDtTm()).thenReturn("2019-12-31T00:00:00+00:00");
		ProcessConsentStatusEnum value=ProcessConsentStatusEnum.PASS;
		Mockito.when(utility.getMockedConsentProcessExecStatus(anyObject(),
				anyObject())).thenReturn(value);
		when(fPaymentConsentsDataRepository.save(any(CustomFilePaymentConsentsPOSTResponse.class)))
		.thenReturn(response);
		ReflectionTestUtils.setField(adapter, "borneByCreditorRegex", "^(111([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "borneByDebtorRegex", "^(222([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "chargeAmount", "111");
		
		adapter.createStagingFilePaymentConsents(customRequest, stageIdentifiers, params);
	}

	
	@Test
	public void testCreateStagingFilePaymentConsentsDebtor() throws Exception{
		
		CustomFilePaymentSetupPOSTRequest customRequest=new CustomFilePaymentSetupPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers=new  CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		OBWriteDataFileConsent1 data=new OBWriteDataFileConsent1();
		CustomFilePaymentConsentsPOSTResponse response=new CustomFilePaymentConsentsPOSTResponse();
		OBFile1 initiation = new OBFile1();
		data.setInitiation(initiation);
		BigDecimal sum= new BigDecimal("2220.23");
		initiation.setControlSum(sum);
		customRequest.setData(data);
		when(sandboxConfig.getChargeAmountLimit()).thenReturn("5");
		Mockito.when(utility.getMockedCutOffDtTm()).thenReturn("2019-12-31T00:00:00+00:00");
		ProcessConsentStatusEnum value=ProcessConsentStatusEnum.PASS;
		Mockito.when(utility.getMockedConsentProcessExecStatus(anyObject(),
				anyObject())).thenReturn(value);
		when(fPaymentConsentsDataRepository.save(any(CustomFilePaymentConsentsPOSTResponse.class)))
		.thenReturn(response);
		ReflectionTestUtils.setField(adapter, "borneByCreditorRegex", "^(111([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "borneByDebtorRegex", "^(222([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "chargeAmount", "111");
		
		adapter.createStagingFilePaymentConsents(customRequest, stageIdentifiers, params);
	}
	
	@Test
	public void testCreateStagingFilePaymentConsentsFollowing() throws Exception{
		
		CustomFilePaymentSetupPOSTRequest customRequest=new CustomFilePaymentSetupPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers=new  CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		OBWriteDataFileConsent1 data=new OBWriteDataFileConsent1();
		CustomFilePaymentConsentsPOSTResponse response=new CustomFilePaymentConsentsPOSTResponse();
		OBFile1 initiation = new OBFile1();
		data.setInitiation(initiation);
		BigDecimal sum= new BigDecimal("3330.23");
		initiation.setControlSum(sum);
		customRequest.setData(data);
		when(sandboxConfig.getChargeAmountLimit()).thenReturn("5");
		Mockito.when(utility.getMockedCutOffDtTm()).thenReturn("2019-12-31T00:00:00+00:00");
		ProcessConsentStatusEnum value=ProcessConsentStatusEnum.PASS;
		Mockito.when(utility.getMockedConsentProcessExecStatus(anyObject(),
				anyObject())).thenReturn(value);
		when(fPaymentConsentsDataRepository.save(any(CustomFilePaymentConsentsPOSTResponse.class)))
		.thenReturn(response);
		ReflectionTestUtils.setField(adapter, "borneByCreditorRegex", "^(111([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "borneByDebtorRegex", "^(222([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "followServiceLevelRegex", "^(333([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "sharedRegex", "^(444([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "chargeAmount", "111");
		
		adapter.createStagingFilePaymentConsents(customRequest, stageIdentifiers, params);
	}
	
	@Test
	public void testCreateStagingFilePaymentConsentsShared() throws Exception{
		
		CustomFilePaymentSetupPOSTRequest customRequest=new CustomFilePaymentSetupPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers=new  CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		OBWriteDataFileConsent1 data=new OBWriteDataFileConsent1();
		CustomFilePaymentConsentsPOSTResponse response=new CustomFilePaymentConsentsPOSTResponse();
		OBFile1 initiation = new OBFile1();
		data.setInitiation(initiation);
		BigDecimal sum= new BigDecimal("4440.23");
		initiation.setControlSum(sum);
		customRequest.setData(data);
		when(sandboxConfig.getChargeAmountLimit()).thenReturn("5");
		Mockito.when(utility.getMockedCutOffDtTm()).thenReturn("2019-12-31T00:00:00+00:00");
		ProcessConsentStatusEnum value=ProcessConsentStatusEnum.PASS;
		Mockito.when(utility.getMockedConsentProcessExecStatus(anyObject(),
				anyObject())).thenReturn(value);
		when(fPaymentConsentsDataRepository.save(any(CustomFilePaymentConsentsPOSTResponse.class)))
		.thenReturn(response);
		ReflectionTestUtils.setField(adapter, "borneByCreditorRegex", "^(111([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "borneByDebtorRegex", "^(222([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "followServiceLevelRegex", "^(333([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "sharedRegex", "^(444([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "chargeAmount", "111");
		
		adapter.createStagingFilePaymentConsents(customRequest, stageIdentifiers, params);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testCreateStagingFilePaymentConsentsException() throws Exception{
		
		ReflectionTestUtils.setField(adapter, "borneByCreditorRegex", "^(111([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "borneByDebtorRegex", "^(222([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "chargeAmount", "111");
		
		CustomFilePaymentSetupPOSTRequest customRequest=new CustomFilePaymentSetupPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers=new  CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		OBWriteDataFileConsent1 data=new OBWriteDataFileConsent1();
		CustomFilePaymentConsentsPOSTResponse response=new CustomFilePaymentConsentsPOSTResponse();
		
		ProcessConsentStatusEnum value=ProcessConsentStatusEnum.PASS;
		OBFile1 initiation = new OBFile1();
		data.setInitiation(initiation);
		BigDecimal sum= new BigDecimal("1110.23");
		initiation.setControlSum(sum);
		customRequest.setData(data);
		when(sandboxConfig.getChargeAmountLimit()).thenReturn("5");
		Mockito.when(utility.getMockedCutOffDtTm()).thenReturn("2019-12-31T00:00:00+00:00");
		Mockito.when(utility.getMockedConsentProcessExecStatus(anyObject(),
				anyObject())).thenReturn(value);
		when(fPaymentConsentsDataRepository.save(any(CustomFilePaymentConsentsPOSTResponse.class)))
		.thenThrow(new DataAccessResourceFailureException("Test"));
		
		adapter.createStagingFilePaymentConsents(customRequest, stageIdentifiers, params);
	}

	@Test(expected=PSD2Exception.class)
	public void testCreateStagingFilePaymentConsentsSandExc() throws Exception{
		
		CustomFilePaymentSetupPOSTRequest customRequest=new CustomFilePaymentSetupPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers=new  CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		OBWriteDataFileConsent1 data=new OBWriteDataFileConsent1();
		CustomFilePaymentConsentsPOSTResponse response=new CustomFilePaymentConsentsPOSTResponse();
		OBFile1 initiation = new OBFile1();
		data.setInitiation(initiation);
		BigDecimal sum= new BigDecimal("1110.23");
		initiation.setControlSum(sum);
		customRequest.setData(data);
		
		Mockito.when(utility.getMockedCutOffDtTm()).thenReturn("2019-12-31T00:00:00+00:00");
		ProcessConsentStatusEnum value=ProcessConsentStatusEnum.PASS;
		Mockito.when(utility.getMockedConsentProcessExecStatus(anyObject(),
				anyObject())).thenReturn(value);
		when(fPaymentConsentsDataRepository.save(any(CustomFilePaymentConsentsPOSTResponse.class)))
		.thenReturn(response);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		
		ReflectionTestUtils.setField(adapter, "borneByCreditorRegex", "^(111([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "borneByDebtorRegex", "^(222([0-9]{0,10}\\.\\d{0,5}))$");
		adapter.createStagingFilePaymentConsents(customRequest, stageIdentifiers, params);
	}
	
	@Test
	public void testfetchFilePaymentMetadata() throws Exception{
	
		CustomPaymentStageIdentifiers stageIdentifiers=new CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		CustomFilePaymentConsentsPOSTResponse response=new CustomFilePaymentConsentsPOSTResponse();
		
		when(fPaymentConsentsDataRepository.findOneByDataConsentId(anyString())).thenReturn(response);
		when(reqAttributes.getMethodType()).thenReturn("POST");
		adapter.fetchFilePaymentMetadata(stageIdentifiers, params);
	}


	@Test
	public void testfetchFilePaymentMetadataSandbox() throws Exception{
	
		CustomPaymentStageIdentifiers stageIdentifiers=new CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		CustomFilePaymentConsentsPOSTResponse response=new CustomFilePaymentConsentsPOSTResponse();
		
		when(fPaymentConsentsDataRepository.findOneByDataConsentId(anyString())).thenReturn(response);
		when(reqAttributes.getMethodType()).thenReturn("POST");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		adapter.fetchFilePaymentMetadata(stageIdentifiers, params);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testfetchFilePaymentMetadataExc() throws Exception{
	
		CustomPaymentStageIdentifiers stageIdentifiers=new CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		
		when(fPaymentConsentsDataRepository.findOneByDataConsentId(anyString())).thenThrow(new DataAccessResourceFailureException("Test"));
		when(reqAttributes.getMethodType()).thenReturn("POST");
		adapter.fetchFilePaymentMetadata(stageIdentifiers, params);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testfetchFilePaymentMetadataSandboxExc() throws Exception{
	
		CustomPaymentStageIdentifiers stageIdentifiers=new CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		CustomFilePaymentConsentsPOSTResponse response=new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsentResponse1 data=new OBWriteDataFileConsentResponse1();
		OBFile1 initiation=new OBFile1();
		BigDecimal controlSum=new BigDecimal("123.33");
		initiation.setControlSum(controlSum);
		data.setInitiation(initiation);
		response.setData(data);
		
		when(fPaymentConsentsDataRepository.findOneByDataConsentId(anyString())).thenReturn(response);
		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		adapter.fetchFilePaymentMetadata(stageIdentifiers, params);
	}

	@Test 
	public void testUpdateStagingFilePaymentConsents() throws Exception {
		
		CustomFilePaymentConsentsPOSTResponse response=new CustomFilePaymentConsentsPOSTResponse();
		
		when(fPaymentConsentsDataRepository.save(any(CustomFilePaymentConsentsPOSTResponse.class)))
		.thenReturn(response);
		adapter.updateStagingFilePaymentConsents(response);
	}

	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testUpdateStagingFilePaymentConsentsException() throws Exception {
		
		CustomFilePaymentConsentsPOSTResponse response=new CustomFilePaymentConsentsPOSTResponse();
		
		when(fPaymentConsentsDataRepository.save(any(CustomFilePaymentConsentsPOSTResponse.class)))
		.thenThrow(DataAccessResourceFailureException.class);
		
		adapter.updateStagingFilePaymentConsents(response);
	}
	
	@Test
	public void testdownloadFilePaymentConsents() throws Exception {
		ReflectionTestUtils.setField(adapter, "awsRegion", "eu-west-1");
		ReflectionTestUtils.setField(awsCredentials, "accessKey", "AKIAJHAQ2CSZAEIG4ZCQ");
		ReflectionTestUtils.setField(awsCredentials, "secretKey", "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");
		ReflectionTestUtils.setField(adapter, "awsS3BucketName", "file-payment-consents-bucket");
		ReflectionTestUtils.setField(adapter, "fileFolder", "/file-payment-consents-bucket/");
		
		Resource resource = new ClassPathResource("/file.pdf");
		File file = resource.getFile();
		InputStream is = new FileInputStream(file);
		ObjectMetadata objMeta = mock(ObjectMetadata.class);		
		
		S3Object fullObject = new S3Object();
		fullObject.setKey("local.pdf");
		fullObject.setBucketName("file-payment-consents-bucket");
		fullObject.setObjectContent(is);
		fullObject.setObjectMetadata(objMeta);
		
		when(s3objectmock.getPaymentAwsClient(any(BasicAWSCredentials.class), anyObject(),anyString())).thenReturn(s3client);

		when(s3objectmock.getPaymentAwsObjectS3Object(any(AmazonS3Client.class), anyString(), anyString())).thenReturn(fullObject);
	
		CustomPaymentStageIdentifiers paymentStageIdentifiers=new CustomPaymentStageIdentifiers();
		paymentStageIdentifiers.setPaymentConsentId("6823e792-f3ff-48e3-becc-a63ce1720663");
		
		adapter.downloadFilePaymentConsents(paymentStageIdentifiers);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testUploadFilePaymentConsentsExc() throws Exception {
		ReflectionTestUtils.setField(adapter, "awsRegion", "eu-west-1");
		ReflectionTestUtils.setField(awsCredentials, "accessKey", "AKIAJHAQ2CSZAEIG4ZCQ");
		ReflectionTestUtils.setField(awsCredentials, "secretKey", "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");
		ReflectionTestUtils.setField(adapter, "awsS3BucketName", "file-payment-consents-bucket");
		
		MultipartFile paymentFile=new MultipartFile() {
			
			@Override
			public void transferTo(File dest) throws IOException, IllegalStateException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public boolean isEmpty() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public long getSize() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public String getOriginalFilename() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public InputStream getInputStream() throws IOException {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getContentType() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public byte[] getBytes() throws IOException {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		Resource resource = new ClassPathResource("/file.pdf");
		File file = resource.getFile();
		InputStream is = new FileInputStream(file);
		ObjectMetadata objMeta = mock(ObjectMetadata.class);		
		ArgumentCaptor<InputStream> streamCaptor = ArgumentCaptor.forClass(InputStream.class);
		PutObjectResult value=new PutObjectResult();
		S3Object fullObject = new S3Object();
		
		fullObject.setKey("local.pdf");
		fullObject.setBucketName("file-payment-consents-bucket");
		fullObject.setObjectContent(is);
		fullObject.setObjectMetadata(objMeta);
		
		CustomPaymentStageIdentifiers paymentStageIdentifiers=new CustomPaymentStageIdentifiers();
		paymentStageIdentifiers.setPaymentConsentId("6823e792-f3ff-48e3-becc-a63ce1720663");
		
		when(s3client.putObject(anyString(), anyString(), streamCaptor.capture(),
                isNull(ObjectMetadata.class))).thenReturn(null);
		
		adapter.uploadFilePaymentConsents(paymentFile, "");

	}
	
}
package com.capgemini.psd2.pisp.file.payment.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;

@Component
public class FPaymentConsentsCoreSystemAdapterFactory implements ApplicationContextAware, FPaymentConsentsAdapterFactory{

	private ApplicationContext applicationContext;
	
	@Override
	public FilePaymentConsentsAdapter getFilePaymentSetupStagingInstance(String adapterName) {
		return (FilePaymentConsentsAdapter) applicationContext.getBean(adapterName);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}

}

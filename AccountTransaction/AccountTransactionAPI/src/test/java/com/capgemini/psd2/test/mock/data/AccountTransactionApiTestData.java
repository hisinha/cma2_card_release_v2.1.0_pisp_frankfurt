package com.capgemini.psd2.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3Data;
import com.capgemini.psd2.aisp.domain.OBTransaction3;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.aisp.domain.OBBalanceType1Code;
import com.capgemini.psd2.aisp.domain.OBTransactionCashBalance;
//import com.capgemini.psd2.aisp.domain.OBTransaction2BalanceAmount;
import com.capgemini.psd2.aisp.domain.OBBankTransactionCodeStructure1;
import com.capgemini.psd2.aisp.domain.OBEntryStatus1Code;
import com.capgemini.psd2.aisp.domain.OBMerchantDetails1;
import com.capgemini.psd2.aisp.domain.OBTransaction3ProprietaryBankTransactionCode;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class AccountTransactionApiTestData {

	static OBTransaction3 data = new OBTransaction3();
	static OBActiveOrHistoricCurrencyAndAmount amount = new OBActiveOrHistoricCurrencyAndAmount();
	static OBTransactionCashBalance balance = new OBTransactionCashBalance();
	static OBActiveOrHistoricCurrencyAndAmount amount2 = new OBActiveOrHistoricCurrencyAndAmount();
	static OBBankTransactionCodeStructure1 bankTransactionCode = new OBBankTransactionCodeStructure1();
	static OBTransaction3ProprietaryBankTransactionCode proprietaryBankTransactionCode = new OBTransaction3ProprietaryBankTransactionCode();
	static OBMerchantDetails1 merchantDetails = new OBMerchantDetails1();

	public static String getTestAccountId() {
		return "269c3ff5-d7f8-419b-a3b9-7136c5b4611a";
	}

	public static String getTestBankID() {
		return "TestBankID";
	}

/*	public static OBReadResponse1 getAccountRequestPOSTResponse() {
		OBReadResponse1 accountRequestPOSTResponse = new OBReadResponse1();
		OBReadDataResponse1 respData = new OBReadDataResponse1();
		respData.setAccountRequestId("123434321");
		respData.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		respData.setTransactionToDateTime("2017-12-03T00:00:00+00:00");
		accountRequestPOSTResponse.setData(respData);
		accountRequestPOSTResponse.setRisk(new OBRisk2());
		return accountRequestPOSTResponse;
	}*/

/*	public static OBReadResponse1 getAccountRequestPOSTResponse2() {
		OBReadResponse1 accountRequestPOSTResponse = new OBReadResponse1();
		Data1 respData = new Data1();
		respData.setAccountRequestId("123434321");
		accountRequestPOSTResponse.setData(respData);
		accountRequestPOSTResponse.setRisk(new Object());
		return accountRequestPOSTResponse;
	}

	public static OBReadResponse1 getAccountRequestPOSTResponse3() {
		OBReadResponse1 accountRequestPOSTResponse = new OBReadResponse1();
		Data1 respData = new Data1();
		respData.setAccountRequestId("123434321");
		respData.setTransactionFromDateTime("2015-01-02T00:00:00+00:00");
		respData.setTransactionToDateTime("2017-01-02T00:00:00+00:00");
		accountRequestPOSTResponse.setData(respData);
		accountRequestPOSTResponse.setRisk(new Object());
		return accountRequestPOSTResponse;
	}

	public static OBReadResponse1 getAccountRequestPOSTResponseWithNoDates() {
		OBReadResponse1 accountRequestPOSTResponse = new OBReadResponse1();
		OBReadDataResponse1 respData = new OBReadDataResponse1();
		respData.setAccountRequestId("123434321");
		accountRequestPOSTResponse.setData(respData);
		accountRequestPOSTResponse.setRisk(new OBRisk2());
		return accountRequestPOSTResponse;
	}

*/	public static List<Object> getMockClaimsList() {
		List<Object> claimList = new ArrayList<Object>();
		claimList.add("ReadTransactionsCredits");
		claimList.add("ReadTransactionsDebits");
		return claimList;
	}

	public static OBReadTransaction3 getAccountTransactionsGETResponse() {
		OBReadTransaction3 resp = new OBReadTransaction3();
		List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(
				OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		OBReadTransaction3Data data3 = new OBReadTransaction3Data();
		data3.setTransaction(dataList);
		resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		
		return resp;
	}
	
	public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseForService() {
		OBReadTransaction3 resp = new OBReadTransaction3();
		List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		OBReadTransaction3Data data3 = new OBReadTransaction3Data();
		data3.setTransaction(dataList);
		resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		PlatformAccountTransactionResponse platResp=new PlatformAccountTransactionResponse();
		platResp.setoBReadTransaction3(resp);
		return platResp;
	}
	
	public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseForServiceTesting() {
		OBReadTransaction3 resp = new OBReadTransaction3();
		List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		OBReadTransaction3Data data3 = new OBReadTransaction3Data();
		data3.setTransaction(dataList);
		//resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		PlatformAccountTransactionResponse platResp=new PlatformAccountTransactionResponse();
		platResp.setoBReadTransaction3(resp);
		return platResp;
	}

	public static Token getMockTokenWithALLFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsCredits");
		value.add("ReadTransactionsDebits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}

	public static Token getMockTokenWithCreditFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsCredits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}

	public static Token getMockTokenWithDebitFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsDebits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}

	public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseWithNullLinks() {
		OBReadTransaction3 resp = new OBReadTransaction3();
		List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		OBReadTransaction3Data data3 = new OBReadTransaction3Data();
		data3.setTransaction(dataList);
		resp.setData(data3);
		Links links = new Links();
		resp.setLinks(links);
		PlatformAccountTransactionResponse transResp=new PlatformAccountTransactionResponse();
		transResp.setoBReadTransaction3(resp);
		return transResp;
	}

	public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseWithNullLinksObject() {
		OBReadTransaction3 resp = new OBReadTransaction3();
		List<OBTransaction3> dataList = new ArrayList<OBTransaction3>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBTransaction3.CreditDebitIndicatorEnum.CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBTransactionCashBalance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		OBReadTransaction3Data data3 = new OBReadTransaction3Data();
		data3.setTransaction(dataList);
		resp.setData(data3);
		PlatformAccountTransactionResponse transResp=new PlatformAccountTransactionResponse();
		transResp.setoBReadTransaction3(resp);
		return transResp;
	}

	public static AispConsent getAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTransactionFromDateTime("2017-05-03T00:00:00");
		aispConsent.setTransactionToDateTime("2017-12-03T00:00:00");
		return aispConsent;
	}
	
	public static AispConsent getAispConsent2() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTransactionFromDateTime("2015-01-02T00:00:00");
		aispConsent.setTransactionToDateTime("2017-01-02T00:00:00");
		return aispConsent;
	}
	
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}
}
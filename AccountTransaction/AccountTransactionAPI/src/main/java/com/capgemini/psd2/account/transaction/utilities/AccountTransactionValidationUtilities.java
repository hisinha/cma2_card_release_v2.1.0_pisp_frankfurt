package com.capgemini.psd2.account.transaction.utilities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@Component
public class AccountTransactionValidationUtilities {

	@Value("${app.minPageSize}")
	private String minPageSize;

	@Value("${app.maxPageSize}")
	private String maxPageSize;

	public void validateParams( Integer pageNumber, Integer pageSize) {

		if ((pageNumber != null) && (pageNumber.intValue() < 0))
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,ErrorMapKeys.VALIDATION_ERROR));
		if ((pageSize != null) && ((pageSize.intValue() < Integer.valueOf(minPageSize))
				|| (pageSize.intValue() > Integer.valueOf(maxPageSize))))
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,ErrorMapKeys.VALIDATION_ERROR));
	}
}

package com.capgemini.psd2.account.transaction.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.account.transaction.mongo.db.adapter.impl.AccountTransactionMongoDbAdaptorImpl;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;

@Configuration
public class AccountTransactionMongoDBAdapterConfig {

	@Bean(name = "accountTransactionMongoDbAdapter")
	public AccountTransactionAdapter accountTransactionMongoDbAdapter(){
		return new AccountTransactionMongoDbAdaptorImpl();
	}
}

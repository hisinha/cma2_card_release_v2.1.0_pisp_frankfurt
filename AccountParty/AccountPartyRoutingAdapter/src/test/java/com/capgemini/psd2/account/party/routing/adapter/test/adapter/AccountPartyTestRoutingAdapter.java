/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.party.routing.adapter.test.adapter;

import java.util.Map;

import com.capgemini.psd2.account.party.routing.adapter.test.mock.data.AccountPartyRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountPartyAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountPartyResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountBalanceTestRoutingAdapter.
 */
public class AccountPartyTestRoutingAdapter implements AccountPartyAdapter{

	@Override
	public PlatformAccountPartyResponse retrieveAccountParty(AccountMapping accountMapping, Map<String, String> params) {
		return AccountPartyRoutingAdapterTestMockData.getMockPartyGETResponse();
	}

	
}

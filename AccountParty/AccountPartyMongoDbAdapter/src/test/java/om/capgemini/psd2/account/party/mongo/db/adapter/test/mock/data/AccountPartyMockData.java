package om.capgemini.psd2.account.party.mongo.db.adapter.test.mock.data;
/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBParty1;
import com.capgemini.psd2.aisp.domain.OBPostalAddress8;
import com.capgemini.psd2.aisp.domain.OBReadParty1;
import com.capgemini.psd2.aisp.domain.OBReadParty1Data;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountPartyCMA2;
import com.capgemini.psd2.logger.LoggerAttribute;

/**
 * The Class AccountBalanceAdapterMockData.
 */
public class AccountPartyMockData {

	public static AccountPartyCMA2 getMockAccountDetails()
	{
		return new AccountPartyCMA2();
	}
	
	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	
	public static OBReadParty1 getMockExpectedPartyResponse()
	{
		OBReadParty1 oBReadParty1 = new OBReadParty1();
		OBReadParty1Data data = new OBReadParty1Data();
		
		/*mockParty.setAccountNumber("11111111");
		mockParty.setAccountNSC("111111");*/
		
		OBParty1 oBParty1 = new OBParty1();
		//List<OBCashBalance1> balanceList = new ArrayList<>();
		
		/*oBParty1.setPartyId("2345");
		oBParty1.setPartyNumber("024");
		oBParty1.setPartyType(OBExternalPartyType1Code.DELEGATE);
		oBParty1.setPhone("+91-8004142576");
		oBParty1.setName("Keli");
		oBParty1.setEmailAddress("keli.34@gmail.com");	
		oBParty1.setMobile("+91-8004142576");*/
		List<OBPostalAddress8> partyList = new ArrayList<>();
		OBPostalAddress8 oBPostalAddress8 = new OBPostalAddress8();
		/*oBPostalAddress8.setAddressLine("KenCircle");
		oBPostalAddress8.setAddressType(OBAddressTypeCode.BUSINESS);
		oBPostalAddress8.setBuildingNumber("502");
		oBPostalAddress8.setCountry("IN");
		oBPostalAddress8.setCountrySubDivision("Atlas");
		oBPostalAddress8.setPostCode("12345");
		oBPostalAddress8.setStreetName("HighStreet");
		oBPostalAddress8.setTownName("DownTown");*/
		
		
		partyList.add(oBPostalAddress8);
		
		oBParty1.addAddressItem(oBPostalAddress8);
		oBParty1.setAddress(partyList);
		oBReadParty1.setData(data);
		data.setParty(oBParty1);
		//data.getParty().addAddressItem(oBPostalAddress8);
		
		return oBReadParty1;
	}
	
	
}

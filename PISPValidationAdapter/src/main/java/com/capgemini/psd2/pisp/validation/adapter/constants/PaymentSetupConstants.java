package com.capgemini.psd2.pisp.validation.adapter.constants;

import java.util.ArrayList;
import java.util.List;

public class PaymentSetupConstants {
	
	public static final String UK_OBIE_IBAN = "UK.OBIE.IBAN";
	public static final String UK_OBIE_SORTCODEACCOUNTNUMBER = "UK.OBIE.SortCodeAccountNumber";
	public static final String UK_OBIE_PAN = "UK.OBIE.PAN";
	public static final String UK_OBIE_PAYM = "UK.OBIE.Paym";
	public static final String UK_OBIE_BBAN = "UK.OBIE.BBAN";
	
	public static final String IBAN = "IBAN";
	public static final String SORTCODEACCOUNTNUMBER = "SortCodeAccountNumber";
	public static final String PAN = "PAN";
	
	public static final List<String> schemeNameList = new ArrayList<>();
	
	public static List<String> prefixToAddSchemeNameList(){
		schemeNameList.add(IBAN);
		schemeNameList.add(SORTCODEACCOUNTNUMBER);
		schemeNameList.add(PAN);	
		return schemeNameList;
	}
	
	
}

package com.capgemini.psd2.product.db.resources.locator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class CollectionNameLocatorImplTest {
	
	@InjectMocks
	private CollectionNameLocatorImpl collectionNameLocatorImpl;
	
	@Before
	public void before() {
		
		MockitoAnnotations.initMocks(this);
		
	}
	
	@Test
	public void getAccountAccessSetupCollectionNameTest(){
		collectionNameLocatorImpl.getAccountAccessSetupCollectionName();
	}
	
	@Test
	public void getAccountAccessSetupCollectionName1Test(){
		collectionNameLocatorImpl.getAccountAccessSetupCollectionName();
	}
	
	@Test
	public void getAispConsentCollectionNameTest(){
		collectionNameLocatorImpl.getAispConsentCollectionName();
	}
	
	@Test
	public void getAispConsentCollectionName1Test(){
		collectionNameLocatorImpl.getAispConsentCollectionName();
	}
	
	@Test
	public void getPaymentSetupCollectionNameTest(){
		collectionNameLocatorImpl.getPaymentSetupCollectionName();
	}
	
	@Test
	public void getPaymentSetupCollectionName1Test(){
		collectionNameLocatorImpl.getPaymentSetupCollectionName();
	}

	@Test
	public void getPaymentSubmissionCollectionNameTest(){
		collectionNameLocatorImpl.getPaymentSubmissionCollectionName();
	}
	
	@Test
	public void getPaymentSubmissionCollectionName1Test(){
		collectionNameLocatorImpl.getPaymentSubmissionCollectionName();
	}
	
	@Test
	public void getFundsSetupCollectionName(){
		collectionNameLocatorImpl.getFundsSetupCollectionName();
	}
	
	@Test
	public void getFundsSetupCollectionName1Test(){
		collectionNameLocatorImpl.getFundsSetupCollectionName();
	}
	
	@Test
	public void getCPNPCollectionName(){
		collectionNameLocatorImpl.getCPNPCollectionName();
	}
	
	@Test
	public void getCPNPCollection1Name(){
		collectionNameLocatorImpl.getCPNPCollectionName();
	}
	
	@Test
	public void getPispConsentCollectionName(){
		collectionNameLocatorImpl.getPispConsentCollectionName();
	}
	
	@Test
	public void getPispConsentCollectionName1(){
		collectionNameLocatorImpl.getPispConsentCollectionName();
	}
	
	@Test
	public void getCispConsentCollectionName(){
		collectionNameLocatorImpl.getCispConsentCollectionName();
	}
	
	@Test
	public void getCispConsentCollection1Name(){
		collectionNameLocatorImpl.getCispConsentCollectionName();
	}
}

package com.capgemini.psd2.cisp.adapter;

import com.capgemini.psd2.cisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;

public interface FundsConfirmationConsentAdapter {

	public OBFundsConfirmationConsentResponse1 createFundsConfirmationConsentPOSTResponse(OBFundsConfirmationConsentDataResponse1 data1);
	
	public OBFundsConfirmationConsentResponse1 getFundsConfirmationConsentPOSTResponse(String consentId);
	
	public void removeFundsConfirmationConsent(String consentId, String cId);

	public OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentResponse(String consentId,
			OBExternalRequestStatus1Code statusEnum);
	
}

package com.capgemini.psd2.cisp.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Specifies the status of consent resource in code form.
 */
public enum OBExternalRequestStatus1Code {
  
  AUTHORISED("Authorised"),
  
  AWAITINGAUTHORISATION("AwaitingAuthorisation"),
  
  REJECTED("Rejected"),
  
  REVOKED("Revoked");

  private String value;

  OBExternalRequestStatus1Code(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBExternalRequestStatus1Code fromValue(String text) {
    for (OBExternalRequestStatus1Code b : OBExternalRequestStatus1Code.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}


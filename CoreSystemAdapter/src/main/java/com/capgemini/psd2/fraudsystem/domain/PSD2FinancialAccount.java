package com.capgemini.psd2.fraudsystem.domain;

import java.util.Map;

public class PSD2FinancialAccount { 

	private String id;
	private String type;
	private String subType;
	private String name;
	private String hashedAccountNumber;
	private String accountHolder;
	private String routingNumber;
	private String currency;
	private Map<String, Object> extension;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHashedAccountNumber() {
		return hashedAccountNumber;
	}
	public void setHashedAccountNumber(String hashedAccountNumber) {
		this.hashedAccountNumber = hashedAccountNumber;
	}
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	public String getRoutingNumber() {
		return routingNumber;
	}
	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}
	public Map<String, Object> getExtension() {
		return extension;
	}
	public void setExtension(Map<String, Object> extension) {
		this.extension = extension;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}

}

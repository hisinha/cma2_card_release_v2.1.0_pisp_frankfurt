package com.capgemini.psd2.fraudsystem.domain;

import java.util.Map;

public class PhoneNumbers {

	private String number;
	private String type;
	private Map<String,String> extension;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, String> getExtension() {
		return extension;
	}

	public void setExtension(Map<String, String> extension) {
		this.extension = extension;
	}
}
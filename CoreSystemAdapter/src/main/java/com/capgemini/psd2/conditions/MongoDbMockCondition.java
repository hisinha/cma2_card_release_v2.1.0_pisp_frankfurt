package com.capgemini.psd2.conditions;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class MongoDbMockCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		if (context.getEnvironment().getProperty("app.mockingEnabled") != null)
			return Boolean.valueOf(context.getEnvironment().getProperty("app.mockingEnabled"));
		return false;
	}
}
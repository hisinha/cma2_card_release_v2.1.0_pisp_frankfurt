package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadProduct2;

public class PlatformAccountProductsResponse {
	
	// CMA2 response
	private OBReadProduct2 obReadProduct2;
	
	// Additional Information
	private Map<String, String> additionalInformation;
	
	public OBReadProduct2 getObReadProduct2() {
		return obReadProduct2;
	}

	public void setoBReadProduct2(OBReadProduct2 obReadProduct2) {
		this.obReadProduct2 = obReadProduct2;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
	
}

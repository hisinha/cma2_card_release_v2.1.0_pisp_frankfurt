package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadAccount2;

public class PlatformAccountInformationResponse {

	// CMA2 response
	private OBReadAccount2 oBReadAccount2;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadAccount2 getoBReadAccount2() {
		return oBReadAccount2;
	}

	public void setoBReadAccount2(OBReadAccount2 oBReadAccount2) {
		this.oBReadAccount2 = oBReadAccount2;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

}

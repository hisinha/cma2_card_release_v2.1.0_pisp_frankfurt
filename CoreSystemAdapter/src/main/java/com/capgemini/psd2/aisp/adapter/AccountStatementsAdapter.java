package com.capgemini.psd2.aisp.adapter;

import java.util.Map;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public interface AccountStatementsAdapter {
	/**
	 * Retrieve account statements.
	 *
	 * @param accountMapping the account mapping
	 * @param params the params
	 * @return the StatementsGETResponse
	 */
	public PlatformAccountStatementsResponse retrieveAccountStatements(AccountMapping accountMapping, Map<String, String> params);
	
	public PlatformAccountStatementsResponse retrieveAccountStatementsByStatementId(AccountMapping accountMapping, Map<String, String> params);

	public PlatformAccountTransactionResponse retrieveAccountTransactionsByStatementsId(AccountMapping accountMapping,
			Map<String, String> params);
	
	public PlatformAccountSatementsFileResponse downloadStatementFileByStatementsId(AccountMapping accountMapping,
			Map<String, String> params);
	
}


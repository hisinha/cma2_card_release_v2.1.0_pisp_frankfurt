package com.capgemini.psd2.aisp.domain;

import java.util.Objects;
import com.capgemini.psd2.aisp.domain.OBStatement1;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OBReadStatement1Data
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-11-01T11:43:19.895+05:30")

public class OBReadStatement1Data   {
  @JsonProperty("Statement")
  @Valid
  private List<OBStatement1> statement = null;

  public OBReadStatement1Data statement(List<OBStatement1> statement) {
    this.statement = statement;
    return this;
  }

  public OBReadStatement1Data addStatementItem(OBStatement1 statementItem) {
    if (this.statement == null) {
      this.statement = new ArrayList<OBStatement1>();
    }
    this.statement.add(statementItem);
    return this;
  }

  /**
   * Provides further details on a statement resource.
   * @return statement
  **/
  @ApiModelProperty(value = "Provides further details on a statement resource.")

  @Valid

  public List<OBStatement1> getStatement() {
    return statement;
  }

  public void setStatement(List<OBStatement1> statement) {
    this.statement = statement;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBReadStatement1Data obReadStatement1Data = (OBReadStatement1Data) o;
    return Objects.equals(this.statement, obReadStatement1Data.statement);
  }

  @Override
  public int hashCode() {
    return Objects.hash(statement);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBReadStatement1Data {\n");
    
    sb.append("    statement: ").append(toIndentedString(statement)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


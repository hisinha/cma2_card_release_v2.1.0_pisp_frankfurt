package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadDirectDebit1;

public class PlatformAccountDirectDebitsResponse {

	// CMA2 response
	private OBReadDirectDebit1 oBReadDirectDebit1;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadDirectDebit1 getoBReadDirectDebit1() {
		return oBReadDirectDebit1;
	}

	public void setoBReadDirectDebit1(OBReadDirectDebit1 oBReadDirectDebit1) {
		this.oBReadDirectDebit1 = oBReadDirectDebit1;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
}

package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStandingOrdersResponse;

public interface AccountStandingOrdersTransformer {
	public <T> PlatformAccountStandingOrdersResponse transformAccountStandingOrders(T source, Map<String, String> params);

}

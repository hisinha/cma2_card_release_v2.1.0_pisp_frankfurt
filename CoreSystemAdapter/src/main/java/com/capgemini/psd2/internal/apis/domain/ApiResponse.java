package com.capgemini.psd2.internal.apis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiResponse {

	
	/** The status code. */
	@JsonProperty("statusCode")
	private String statusCode;
	
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("status")
	private ActionEnum getStatus;
	
	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public ActionEnum getGetStatus() {
		return getStatus;
	}

	public void setGetStatus(ActionEnum getStatus) {
		this.getStatus = getStatus;
	}
	
}

package com.capgemini.psd2.internal.apis.domain;

public class TppStatusDetails {

	private String tppId;
	private Boolean xBlock;
	
	public String getTppId() {
		return tppId;
	}
	public void setTppId(String tppId) {
		this.tppId = tppId;
	}
	public Boolean getxBlock() {
		return xBlock;
	}
	public void setxBlock(Boolean xBlock) {
		this.xBlock = xBlock;
	}
}

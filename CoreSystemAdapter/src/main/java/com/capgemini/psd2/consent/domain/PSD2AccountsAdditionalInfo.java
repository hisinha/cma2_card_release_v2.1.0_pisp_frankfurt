package com.capgemini.psd2.consent.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PSD2AccountsAdditionalInfo {
	
	@JsonProperty("accountDetails")
	private List<PSD2Account> accountdetails;
	
	@JsonProperty("fsHeaders")
	private String headers;


	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public List<PSD2Account> getAccountdetails() {
		return accountdetails;
	}

	public void setAccountdetails(List<PSD2Account> accountdetails) {
		this.accountdetails = accountdetails;
	}

}

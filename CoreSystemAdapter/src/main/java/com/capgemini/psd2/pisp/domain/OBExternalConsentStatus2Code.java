package com.capgemini.psd2.pisp.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Specifies the status of consent resource in code form.
 */
public enum OBExternalConsentStatus2Code {
  
  AUTHORISED("Authorised"),
  
  AWAITINGAUTHORISATION("AwaitingAuthorisation"),
  
  AWAITINGUPLOAD("AwaitingUpload"),
  
  CONSUMED("Consumed"),
  
  REJECTED("Rejected");

  private String value;

  OBExternalConsentStatus2Code(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBExternalConsentStatus2Code fromValue(String text) {
    for (OBExternalConsentStatus2Code b : OBExternalConsentStatus2Code.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}


package com.capgemini.psd2.pisp.stage.domain;

public class CustomFileData {

	private String numberOfTransaction;
	private String fileReference;
	private String numberOfPayees;
	private String localInstrument;
	private String requestedExecutionDateTime;

	public String getNumberOfTransaction() {
		return numberOfTransaction;
	}

	public void setNumberOfTransaction(String numberOfTransaction) {
		this.numberOfTransaction = numberOfTransaction;
	}

	public String getFileReference() {
		return fileReference;
	}

	public void setFileReference(String fileReference) {
		this.fileReference = fileReference;
	}

	public String getNumberOfPayees() {
		return numberOfPayees;
	}

	public void setNumberOfPayees(String numberOfPayees) {
		this.numberOfPayees = numberOfPayees;
	}

	public String getLocalInstrument() {
		return localInstrument;
	}

	public void setLocalInstrument(String localInstrument) {
		this.localInstrument = localInstrument;
	}

	public String getRequestedExecutionDateTime() {
		return requestedExecutionDateTime;
	}

	public void setRequestedExecutionDateTime(String requestedExecutionDateTime) {
		this.requestedExecutionDateTime = requestedExecutionDateTime;
	}

	@Override
	public String toString() {
		return "CustomFileData [numberOfTransaction=" + numberOfTransaction + ", fileReference=" + fileReference
				+ ", numberOfPayees=" + numberOfPayees + ", localInstrument=" + localInstrument
				+ ", requestedExecutionDateTime=" + requestedExecutionDateTime + "]";
	}

}

package com.capgemini.psd2.pisp.enums;

import java.util.Arrays;
import java.util.Optional;

public enum PaymentTypeEnum {
	DOMESTIC_PAY("DomesticPayments"),
	DOMESTIC_SCH_PAY("DomesticScheduledPayments"),
	DOMESTIC_ST_ORD("DomesticStandingOrdersPayments"),
	INTERNATIONAL_PAY("InternationalPayments"),
	INTERNATIONAL_SCH_PAY("InternationalScheduledPayments"),
	INTERNATIONAL_ST_ORD("InternationalStandingOrdersPayments"),
	FILE_PAY("FilePayments");
	
	private String paymentType;

	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	private PaymentTypeEnum(String paymentType) {
		this.paymentType = paymentType;
	} 
	
	public static PaymentTypeEnum locatePaymentTypeEnum(String paymentTypeVal) {
		Optional<PaymentTypeEnum> paymentTypeEnums= Arrays.asList(PaymentTypeEnum.values()).stream().filter(e -> e.getPaymentType().equalsIgnoreCase(paymentTypeVal)).findFirst();
		
		return paymentTypeEnums.isPresent() ? paymentTypeEnums.get() : null;
	}
}

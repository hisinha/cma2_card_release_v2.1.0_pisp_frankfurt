package com.capgemini.psd2.pisp.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface InternationalScheduledPaymentsAdapter {

	// Called from International Payments API to retrieve payment
	public CustomISPaymentsPOSTResponse retrieveStagedISPaymentsResponse(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);

	// Called from International Payments API to process payment
	public GenericPaymentsResponse processISPayments(CustomISPaymentsPOSTRequest customRequest,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params);
}

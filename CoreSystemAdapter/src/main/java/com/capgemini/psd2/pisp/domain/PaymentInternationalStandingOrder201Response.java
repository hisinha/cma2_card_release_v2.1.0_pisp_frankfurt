package com.capgemini.psd2.pisp.domain;

import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class PaymentInternationalStandingOrder201Response extends OBWriteInternationalStandingOrderConsentResponse1 {
	@JsonProperty("Links")
	private PaymentSetupPOSTResponseLinks links = null;

	@JsonProperty("Meta")
	private PaymentSetupPOSTResponseMeta meta = null;

	public PaymentInternationalStandingOrder201Response data(OBWriteInternationalStandingOrderConsentResponse1 data) {
		return this;
	}

	/**
	 * Get data
	 * 
	 * @return data
	 **/
	@ApiModelProperty(value = "")

	public PaymentInternationalStandingOrder201Response links(PaymentSetupPOSTResponseLinks links) {
		this.links = links;
		return this;
	}

	/**
	 * Get links
	 * 
	 * @return links
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public PaymentSetupPOSTResponseLinks getLinks() {
		return links;
	}

	public void setLinks(PaymentSetupPOSTResponseLinks links) {
		this.links = links;
	}

	public PaymentInternationalStandingOrder201Response meta(PaymentSetupPOSTResponseMeta meta) {
		this.meta = meta;
		return this;
	}

	/**
	 * Get meta
	 * 
	 * @return meta
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public PaymentSetupPOSTResponseMeta getMeta() {
		return meta;
	}

	public void setMeta(PaymentSetupPOSTResponseMeta meta) {
		this.meta = meta;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PaymentInternationalStandingOrder201Response paymentSubmitPOST201Response = (PaymentInternationalStandingOrder201Response) o;
		return Objects.equals(this.getData(), paymentSubmitPOST201Response.getData())
				&& Objects.equals(this.links, paymentSubmitPOST201Response.links)
				&& Objects.equals(this.getRisk(), paymentSubmitPOST201Response.getRisk())
				&& Objects.equals(this.meta, paymentSubmitPOST201Response.meta);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getData(), links, meta);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class PaymentSubmitPOST201Response {\n");
		sb.append("    data:").append(toIndentedString(this.getData())).append("\n");
		sb.append("    risk: ").append(toIndentedString(this.getRisk())).append("\n");
		sb.append("    links: ").append(toIndentedString(links)).append("\n");
		sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

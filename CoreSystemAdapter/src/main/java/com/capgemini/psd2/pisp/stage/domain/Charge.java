package com.capgemini.psd2.pisp.stage.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class Charge {

	@JsonProperty("ChargeBearer")
	private OBChargeBearerType1Code chargeBearer = null;

	@JsonProperty("Type")
	private String type = null;

	@JsonProperty("Amount")
	private AmountDetails amount = null;

	public Charge chargeBearer(OBChargeBearerType1Code chargeBearer) {
		this.chargeBearer = chargeBearer;
		return this;
	}

	/**
	 * Get chargeBearer
	 * 
	 * @return chargeBearer
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Valid

	public OBChargeBearerType1Code getChargeBearer() {
		return chargeBearer;
	}

	public void setChargeBearer(OBChargeBearerType1Code chargeBearer) {
		this.chargeBearer = chargeBearer;
	}

	public Charge type(String type) {
		this.type = type;
		return this;
	}

	/**
	 * Charge type, in a coded form.
	 * 
	 * @return type
	 **/
	@ApiModelProperty(required = true, value = "Charge type, in a coded form.")
	@NotNull

	@Size(min = 1, max = 40)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Charge amount(AmountDetails amount) {
		this.amount = amount;
		return this;
	}

	/**
	 * Get amount
	 * 
	 * @return amount
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Valid

	public AmountDetails getAmount() {
		return amount;
	}

	public void setAmount(AmountDetails amount) {
		this.amount = amount;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Charge Charge = (Charge) o;
		return Objects.equals(this.chargeBearer, Charge.chargeBearer) && Objects.equals(this.type, Charge.type)
				&& Objects.equals(this.amount, Charge.amount);
	}

	@Override
	public int hashCode() {
		return Objects.hash(chargeBearer, type, amount);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Charge {\n");

		sb.append("    chargeBearer: ").append(toIndentedString(chargeBearer)).append("\n");
		sb.append("    type: ").append(toIndentedString(type)).append("\n");
		sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}

package com.capgemini.psd2.pisp.domain;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Error
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-10T11:44:00.143+05:30")

public class Error   {
  @JsonProperty("ErrorCode")
  private String errorCode = null;

  @JsonProperty("Message")
  private String message = null;

  @JsonProperty("Path")
  private String path = null;

  @JsonProperty("Url")
  private String url = null;

  public Error errorCode(String errorCode) {
    this.errorCode = errorCode;
    return this;
  }

  /**
   * Textual error code
   * @return errorCode
  **/
  @ApiModelProperty(required = true, value = "Textual error code")
  @NotNull

@Size(min=1,max=40) 
  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public Error message(String message) {
    this.message = message;
    return this;
  }

  /**
   * Error message with specific reference to the cause of the problem, e.g., MISSING_AMOUNT_CURRENCY
   * @return message
  **/
  @ApiModelProperty(required = true, value = "Error message with specific reference to the cause of the problem, e.g., MISSING_AMOUNT_CURRENCY")
  @NotNull

@Size(min=1,max=256) 
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Error path(String path) {
    this.path = path;
    return this;
  }

  /**
   * Optionally, reference to the field with error, e.g., /Data/Initiation/InstructedAmount/Currency
   * @return path
  **/
  @ApiModelProperty(value = "Optionally, reference to the field with error, e.g., /Data/Initiation/InstructedAmount/Currency")

@Size(min=1,max=256) 
  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Error url(String url) {
    this.url = url;
    return this;
  }

  /**
   * Optionally, a URL to help remediate the problem, to API Reference, or help etc
   * @return url
  **/
  @ApiModelProperty(value = "Optionally, a URL to help remediate the problem, to API Reference, or help etc")

@Size(min=1,max=256) 
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Error error = (Error) o;
    return Objects.equals(this.errorCode, error.errorCode) &&
        Objects.equals(this.message, error.message) &&
        Objects.equals(this.path, error.path) &&
        Objects.equals(this.url, error.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(errorCode, message, path, url);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Error {\n");
    
    sb.append("    errorCode: ").append(toIndentedString(errorCode)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    path: ").append(toIndentedString(path)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package com.capgemini.psd2.pisp.domain;

public class CustomDScheduledPaymentConsentsPOSTRequest extends OBWriteDomesticScheduledConsent1 {

	private OBExternalConsentStatus1Code paymentConsentStatus;
	private String paymentConsentStatusUpdateDateTime;
	/**
	 * @return the paymentConsentStatus
	 */
	public OBExternalConsentStatus1Code getPaymentConsentStatus() {
		return paymentConsentStatus;
	}
	/**
	 * @param paymentConsentStatus the paymentConsentStatus to set
	 */
	public void setPaymentConsentStatus(OBExternalConsentStatus1Code paymentConsentStatus) {
		this.paymentConsentStatus = paymentConsentStatus;
	}
	/**
	 * @return the paymentConsentStatusUpdateDateTime
	 */
	public String getPaymentConsentStatusUpdateDateTime() {
		return paymentConsentStatusUpdateDateTime;
	}
	/**
	 * @param paymentConsentStatusUpdateDateTime the paymentConsentStatusUpdateDateTime to set
	 */
	public void setPaymentConsentStatusUpdateDateTime(String paymentConsentStatusUpdateDateTime) {
		this.paymentConsentStatusUpdateDateTime = paymentConsentStatusUpdateDateTime;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomDScheduledPaymentConsentsPOSTRequest [paymentConsentStatus=" + paymentConsentStatus
				+ ", paymentConsentStatusUpdateDateTime=" + paymentConsentStatusUpdateDateTime + "]";
	}
}

package com.capgemini.psd2.pisp.stage.operations.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;



public interface DomesticScheduledPaymentStagingAdapter {

		//Called from Domestic Consents API
		public GenericPaymentConsentResponse processDomesticScheduledPaymentConsents(CustomDSPConsentsPOSTRequest domesticScheduledPaymentRequest, CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,OBExternalConsentStatus1Code successStatus,OBExternalConsentStatus1Code failureStatus);
		
		//Called from Domestic Consents API, Submission API, Consent Application
		public CustomDSPConsentsPOSTResponse retrieveStagedDomesticScheduledPaymentConsents(CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);		 
		
}

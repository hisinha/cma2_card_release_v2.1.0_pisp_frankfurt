package com.capgemini.psd2.pisp.stage.operations.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface DomesticStandingOrdersPaymentStagingAdapter {

	// createStage
	// Called from Domestic Consents API
	public CustomDStandingOrderConsentsPOSTResponse processStandingOrderConsents(
			CustomDStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params, OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus);

	// retrieveStage
	// Called from Domestic Consents API, Submission API, Consent Application
	public CustomDStandingOrderConsentsPOSTResponse retrieveStagedDomesticStandingOrdersConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);

}

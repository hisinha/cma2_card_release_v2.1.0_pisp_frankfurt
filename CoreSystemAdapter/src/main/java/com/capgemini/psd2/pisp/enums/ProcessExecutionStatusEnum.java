package com.capgemini.psd2.pisp.enums;

public enum ProcessExecutionStatusEnum {
	PASS("PASS"), 
	FAIL("FAIL"), 
	INCOMPLETE("INCOMPLETE"), 
	ABORT("ABORT");

	private String value;

	ProcessExecutionStatusEnum(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	private ProcessExecutionStatusEnum processExecutionStatus = null;

	public ProcessExecutionStatusEnum getProcessExecutionStatus() {
		return processExecutionStatus;
	}

}

package com.capgemini.psd2.pisp.domain;

import java.util.Objects;

import javax.validation.Valid;

import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * PaymentSubmitPOST201Response
 */
public class PaymentInternationalSubmitPOST201Response extends OBWriteInternationalResponse1 {

	@JsonProperty("Links")
	private PaymentSetupPOSTResponseLinks links = null;

	@JsonProperty("Meta")
	private PaymentSetupPOSTResponseMeta meta = null;

	// Added manually for cma3 design
	private ProcessExecutionStatusEnum processExecutionStatus = null;

	public PaymentInternationalSubmitPOST201Response data(OBWriteDataInternationalConsentResponse1 data) {
		return this;
	}

	/**
	 * Get data
	 * 
	 * @return data
	 **/
	@ApiModelProperty(value = "")

	public PaymentInternationalSubmitPOST201Response links(PaymentSetupPOSTResponseLinks links) {
		this.links = links;
		return this;
	}

	/**
	 * Get links
	 * 
	 * @return links
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public PaymentSetupPOSTResponseLinks getLinks() {
		return links;
	}

	public void setLinks(PaymentSetupPOSTResponseLinks links) {
		this.links = links;
	}

	public PaymentInternationalSubmitPOST201Response meta(PaymentSetupPOSTResponseMeta meta) {
		this.meta = meta;
		return this;
	}

	/**
	 * Get meta
	 * 
	 * @return meta
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public PaymentSetupPOSTResponseMeta getMeta() {
		return meta;
	}

	public void setMeta(PaymentSetupPOSTResponseMeta meta) {
		this.meta = meta;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PaymentInternationalSubmitPOST201Response paymentSubmitPOST201Response = (PaymentInternationalSubmitPOST201Response) o;
		return Objects.equals(this.getData(), paymentSubmitPOST201Response.getData())
				&& Objects.equals(this.links, paymentSubmitPOST201Response.links)
				&& Objects.equals(this.meta, paymentSubmitPOST201Response.meta);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getData(), links, meta);
	}

	@Override
	public String toString() {
		return "PaymentInternationalSubmitPOST201Response [links=" + links + ", meta=" + meta
				+ ", processExecutionStatus=" + processExecutionStatus + "]";
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	@JsonIgnore
	public ProcessExecutionStatusEnum getProcessExecutionStatus() {
		return processExecutionStatus;
	}

	public void setProcessExecutionStatus(ProcessExecutionStatusEnum processExecutionStatus) {
		this.processExecutionStatus = processExecutionStatus;
	}
}

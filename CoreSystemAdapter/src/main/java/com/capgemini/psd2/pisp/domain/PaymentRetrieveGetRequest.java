package com.capgemini.psd2.pisp.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;

public class PaymentRetrieveGetRequest {

	private String consentId;
	
	private PaymentTypeEnum paymentTypeEnum;
		
	@NotNull
	@Size(min=1,max=128)
	public String getConsentId() {
		return consentId;
	}
	public void setConsentId(String consentId) {
		this.consentId = consentId;
	}
	public PaymentTypeEnum getPaymentTypeEnum() {
		return paymentTypeEnum;
	}
	public void setPaymentTypeEnum(PaymentTypeEnum paymentTypeEnum) {
		this.paymentTypeEnum = paymentTypeEnum;
	}
	
	
}

package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "filePaymentsSetupFoundationResources")
public class CustomFilePaymentsPOSTResponse extends OBWriteFileResponse1 {

	@Id
	private String id;
	/*
	 * private String paymentId; private OBTransactionIndividualStatus1Code
	 * paymentStatus; private String paymentSubmissionStatusUpdateDateTime;
	 */
	
	// Added manually for cma3 design
	private ProcessExecutionStatusEnum processExecutionStatus = null;

	public CustomFilePaymentsPOSTResponse data(OBWriteFileConsentResponse1 data) {
		return this;
	}
	
	@JsonIgnore
	public ProcessExecutionStatusEnum getProcessExecutionStatus() {
		return processExecutionStatus;
	}

	public void setProcessExecutionStatus(ProcessExecutionStatusEnum processExecutionStatus) {
		this.processExecutionStatus = processExecutionStatus;
	}

	@JsonIgnore
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "CustomFilePaymentsPOSTResponse [id=" + id + ", processExecutionStatus=" + processExecutionStatus + "]";
	}

	
}

package com.capgemini.psd2.helper;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.config.PFClientRegConfig;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mockdata.DynamicClientMockData;
import com.capgemini.psd2.mockdata.TppPortalMockData;
import com.capgemini.tpp.dtos.GrantScopes;
import com.capgemini.tpp.ldap.client.model.ClientModel;
import com.capgemini.tpp.ssa.model.SSAModel;

@RunWith(SpringJUnit4ClassRunner.class)
public class TppSoftwareHelperTest {

	@InjectMocks
	private TppSoftwareHelper helper;
	
	@Mock
	private RequestHeaderAttributes attributes;
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void test() {
		helper.init();
	}
	
	@Test
	@Ignore
	public void populateClientRequest()
	{
		SSAModel ssaElements=DynamicClientMockData.setSSAModel();
		String secret="";
		PFClientRegConfig pfClientRegConfig=new PFClientRegConfig();
		pfClientRegConfig
				.setClientByIdUrl("https://LIN51003588.corp.capgemini.com:9999/pf-admin-api/v1/oauth/clients/{clientId}");
		pfClientRegConfig.setClientsUrl("https://LIN51003588.corp.capgemini.com:9999/pf-admin-api/v1/oauth/clients");
		//pfClientRegConfig.setAdminuser("ApiAdmin");
		//pfClientRegConfig.setAdminpwd("Test@2017");
		pfClientRegConfig.getAdminuser().put("Admin", "ApiAdmin");
		pfClientRegConfig.getAdminpwdV2().put("pwd", "Test@2017");
		pfClientRegConfig.getAdminpwd("pwd");
		pfClientRegConfig.getAdminuser("Admin");
		pfClientRegConfig.getTokenMgrRefId().put("BOIUK","INTREFATM");
		pfClientRegConfig.setCaImportUrl("https://test.com");
		pfClientRegConfig.setReplicateUrl("https://replicateurl");
		pfClientRegConfig.getPrefix().put("PSD2", "https://localhost:8000/123455");
		pfClientRegConfig.getPrefix("PSD2");
		GrantScopes value=new GrantScopes();
		List<String> scopes=new ArrayList<String>();
		scopes.add("accounts");
	//	scopes.add("payments");
		value.setScopes(scopes);
		List<String> granttypes=new ArrayList<>();
		granttypes.add("client_credentials");
		granttypes.add("authorization_code");
		value.setGranttypes(granttypes);
		pfClientRegConfig.getGrantsandscopes().put("aisp", value);
		
		
		GrantScopes value1=new GrantScopes();
		List<String> scopes1=new ArrayList<String>();
		scopes1.add("payments");
		value1.setScopes(scopes);
		List<String> granttypes1=new ArrayList<>();
		granttypes1.add("client_credentials");
		granttypes1.add("authorization_code");
		value1.setGranttypes(granttypes1);
		
		pfClientRegConfig.getGrantsandscopes().put("pisp", value1);
		
		TppSoftwareHelper.populateClientRequest(ssaElements, pfClientRegConfig, secret);
		ClientModel clientModel=TppPortalMockData.getClientModel();
		TppSoftwareHelper.populateClientRequestForClientSecret(clientModel, pfClientRegConfig, secret);
	}
	
	
}

package com.capgemini.psd2.customer.account.profile.mock.foundationservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.Account;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.AccountEntitlements;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.Brand;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.BrandCode;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.DigitalUser;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.DigitalUserProfile;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.repository.CustomerAccountInformationRepository;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.service.CustomerAccountInformationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;

@Service
public class CustomerAccountInformationServiceImpl implements CustomerAccountInformationService {
	@Autowired
	CustomerAccountInformationRepository customerAccountInformationRepository;
	
	@Override
	public DigitalUserProfile retrieveCustomerAccountInformation(String id) throws Exception {
	
		DigitalUserProfile digitalUserProfile = customerAccountInformationRepository.findByDigitalUserProfileID(id);
		/*DigitalUserProfile digitalUserProfile= new DigitalUserProfile();
		digitalUserProfile.setDigitalUserProfileID("12345678");
		DigitalUser digitalUser= new DigitalUser();
		List<AccountEntitlements> list = new ArrayList<AccountEntitlements>();
		AccountEntitlements accountEntitlements= new AccountEntitlements();
		Account account= new Account();
		Brand accountBrand= new Brand();
		accountBrand.setBrandCode(BrandCode.BOI_ROI);
		account.setAccountBrand(accountBrand);
		accountEntitlements.setAccount(account);
		digitalUserProfile.setAccountEntitlements(list);
		list.add(accountEntitlements);
		digitalUserProfile.setDigitalUser(digitalUser);
		digitalUserProfile.setDigitalUser(digitalUser);
		customerAccountInformationRepository.save(digitalUserProfile);*/
		if(digitalUserProfile == null)
		{
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.NO_ACOUNT_FOUND_PPR_DUP);
		}
		return digitalUserProfile;

	}

}

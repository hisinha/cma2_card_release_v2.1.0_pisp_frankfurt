package com.capgemini.psd2.authentication.application.mock.foundationservice.service;

import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.AuthenticationRequest;
import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.DigitalUserRequest;

public interface AuthenticationApplicationService {

	public DigitalUserRequest retrieveBOLUserCredentials(DigitalUserRequest digitalId);
	
	public AuthenticationRequest retrieveB365UserCredentials(String userName, String password);

}

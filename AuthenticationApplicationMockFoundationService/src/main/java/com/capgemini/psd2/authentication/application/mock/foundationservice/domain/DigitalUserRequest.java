package com.capgemini.psd2.authentication.application.mock.foundationservice.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class DigitalUserRequest {

	private DigitalUser digitalUser;
	
	 String digitalId;
	  
	  public String getDigitalId() {
		return digitalId;
	}

	public void setDigitalId(String digitalId) {
		this.digitalId = digitalId;
	}
	
	public DigitalUser getDigitalUser() {
		return digitalUser;
	}
	public void setDigitalUser(DigitalUser digitalUser) {
		this.digitalUser = digitalUser;
	}
	
}

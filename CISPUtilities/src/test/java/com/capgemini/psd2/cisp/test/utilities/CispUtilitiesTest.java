package com.capgemini.psd2.cisp.test.utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.cisp.utilities.CispUtilities;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class CispUtilitiesTest {
	
	@Before
	public void setUp() {
		Map<String,String> genericErrorMessages=new HashMap<>();
		Map<String,String> specificErrorMessages=new HashMap<>();
		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);
	}

	
	@Test
    public void testConstructorIsPrivate() throws Exception {
      Constructor<CispUtilities> constructor = CispUtilities.class.getDeclaredConstructor();
      assertTrue(Modifier.isPrivate(constructor.getModifiers()));
      constructor.setAccessible(true);
      constructor.newInstance();
    }
	/*
	 * Test with not null value
	 */
	@Test
	public void testIsEmptyNotNull(){
		String str = "Test";
		assertNotEquals(true, CispUtilities.isEmpty(str));
	}
	
	/*
	 * Test with null value
	 */
	@Test
	public void testIsEmptyNull(){
		String str = null;
		assertEquals(true, CispUtilities.isEmpty(str));
	}
	
	/*
	 * Test with empty string
	 */
	@Test
	public void testIsEmptyString(){
		String str = "";
		assertEquals(true, CispUtilities.isEmpty(str));
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetMilliSecondsNullException(){
		String duration = null;
		CispUtilities.getMilliSeconds(duration);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetMilliSecondsInvalidException(){
		String duration = "ABC";
		CispUtilities.getMilliSeconds(duration);
	}
	
	@Test
	public void testGetMilliSecondsPositive(){
		String[] inputData = {"24H","24M","24S"};
		for(String duration: inputData)
			CispUtilities.getMilliSeconds(duration);
	}
	
	@Test
	public void testGetObjectMapper(){
		CispUtilities.getObjectMapper();		
	}

	@Test
	public void testGetCurrentDateInISOFormat(){
		CispUtilities.getCurrentDateInISOFormat();		
	}
	
	@Test
	public void testGetTemporalAccessorInISOFormat(){
		TemporalAccessor i=ZonedDateTime.now().plusDays(1);
		CispUtilities.getTemporalAccessorInISOFormat(i);		
	}
	
	@Test(expected = NullPointerException.class)
	public void testGetTemporalAccessorInISOFormatNull(){
		TemporalAccessor i=null;
		CispUtilities.getTemporalAccessorInISOFormat(i);		
	}
	
	@Test
	public void testPopulateLinks(){
		String methodType = "POST";
		String selfUrl = "www.localtesting.com/funds-confirmation-consents/d73cc881-bae2-4b70-9a11-18a95d1b1fff";
		String id = "2";
		assertNotNull(CispUtilities.populateLinks(id, methodType, selfUrl));
	}
	 
}

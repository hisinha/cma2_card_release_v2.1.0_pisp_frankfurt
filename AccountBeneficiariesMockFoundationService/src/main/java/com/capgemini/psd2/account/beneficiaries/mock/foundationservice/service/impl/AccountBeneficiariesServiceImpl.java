package com.capgemini.psd2.account.beneficiaries.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.repository.AccountBeneficiariesRepository;
import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.service.AccountBeneficiariesService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;

@Service
public class AccountBeneficiariesServiceImpl implements AccountBeneficiariesService {

	@Autowired
	AccountBeneficiariesRepository repository;

	@Override
	public PartiesPaymentBeneficiariesresponse getBeneficiaries(String userId) {
		PartiesPaymentBeneficiariesresponse beneficiaries = repository.findByUserId(userId);
		/*
		 * PartiesPaymentBeneficiariesresponse beneficiaries= new
		 * PartiesPaymentBeneficiariesresponse(); beneficiaries.setTotal(2.0);
		 * beneficiaries.setUserId("1234"); repository.save(beneficiaries);
		 */
		if (null == beneficiaries) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.ACCOUNT_NOT_FOUND_PPR_CPA);
		} else {
			return beneficiaries;
		}
	}
}

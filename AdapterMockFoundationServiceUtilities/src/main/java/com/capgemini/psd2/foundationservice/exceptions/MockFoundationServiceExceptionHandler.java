
package com.capgemini.psd2.foundationservice.exceptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.capgemini.psd2.foundationservice.utilities.MockServiceUtility;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;

@ControllerAdvice
public class MockFoundationServiceExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MockServiceUtility mockServiceUtility;
	
	@ExceptionHandler({ Exception.class })
	public final ResponseEntity<?> handleInvalidRequest(Exception ex, WebRequest request) {

		HttpStatus status = null;
		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation validationViolation = null;
		AuthenticationResponse authenticationResponse = null;
		
		if (ex != null && ex instanceof MockFoundationServiceException) {
			validationViolation = new ValidationViolation();
			validationViolation = ((MockFoundationServiceException) ex).getErrorInfo();
			status = HttpStatus.valueOf(Integer.parseInt(validationViolation.getErrorValue()));
		}
		
		if (ex != null && ex instanceof MockFoundationServiceAuthException) {
			authenticationResponse = new AuthenticationResponse();
			authenticationResponse = ((MockFoundationServiceAuthException) ex).getErrorInfo();
			if(authenticationResponse.getResponseCode().equals("FS_AUTH_001"))
				status= HttpStatus.BAD_REQUEST;
			else if(authenticationResponse.getResponseCode().equals("FS_AUTH_002"))
				status= HttpStatus.BAD_REQUEST;
			else if(authenticationResponse.getResponseCode().equals("FS_AUTH_003"))
				status= HttpStatus.BAD_REQUEST;
			else if(authenticationResponse.getResponseCode().equals("FS_AUTH_004"))
				status= HttpStatus.BAD_REQUEST;
			else if(authenticationResponse.getResponseCode().equals("FS_AUTH_005"))
				status= HttpStatus.BAD_REQUEST;
			else if(authenticationResponse.getResponseCode().equals("FS_AUTH_006"))
				status= HttpStatus.BAD_REQUEST;
			else if(authenticationResponse.getResponseCode().equals("FS_AUTH_007"))
				status= HttpStatus.BAD_REQUEST;
			else if(authenticationResponse.getResponseCode().equals("FS_AUTH_008"))
				status= HttpStatus.UNAUTHORIZED;
			else if(authenticationResponse.getResponseCode().equals("FS_AUTH_010"))
				status= HttpStatus.BAD_REQUEST;
			else {
				authenticationResponse = new AuthenticationResponse();
				authenticationResponse.setResponseCode("FS_AUTH_009");
				authenticationResponse.setResponseMessage("Technical Error. Please try again later");
				status= HttpStatus.INTERNAL_SERVER_ERROR;
			}
			return new ResponseEntity<AuthenticationResponse>(authenticationResponse, status);
		}
		
		if (request instanceof ServletWebRequest && ((ServletWebRequest)request).getRequest().getRequestURI().contains("/fs-authentication-service/services/")) {
		authenticationResponse = new AuthenticationResponse();
		authenticationResponse.setResponseCode("FS_AUTH_009");
		authenticationResponse.setResponseMessage("Technical Error. Please try again later");
		status= HttpStatus.INTERNAL_SERVER_ERROR;
		return new ResponseEntity<AuthenticationResponse>(authenticationResponse, status);
		}
		
		if (validationViolation == null) {
			
			validationViolation = new ValidationViolation();
			String defaultError = mockServiceUtility.getErrorCode().get("defaultError");
			defaultError = NullCheckUtils.isNullOrEmpty(defaultError) ? "Technical_Error" : defaultError;
			validationViolation.setErrorCode(defaultError);
			validationViolation.setErrorText("Technical Error. Please try again later");
			validationViolation.setErrorField("Internal Server Error");
			validationViolation.setErrorValue(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		validationViolations.getValidationViolation().add(validationViolation);

		return new ResponseEntity<ValidationViolations>(validationViolations, status);

	}
}

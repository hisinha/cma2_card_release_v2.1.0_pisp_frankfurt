package com.capgemini.psd2.foundationservice.raml.exceptions;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ReplayIntervalType {
    
	IMMEDIATE("Immediate"),

    DELAYED_1("Delayed_1"),

    DELAYED_15("Delayed_15"),

    DELAYEDCUSTOM("Delayed_Custom");

	private String value;

	ReplayIntervalType(String value) {
	    this.value = value;
	   }

	  @Override
	  @JsonValue
	  public String toString() {
	    return String.valueOf(value);
	  }

	  @JsonCreator
	  public static ReplayIntervalType fromValue(String text) {
	    for (ReplayIntervalType b : ReplayIntervalType.values()) {
	      if (String.valueOf(b.value).equals(text)) {
	        return b;
	      }
	    }
	    return null;
	  }

  
}

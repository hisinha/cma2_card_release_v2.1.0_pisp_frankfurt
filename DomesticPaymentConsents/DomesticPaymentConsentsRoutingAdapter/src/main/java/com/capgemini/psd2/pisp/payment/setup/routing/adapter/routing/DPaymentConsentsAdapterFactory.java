package com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing;

import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;

@FunctionalInterface
public interface DPaymentConsentsAdapterFactory {

	public DomesticPaymentStagingAdapter getDomesticPaymentSetupStagingInstance(String coreSystemName);

}

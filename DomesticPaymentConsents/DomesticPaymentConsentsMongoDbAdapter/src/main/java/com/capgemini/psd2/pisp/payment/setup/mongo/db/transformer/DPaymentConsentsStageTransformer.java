package com.capgemini.psd2.pisp.payment.setup.mongo.db.transformer;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.domain.CustomDomesticPaymentSetupCma1POSTResponse;
import com.capgemini.psd2.pisp.status.PaymentStatusCompatibilityMapEnum;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DPaymentConsentsStageTransformer {

	public CustomDPaymentConsentsPOSTResponse transformCma1ToCma3Response(
			CustomDomesticPaymentSetupCma1POSTResponse paymentCma1BankResource) {
		if (NullCheckUtils.isNullOrEmpty(paymentCma1BankResource)
				|| NullCheckUtils.isNullOrEmpty(paymentCma1BankResource.getData())) {
			return null;
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentCma1BankResource.getData().getStatus())) {
			String setupCompatibleStatus = PaymentStatusCompatibilityMapEnum
					.valueOf(paymentCma1BankResource.getData().getStatus().toUpperCase()).getCompatibleStatus();
			paymentCma1BankResource.getData().setStatus(setupCompatibleStatus);
		}

		String paymentRequest = JSONUtilities.getJSONOutPutFromObject(paymentCma1BankResource);
		try {
			CustomDPaymentConsentsPOSTResponse bankResponse = JSONUtilities.getObjectFromJSONString(
					PispUtilities.getObjectMapper(), paymentRequest, CustomDPaymentConsentsPOSTResponse.class);
			bankResponse.getData().setConsentId(paymentCma1BankResource.getData().getPaymentId());

			return bankResponse;
		} catch (PSD2Exception pe) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INCOMPATIBLE_SETUP_FOUND_IN_SYSTEM);
		}
	}
}

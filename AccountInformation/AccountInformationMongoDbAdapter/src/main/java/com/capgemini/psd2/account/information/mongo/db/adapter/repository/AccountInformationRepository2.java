package com.capgemini.psd2.account.information.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountInformationCMA2;

public interface AccountInformationRepository2 extends MongoRepository<AccountInformationCMA2, String>{
	
	public List<AccountInformationCMA2> findByPsuId(String psuId);
	
	public List<AccountInformationCMA2> findByAccountNumberAndNsc(String accountNumber,String nsc);

}

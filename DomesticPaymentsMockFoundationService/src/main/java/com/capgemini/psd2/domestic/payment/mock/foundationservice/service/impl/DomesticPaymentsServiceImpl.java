/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.domestic.payment.mock.foundationservice.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.domestic.payment.mock.foundationservice.constants.DomesticPaymentSubmissionFoundationConstants;
import com.capgemini.psd2.domestic.payment.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.payment.mock.foundationservice.repository.DomesticPaymentsRepository;
import com.capgemini.psd2.domestic.payment.mock.foundationservice.repository.PaymentRetrievalRepository;
import com.capgemini.psd2.domestic.payment.mock.foundationservice.service.DomesticPaymentsService;
import com.capgemini.psd2.foundationservice.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.pisp.transformer.PaymentFoundationServiceTransformer;
import com.capgemini.psd2.foundationservice.pisp3.domain.Amount;
import com.capgemini.psd2.foundationservice.pisp3.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.pisp3.domain.Currency;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstruction1;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstructionProposalComposite;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstructionStatusCode;

@Service
public class DomesticPaymentsServiceImpl implements DomesticPaymentsService{
	
	@Autowired
	private DomesticPaymentsRepository domesticPaymentsRepository;
	
	@Autowired
	private PaymentRetrievalRepository paymentRepository;
	
	@Autowired
	private PaymentFoundationServiceTransformer paymentFoundationServiceTransformer;
	
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	@Override
	public PaymentInstructionProposalComposite retrieveDocumentPaymentInformation(String paymentInstruction)
			throws Exception {


		PaymentInstructionProposalComposite paymentInstructionProposalComposite = domesticPaymentsRepository.findByPaymentInstructionPaymentInstructionNumber(paymentInstruction);
		
		if (null == paymentInstructionProposalComposite) {
			com.capgemini.psd2.foundationservice.pisp1.domain.PaymentInstruction returnedPaymentInstruction = paymentRepository.findByPaymentPaymentInformationSubmissionID(paymentInstruction);

			if (null != returnedPaymentInstruction) {
				paymentInstructionProposalComposite = paymentFoundationServiceTransformer.transformPaymentInstructionOrder(returnedPaymentInstruction);
			}
		}	
		if(paymentInstructionProposalComposite==null)
		{
			throw new RecordNotFoundException("Not Found :"+paymentInstruction);
		}

		return paymentInstructionProposalComposite;
	}

	@Override
	public PaymentInstructionProposalComposite createDomesticPaymentSubmissionResource(
			PaymentInstructionProposalComposite domesticPaymentRequest) throws RecordNotFoundException {
		
		PaymentInstruction1 paymentInstruction = new PaymentInstruction1();
		String paymentSubmissionId = null;
		
		if (null == domesticPaymentRequest) {

			throw new RecordNotFoundException(DomesticPaymentSubmissionFoundationConstants.RECORD_NOT_FOUND);
		}
		
		if ((domesticPaymentRequest.getPaymentInstructionProposal().getInstructionEndToEndReference())
				.equalsIgnoreCase(DomesticPaymentSubmissionFoundationConstants.INSTRUCTION_ENDTOEND_REF)) {
			paymentSubmissionId = UUID.randomUUID().toString();
			paymentInstruction.setPaymentInstructionNumber(paymentSubmissionId);
			paymentInstruction.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.AcceptedSettlementInProcess);
			Date date = Calendar.getInstance().getTime();  
			/*DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");  
			String strDate = dateFormat.format(date);*/
			String strDate = timeZoneDateTimeAdapter.parseDateTimeCMA(date);
			paymentInstruction.setInstructionIssueDate(strDate);
//			Date d = new Date();
//			d.setDate(12);
//			d.setMonth(12);
//			d.setYear(2019);
			paymentInstruction.setInstructionStatusUpdateDateTime(strDate);
			List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
			PaymentInstructionCharge charge = new PaymentInstructionCharge();
			charge.setChargeBearer(ChargeBearer.BorneByDebtor);
			charge.setType("UK.OBIE.ChapsOut");
			Amount amount = new Amount();
			amount.setTransactionCurrency(500.0);
			charge.setAmount(amount);
			Currency currency = new Currency();
			currency.setIsoAlphaCode("GBP");
			charge.setCurrency(currency);
			//charges.add(charge);
			//domesticPaymentRequest.getPaymentInstructionProposal().setCharges(charges);
			domesticPaymentRequest.setPaymentInstruction(paymentInstruction);
		}else {
			paymentSubmissionId = UUID.randomUUID().toString();
			paymentInstruction.setPaymentInstructionNumber(paymentSubmissionId);
			paymentInstruction.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.Rejected);
			Date date = Calendar.getInstance().getTime();  
			/*DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");  
			String strDate = dateFormat.format(date);*/
			String strDate = timeZoneDateTimeAdapter.parseDateTimeCMA(date);
			paymentInstruction.setInstructionIssueDate(strDate);
			paymentInstruction.setInstructionStatusUpdateDateTime(strDate);
			List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
			PaymentInstructionCharge charge = new PaymentInstructionCharge();
			charge.setChargeBearer(ChargeBearer.BorneByDebtor);
			charge.setType("UK.OBIE.ChapsOut");
			Amount amount = new Amount();
			amount.setTransactionCurrency(500.0);
			charge.setAmount(amount);
			Currency currency = new Currency();
			currency.setIsoAlphaCode("GBP");
			charge.setCurrency(currency);
			//charges.add(charge);
			//domesticPaymentRequest.getPaymentInstructionProposal().setCharges(charges);
			domesticPaymentRequest.setPaymentInstruction(paymentInstruction);
		}
		
		domesticPaymentsRepository.save(domesticPaymentRequest);
		return domesticPaymentRequest;
	}


}
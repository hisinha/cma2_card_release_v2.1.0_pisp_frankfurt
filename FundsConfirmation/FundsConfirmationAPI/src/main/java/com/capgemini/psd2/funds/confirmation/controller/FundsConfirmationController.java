package com.capgemini.psd2.funds.confirmation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.funds.confirmation.service.FundsConfirmationService;

@RestController
public class FundsConfirmationController {

	@Autowired
	private FundsConfirmationService fundsConfirmationService;	
	
	@RequestMapping(value = "/funds-confirmations", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public OBFundsConfirmationResponse1 createFundsConfirmation(
			@RequestBody OBFundsConfirmation1 fundsConfirmationPOSTRequest) {
		
		return fundsConfirmationService.createFundsConfirmation(fundsConfirmationPOSTRequest);
	}
}

package com.capgemini.psd2.funds.confirmation.service.impl;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationAdapter;
import com.capgemini.psd2.cisp.domain.Links;
import com.capgemini.psd2.cisp.domain.Meta;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.cisp.utilities.CommonCardValidations;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.service.FundsConfirmationService;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class FundsConfirmationServiceImpl implements FundsConfirmationService {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	@Qualifier("fundsConfirmationRoutingAdapter")
	private FundsConfirmationAdapter fundsConfirmationAdapter;

	/** The aisp consent adapter. */
	@Autowired
	private CispConsentAdapter cispConsentAdapter;
	
	@Autowired
	private CommonCardValidations commonCardValidation;

	@Override
	public OBFundsConfirmationResponse1 createFundsConfirmation(OBFundsConfirmation1 fundsConfirmationPOSTRequest) {

		//Custom validation on Request parameters
		commonCardValidation.validateSubmissionRequestParams(fundsConfirmationPOSTRequest);
		
		if (!fundsConfirmationPOSTRequest.getData().getConsentId()
				.equalsIgnoreCase(reqHeaderAtrributes.getToken().getRequestId()))
			throw PSD2Exception.populatePSD2Exception(
					ErrorCodeEnum.CISP_TOKEN_CONSENT_ID_NOT_MATCHED_WITH_CONFIRMATION_CONSENT_ID);

		CispConsent cispConsent = cispConsentAdapter
				.retrieveConsentByFundsIntentId(fundsConfirmationPOSTRequest.getData().getConsentId());

		if (cispConsent == null)
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.INVALID_INTENT_ID));
		
		if (!cispConsent.getConsentId()
				.equalsIgnoreCase(reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCESS_GIVEN_TO_REQUESTED_ACCOUNT);

        AccountDetails accountDetails = cispConsent.getAccountDetails();		
        
        Map<String,String> params=reqHeaderAtrributes.getToken().getSeviceParams();
		params.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId()); 
		params.put(PSD2Constants.CORRELATION_ID, reqHeaderAtrributes.getCorrelationId());
		params.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		
		OBFundsConfirmationResponse1 fundsConfirmationResponse = fundsConfirmationAdapter.getFundsData(accountDetails,
				fundsConfirmationPOSTRequest,params);
		if (NullCheckUtils.isNullOrEmpty(fundsConfirmationResponse))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.FUNDS_DATA_NOT_FOUND);

	    fundsConfirmationResponse.setLinks(new Links());
		fundsConfirmationResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl() + PSD2Constants.SLASH
				+ fundsConfirmationResponse.getData().getFundsConfirmationId());
		
		if(fundsConfirmationResponse.getMeta()==null)
			fundsConfirmationResponse.setMeta(new Meta());

		// swagger validations and custom validations on Response Object.
		commonCardValidation.validateSubmissionResponseParams(fundsConfirmationResponse);
		
		return fundsConfirmationResponse;
    }

}

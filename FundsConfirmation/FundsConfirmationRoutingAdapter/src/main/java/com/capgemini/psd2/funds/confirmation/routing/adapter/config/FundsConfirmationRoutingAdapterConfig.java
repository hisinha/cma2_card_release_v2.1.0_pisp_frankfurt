package com.capgemini.psd2.funds.confirmation.routing.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationAdapter;
import com.capgemini.psd2.funds.confirmation.routing.adapter.impl.FundsConfirmationRoutingAdapter;

/**
 * Hello world!
 *
 */
@Configuration
public class FundsConfirmationRoutingAdapterConfig {

	@Bean
	public FundsConfirmationAdapter fundsConfirmationRoutingAdapter() {
		return new FundsConfirmationRoutingAdapter();
	}

}

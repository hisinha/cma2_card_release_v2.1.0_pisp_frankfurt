package com.capgemini.psd2.funds.confirmation.routing.adapter.test.impl;

import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.funds.confirmation.routing.adapter.impl.FundsConfirmationRoutingAdapter;
import com.capgemini.psd2.funds.confirmation.routing.adapter.routing.FundsConfirmationAdapterFactory;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.Token;

public class FundsConfirmationRoutingAdapterTest {

	@Mock
	private FundsConfirmationAdapterFactory fundsConfirmationConsentAdapterFactory;

	@InjectMocks
	private FundsConfirmationRoutingAdapter fundsConfirmationRoutingAdapter;

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
 
	
	@Test
	public void testGetFundsData() {

		AccountDetails accountDetails = new AccountDetails();
		OBFundsConfirmation1 fundsConfirmationPOSTRequest = new OBFundsConfirmation1();
		FundsConfirmationAdapter fundsConfirmationAdapter = new FundsConfirmationAdapter() {

			
			@Override
			public OBFundsConfirmationResponse1 getFundsData(AccountDetails accountDetails,
					OBFundsConfirmation1 fundsConfirmationPOSTRequest, Map<String, String> params) {
				return null;
			}
		};
		Token token = new Token();
		Map<String, String> map  = new HashMap<>();
		token.setSeviceParams(map);
		Mockito.when(requestHeaderAttributes.getToken()).thenReturn(token);
		Mockito.when(fundsConfirmationConsentAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(fundsConfirmationAdapter);
		fundsConfirmationRoutingAdapter.getFundsData(accountDetails, fundsConfirmationPOSTRequest, new HashMap<>());
	}
	
	@Test
	public void testGetFundsDataNull() {

		AccountDetails accountDetails = new AccountDetails();
		OBFundsConfirmation1 fundsConfirmationPOSTRequest = new OBFundsConfirmation1();
		FundsConfirmationAdapter fundsConfirmationAdapter = new FundsConfirmationAdapter() {

			
			@Override
			public OBFundsConfirmationResponse1 getFundsData(AccountDetails accountDetails,
					OBFundsConfirmation1 fundsConfirmationPOSTRequest, Map<String, String> params) {
				return null;
			}
		};
		Token token = new Token();
		Map<String, String> map  = new HashMap<>();
		token.setSeviceParams(map);
		Mockito.when(requestHeaderAttributes.getToken()).thenReturn(token);
		Mockito.when(fundsConfirmationConsentAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(fundsConfirmationAdapter);
		fundsConfirmationRoutingAdapter.getFundsData(accountDetails, fundsConfirmationPOSTRequest, null);
	}

}

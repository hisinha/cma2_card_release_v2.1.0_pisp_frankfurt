package com.capgemini.psd2.model.qtsp;

import java.util.ArrayList;
import java.util.List;

public class Items {

	List<PFResponse> itemList;

	public List<PFResponse> getItems() {
		if (null == itemList)
			itemList = new ArrayList<>();
		return itemList;
	}

	public void setItems(List<PFResponse> itemList) {
		this.itemList = itemList;
	}
}
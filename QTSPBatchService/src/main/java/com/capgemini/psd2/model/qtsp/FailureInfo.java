package com.capgemini.psd2.model.qtsp;

public class FailureInfo {

	private QTSPResource qtspResource;

	private String instance;

	private String tenant;

	private String failureStage;

	public FailureInfo(QTSPResource qtspResource, String instance, String tenant, String failureStage) {
		this.qtspResource = qtspResource;
		this.instance = instance;
		this.tenant = tenant;
		this.failureStage = failureStage;
	}

	public QTSPResource getQtspResource() {
		return qtspResource;
	}

	public void setQtspResource(QTSPResource qtspResource) {
		this.qtspResource = qtspResource;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public String getTenant() {
		return tenant;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	public String getFailureStage() {
		return failureStage;
	}

	public void setFailureStage(String failureStage) {
		this.failureStage = failureStage;
	}

	@Override
	public String toString() {
		return "FailureInfo [qtspResource=" + qtspResource + ", instance=" + instance + ", tenant=" + tenant
				+ ", failureStage=" + failureStage + "]";
	}
}
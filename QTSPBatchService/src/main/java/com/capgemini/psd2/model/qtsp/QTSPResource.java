package com.capgemini.psd2.model.qtsp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "qtspResource")
public class QTSPResource {

	@JsonProperty("CertificateFingerprint")
	private String certificateFingerprint;

	@JsonProperty("CreateTimestamp")
	private String createTimestamp;

	@JsonProperty("ModifyTimestamp")
	private String modifyTimestamp;

	@JsonProperty("ServiceCountry")
	private String serviceCountry;

	@JsonProperty("ServiceExtensions")
	private List<QTSPServiceExtension> serviceExtensions;

	@JsonProperty("ServiceLanguage")
	private String serviceLanguage;

	@JsonProperty("ServiceName")
	private String serviceName;

	@JsonProperty("ServiceStatus")
	private String serviceStatus;

	@JsonProperty("ServiceType")
	private String serviceType;

	@JsonProperty("StatusStartingTime")
	private String statusStartingTime;

	@JsonProperty("X509Certificate")
	private String x509Certificate;

	@JsonProperty("X509SKI")
	private String x509SKI;

	@JsonProperty("X509SubjectName")
	private String x509SubjectName;

	private String id;

	private Meta meta;

	private List<String> schemas;
	
	private List<String> tenantIds;

	public String getCertificateFingerprint() {
		return certificateFingerprint;
	}

	public void setCertificateFingerprint(String certificateFingerprint) {
		this.certificateFingerprint = certificateFingerprint;
	}

	public String getCreateTimestamp() {
		return createTimestamp;
	}

	public void setCreateTimestamp(String createTimestamp) {
		this.createTimestamp = createTimestamp;
	}

	public String getModifyTimestamp() {
		return modifyTimestamp;
	}

	public void setModifyTimestamp(String modifyTimestamp) {
		this.modifyTimestamp = modifyTimestamp;
	}

	public String getServiceCountry() {
		return serviceCountry;
	}

	public void setServiceCountry(String serviceCountry) {
		this.serviceCountry = serviceCountry;
	}

	public List<QTSPServiceExtension> getServiceExtensions() {
		return serviceExtensions;
	}

	public void setServiceExtensions(List<QTSPServiceExtension> serviceExtensions) {
		this.serviceExtensions = serviceExtensions;
	}

	public String getServiceLanguage() {
		return serviceLanguage;
	}

	public void setServiceLanguage(String serviceLanguage) {
		this.serviceLanguage = serviceLanguage;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceStatus() {
		return serviceStatus;
	}

	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getStatusStartingTime() {
		return statusStartingTime;
	}

	public void setStatusStartingTime(String statusStartingTime) {
		this.statusStartingTime = statusStartingTime;
	}

	public String getX509Certificate() {
		return x509Certificate;
	}

	public void setX509Certificate(String x509Certificate) {
		this.x509Certificate = x509Certificate;
	}

	public String getX509SKI() {
		return x509SKI;
	}

	public void setX509SKI(String x509ski) {
		x509SKI = x509ski;
	}

	public String getX509SubjectName() {
		return x509SubjectName;
	}

	public void setX509SubjectName(String x509SubjectName) {
		this.x509SubjectName = x509SubjectName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Meta getMeta() {
		return meta;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public List<String> getSchemas() {
		return schemas;
	}

	public void setSchemas(List<String> schemas) {
		this.schemas = schemas;
	}

	public List<String> getTenantIds() {
		if (null == tenantIds)
			tenantIds = new ArrayList<>();
		return tenantIds;
	}

	public void addTenantId(String tenantId) {
		if (null == this.tenantIds)
			this.tenantIds = new ArrayList<>();
		this.tenantIds.add(tenantId);
	}

	public void addTenantIds(List<String> tenantIds) {
		if (null == this.tenantIds)
			this.tenantIds = new ArrayList<>();
		this.tenantIds.addAll(tenantIds);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof QTSPResource))
			return false;
		QTSPResource other = (QTSPResource) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "QTSPResource [certificateFingerprint=" + certificateFingerprint + ", x509Certificate=" + x509Certificate
				+ ", serviceStatus=" + serviceStatus + ", location=" + meta.getLocation() + "]";
	}
}
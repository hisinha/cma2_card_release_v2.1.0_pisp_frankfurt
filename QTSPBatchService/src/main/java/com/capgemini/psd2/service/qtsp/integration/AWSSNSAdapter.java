package com.capgemini.psd2.service.qtsp.integration;

import java.util.List;

import com.capgemini.psd2.model.qtsp.FailureInfo;

public interface AWSSNSAdapter {

	void publishFailures(List<FailureInfo> failureList);

}
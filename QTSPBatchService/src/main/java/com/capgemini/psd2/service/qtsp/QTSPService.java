package com.capgemini.psd2.service.qtsp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.service.qtsp.config.PFConfig;
import com.capgemini.psd2.service.qtsp.integration.MongoDbAdapter;
import com.capgemini.psd2.service.qtsp.integration.QTSPServiceRepository;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

@Service
public class QTSPService {

	@Autowired
	private MongoDbAdapter qtspServiceMongoDbAdapter;

	@Autowired
	private QTSPServiceRepository qtspServiceRepository;
	
	@Autowired
	private PFConfig pfConfig;

	private List<QTSPResource> newQTSPs;

	private List<QTSPResource> modifiedQTSPs;

	public List<QTSPResource> getNewQTSPs() {
		return newQTSPs;
	}

	public List<QTSPResource> getModifiedQTSPs() {
		return modifiedQTSPs;
	}

	public Map<String, List<QTSPResource>> processQTSPResponse(List<QTSPResource> qtsps) {
		List<QTSPResource> retrievedQTSPs = retrieveQTSPsFromDB();
		List<QTSPResource> fetchedQTSPs = qtsps;
		if (!fetchedQTSPs.isEmpty())
			return compareQTSPResponse(retrievedQTSPs, fetchedQTSPs);
		return new HashMap<>();
	}

	private List<QTSPResource> retrieveQTSPsFromDB() {
		return qtspServiceMongoDbAdapter.retrieveQTSPs();
	}

	private Map<String, List<QTSPResource>> compareQTSPResponse(List<QTSPResource> retrievedQTSPs,
			List<QTSPResource> fetchedQTSPs) {
		Map<String, List<QTSPResource>> updatedQTSPs = new HashMap<>();
		newQTSPs = fetchedQTSPs.stream().filter(qtsp -> !retrievedQTSPs.contains(qtsp)).collect(Collectors.toList());
		newQTSPs.forEach(qtsp -> qtsp.addTenantIds(getTenantIds()));
		modifiedQTSPs = fetchedQTSPs.stream()
				.filter(qtsp -> retrievedQTSPs.contains(qtsp)
						&& !qtsp.getServiceStatus().equals(getQTSP(retrievedQTSPs, qtsp.getId()).getServiceStatus()))
				.collect(Collectors.toList());
		updatedQTSPs.put(QTSPServiceUtility.NEW_QTSPS, newQTSPs);
		updatedQTSPs.put(QTSPServiceUtility.MODIFIED_QTSPS, modifiedQTSPs);
		return updatedQTSPs;
	}

	private QTSPResource getQTSP(List<QTSPResource> qtsps, String id) {
		for (QTSPResource qtsp : qtsps) {
			if (qtsp.getId().equalsIgnoreCase(id))
				return qtsp;
		}
		return new QTSPResource();
	}

	public QTSPResource findQTSP(String id) {
		return qtspServiceRepository.findOne(id);
	}

	public QTSPResource saveQTSP(QTSPResource qtsp) {
		return qtspServiceRepository.save(qtsp);
	}
	
	private List<String> getTenantIds() {
		List<String> tenantIds = new ArrayList<>();
		if (null != pfConfig.getPrefix())
			tenantIds.addAll(pfConfig.getPrefix().keySet());
		return tenantIds;
	}
}
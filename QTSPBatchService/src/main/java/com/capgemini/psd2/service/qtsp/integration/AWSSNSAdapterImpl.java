package com.capgemini.psd2.service.qtsp.integration;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.capgemini.psd2.model.qtsp.FailureInfo;
import com.capgemini.psd2.service.qtsp.config.AWSSNSConfig;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

@Component
@Configuration
public class AWSSNSAdapterImpl implements AWSSNSAdapter {

	@Autowired
	private AWSSNSConfig awsSNSConfig;

	@Value("${aws.proxyHost:null}")
	private String proxyHost;

	@Value("${aws.proxyPort:0}")
	private String proxyPort;

	@Value("#{T(com.amazonaws.Protocol).valueOf('${aws.protocol:HTTPS}')}")
	private Protocol protocol;

	private static final Logger LOGGER = LoggerFactory.getLogger(AWSSNSAdapterImpl.class);

	@Override
	public void publishFailures(List<FailureInfo> failureList) {

		ClientConfiguration clientConfiguration = new ClientConfiguration();
		clientConfiguration.setProxyHost(proxyHost);
		clientConfiguration.setProxyPort(Integer.valueOf(proxyPort));
		clientConfiguration.setProtocol(protocol);
		AWSCredentialsProvider awsCredentials = new DefaultAWSCredentialsProviderChain();
		AmazonSNS snsClient = AmazonSNSClient.builder().withRegion(awsSNSConfig.getRegion())
				.withClientConfiguration(clientConfiguration).withCredentials(awsCredentials).build();
		String message;
		PublishRequest publishRequest;
		for (FailureInfo failureInfo : failureList) {
			switch (failureInfo.getFailureStage()) {
			case QTSPServiceUtility.PF_FAILURE_STAGE_ADD:
				message = String.format(awsSNSConfig.getPfAdd() + awsSNSConfig.getMessage(),
						failureInfo.getQtspResource().getServiceName(),
						QTSPServiceUtility.parseForNS(failureInfo.getQtspResource().getX509Certificate()),
						failureInfo.getTenant());
				publishRequest = new PublishRequest(awsSNSConfig.getTopicArn(), message, awsSNSConfig.getPfAdd());
				break;
			case QTSPServiceUtility.PF_FAILURE_STAGE_REMOVE:
				message = String.format(awsSNSConfig.getPfRemove() + awsSNSConfig.getMessage(),
						failureInfo.getQtspResource().getServiceName(),
						QTSPServiceUtility.parseForNS(failureInfo.getQtspResource().getX509Certificate()),
						failureInfo.getTenant());
				publishRequest = new PublishRequest(awsSNSConfig.getTopicArn(), message, awsSNSConfig.getPfRemove());
				break;
			case QTSPServiceUtility.NS_FAILURE_STAGE_ADD:
				message = String.format(awsSNSConfig.getNsAdd() + awsSNSConfig.getMessage(),
						failureInfo.getQtspResource().getServiceName(),
						QTSPServiceUtility.parseForNS(failureInfo.getQtspResource().getX509Certificate()),
						failureInfo.getTenant());
				publishRequest = new PublishRequest(awsSNSConfig.getTopicArn(), message, awsSNSConfig.getNsAdd());
				break;
			case QTSPServiceUtility.NS_FAILURE_STAGE_REMOVE:
				message = String.format(awsSNSConfig.getNsRemove() + awsSNSConfig.getMessage(),
						failureInfo.getQtspResource().getServiceName(),
						QTSPServiceUtility.parseForNS(failureInfo.getQtspResource().getX509Certificate()),
						failureInfo.getTenant());
				publishRequest = new PublishRequest(awsSNSConfig.getTopicArn(), message, awsSNSConfig.getNsRemove());
				break;
			default:
				publishRequest = new PublishRequest();
			}
			try {
				snsClient.publish(publishRequest);
			} catch (Exception e) {
				LOGGER.error("Failed to publish {}, Exception : {}", failureInfo, e.getMessage());
			}
		}
	}

}

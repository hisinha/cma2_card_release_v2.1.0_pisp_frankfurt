package com.capgemini.psd2.service.qtsp.integration;

import java.util.List;
import java.util.Map;

import com.capgemini.psd2.model.qtsp.QTSPResource;

public interface NetScalerAdapter {

	void processCert(List<? extends Map<String, List<QTSPResource>>> updatedQTSPs);
}
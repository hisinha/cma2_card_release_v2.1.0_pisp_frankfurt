package com.capgemini.psd2.service.qtsp;

import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.model.qtsp.Meta;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.service.qtsp.integration.NetScalerAdapter;
import com.capgemini.psd2.service.qtsp.integration.PingFederateAdapter;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;
import com.nimbusds.jose.util.X509CertUtils;

@Service
public class UploadQTSPServiceImpl implements UploadQTSPService {

	@Lazy
	@Autowired
	private PingFederateAdapter pingFederateAdapter;

	@Lazy
	@Autowired
	private NetScalerAdapter netScalerAdapter;

	@Autowired
	private QTSPServiceUtility utility;

	@Autowired
	private QTSPService service;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	private static final Logger LOGGER = LoggerFactory.getLogger(UploadQTSPService.class);

	private final Object lock = new Object();

	@Override
	public boolean uploadQTSP(QTSPResource qtsp) {
		LOGGER.info("Starting to upload Certificate with Fingerprint : \"{}\"", qtsp.getId());
		transformRequestBody(qtsp);
		boolean isQTSPUploaded = true;
		List<Map<String, List<QTSPResource>>> updatedQTSPs = new ArrayList<>();
		Map<String, List<QTSPResource>> map = new HashMap<>();
		List<QTSPResource> newQTSPs = new ArrayList<>();
		newQTSPs.add(qtsp);
		map.put(QTSPServiceUtility.NEW_QTSPS, newQTSPs);
		map.put(QTSPServiceUtility.MODIFIED_QTSPS, new ArrayList<>());
		updatedQTSPs.add(map);
		synchronized (lock) {
			utility.getUploadCertFailures().clear();
			if (!isCertPresent(qtsp.getId())) {
				List<String> tenantIds;
				if (null != service.findQTSP(qtsp.getId()) && null != service.findQTSP(qtsp.getId()).getTenantIds())
					tenantIds = service.findQTSP(qtsp.getId()).getTenantIds();
				else
					tenantIds = new ArrayList<>();
				tenantIds.add(requestHeaderAttributes.getTenantId());
				qtsp.addTenantIds(tenantIds);
				LOGGER.info("Starting to import certificate in Pingfederate");
				pingFederateAdapter.importCAInPF(newQTSPs);
				if (utility.getUploadCertFailures().isEmpty()) {
					LOGGER.info("Starting to upload certificate to NetScaler");
					netScalerAdapter.processCert(updatedQTSPs);
					if (utility.getUploadCertFailures().isEmpty()) {
						LOGGER.info("Successfully uploaded certificate to NetScaler");
						service.saveQTSP(qtsp);
					} else {
						LOGGER.error("Could not upload certificate to NetScaler");
						isQTSPUploaded = false;
					}
				} else {
					LOGGER.error("Could not import certificate in PingFederate");
					isQTSPUploaded = false;
				}
			} else {
				LOGGER.info("Certificate already present in database, skipping Pingfederate and NetScaler calls");
				isQTSPUploaded = true;
			}
		}
		return isQTSPUploaded;
	}

	@Override
	public boolean validateRequestBody(QTSPResource qtsp) {
		LOGGER.info("Validating request");
		String parsedCert;
		boolean isValidRequest = true;
		if (null == qtsp.getX509Certificate() || qtsp.getX509Certificate().isEmpty()) {
			LOGGER.error("Validation failed : \"X509Certificate\" is not present in the request");
			isValidRequest = false;
		} else {
			parsedCert = QTSPServiceUtility.parseForNS(qtsp.getX509Certificate());
			X509Certificate x509Cert = X509CertUtils.parse(parsedCert);
			if (null == x509Cert) {
				LOGGER.error("Validation failed : \"X509Certificate\" is invalid");
				isValidRequest = false;
			} else {
				try {
					qtsp.setId(QTSPServiceUtility.extractSha1Thumbprint(x509Cert));
				} catch (CertificateEncodingException | NoSuchAlgorithmException e) {
					LOGGER.error("Validation failed : \"X509Certificate\" is invalid");
					isValidRequest = false;
				}
			}
		}
		return isValidRequest;
	}

	private void transformRequestBody(QTSPResource qtsp) {
		LOGGER.info("Transforming RequestBody to QTSP resource");
		Meta meta = new Meta();
		meta.setLocation("");
		meta.setResourceType("");
		qtsp.setCreateTimestamp("");
		qtsp.setCertificateFingerprint(qtsp.getId());
		qtsp.setMeta(meta);
		qtsp.setModifyTimestamp("");
		qtsp.setSchemas(new ArrayList<String>());
		qtsp.setServiceCountry("");
		qtsp.setServiceExtensions(new ArrayList<>());
		qtsp.setServiceLanguage("");
		qtsp.setServiceName("");
		qtsp.setServiceStatus("");
		qtsp.setServiceType("");
		qtsp.setStatusStartingTime("");
		qtsp.setX509SKI("");
		qtsp.setX509SubjectName("");
	}

	private boolean isCertPresent(String id) {
		QTSPResource qtsp = service.findQTSP(id);
		if (null == qtsp)
			return false;
		else {
			String tenantId = requestHeaderAttributes.getTenantId();
			if (null != qtsp.getTenantIds() && qtsp.getTenantIds().contains(tenantId))
				return true;
			else
				return false;
		}
	}
}

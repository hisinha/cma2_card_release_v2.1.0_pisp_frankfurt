package com.capgemini.psd2.service.qtsp.utility;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.model.qtsp.FailureInfo;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.nimbusds.jose.util.X509CertUtils;

@Component
public class QTSPServiceUtility {

	private static final String PEM_BEGIN_MARKER = "-----BEGIN CERTIFICATE-----";

	private static final String PEM_END_MARKER = "-----END CERTIFICATE-----";

	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	public static final String NEW_QTSPS = "newQTSPs";

	public static final String MODIFIED_QTSPS = "modifiedQTSPs";

	public static final String PF_FAILURE_STAGE_ADD = "PF_Add";

	public static final String PF_FAILURE_STAGE_REMOVE = "PF_Remove";

	public static final String NS_FAILURE_STAGE_ADD = "NS_Add";

	public static final String NS_FAILURE_STAGE_REMOVE = "NS_Remove";

	private static final Logger LOGGER = LoggerFactory.getLogger(QTSPServiceUtility.class);

	private List<FailureInfo> failures;

	private List<FailureInfo> uploadCertFailures;

	private boolean isUploadCertRequest;

	public static String parseForPF(String cert) {
		String result = cert;
		if (result == null || result.isEmpty())
			return null;

		int markerStart = cert.indexOf(PEM_BEGIN_MARKER);
		int markerEnd = cert.indexOf(PEM_END_MARKER);

		if (markerStart >= 0)
			result = result.substring(PEM_BEGIN_MARKER.length(), result.length());

		if (markerEnd >= 0)
			result = result.substring(0, result.length() - PEM_END_MARKER.length());

		return String.format(result);
	}

	public static String parseForNS(String cert) {
		String result = cert;
		if (result == null || result.isEmpty())
			return null;

		int markerStart = cert.indexOf(PEM_BEGIN_MARKER);
		int markerEnd = cert.indexOf(PEM_END_MARKER);

		if (markerStart < 0)
			result = PEM_BEGIN_MARKER + result;

		if (markerEnd < 0)
			result = result + PEM_END_MARKER;

		String prettifiedCert = "";
		try {
			X509Certificate x509Cert = X509CertUtils.parse(result);
			final Base64.Encoder encoder = Base64.getMimeEncoder(64, LINE_SEPARATOR.getBytes());
			final byte[] rawCert = x509Cert.getEncoded();
			final String encodedCertText = new String(encoder.encode(rawCert));
			prettifiedCert = PEM_BEGIN_MARKER + LINE_SEPARATOR + encodedCertText + LINE_SEPARATOR + PEM_END_MARKER;
		} catch (Exception e) {
			LOGGER.error("Formatting cert for NetScaler failed: {}", e.getMessage());
		}

		return prettifiedCert;
	}

	public static String extractSha1Thumbprint(X509Certificate xCert)
			throws NoSuchAlgorithmException, CertificateEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		byte[] encodedCert = xCert.getEncoded();
		md.update(encodedCert);
		byte[] digest = md.digest();
		String digestHex = DatatypeConverter.printHexBinary(digest);
		return digestHex.toLowerCase();
	}

	public static void addDelay(String millis) {
		try {
			Thread.sleep(getLong(millis));
		} catch (Exception e) {
			// To Continue
		}
	}

	private static long getLong(String millis) {
		try {
			return Long.parseLong(millis);
		} catch (Exception e) {
			return 0;
		}
	}

	public void logFailure(QTSPResource qtsp, String prefix, String uri, String key, String failureStage) {
		if (isUploadCertRequest()) {
			getUploadCertFailures().add(new FailureInfo(qtsp, prefix + uri, key, failureStage));
		} else {
			getFailures().add(new FailureInfo(qtsp, prefix + uri, key, failureStage));
		}
	}

	public void logFailure(QTSPResource qtsp, String prefix, String uri, String key, String failureStage,
			String vserverName) {
		if (isUploadCertRequest()) {
			getUploadCertFailures().add(new FailureInfo(qtsp, prefix + uri + ":" + vserverName, key, failureStage));
		} else {
			getFailures().add(new FailureInfo(qtsp, prefix + uri + ":" + vserverName, key, failureStage));
		}
	}

	public List<FailureInfo> getFailures() {
		if (null == failures)
			failures = new ArrayList<>();
		return failures;
	}

	public void setFailures(List<FailureInfo> failures) {
		this.failures = failures;
	}

	public List<FailureInfo> getUploadCertFailures() {
		if (null == uploadCertFailures)
			uploadCertFailures = new ArrayList<>();
		return uploadCertFailures;
	}

	public void setUploadCertFailures(List<FailureInfo> uploadCertFailures) {
		this.uploadCertFailures = uploadCertFailures;
	}

	public boolean isUploadCertRequest() {
		return isUploadCertRequest;
	}

	public void setUploadCertRequest(boolean isUploadCertRequest) {
		this.isUploadCertRequest = isUploadCertRequest;
	}
}
package com.capgemini.psd2.controller.qtsp;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.service.qtsp.UploadQTSPService;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

@RestController
public class UploadQTSPController {

	private static final String KEY = "message";

	private static final String ERROR_MESSAGE = "Failed to upload certificate";

	private static final String SUCCESS_MESSAGE = "Certificate uploaded successfully";

	private static final String BAD_REQUEST_MESSAGE = "Certificate is either invalid or not present";

	@Autowired
	private UploadQTSPService uploadQTSPService;

	@Autowired
	private QTSPServiceUtility utility;

	@Autowired
	private RequestHeaderAttributes reqHeaderAttribute;

	private static final Logger LOGGER = LoggerFactory.getLogger(UploadQTSPController.class);

	@PostMapping(value = "/uploadcertificate", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<Map<String, String>> uploadCertificate(@RequestBody QTSPResource qtsp,
			@RequestHeader("tenantid") String tenantId) {
		utility.setUploadCertRequest(true);
		reqHeaderAttribute.setTenantId(tenantId);
		Map<String, String> responseMap = new HashMap<>();
		HttpStatus status;
		if (uploadQTSPService.validateRequestBody(qtsp)) {
			if (!uploadQTSPService.uploadQTSP(qtsp)) {
				responseMap.put(KEY, ERROR_MESSAGE);
				status = HttpStatus.INTERNAL_SERVER_ERROR;
				LOGGER.info("Failed to process the request due to status: \"{}\", {}", status, responseMap);
				utility.setUploadCertRequest(false);
				return new ResponseEntity<>(responseMap, status);
			}
		} else {
			responseMap.put(KEY, BAD_REQUEST_MESSAGE);
			status = HttpStatus.BAD_REQUEST;
			LOGGER.info("Unable to process request due to status: \"{}\", {}", status, responseMap);
			utility.setUploadCertRequest(false);
			return new ResponseEntity<>(responseMap, status);
		}
		LOGGER.info("Successfully uploaded certificate");
		responseMap.put(KEY, SUCCESS_MESSAGE);
		status = HttpStatus.OK;
		LOGGER.info("Completing request with status: \"{}\", {}", status, responseMap);
		utility.setUploadCertRequest(false);
		return new ResponseEntity<>(responseMap, status);
	}
}

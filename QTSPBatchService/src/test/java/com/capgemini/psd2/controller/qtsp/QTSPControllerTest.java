package com.capgemini.psd2.controller.qtsp;

import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.capgemini.psd2.service.qtsp.config.QTSPServiceConfig;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

public class QTSPControllerTest {

	@InjectMocks
	private QTSPController qtspController;

	@Mock
	private JobLauncher jobLauncher;

	@Mock
	private Job job;

	@Mock
	private QTSPServiceConfig qtspServiceConfig;

	@Mock
	private QTSPServiceUtility utility;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(utility.getFailures()).thenReturn(new ArrayList<>());
	}

	@Test
	public void processQTSPJob() {
		try {
			ResponseEntity<String> response = qtspController.processQTSPJob();
			Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		} catch (JobExecutionException e) {
		}
	}

}

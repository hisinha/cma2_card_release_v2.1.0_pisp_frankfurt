package com.capgemini.psd2.service.qtsp.config;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.capgemini.psd2.service.qtsp.restclient.QTSPRestClient;

@RunWith(SpringJUnit4ClassRunner.class)
public class QTSPReaderTest {

	@InjectMocks
	private QTSPReader qtspReader;

	@Mock
	private QTSPRestClient qtspRestClient;

	@Mock
	private QTSPServiceConfig qtspServiceConfig;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void readTest_false() {
		when(qtspServiceConfig.isBatchJobState()).thenReturn(true);
		assertNull(qtspReader.read());
	}

	@Test
	public void testReadFromFile() {
		when(qtspServiceConfig.isBatchJobState()).thenReturn(false);
		when(qtspServiceConfig.isConnectToOB()).thenReturn(false);
		Assert.isInstanceOf(List.class, qtspReader.read());
	}

	@Test
	public void testReadFromOB() {
		when(qtspServiceConfig.isBatchJobState()).thenReturn(false);
		when(qtspServiceConfig.isConnectToOB()).thenReturn(true);
		Assert.isInstanceOf(List.class, qtspReader.read());
	}
}

package com.capgemini.psd2.service.qtsp.integration;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.model.qtsp.Items;
import com.capgemini.psd2.model.qtsp.PFResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.service.qtsp.config.PFConfig;
import com.capgemini.psd2.service.qtsp.stub.QTSPStub;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class PingFederateAdapterImplTest {

	@InjectMocks
	private PingFederateAdapterImpl pingFederateAdapterImpl;

	@Mock
	private PFConfig pfConfig;

	@Mock
	private RestClientSync restClientSync;

	@Mock
	private QTSPServiceUtility utility;

	@Before 
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this); 
	}

	private Items item;

	@Before
	public void init() {

		Map<String, String> adminUser = new HashMap<>();
		Map<String, String> adminPwd = new HashMap<>();
		Map<String, String> prefix = new HashMap<>();

		PFResponse pfResponse1 = new PFResponse();
		pfResponse1.setSha256Fingerprint(QTSPStub.getCerts().get(0).getCertificateFingerprint().toUpperCase());
		PFResponse pfResponse2 = new PFResponse();
		pfResponse2.setSha256Fingerprint(QTSPStub.getCerts().get(1).getCertificateFingerprint().toUpperCase());
		List<PFResponse> list = new ArrayList<>();
		list.add(pfResponse1);
		list.add(pfResponse2);
		item = new Items();
		item.setItems(list);

		adminUser.put("BOI", "Apiadmin");
		adminPwd.put("BOI", "Test@2017");
		prefix.put("BOI", "https://pfadmin.apiboidev.com/pf-admin-api");
		when(pfConfig.getAdminPwd()).thenReturn(adminPwd);
		when(pfConfig.getAdminUser()).thenReturn(adminUser);
		when(pfConfig.getPrefix()).thenReturn(prefix);
		when(pfConfig.getCaImportUrl()).thenReturn("/v1/certificates/ca/import");
		when(pfConfig.getCaRemoveUrl()).thenReturn("/v1/certificates/ca");
		when(pfConfig.getReplicateUrl()).thenReturn("/v1/cluster/replicate");

	}

	@Test
	public void importCAInPFTest() {
		pingFederateAdapterImpl.importCAInPF(QTSPStub.getCerts());
	}

	@Test
	public void importCAInPFTest_duplicateErr() {

		Exception e = new IndexOutOfBoundsException("{\"resultId\":\"validation_error\",\"message\":\"Validation error(s) occurred. "
				+ "Please review the error(s) and address accordingly.\",\"validationErrors\":[{\"message\":\"I/O "
				+ "Error occurred while importing the file.\",\"fieldPath\":\"fileData\",\"errorId\":\""
				+ "cert_import_duplication_error\"}]}");

		try {
			when(restClientSync.callForPost(any(RequestInfo.class), anyMap(), anyObject(), any(HttpHeaders.class)))
			.thenThrow(e);
		} catch (Exception e2) {
		}
		pingFederateAdapterImpl.importCAInPF(QTSPStub.getCerts());
	}

	@Test
	public void importCAInPFTest_importErr() {

		Exception e = new IndexOutOfBoundsException("{\"resultId\":\"validation_error\",\"message\":\"Validation error(s) occurred. "
				+ "Please review the error(s) and address accordingly.\",\"validationErrors\":[{\"message\":\"I/O "
				+ "Error occurred while importing the file.\",\"fieldPath\":\"fileData\",\"errorId\":\""
				+ "cert_import_error_importing_file\"}]}");

		try {
			when(restClientSync.callForPost(any(RequestInfo.class), anyMap(), anyObject(), any(HttpHeaders.class)))
			.thenThrow(e);
		} catch (Exception e2) {
		}
		pingFederateAdapterImpl.importCAInPF(QTSPStub.getCerts());
	}

	@Test
	public void importCAInPFTest_parseErr() {

		Exception e = new IndexOutOfBoundsException("invalid String");

		try {
			when(restClientSync.callForPost(any(RequestInfo.class), anyMap(), anyObject(), any(HttpHeaders.class)))
			.thenThrow(e);
		} catch (Exception e2) {
		}
		pingFederateAdapterImpl.importCAInPF(QTSPStub.getCerts());
	}

	@Test 
	public void removeCAFromPFTest() {

		when(restClientSync.callForGet(any(RequestInfo.class), anyObject(), any(HttpHeaders.class))).thenReturn(item);
		pingFederateAdapterImpl.removeCAFromPF(QTSPStub.getCerts()); 
	}

	@Test 
	public void removeCAFromPFTest_exceptionforGet() {

		Exception e = new IndexOutOfBoundsException("{\"resultId\":\"validation_error\",\"message\":\"Validation error(s) occurred. "
				+ "Please review the error(s) and address accordingly.\",\"validationErrors\":[{\"message\":\"I/O "
				+ "Error occurred while importing the file.\",\"fieldPath\":\"fileData\",\"errorId\":\""
				+ "resource_not_found\"}]}");
		try {
			when(restClientSync.callForGet(any(RequestInfo.class), anyObject(), any(HttpHeaders.class))).thenThrow(e);
		} catch (Exception e2) {
		}
		pingFederateAdapterImpl.removeCAFromPF(QTSPStub.getCerts()); 
	}

	@Test
	public void removeCAFromPFTest_exceptionforDelete_resourceNotFound() {

		Exception e = new IndexOutOfBoundsException("{\"resultId\":\"validation_error\",\"message\":\"Validation error(s) occurred. "
				+ "Please review the error(s) and address accordingly.\",\"validationErrors\":[{\"message\":\"I/O "
				+ "Error occurred while importing the file.\",\"fieldPath\":\"fileData\",\"errorId\":\""
				+ "resource_not_found\"}]}");

		when(restClientSync.callForGet(any(RequestInfo.class), anyObject(), any(HttpHeaders.class))).thenReturn(item);
		try {
			when(restClientSync.callForDelete(any(RequestInfo.class), anyObject(), any(HttpHeaders.class))).thenThrow(e);
		} catch (Exception e2) {
		}
		pingFederateAdapterImpl.removeCAFromPF(QTSPStub.getCerts()); 
	}

	@Test
	public void removeCAFromPFTest_exceptionforDelete_random() {

		Exception e = new IndexOutOfBoundsException("{\"resultId\":\"validation_error\",\"message\":\"Validation error(s) occurred. "
				+ "Please review the error(s) and address accordingly.\",\"validationErrors\":[{\"message\":\"I/O "
				+ "Error occurred while importing the file.\",\"fieldPath\":\"fileData\",\"errorId\":\""
				+ "Unknown_Error_Occured\"}]}");

		when(restClientSync.callForGet(any(RequestInfo.class), anyObject(), any(HttpHeaders.class))).thenReturn(item);
		try {
			when(restClientSync.callForDelete(any(RequestInfo.class), anyObject(), any(HttpHeaders.class))).thenThrow(e);
		} catch (Exception e2) {
		}
		pingFederateAdapterImpl.removeCAFromPF(QTSPStub.getCerts()); 
	}

	@Test
	public void removeCAFromPFTest_exceptionforDelete_invalid() {

		Exception e = new IndexOutOfBoundsException("invalid String");

		when(restClientSync.callForGet(any(RequestInfo.class), anyObject(), any(HttpHeaders.class))).thenReturn(item);
		try {
			when(restClientSync.callForDelete(any(RequestInfo.class), anyObject(), any(HttpHeaders.class))).thenThrow(e);
		} catch (Exception e2) {
		}
		pingFederateAdapterImpl.removeCAFromPF(QTSPStub.getCerts()); 
	}

	@Test
	public void removeCAFromPFTest_emptyCerts() {

		Exception e = new IndexOutOfBoundsException("invalid String");
		item.getItems().get(0).setSha256Fingerprint("err");
		item.getItems().get(1).setSha256Fingerprint("invalid");
		when(restClientSync.callForGet(any(RequestInfo.class), anyObject(), any(HttpHeaders.class))).thenReturn(item);
		try {
			when(restClientSync.callForDelete(any(RequestInfo.class), anyObject(), any(HttpHeaders.class))).thenThrow(e);
		} catch (Exception e2) {
		}
		pingFederateAdapterImpl.removeCAFromPF(QTSPStub.getCerts()); 
	}
}

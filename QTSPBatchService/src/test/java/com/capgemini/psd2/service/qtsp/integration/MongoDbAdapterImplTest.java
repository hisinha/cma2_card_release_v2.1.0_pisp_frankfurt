package com.capgemini.psd2.service.qtsp.integration;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.capgemini.psd2.service.qtsp.stub.QTSPStub;

@RunWith(SpringJUnit4ClassRunner.class)
public class MongoDbAdapterImplTest {

	@InjectMocks
	private MongoDbAdapterImpl mongoDbAdapterImpl;

	@Mock
	private QTSPServiceRepository qtspServiceRepository;

	@Before 
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this); 
	}

	@Test
	public void saveQTSPsTest() {
		Assert.isInstanceOf(List.class, mongoDbAdapterImpl.saveQTSPs(QTSPStub.getCerts()));
	}

	@Test
	public void retreiveQTSPsTest() {
		Assert.isInstanceOf(List.class, mongoDbAdapterImpl.retrieveQTSPs());
	}
}

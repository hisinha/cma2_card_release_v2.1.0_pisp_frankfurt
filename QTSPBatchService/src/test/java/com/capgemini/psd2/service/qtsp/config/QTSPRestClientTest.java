
package com.capgemini.psd2.service.qtsp.config;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.config.OBConfig;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.model.qtsp.OBResponse;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.restclient.ScimRestClient;
import com.capgemini.psd2.service.qtsp.restclient.QTSPRestClient;
import com.capgemini.psd2.util.JwtCreate;

@RunWith(SpringJUnit4ClassRunner.class)
public class QTSPRestClientTest {

	@InjectMocks
	private QTSPRestClient qtspRestClient;

	@Mock
	private ScimRestClient scimRestClient;

	@Mock
	private JwtCreate jwtCreate;

	@Mock
	private RestClientSync restClientSync;

	@Mock
	private OBConfig obConfig;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void buildSSLRequestTest() {
		doNothing().when(restClientSync).buildSSLRequest("");
		qtspRestClient.buildSSLRequest();
	}

	@Test
	public void retrieveOBAccessTokenTest() {
		when(qtspRestClient.retrieveOBAccessToken()).thenReturn("result");
		assertEquals("result", qtspRestClient.retrieveOBAccessToken());
	}

	@Test
	public void retrieveQTSPsTest() {
		int totalQTSPs = 500;
		List<QTSPResource> list = new ArrayList<>();
		for (int i = 0; i < totalQTSPs; i++) {
			list.add(new QTSPResource());
		}
		OBResponse res = new OBResponse();
		res.setResources(list);
		res.setSchemas(new ArrayList<>());

		when(restClientSync.callForGet(any(), any(), any())).thenReturn(res);
		qtspRestClient.retrieveQTSPs("");
	}

	@Test
	public void retrieveQTSPsTest_Failure() {

		OBResponse res = new OBResponse();
		res.setResources(new ArrayList<>());
		res.setSchemas(new ArrayList<>());
		res.setTotalResults("1000");

		when(restClientSync.callForGet(any(), any(), any()))
				.thenThrow(new PSD2Exception("Could not get QTSPs", new ErrorInfo()));
		try {
			qtspRestClient.retrieveQTSPs("");
		} catch (Exception e) {
		}
	}
}

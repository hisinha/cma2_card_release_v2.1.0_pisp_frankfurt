package com.capgemini.psd2.service.qtsp.config;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.model.qtsp.FailureInfo;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.service.qtsp.integration.AWSSNSAdapter;
import com.capgemini.psd2.service.qtsp.integration.MongoDbAdapter;
import com.capgemini.psd2.service.qtsp.integration.NetScalerAdapter;
import com.capgemini.psd2.service.qtsp.integration.PingFederateAdapter;
import com.capgemini.psd2.service.qtsp.stub.QTSPStub;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class QTSPWriterTest {

	@InjectMocks
	private QTSPWriter qtspWriter;

	@Mock
	private QTSPServiceConfig qtspServiceConfig;

	@Mock
	private MongoDbAdapter qtspServiceMongoDbAdapter;

	@Mock
	private PingFederateAdapter pingFederateAdapter;

	@Mock
	private NetScalerAdapter netScalerAdapter;

	@Mock
	private AWSSNSAdapter awsSNSAdapter;

	@Mock
	private QTSPServiceUtility utility;

	List<Map<String, List<QTSPResource>>> updatedQTSPs = new ArrayList<>();
	Map<String, List<QTSPResource>> map = new HashMap<>();

	@Before 
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(qtspServiceConfig.isWriteToDB()).thenReturn(true);
		when(qtspServiceConfig.isWriteToNetScaler()).thenReturn(true);
		when(qtspServiceConfig.isWriteToPingFederate()).thenReturn(true);
		when(qtspServiceConfig.isPublishToAWSSNS()).thenReturn(true);
		List<FailureInfo> failures = new ArrayList<>();
		FailureInfo failureInfo = new FailureInfo(null, "aws", "abc", "stage1");
		failures.add(failureInfo);
		when(utility.getFailures()).thenReturn(failures);
	}

	@Test
	public void writeTest() {

		map.put("newQTSPs", QTSPStub.getCerts());
		map.put("modifiedQTSPs", QTSPStub.getCerts());
		updatedQTSPs.add(map);
		try {
			qtspWriter.write(updatedQTSPs);
		} catch (Exception e) {

		}
	}

	@Test
	public void writeTest_bothEmptyLists() {
		map.put("newQTSPs", new ArrayList<>());
		map.put("modifiedQTSPs", new ArrayList<>());
		updatedQTSPs.add(map);
		try {
			qtspWriter.write(updatedQTSPs);
		} catch (Exception e) {

		}
	}

	@Test
	public void writeTest_null() {
		updatedQTSPs.add(map);
		try {
			qtspWriter.write(updatedQTSPs);
		} catch (Exception e) {

		}
	}

	@Test
	public void writeTest_newQTSPsEmpty() {
		map.put("newQTSPs", new ArrayList<>());
		map.put("modifiedQTSPs", QTSPStub.getCerts());
		updatedQTSPs.add(map);
		try {
			qtspWriter.write(updatedQTSPs);
		} catch (Exception e) {

		}
	}

}

package com.capgemini.psd2.service.qtsp.config;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.builder.SimpleStepBuilder;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.capgemini.psd2.model.qtsp.QTSPResource;

@RunWith(SpringJUnit4ClassRunner.class)
public class QTSPServiceBatchConfigurationTest {

	@InjectMocks
	private QTSPServiceBatchConfiguration qtspServiceBatchConfig;

	@Mock
	private JobBuilderFactory jobBuilderFactory;

	@Mock
	private StepBuilderFactory stepBuilderFactory;

	@Mock
	private StepBuilder stepBuilder;

	@Mock
	private SimpleStepBuilder<List<QTSPResource>, Map<String, List<QTSPResource>>> ssb;

	@Before 
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void readerTest() {
		Assert.isInstanceOf(ItemReader.class, qtspServiceBatchConfig.reader());
	}

	@Test
	public void processorTest() {
		Assert.isInstanceOf(ItemProcessor.class, qtspServiceBatchConfig.processor());
	}

	@Test
	public void writerTest() {
		Assert.isInstanceOf(ItemWriter.class, qtspServiceBatchConfig.writer());
	}
}

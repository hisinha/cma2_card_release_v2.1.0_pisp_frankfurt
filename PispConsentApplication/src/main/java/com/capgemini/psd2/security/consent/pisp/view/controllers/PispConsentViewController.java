/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.consent.pisp.view.controllers;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriUtils;

import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.aisp.domain.OBReadAccount2Data;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.fraudsystem.utilities.FraudSystemUtilities;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.PaymentSetupPlatformAdapterImpl;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.scaconsenthelper.config.CdnConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.pisp.config.PispHostNameConfig;
import com.capgemini.psd2.security.consent.pisp.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.SandboxConfig;

/**
 * ConsentView Controller
 * 
 * @author Capgemini
 *
 */
@Controller
@ConfigurationProperties("app")
public class PispConsentViewController {
	private static final Logger LOG = LoggerFactory.getLogger(PispConsentViewController.class);
	@Autowired
	private PispHostNameConfig pispHostNameConfig;

	@Value("${spring.application.name}")
	private String applicationName;

	@Value("${app.fraudSystem.resUrl}")
	private String resUrl;

	@Value("${app.fraudSystem.hdmUrl}")
	private String hdmUrl;

	@Value("${app.fraudSystem.hdmInputName}")
	private String hdmInputName;

	@Value("${app.fraudSystem.jsc-file-path}")
	private String jscFilePath;

	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;

	@Autowired
	private UIStaticContentUtilityController uiController;

	@Autowired
	private RequestHeaderAttributes requestHeaders;

	@Autowired
	private PispConsentCreationDataHelper consentCreationDataHelper;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;

	@Autowired
	private DataMask dataMask;

	@Autowired
	private SandboxConfig sandboxConfig;
	
	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Value("${cdn.baseURL}")
	private String cdnBaseURL;

	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();

	public Map<String, String> getConsentSupportedSchemeMap() {
		return consentSupportedSchemeMap;
	}

	@RequestMapping(value = "/pisp/home")
	public ModelAndView homePage(Map<String, Object> model) {
		SCAConsentHelper.populateSecurityHeaders(response);

		String homeScreen = "/customerconsentview";
		String requestUrl = request.getRequestURI();
		requestUrl = requestUrl.replace("/home", homeScreen);
		model.put("redirectUrl", pispHostNameConfig.getTenantSpecificEdgeserverhost(requestHeaders.getTenantId())
				.concat("/").concat(applicationName).concat(requestUrl).concat("?").concat(request.getQueryString()));
		model.put(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, requestHeaders.getTenantId());
		model.put(PSD2Constants.IS_SANDBOX_ENABLED, sandboxConfig.isSandboxEnabled());
		return new ModelAndView("homePage", model);
	}

	/**
	 * customerconsentview end-point of consent application gets invoked by
	 * OAuth2 server with some params like - call back URI of OAuth2 server & ID
	 * token which contains the identity information of the customer. It
	 * collects the data for allowed scopes of the caller AISP & fetches
	 * accounts list of the current customer by using the user-id information
	 * taken from the ID token (SaaS Server issued ID token) which gets relayed
	 * by the OAuth server to consent app.
	 * 
	 * @param model
	 * @param oAuthUrl
	 * @param idToken
	 * @return
	 * @throws NamingException
	 */
	@RequestMapping(value = "/pisp/customerconsentview")
	public ModelAndView consentView(Map<String, Object> model) throws NamingException {
		Long currTime =  new Date().getTime();
		
		 Timestamp ts = new Timestamp(currTime);
		boolean isAccounSelected = false;

		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);

		/*
		 * Setting channelId in reqHeader to be available in all adapter calls
		 */
		requestHeaders.setChannelId(pickupDataModel.getChannelId());

		String stagedDebtorSchemeName = null;
		LOG.debug("Inside Method:consentView at "+ ts);

		/* Removed call for retrieve payment stage details */
		CustomConsentAppViewData consentAppPaymentSetupData = consentCreationDataHelper
				.retrieveConsentAppStagedViewData(pickupDataModel.getIntentId());
		LOG.debug("returned to viewcontroller after adapter call in "+ (System.currentTimeMillis()-currTime));
		CustomDebtorDetails stagedSetupDebtorDetails = consentAppPaymentSetupData.getDebtorDetails();
		if (!NullCheckUtils.isNullOrEmpty(stagedSetupDebtorDetails)) {
			stagedDebtorSchemeName = stagedSetupDebtorDetails.getSchemeName();
			if (!consentSupportedSchemeMap.containsKey(stagedSetupDebtorDetails.getSchemeName())) {
				throw PSD2Exception
						.populatePSD2Exception(ErrorCodeEnum.PROVIDED_DEBTOR_SCHEME_NOT_SUPPORTED_FOR_CONSENT_APP);
			}
		}
		LOG.debug("Before Method:retrieveCustomerAccountListInfo in ViewController at "+ new Timestamp(new Date().getTime()) );
		OBReadAccount2 customerAccountList = consentCreationDataHelper.retrieveCustomerAccountListInfo(
				pickupDataModel.getUserId(), pickupDataModel.getClientId(),
				pickupDataModel.getIntentTypeEnum().getIntentType(), requestHeaders.getCorrelationId(),
				pickupDataModel.getChannelId(), stagedDebtorSchemeName, requestHeaders.getTenantId(),
				pickupDataModel.getIntentId());
		LOG.debug("exiting Method:retrieveCustomerAccountListInfo in ViewController at "+ new Timestamp(new Date().getTime()) );
		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId());
		String tppApplicationName = tppInformationAdaptor.fetchApplicationName(pickupDataModel.getClientId());

		if (!NullCheckUtils.isNullOrEmpty(stagedSetupDebtorDetails)) {
			isAccounSelected = true;
			customerAccountList = validateAccountWithAccountList(stagedSetupDebtorDetails, customerAccountList);
		}

		// Remove setUpResponseDataforUI once UI changes done.
		CustomDPaymentConsentsPOSTResponse setUpResponseDataforUI = populateLimitedSetUpDataforUI(
				consentAppPaymentSetupData);

		String customerAccount = dataMask.maskResponseGenerateString(customerAccountList, "account");
		model.put(PSD2SecurityConstants.CUSTOMER_ACCOUNT_LIST, customerAccount);

		model.put(PSD2Constants.IS_SANDBOX_ENABLED, sandboxConfig.isSandboxEnabled());

		// To be used. Changes needed at UI.
		
		//model.put(PSD2Constants.PAYMENT_CONSENT_DATA,JSONUtilities.getJSONOutPutFromObject(consentAppPaymentSetupData));
		String consentAppPaymentSetupData1 = dataMask.maskResponseGenerateString(consentAppPaymentSetupData, "paymentSetupData");
		model.put(PSD2Constants.PAYMENT_CONSENT_DATA,consentAppPaymentSetupData1);
		
		model.put(PSD2SecurityConstants.CONSENT_SETUP_DATA,
				JSONUtilities.getJSONOutPutFromObject(setUpResponseDataforUI));
		model.put(PSD2Constants.CONSENT_FLOW_TYPE, pickupDataModel.getIntentTypeEnum().getIntentType());
		model.put(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));
		model.put(PSD2Constants.CO_RELATION_ID, requestHeaders.getCorrelationId());

		JSONObject tppInfoJson = new JSONObject();

		if (tppInformationObj != null && tppInformationObj instanceof BasicAttributes) {
			BasicAttributes tppInfoAttributes = (BasicAttributes) tppInformationObj;
			String legalEntityName = returnLegalEntityName(tppInfoAttributes);
			model.put(PSD2SecurityConstants.TPP_NAME, legalEntityName);
			tppInfoJson.put(PSD2SecurityConstants.TPP_NAME, legalEntityName);

		}
		tppInfoJson.put(PSD2SecurityConstants.APP_NAME, tppApplicationName);
		model.put(PSD2SecurityConstants.TPP_INFO, tppInfoJson.toString());

		model.put(PSD2SecurityConstants.ACCOUNT_SELECTED, isAccounSelected);
		model.put(PSD2SecurityConstants.USER_ID, pickupDataModel.getUserId());
		model.put(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));
		model.put(PSD2Constants.CO_RELATION_ID, requestHeaders.getCorrelationId());
		model.put(PSD2Constants.APPLICATION_NAME, applicationName);
		model.put(OIDCConstants.CHANNEL_ID, pickupDataModel.getChannelId());
		model.put(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, requestHeaders.getTenantId());

		JSONObject fraudHdmInfo = new JSONObject();
		try {
			fraudHdmInfo.put(PSD2SecurityConstants.RES_URL, UriUtils.encode(resUrl, StandardCharsets.UTF_8.toString()));
			fraudHdmInfo.put(PSD2SecurityConstants.HDM_URL, UriUtils.encode(hdmUrl, StandardCharsets.UTF_8.toString()));
		} catch (UnsupportedEncodingException e) {
			throw PSD2SecurityException.populatePSD2SecurityException(e.getMessage(),
					SCAConsentErrorCodeEnum.TECHNICAL_ERROR);
		}
		fraudHdmInfo.put(PSD2SecurityConstants.HDM_INPUT_NAME, hdmInputName);
		model.put(PSD2SecurityConstants.FRAUD_HDM_INFO, fraudHdmInfo.toString());
		model.put(PSD2SecurityConstants.JSC_FILE_PATH, jscFilePath);
		model.put(FraudSystemRequestMapping.FS_HEADERS, Base64.getEncoder()
				.encodeToString(FraudSystemUtilities.populateFraudSystemHeaders(request).getBytes()));
		model.put(PSD2Constants.UICONTENT, uiController.getConfigVariable());
		model.putAll(CdnConfig.populateCdnAttributes());
		
		CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter
				.populateStageIdentifiers(pickupDataModel.getIntentId());
		model.put(PSD2Constants.CMAVERSION, stageIdentifiers.getPaymentSetupVersion());

		return new ModelAndView("index", model);
	}

	private String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException {
		return getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}

	private OBReadAccount2 validateAccountWithAccountList(CustomDebtorDetails debtorDetails,
			OBReadAccount2 obReadAccount2) {
		boolean isAccountMatch = false;

		String debtorSchemeName = debtorDetails.getSchemeName();
		
		if (obReadAccount2 != null && !obReadAccount2.getData().getAccount().isEmpty()) {
			for (OBAccount2 account : obReadAccount2.getData().getAccount()) {

				if (account instanceof PSD2Account) {
					Map<String, String> additionalInfo = ((PSD2Account) account).getAdditionalInformation();

					if (!consentSupportedSchemeMap.containsKey(debtorSchemeName)) {
						throw PSD2Exception.populatePSD2Exception(
								ErrorCodeEnum.PROVIDED_DEBTOR_SCHEME_NOT_SUPPORTED_FOR_CONSENT_APP);
					}               

					String ymlScheme = debtorDetails.getSchemeName(); // Scheme From Payload
					
					int len = ymlScheme.split("\\.").length;
					ymlScheme = ymlScheme.split("\\.")[len - 1];
					
					/*String ymlScheme = account.getAccount().get(0).getSchemeName(); // SortCode Scheme
					if (null == additionalInfo.get(consentSupportedSchemeMap.get(ymlScheme))) {
						int len = account.getAccount().get(0).getSchemeName().split("\\.").length;
						ymlScheme = account.getAccount().get(0).getSchemeName().split("\\.")[len - 1];
					}
					
					if ((debtorDetails.getSchemeName().contains(account.getAccount().get(0))
							|| account.getAccount().get(0).getSchemeName().contains(debtorDetails.getSchemeName()))
							&& additionalInfo.get(consentSupportedSchemeMap.get(ymlScheme))
									.equals(debtorDetails.getIdentification())) {
						isAccountMatch = true;
						obReadAccount3 = populateAccountData(account, debtorDetails);
						break;
					}*/
					
					System.out.println("--additionalInfo :"+additionalInfo);
					System.out.println("--ymlScheme :"+ymlScheme);
					System.out.println("--debtorDetails.getIdentification() :"+debtorDetails.getIdentification());

					if((additionalInfo.containsKey(ymlScheme) 
							&& additionalInfo.get(ymlScheme).equals(debtorDetails.getIdentification()) )) {
						System.out.println("after IF isAccountMatch "+isAccountMatch);
						isAccountMatch = true;
						obReadAccount2 = populateAccountData(account, debtorDetails);
						break;
					}
				}
			}
			if (!isAccountMatch)
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		} else
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		return obReadAccount2;
	}

	private OBReadAccount2 populateAccountData(OBAccount2 account, CustomDebtorDetails debtorDetails) {
		List<OBAccount2> accountData = new ArrayList<>();
		List<OBCashAccount3> obAccount2AccountList = new ArrayList<>();
		OBCashAccount3 accountDetails = new OBCashAccount3();

		accountDetails.setName(debtorDetails.getName());
		accountDetails.setSecondaryIdentification(debtorDetails.getSecondaryIdentification());
		accountDetails.setIdentification(debtorDetails.getIdentification());
		accountDetails.setSchemeName(debtorDetails.getSchemeName());
		obAccount2AccountList.add(accountDetails);
		account.setAccount(obAccount2AccountList);
		accountData.add(account);
		OBReadAccount2Data data2 = new OBReadAccount2Data();
		data2.setAccount(accountData);
		OBReadAccount2 oBReadAccount2 = new OBReadAccount2();
		oBReadAccount2.setData(data2);
		return oBReadAccount2;
	}

	private CustomDPaymentConsentsPOSTResponse populateLimitedSetUpDataforUI(
			CustomConsentAppViewData consentAppPaymentSetupData) {

		CustomDPaymentConsentsPOSTResponse pmtPostResp = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 pmtSetupResp = new OBWriteDataDomesticConsentResponse1();
		OBDomestic1 pmtSetupRepsInit = new OBDomestic1();
		OBDomestic1InstructedAmount resultInstAmount = new OBDomestic1InstructedAmount();
		OBCashAccountCreditor2 resultCreditorAccount = new OBCashAccountCreditor2();
		OBRemittanceInformation1 resultRemittanceInfo = new OBRemittanceInformation1();

		if (consentAppPaymentSetupData != null) {
			if (consentAppPaymentSetupData.getAmountDetails() != null) {
				resultInstAmount.setAmount(consentAppPaymentSetupData.getAmountDetails().getAmount());
				resultInstAmount.setCurrency(consentAppPaymentSetupData.getAmountDetails().getCurrency());
			}
			if (consentAppPaymentSetupData.getCreditorDetails() != null) {
				resultCreditorAccount.setName(consentAppPaymentSetupData.getCreditorDetails().getName());
				resultCreditorAccount.setSecondaryIdentification(
						consentAppPaymentSetupData.getCreditorDetails().getSecondaryIdentification());
			}
			if (consentAppPaymentSetupData.getRemittanceDetails() != null) {
				resultRemittanceInfo.setReference(consentAppPaymentSetupData.getRemittanceDetails().getReference());
				resultRemittanceInfo
						.setUnstructured(consentAppPaymentSetupData.getRemittanceDetails().getUnstructured());
			}
		}

		pmtSetupRepsInit.setInstructedAmount(resultInstAmount);
		pmtSetupRepsInit.setCreditorAccount(resultCreditorAccount);
		pmtSetupRepsInit.setRemittanceInformation(resultRemittanceInfo);
		pmtSetupResp.setInitiation(pmtSetupRepsInit);
		pmtPostResp.setData(pmtSetupResp);
		return pmtPostResp;
	}
}

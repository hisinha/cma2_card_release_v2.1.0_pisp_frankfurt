/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.consent.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;

/**
 * The Interface AispConsentMongoRepository.
 */
public interface AispConsentMongoRepository extends MongoRepository<AispConsent, String> {
	
	/**
	 * Find by consent id.
	 *
	 * @param consentId the consent id
	 * @return the aispConsent
	 */
	public AispConsent findByConsentIdAndCmaVersionIn(String consentId, List<String> cmaVersion);

	public AispConsent findByAccountRequestIdAndStatusAndCmaVersionIn(String accountRequestId,ConsentStatusEnum status, List<String> cmaVersion);
	
	public List<AispConsent> findByPsuIdAndStatusAndCmaVersionIn(String psuId, ConsentStatusEnum status, List<String> cmaVersion);

	public List<AispConsent> findByPsuIdAndCmaVersionIn(String psuId, List<String> cmaVersion);
	
	public AispConsent findByAccountDetailsAccountIdAndCmaVersionIn(String accountId, List<String> cmaVersion);
	
	public AispConsent findByAccountRequestIdAndCmaVersionIn(String accountRequestId,List<String> cmaVersion);

	public List<AispConsent> findByPsuIdAndTenantIdAndCmaVersionIn(String psuId, String tenantId, List<String> cmaVersion);
	
	public List<AispConsent> findByPsuIdAndStatusAndTenantIdAndCmaVersionIn(String psuId, ConsentStatusEnum status, String tenantid, List<String> cmaVersion);

}

package com.capgemini.psd2.pisp.sequence.test.adapter.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.mongodb.core.MongoOperations;

import com.capgemini.psd2.pisp.sequence.adapter.impl.SequenceGeneratorAdapter;

public class SequenceGeneratorAdapterTest {

	@Mock
	private MongoOperations mongoOperation;

	@InjectMocks
	private SequenceGeneratorAdapter sequenceGeneratorAdapter;

	private String sequenceName = "sequence1";
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getNextSequenceIdExceptionCaseTest(){
		assertEquals(0,sequenceGeneratorAdapter.getNextSequenceId(sequenceName));
	}
	
	@Test
	public void getCustomNextSequenceIdTest(){
		assertEquals("SUCCESS", sequenceGeneratorAdapter.getCustomNextSequenceId(sequenceName, "test"));
	}
}

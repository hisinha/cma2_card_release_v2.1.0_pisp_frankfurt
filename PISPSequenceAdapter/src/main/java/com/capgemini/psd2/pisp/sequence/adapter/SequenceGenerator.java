package com.capgemini.psd2.pisp.sequence.adapter;

public interface SequenceGenerator {
	public long getNextSequenceId(String sequenceName);
	public String getCustomNextSequenceId(String sequenceName,String customParameter);	
}

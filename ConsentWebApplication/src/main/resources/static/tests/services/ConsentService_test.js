"use strict";
describe("ConsentApp.ConsentService_test", function() {

    beforeEach(module("consentApp"));

    var service;

    beforeEach(inject(function($injector) {
        service = $injector.get("ConsentService");
    }));

    describe("test accountRequest", function() {
        it("test with request data", function() {
            inject(function($http) {
                spyOn($http, "post").and.returnValue("output");

                var reqData = "data";
                var authUrl = "url";
                var output = "output";
                var consentType = "AISP";

                output = service.accountRequest(consentType, reqData, authUrl);
                expect($http.post).toHaveBeenCalledWith("./" + consentType + "/consent?oAuthUrl=" + authUrl, reqData);
                expect(output).toBe("output");
            });
        });

        it("test without request data", function() {
            inject(function($http) {
                spyOn($http, "post");

                var reqData = null;
                var authUrl = "url";
                var output = null;
                var consentType = "AISP";

                output = service.accountRequest(consentType, reqData, authUrl);
                expect($http.post).not.toHaveBeenCalled();
                expect(output).toBeUndefined();
            });
        });
    });

    describe("test cancelRequest", function() {
        it("test request", function() {
            inject(function($http) {
                spyOn($http, "put").and.returnValue("output");

                var reqData = "data";
                var authUrl = "url";
                var output = "output";
                var consentType = "AISP";

                output = service.cancelRequest(consentType, reqData, authUrl);
                expect($http.put).toHaveBeenCalledWith("./" + consentType + "/cancelConsent?oAuthUrl=" + authUrl, reqData);
            });
        });
    });

    describe("test fundsCheckRequest", function() {
        it("test request", function() {
            inject(function($http) {
                spyOn($http, "post").and.returnValue("output");

                var reqData = "data";
                var authUrl = "url";
                var output = "output";
                var userId = "undefined";

                output = service.fundsCheckRequest(reqData, authUrl);
                // expect($http.get).toHaveBeenCalledWith("./js/services/FundsCheck.json");
                expect($http.post).toHaveBeenCalledWith("./preAuthorisation?oAuthUrl=" + authUrl + "&x-user-id=" + userId, reqData);
            });
        });
    });

    describe("test sessionLogout", function() {
        var $httpBackend;
        beforeEach(inject(function($injector) {
            // Set up the mock http service responses
            $httpBackend = $injector.get("$httpBackend");
        }));

        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it("test sessionLogout", function() {
            inject(function($http, $q) {
                $httpBackend.expectPOST("./logoutActionOnClose").respond({});
                service.sessionLogout();
                $httpBackend.flush();
            });
        });

    });
});
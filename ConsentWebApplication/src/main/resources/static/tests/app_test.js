"use strict";
describe("test App Bootstrapping for CISP", function() {
    var $rootScope, config, $location;
    beforeEach(module("consentApp"));
    beforeEach(function() {
        appendSetFixtures('<input type="hidden" id="consentType" name="consentType" value="CISP"/>');
    });
    beforeEach(inject(function(_$rootScope_, _config_, _$location_) {
        $rootScope = _$rootScope_;
        config = _config_;
        $location = _$location_;
        spyOn($location, "path").and.returnValue("/");
        spyOn($rootScope, "$broadcast");
        $rootScope.$broadcast.and.callThrough();
    }));
    describe("test all", function() {
        it("test on page load scoll to be on top", function() {
            $rootScope.$broadcast("$stateChangeSuccess");
            expect(document.body.scrollTop).toEqual(0);
            expect(document.body.scrollTop).toEqual(document.documentElement.scrollTop);
        });
        it("Should load the account page if there is no data for review page", function() {
            $rootScope.$broadcast("$stateChangeStart", { "name": "aispReview" }, { "accountRequestDetails": null });
            expect($location.path).toHaveBeenCalled();
        });

        it("should set CISP as consent type  in config", function() {
            expect(config.applicationType).toEqual("/cisp-account");
        });
    });
});
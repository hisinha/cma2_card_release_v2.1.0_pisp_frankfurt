"use strict";
describe("ConsentApp.AispReviewCtrl_test", function() {

    beforeEach(module("consentApp"));

    var $controller,
        $scope,
        state,
        mockConsentService,
        mockBlockUI,
        q,
        deferred,
        config,
        accountRequestDetails,
        tppInfo,
        $translate,
        translateDeferred,
        windowObj = { location: { href: "" } },
        errorData = { errorCode: 999, correlationId: null },
        defaultErrorData = { errorCode: "800", correlationId: null };

    beforeEach(function() {
        mockConsentService = {
            cancelRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            },
            accountRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            },
            sessionLogout: function() { /**/ }
        };
        module(function($provide) {
            $provide.value("$window", windowObj);
        });
        accountRequestDetails = { "accountReqData": { "Data": { "AccountRequestId": "03be465a-0555-4bb4-8fa2-d22e131986ce", "Status": "AwaitingAuthorisation", "CreationDateTime": "2017-07-07T16:48:07.159", "Permissions": ["ReadAccountsBasic", "ReadAccountsDetail", "ReadBalances", "ReadBeneficiariesBasic", "ReadBeneficiariesDetail", "ReadDirectDebits", "ReadStandingOrdersBasic", "ReadStandingOrdersDetail", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadTransactionsDetail"], "ExpirationDateTime": "2017-05-02T00:00:00-00:00", "TransactionFromDateTime": "2017-05-03T00:00:00-00:00", "TransactionToDateTime": "2017-12-03T00:00:00-00:00" }, "Risk": null, "SelectedAccounts": [{ "userId": "1245", "accountNumber": "10203355", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "Salary", "accountNSC": " SC802001", "selected": true, "$$hashKey": "object:68" }, { "userId": "1246", "accountNumber": "10203356", "accountName": "John Doe", "accountType": "checking", "currency": "doller", "nickname": "Salary", "accountNSC": " SC802001", "selected": true, "$$hashKey": "object:69" }] } };
        state = { "params": { "accountRequestDetails": accountRequestDetails } };
        mockBlockUI = { start: function() { /**/ }, stop: function() { /**/ } };
    });

    beforeEach(inject(function(_$controller_, $rootScope, ConsentService, $state, $q, _$window_) {
        jasmine.getFixtures().fixturesPath = "base/tests/fixtures/";
        $scope = $rootScope.$new();
        q = $q;
        windowObj = _$window_;
        translateDeferred = q.defer();
        $translate = jasmine.createSpy("$translate").and.returnValue(translateDeferred.promise);
        var controller = _$controller_("AispReviewCtrl", { $scope: $scope, ConsentService: mockConsentService, $state: state, $window: windowObj, blockUI: mockBlockUI, $translate: $translate });
        config = {
            maskAccountNumberLength: 4,
            visibleSelectedAccounts: 3
        };
        tppInfo = "Moneywise";
        loadFixtures("index-aisp.html");
    }));

    describe("test init method", function() {
        it("initiate happy flow", function() {
            inject(function(config, $state) {
                $state.params.accountRequestDetails = accountRequestDetails;
                $scope.init();
                expect($scope.accountReqDetails).toBe($state.params.accountRequestDetails);
                expect($scope.permissionsList).toBe(accountRequestDetails.accountReqData.Data.Permissions);
                expect($scope.selectedAcctTableData).toBe(accountRequestDetails.accountReqData.SelectedAccounts);
                expect($scope.expiryDate).toBe(accountRequestDetails.accountReqData.Data.ExpirationDateTime);
                expect($scope.termsConditn).toBe(false);
                expect($scope.totalNumberVisibleRows).toBe(config.totalNumberVisibleRows);
                var isCollapsed = $scope.selectedAcctTableData.length > $scope.totalNumberVisibleRows;
                expect($scope.isCollapsed).toBe(isCollapsed);
                expect($scope.tppInfo).toBe(tppInfo);
                expect($scope.tillDate).toBe(accountRequestDetails.accountReqData.Data.TransactionToDateTime);
                expect($scope.fromDate).toBe(accountRequestDetails.accountReqData.Data.TransactionFromDateTime);
            });
        });

        it("h2", function() {
            inject(function(config, $state) {
                var accntReqDet = accountRequestDetails;
                accntReqDet.accountReqData.Data.Permissions = ["ReadAccountsBasic", "ReadAccountsDetail", "ReadBalances", "ReadBeneficiariesBasic", "ReadBeneficiariesDetail", "ReadDirectDebits", "ReadStandingOrdersBasic", "ReadStandingOrdersDetail", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDetail"];
                $state.params.accountRequestDetails = accountRequestDetails;
                $scope.init();
                //expect($scope.permissionListData).toBe(Array);
            });
        });
        it("h3", function() {
            inject(function(config, $state) {
                var accntReqDet = accountRequestDetails;
                accntReqDet.accountReqData.Data.Permissions = ["ReadAccountsBasic", "ReadAccountsDetail", "ReadBalances", "ReadBeneficiariesBasic", "ReadBeneficiariesDetail", "ReadDirectDebits", "ReadStandingOrdersBasic", "ReadStandingOrdersDetail", "ReadTransactionsBasic", "ReadTransactionsDebits", "ReadTransactionsDetail"];
                $state.params.accountRequestDetails = accountRequestDetails;
                $scope.init();
                //expect($scope.permissionListData).toBe(Array);
            });
        });
    });

    describe("test allowSubmission happy flow", function() {
        it("allowSubmission", function() {
            inject(function(ConsentService, blockUI) {
                var authUrl = $("#oAuthUrl").val();
                var payload = accountRequestDetails.accountReqData.SelectedAccounts;
                spyOn(blockUI, "start").and.returnValue({});
                spyOn(blockUI, "stop").and.returnValue({});
                spyOn(ConsentService, "accountRequest").and.returnValue({});
                spyOn($.fn, "submit").and.returnValue({});
                $scope.allowSubmission();
                //expect(ConsentService.cancelRequest).toHaveBeenCalledWith(null, authUrl);
                deferred.resolve({ status: 200, data: { redirectUri: "redirect-to" } });
                $scope.$digest();
                expect($("form").length).toEqual(1);
                expect($("form").attr("action")).toEqual("redirect-to");
                expect($("#user_oauth_approval", $("form")[0]).length).toEqual(1);
                expect($("#csrf", $("form")[0]).length).toEqual(1);
                expect($("form").submit).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe("test allowSubmission error flow", function() {
        it("allowSubmission branch if", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "accountRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.allowSubmission();

                spyOn($scope, "openSessionOutModal");
                deferred.reject({ status: 200, data: { exception: { errorCode: "731" }, redirectUri: "url" }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.openSessionOutModal).toHaveBeenCalledTimes(1);
            });
        });
        it("allowSubmission branch else", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "accountRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.allowSubmission();

                deferred.reject({ status: 200, data: { exception: { errorCode: 999 } }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.errorData).toEqual(errorData);
            });
        });
    });

    describe("test cancelSubmission happy flow", function() {
        it("cancelSubmission", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();
                //expect(ConsentService.cancelRequest).toHaveBeenCalledWith(null, authUrl);

                deferred.resolve({ status: 200, data: { redirectUri: "redirect-to" } });
                $scope.$digest();
                expect(windowObj.location.href).toEqual("redirect-to");
            });
        });
    });

    describe("test cancelSubmission error flow", function() {
        it("cancelSubmission else flow", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                deferred.reject({ status: 200, data: { exception: { errorCode: 999 } }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.errorData).toEqual(errorData);
            });
        });
        it("cancelSubmission branch 800", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                deferred.reject({ status: 200, data: { exception: { errorCode: "800" } }, headers: function() { return null } });
                $scope.$digest();
                expect($scope.errorData).toEqual(defaultErrorData);
            });
        });
        it("cancelSubmission", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                spyOn($scope, "openSessionOutModal");
                deferred.reject({ status: 200, data: { exception: { errorCode: "731" }, redirectUri: "url" }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.openSessionOutModal).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe("test sessionSubmission", function() {
        it("sessionSubmission", function() {
            $scope.redirectUri = "redirect-to";
            $scope.sessionSubmission();
            expect(windowObj.location.href).toEqual("redirect-to");
        });
    });

    describe("test openModal", function() {
        it("test openModal happy flow", function() {
            spyOn($scope.modelPopUpConf, "open");
            $scope.cancelSubmission = "cancelSubmissionState";

            $scope.openModal();
            expect($translate).toHaveBeenCalledWith(["CANCEL_POPUP_HEADER", "CANCEL_POPUP_BODY", "NO_BUTTON", "YES_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("cancelSubmissionState");
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.resolve({
                CANCEL_POPUP_HEADER: "RESOLVED_CANCEL_POPUP_HEADER",
                CANCEL_POPUP_BODY: "RESOLVED_CANCEL_POPUP_BODY",
                NO_BUTTON: "RESOLVED_NO_BUTTON",
                YES_BUTTON: "RESOLVED_YES_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("RESOLVED_CANCEL_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("RESOLVED_CANCEL_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("RESOLVED_YES_BUTTON");
            expect($scope.modelPopUpConf.btn.cancelbtn.label).toBe("RESOLVED_NO_BUTTON");
        });

        it("test openModal error flow", function() {
            spyOn($scope.modelPopUpConf, "open");
            $scope.cancelSubmission = "cancelSubmissionState";

            $scope.openModal();
            expect($translate).toHaveBeenCalledWith(["CANCEL_POPUP_HEADER", "CANCEL_POPUP_BODY", "NO_BUTTON", "YES_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("cancelSubmissionState");
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.reject({
                CANCEL_POPUP_HEADER: "REJECTED_CANCEL_POPUP_HEADER",
                CANCEL_POPUP_BODY: "REJECTED_CANCEL_POPUP_BODY",
                NO_BUTTON: "REJECTED_NO_BUTTON",
                YES_BUTTON: "REJECTED_YES_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("REJECTED_CANCEL_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("REJECTED_CANCEL_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("REJECTED_YES_BUTTON");
            expect($scope.modelPopUpConf.btn.cancelbtn.label).toBe("REJECTED_NO_BUTTON");
        });
    });

    describe("test openSessionOutModal", function() {
        it("test openSessionOutModal happy flow", function() {
            $scope.sessionSubmission = "sessionSubmissionState";
            spyOn($scope.modelPopUpConf, "open");
            $scope.openSessionOutModal();
            expect($translate).toHaveBeenCalledWith(["SESSION_TIMEOUT_POPUP_HEADER", "SESSION_TIMEOUT_POPUP_BODY", "OK_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(false);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("sessionSubmissionState");
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.resolve({
                SESSION_TIMEOUT_POPUP_HEADER: "RESOLVED_SESSION_TIMEOUT_POPUP_HEADER",
                SESSION_TIMEOUT_POPUP_BODY: "RESOLVED_SESSION_TIMEOUT_POPUP_BODY",
                OK_BUTTON: "RESOLVED_OK_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("RESOLVED_SESSION_TIMEOUT_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("RESOLVED_SESSION_TIMEOUT_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("RESOLVED_OK_BUTTON");
        });

        it("test openSessionOutModal error flow", function() {
            $scope.sessionSubmission = "sessionSubmissionState";
            spyOn($scope.modelPopUpConf, "open");

            $scope.openSessionOutModal();
            expect($translate).toHaveBeenCalledWith(["SESSION_TIMEOUT_POPUP_HEADER", "SESSION_TIMEOUT_POPUP_BODY", "OK_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(false);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("sessionSubmissionState");
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.reject({
                SESSION_TIMEOUT_POPUP_HEADER: "REJECTED_SESSION_TIMEOUT_POPUP_HEADER",
                SESSION_TIMEOUT_POPUP_BODY: "REJECTED_SESSION_TIMEOUT_POPUP_BODY",
                OK_BUTTON: "REJECTED_OK_BUTTON"
            });
            $scope.$digest();
            expect($scope.modelPopUpConf.modelpopupTitle).toBe("REJECTED_SESSION_TIMEOUT_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("REJECTED_SESSION_TIMEOUT_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("REJECTED_OK_BUTTON");

        });
    });
});
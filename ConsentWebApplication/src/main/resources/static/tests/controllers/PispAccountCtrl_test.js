"use strict";
describe("ConsentApp.PispAccountCtrl_test", function() {
    beforeEach(module("consentApp"));

    var $controller,
        $scope,
        mockConsentService,
        q,
        deferred,
        config,
        paymentSetup,
        accountSelected,
        eventObj,
        psuAccounts,
        tppInfo,
        selAccounts,
        accountService,
        fundsCheckService,
        $translate,
        translateDeferred,
        blockUI,
        windowObj = { location: { href: "" } },
        errorData = { errorCode: 999, correlationId: null },
        defaultErrorData = { errorCode: "800", correlationId: null };

    beforeEach(function() {
        mockConsentService = {
            cancelRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            },
            fundsCheckRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            },
            sessionLogout: function() { /**/ }
        };
        module(function($provide) {
            $provide.value("$window", windowObj);
        });
    });

    beforeEach(inject(function($controller, $rootScope, ConsentService, $q, _$window_) {
        jasmine.getFixtures().fixturesPath = "base/tests/fixtures/";
        $scope = $rootScope.$new();
        q = $q;
        windowObj = _$window_;
        translateDeferred = q.defer();
        $translate = jasmine.createSpy("$translate").and.returnValue(translateDeferred.promise);
        blockUI = jasmine.createSpyObj("blockUI", ["stop"]);
        var controller = $controller("PispAccountCtrl", { $scope: $scope, ConsentService: mockConsentService, $window: windowObj, $translate: $translate, blockUI: blockUI });
        config = {
            accTypeChecking: "Checking",
            accTypeCredit: "CreditCard",
            minConsentPeriod: 1,
            maxConsentPeriod: 180,
            minTransHistoryPeriod: 1,
            maxTransHistoryPeriod: 36,
            lang: "en",
            defaultCssTheme: "default-theme.css",
            logoPath: "img/logo.png",
            aispConsent: "AISP",
            pispConsent: "PISP",
            cispConsent: "CISP",
            searchOnAccounts: false,
            pageSize: 10,
            maxPaginationSize: 4,
            maskAccountNumberLength: 4,
            visibleSelectedAccounts: 3,
            modelpopupConfig: {
                "open": function() { /**/ },
                "modelpopupTitle": "Title",
                "modelpopupBodyContent": "Content",
                "btn": {
                    "okbtn": { "visible": true, "label": "", "action": null },
                    "cancelbtn": { "visible": true, "label": "", "action": null }
                }
            }
        };
        tppInfo = "Moneywise";
        eventObj = jasmine.createSpyObj("eventObj", ["preventDefault"]);
        paymentSetup = { "Data": { "PaymentId": "10319", "Status": "AcceptedTechnicalValidation", "CreationDateTime": "2017-08-08T15:11:53.859Z", "Initiation": { "InstructionIdentification": "ABDDC", "EndToEndIdentification": "FRESCO.21302.GFX.28", "InstructedAmount": { "Amount": "777777777.0", "Currency": "EUR" }, "DebtorAgent": { "SchemeName": "UKSortCode", "Identification": "SC112800" }, "DebtorAccount": { "SchemeName": "IBAN", "Identification": "GB29NWBK60161331926819", "Name": "Andrea Smith", "SecondaryIdentification": "0002" }, "CreditorAgent": { "SchemeName": "UKSortCode", "Identification": "SC080801" }, "CreditorAccount": { "SchemeName": "BBAN", "Identification": "NWBK60161331926819", "Name": "Test user", "SecondaryIdentification": "0002" }, "RemittanceInformation": { "Unstructured": "Internal ops code 5120101", "Reference": "FRESCO-101" } } }, "Risk": { "MerchantCategoryCode": "5967", "MerchantCustomerIdentification": "053598653254", "DeliveryAddress": { "AddressLine": ["Flat 7", "Acacia Lodge"], "StreetName": "AcaciaAvenue", "BuildingNumber": "27", "PostCode": "GU31 2ZZ", "TownName": "Sparsholt", "CountrySubDivision": ["ABCDABCDABCDABCDAB", "DEFG"], "Country": "UK" } }, "Links": { "self": "https://LIN51005623.corp.capgemini.com:80/fundtransferv1/banks/payments/10319" }, "Meta": {} };
        psuAccounts = { "Data": [{ "AccountId": "e51f7007-23ff-4de4-b3b1-ef6c310e1e9e", "Currency": "GBP", "Nickname": "Bills", "Account": { "SchemeName": "BBAN", "Identification": "76528775", "Name": "Mr Kevin", "SecondaryIdentification": "0011" }, "Servicer": { "SchemeName": "UKSortCode", "Identification": "SC802001" } }, { "AccountId": "e51f7008-23ff-4de4-b3b1-ef6c310e1e9e", "Currency": "GBP", "Nickname": "Hills", "Account": { "SchemeName": "BBAN", "Identification": "76528776", "Name": "Mr piter", "SecondaryIdentification": "0011" }, "Servicer": { "SchemeName": "UKSortCode", "Identification": "SC802001" } }], "Links": { "self": "/accounts/" }, "Meta": { "total-pages": 1 } };
        accountSelected = false;
        selAccounts = [{ "AccountId": "e51f7007-23ff-4de4-b3b1-ef6c310e1e9e", "Currency": "GBP", "Nickname": "Bills", "Account": { "SchemeName": "BBAN", "Identification": "76528775", "Name": "Mr Kevin", "SecondaryIdentification": "0011" }, "Servicer": { "SchemeName": "UKSortCode", "Identification": "SC802001" } }];
        loadFixtures("index-pisp.html");
    }));

    describe("test init method", function() {
        it("initiate happy flow", function() {
            inject(function(config) {
                spyOn($scope, "createCustomErr");
                spyOn($scope, "selectAccount");
                appendSetFixtures("<input type='hidden' id='accountSelected' name='accountSelected' value='false'>");

                var error = { "errorCode": 999 };
                $scope.init();
                expect($scope.errorData).toBe(null);
                expect($scope.isDataFound).toBe(false);
                expect($scope.retry).toBe(null);
                expect($scope.modelPopUpConf).toBe(config.modelpopupConfig);
                expect($scope.paymentinfo).toEqual(angular.fromJson(paymentSetup));
                expect($scope.psuAcct).toEqual(angular.fromJson(psuAccounts));
                expect($scope.accountSelected).toEqual(angular.fromJson(accountSelected));
                expect($scope.payInstBy).toBe(tppInfo);
                expect($scope.maskAccountNumberLength).toBe(config.maskAccountNumberLength);
                expect($scope.isAccSelected).toBe(false);
                expect($scope.termsConditn).toBe(false);
                expect($scope.stopCnfirm).toBe(true);
                expect($scope.accData).toEqual(psuAccounts.Data);
                expect($scope.payeeDetails).toEqual(paymentSetup.Data.Initiation);
                expect($scope.payeeName).toEqual(paymentSetup.Data.Initiation.CreditorAccount.Name);
                expect($scope.payeeReference).toBe(paymentSetup.Data.Initiation.RemittanceInformation.Reference);
                expect($scope.amount).toBe(paymentSetup.Data.Initiation.InstructedAmount.Amount);
                expect($scope.currency).toBe(paymentSetup.Data.Initiation.InstructedAmount.Currency);
            });
        });

        it("initiate error flow", function() {
            appendSetFixtures("<input type='hidden' id='error' name='error' value='{&quot;errorCode&quot;:999}' />");
            spyOn($scope, "createCustomErr");
            $scope.init();
            expect($scope.createCustomErr).toHaveBeenCalled();
        });

        it("initiate no data flow", function() {
            loadFixtures("index-no-data-pisp.html");
            spyOn($scope, "createCustomErr");
            $scope.init();
            expect($scope.psuAcct.Data.length).toBe(0);
        });

        it("initiate selected account data flow", function() {
            appendSetFixtures("<input type='hidden' id='accountSelected' name='accountSelected' value='true'>");
            spyOn($scope, "createCustomErr");
            $scope.init();
            expect($scope.selectedAcctObject).toEqual(psuAccounts.Data[0]);
            expect($scope.stopCnfirm).toBe(false);
        });

        it("back button", function() {
            inject(function($state) {
                var pispContractDetails = { "payInstBy": "Moneywise", "payeeName": "Test user", "payeeRef": "FRESCO-101", "amount": "777777777.0", "currency": "EUR", "accountDetails": { "AccountId": "e51f7007-23ff-4de4-b3b1-ef6c310e1e9e", "Currency": "GBP", "Nickname": "Bills", "Account": { "SchemeName": "BBAN", "Identification": "76528775", "Name": "Mr Kevin", "SecondaryIdentification": "0011" }, "Servicer": { "SchemeName": "UKSortCode", "Identification": "SC802001" } } };
                spyOn($scope, "selectAccount");
                $state.params.pispContractDetails = pispContractDetails;
                $scope.init();
                expect($scope.selectedAcctObject).toEqual(pispContractDetails.accountDetails);
                expect($scope.selectAccount).toHaveBeenCalledWith(pispContractDetails.accountDetails, event);
            });
        });
    });

    describe("test confirm", function() {
        it("confirm happy flow", function() {
            inject(function(ConsentService, $state) {
                var authUrl = $("#oAuthUrl").val();
                $scope.selectedAcctObject = selAccounts[0];
                $scope.accData = psuAccounts.Data;
                $scope.payInstBy = tppInfo;
                $scope.payeeName = paymentSetup.Data.Initiation.CreditorAccount.Name;
                $scope.payeeReference = paymentSetup.Data.Initiation.RemittanceInformation.Reference;
                $scope.amount = paymentSetup.Data.Initiation.InstructedAmount.Amount;
                $scope.currency = paymentSetup.Data.Initiation.InstructedAmount.Currency;
                var paymentDetails = {
                    payInstBy: $scope.payInstBy,
                    payeeName: $scope.payeeName,
                    payeeRef: $scope.payeeReference,
                    amount: $scope.amount,
                    currency: $scope.currency,
                    accountDetails: selAccounts[0]
                };
                spyOn($state, "go");
                spyOn(ConsentService, "fundsCheckRequest").and.returnValue({});
                $scope.confirm();
                deferred.resolve({ status: 200, data: {} });
                $scope.$digest();
                expect($state.go).toHaveBeenCalledWith("pispReview", {
                    "paymentDetails": paymentDetails
                });
            });
        });
    });

    describe("test confirm error flow", function() {
        it("confirm branch if", function() {
            inject(function(ConsentService) {
                var authUrl = $("#oAuthUrl").val();
                $scope.selectedAcctObject = selAccounts[0];
                $scope.accData = psuAccounts.Data;
                $scope.payInstBy = tppInfo;
                $scope.payeeName = paymentSetup.Data.Initiation.CreditorAccount.Name;
                $scope.payeeReference = paymentSetup.Data.Initiation.RemittanceInformation.Reference;
                $scope.amount = paymentSetup.Data.Initiation.InstructedAmount.Amount;
                $scope.currency = paymentSetup.Data.Initiation.InstructedAmount.Currency;
                var paymentDetails = {
                    payInstBy: $scope.payInstBy,
                    payeeName: $scope.payeeName,
                    payeeRef: $scope.payeeReference,
                    amount: $scope.amount,
                    currency: $scope.currency,
                    accountDetails: selAccounts[0]
                };
                spyOn(ConsentService, "fundsCheckRequest").and.returnValue({});
                $scope.confirm();

                spyOn($scope, "openSessionOutModal");
                deferred.reject({ status: 200, data: { exception: { errorCode: "731" }, redirectUri: "url" }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.openSessionOutModal).toHaveBeenCalledWith();
            });
        });
        it("confirm branch else", function() {
            inject(function(ConsentService) {
                var authUrl = $("#oAuthUrl").val();
                $scope.selectedAcctObject = selAccounts[0];
                $scope.accData = psuAccounts.Data;
                $scope.payInstBy = tppInfo;
                $scope.payeeName = paymentSetup.Data.Initiation.CreditorAccount.Name;
                $scope.payeeReference = paymentSetup.Data.Initiation.RemittanceInformation.Reference;
                $scope.amount = paymentSetup.Data.Initiation.InstructedAmount.Amount;
                $scope.currency = paymentSetup.Data.Initiation.InstructedAmount.Currency;
                var paymentDetails = {
                    payInstBy: $scope.payInstBy,
                    payeeName: $scope.payeeName,
                    payeeRef: $scope.payeeReference,
                    amount: $scope.amount,
                    currency: $scope.currency,
                    accountDetails: selAccounts[0]
                };
                spyOn(ConsentService, "fundsCheckRequest").and.returnValue({});
                $scope.confirm();

                deferred.reject({ status: 200, data: { exception: { errorCode: 999 } }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.stopCnfirm).toBe(true);
                expect($scope.errorData).toEqual(errorData);
            });
        });
    });
    describe("test selected account", function() {
        it("selectedAccount happy flow", function() {
            var selectedAccount = { "AccountId": "e51f7007-23ff-4de4-b3b1-ef6c310e1e9e", "Currency": "GBP", "Nickname": "Bills", "Account": { "SchemeName": "BBAN", "Identification": "76528775", "Name": "Mr Kevin", "SecondaryIdentification": "0011" }, "Servicer": { "SchemeName": "UKSortCode", "Identification": "SC802001" }, "$$hashKey": "object:12" };
            $scope.selectAccount(selectedAccount, eventObj);
            expect($scope.selectedAcctObject).toBe(selectedAccount);
            expect($scope.isAccSelected).toBe(true);
            expect($scope.stopCnfirm).toBe(false);
        });

        it("selectedAccount error flow", function() {
            var selectedAccount = null;
            $scope.selectAccount(selectedAccount, eventObj);
            expect($scope.selectedAcctObject).toBe(null);
            expect($scope.isAccSelected).toBe(false);
            expect($scope.stopCnfirm).toBe(true);
        });
    });


    describe("test cancelSubmission happy flow", function() {
        it("cancelSubmission", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();
                //expect(ConsentService.cancelRequest).toHaveBeenCalledWith(null, authUrl);

                deferred.resolve({ status: 200, data: { redirectUri: "redirect-to" } });
                $scope.$digest();
                expect(windowObj.location.href).toEqual("redirect-to");
            });
        });
    });

    describe("test cancelSubmission error flow", function() {
        it("cancelSubmission else flow", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                deferred.reject({ status: 200, data: { exception: { errorCode: 999 } }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.errorData).toEqual(errorData);
            });
        });
        it("cancelSubmission branch 800", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                deferred.reject({ status: 200, data: { exception: { errorCode: "800" } }, headers: function() { return null } });
                $scope.$digest();
                expect($scope.errorData).toEqual(defaultErrorData);
            });
        });
        it("cancelSubmission", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                spyOn($scope, "openSessionOutModal");
                deferred.reject({ status: 200, data: { exception: { errorCode: "731" }, redirectUri: "url" }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.openSessionOutModal).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe("test sessionSubmission", function() {
        it("sessionSubmission", function() {
            $scope.redirectUri = "redirect-to";
            $scope.sessionSubmission();
            expect(windowObj.location.href).toEqual("redirect-to");
        });
    });

    describe("test openCancelModal", function() {
        it("test openCancelModal happy flow", function() {
            spyOn($scope.modelPopUpConf, "open");
            $scope.cancelSubmission = "cancelSubmissionState";

            $scope.openCancelModal();
            expect($translate).toHaveBeenCalledWith(["CANCEL_POPUP_HEADER", "CANCEL_POPUP_BODY", "NO_BUTTON", "YES_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("cancelSubmissionState");
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.resolve({
                CANCEL_POPUP_HEADER: "RESOLVED_CANCEL_POPUP_HEADER",
                CANCEL_POPUP_BODY: "RESOLVED_CANCEL_POPUP_BODY",
                NO_BUTTON: "RESOLVED_NO_BUTTON",
                YES_BUTTON: "RESOLVED_YES_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("RESOLVED_CANCEL_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("RESOLVED_CANCEL_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("RESOLVED_YES_BUTTON");
            expect($scope.modelPopUpConf.btn.cancelbtn.label).toBe("RESOLVED_NO_BUTTON");
        });

        it("test openCancelModal error flow", function() {
            spyOn($scope.modelPopUpConf, "open");
            $scope.cancelSubmission = "cancelSubmissionState";

            $scope.openCancelModal();
            expect($translate).toHaveBeenCalledWith(["CANCEL_POPUP_HEADER", "CANCEL_POPUP_BODY", "NO_BUTTON", "YES_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("cancelSubmissionState");
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.reject({
                CANCEL_POPUP_HEADER: "REJECTED_CANCEL_POPUP_HEADER",
                CANCEL_POPUP_BODY: "REJECTED_CANCEL_POPUP_BODY",
                NO_BUTTON: "REJECTED_NO_BUTTON",
                YES_BUTTON: "REJECTED_YES_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("REJECTED_CANCEL_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("REJECTED_CANCEL_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("REJECTED_YES_BUTTON");
            expect($scope.modelPopUpConf.btn.cancelbtn.label).toBe("REJECTED_NO_BUTTON");
        });
    });

    describe("test openSessionOutModal", function() {
        it("test openSessionOutModal happy flow", function() {
            $scope.sessionSubmission = "sessionSubmissionState";
            spyOn($scope.modelPopUpConf, "open");
            $scope.openSessionOutModal();
            expect($translate).toHaveBeenCalledWith(["SESSION_TIMEOUT_POPUP_HEADER", "SESSION_TIMEOUT_POPUP_BODY", "OK_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(false);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("sessionSubmissionState");
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.resolve({
                SESSION_TIMEOUT_POPUP_HEADER: "RESOLVED_SESSION_TIMEOUT_POPUP_HEADER",
                SESSION_TIMEOUT_POPUP_BODY: "RESOLVED_SESSION_TIMEOUT_POPUP_BODY",
                OK_BUTTON: "RESOLVED_OK_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("RESOLVED_SESSION_TIMEOUT_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("RESOLVED_SESSION_TIMEOUT_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("RESOLVED_OK_BUTTON");
        });

        it("test openSessionOutModal error flow", function() {
            $scope.sessionSubmission = "sessionSubmissionState";
            spyOn($scope.modelPopUpConf, "open");
            $scope.openSessionOutModal();
            expect($translate).toHaveBeenCalledWith(["SESSION_TIMEOUT_POPUP_HEADER", "SESSION_TIMEOUT_POPUP_BODY", "OK_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(false);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("sessionSubmissionState");
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.reject({
                SESSION_TIMEOUT_POPUP_HEADER: "REJECTED_SESSION_TIMEOUT_POPUP_HEADER",
                SESSION_TIMEOUT_POPUP_BODY: "REJECTED_SESSION_TIMEOUT_POPUP_BODY",
                OK_BUTTON: "OK_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("REJECTED_SESSION_TIMEOUT_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("REJECTED_SESSION_TIMEOUT_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("OK_BUTTON");
        });
    });
});
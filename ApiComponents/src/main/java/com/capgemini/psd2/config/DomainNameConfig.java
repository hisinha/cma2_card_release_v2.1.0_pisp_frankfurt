package com.capgemini.psd2.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
public class DomainNameConfig {

	private Map<String,String> domainNameMap = new HashMap<>();

	public Map<String, String> getDomainNameMap() {
		return domainNameMap;
	}

	public void setDomainNameMap(Map<String, String> domainNameMap) {
		this.domainNameMap = domainNameMap;
	}
	
	public String getTenantSpecificDomain(String id) {
		return this.domainNameMap.get(id);
	}
}

/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exceptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * The Class PSD2ExceptionHandler.
 */

@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class AspectExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private ApiEndExceptionHandler apiEndExceptionHandler;

	/**
	 * Handle invalid request.
	 *
	 * @param e
	 *            the e
	 * @param request
	 *            the request
	 * @return the response entity
	 */
	@ExceptionHandler({ Exception.class })
	protected ResponseEntity<Object> handleInvalidRequest(Exception e, WebRequest request) {
		ExceptionAttributes exceptionAttributes = apiEndExceptionHandler.handleControllerAdviceException(e, request);
		
		if(exceptionAttributes.getObErrorResponse1()!=null)
			return handleExceptionInternal(e, exceptionAttributes.getObErrorResponse1(), exceptionAttributes.getHeaders(),
				exceptionAttributes.getStatus(), request);
		else
			return handleExceptionInternal(e, exceptionAttributes.getErrorInfo(), exceptionAttributes.getHeaders(),
					exceptionAttributes.getStatus(), request);
	}
}

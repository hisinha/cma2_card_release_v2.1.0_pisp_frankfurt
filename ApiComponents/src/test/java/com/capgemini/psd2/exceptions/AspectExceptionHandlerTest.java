package com.capgemini.psd2.exceptions;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.capgemini.psd2.pisp.domain.OBErrorResponse1;

public class AspectExceptionHandlerTest {
	
	@Mock
	private WebRequest request;

	@Mock
	private ApiEndExceptionHandler apiEndExceptionHandler;
	
	@Mock
	private ResponseEntityExceptionHandler responseEntityExceptionHandler;
	
	@InjectMocks
	private AspectExceptionHandler obj=new AspectExceptionHandler();
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testHandleInvalidRequest() {
		ExceptionAttributes exception=new ExceptionAttributes();
		exception.setStatus(HttpStatus.BAD_REQUEST);
		exception.setErrorInfo(new ErrorInfo());
		//exception.setHeaders(HttpHeaders.);
		
		when(apiEndExceptionHandler.handleControllerAdviceException(any(Exception.class), any(WebRequest.class))).thenReturn(exception);
		obj.handleInvalidRequest(new Exception(), request);
		OBErrorResponse1 errorResponse1=new OBErrorResponse1();
		exception.setObErrorResponse1(errorResponse1);
		when(apiEndExceptionHandler.handleControllerAdviceException(any(Exception.class), any(WebRequest.class))).thenReturn(exception);
		obj.handleInvalidRequest(new OBPSD2Exception(errorResponse1, "error message"), request);
	}
}

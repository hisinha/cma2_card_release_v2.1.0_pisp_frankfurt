package com.capgemini.psd2.pisp.standing.order.mongodb.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsentResponse1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.standing.order.mongodb.adapter.impl.DStandingOrderConsentStagingMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.standing.order.mongodb.adapter.repository.DStandingOrderConsentFoundationRepository;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class DStandingOrderConsentStagingMongoDbAdapterImplTest {

	@Mock
	private SandboxValidationUtility utility;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@Mock
	private DStandingOrderConsentFoundationRepository repository;

	@InjectMocks
	private List<OBCharge1> chargesList = new ArrayList<OBCharge1>();

	@InjectMocks
	private DStandingOrderConsentStagingMongoDbAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testProcessStandingOrderConsents() {
		CustomDStandingOrderConsentsPOSTRequest request = new CustomDStandingOrderConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		request.setData(new OBWriteDataDomesticStandingOrderConsent1());
		request.getData().setInitiation(new OBDomesticStandingOrder1());
		request.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		request.getData().getInitiation().getFirstPaymentAmount().setCurrency("EUR");
		request.getData().getInitiation().getFirstPaymentAmount().setAmount("786");

		request.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		request.getData().getInitiation().getCreditorAccount().setSecondaryIdentification("Valid");

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setStatus(OBExternalConsentStatus1Code.AUTHORISED);

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.PASS);
		when(utility.getMockedCutOffDtTm()).thenReturn("2019-12-31T00:00:00+00:00");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(utility.isValidSecIdentification(anyString())).thenReturn(false);
		Mockito.when(repository.save(any(CustomDStandingOrderConsentsPOSTResponse.class))).thenReturn(response);
		adapter.processStandingOrderConsents(request, stageIdentifiers, params, OBExternalConsentStatus1Code.AUTHORISED,
				OBExternalConsentStatus1Code.REJECTED);
		assertNotNull(response);
		assertNotNull(request);
	}

	@Test
	public void testProcessStandingOrderConsentswithNullConsentId() {
		CustomDStandingOrderConsentsPOSTRequest request = new CustomDStandingOrderConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		request.setData(new OBWriteDataDomesticStandingOrderConsent1());
		request.getData().setInitiation(new OBDomesticStandingOrder1());
		request.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		request.getData().getInitiation().getFirstPaymentAmount().setCurrency("EUR");
		request.getData().getInitiation().getFirstPaymentAmount().setAmount("786");

		request.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		request.getData().getInitiation().getCreditorAccount().setSecondaryIdentification("Valid");

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setStatus(OBExternalConsentStatus1Code.REJECTED);

		when(utility.getMockedCutOffDtTm()).thenReturn("2019-12-31T00:00:00+00:00");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(utility.isValidSecIdentification(anyString())).thenReturn(false);
		Mockito.when(repository.save(any(CustomDStandingOrderConsentsPOSTResponse.class))).thenReturn(response);
		adapter.processStandingOrderConsents(request, stageIdentifiers, params, OBExternalConsentStatus1Code.AUTHORISED,
				OBExternalConsentStatus1Code.REJECTED);
		
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessStandingOrderConsentsValidAmountException() {
		CustomDStandingOrderConsentsPOSTRequest request = new CustomDStandingOrderConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		request.setData(new OBWriteDataDomesticStandingOrderConsent1());
		request.getData().setInitiation(new OBDomesticStandingOrder1());
		request.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		request.getData().getInitiation().getFirstPaymentAmount().setCurrency("EUR");
		request.getData().getInitiation().getFirstPaymentAmount().setAmount("786");

		request.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		request.getData().getInitiation().getCreditorAccount().setSecondaryIdentification("Valid");

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setStatus(OBExternalConsentStatus1Code.REJECTED);

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.FAIL);
		when(utility.getMockedCutOffDtTm()).thenReturn("2019-12-31T00:00:00+00:00");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		when(utility.isValidSecIdentification(anyString())).thenReturn(false);
		Mockito.when(repository.save(any(CustomDStandingOrderConsentsPOSTResponse.class))).thenReturn(response);
		adapter.processStandingOrderConsents(request, stageIdentifiers, params, OBExternalConsentStatus1Code.AUTHORISED,
				OBExternalConsentStatus1Code.REJECTED);
		assertTrue(utility.isValidAmount(anyString(), anyString()));
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessStandingOrderConsentsValidSecIdentificationException() {
		CustomDStandingOrderConsentsPOSTRequest request = new CustomDStandingOrderConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		request.setData(new OBWriteDataDomesticStandingOrderConsent1());
		request.getData().setInitiation(new OBDomesticStandingOrder1());
		request.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		request.getData().getInitiation().getFirstPaymentAmount().setCurrency("EUR");
		request.getData().getInitiation().getFirstPaymentAmount().setAmount("786");

		request.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		request.getData().getInitiation().getCreditorAccount().setSecondaryIdentification("Valid");

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setStatus(OBExternalConsentStatus1Code.REJECTED);

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.FAIL);
		when(utility.getMockedCutOffDtTm()).thenReturn("2019-12-31T00:00:00+00:00");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(utility.isValidSecIdentification(anyString())).thenReturn(true);
		Mockito.when(repository.save(any(CustomDStandingOrderConsentsPOSTResponse.class))).thenReturn(response);
		adapter.processStandingOrderConsents(request, stageIdentifiers, params, OBExternalConsentStatus1Code.AUTHORISED,
				OBExternalConsentStatus1Code.REJECTED);
		assertTrue(utility.isValidSecIdentification(anyString()));
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessStandingOrderConsentsDataAccessResourceFailureException() {
		CustomDStandingOrderConsentsPOSTRequest request = new CustomDStandingOrderConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		request.setData(new OBWriteDataDomesticStandingOrderConsent1());
		request.getData().setInitiation(new OBDomesticStandingOrder1());
		request.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		request.getData().getInitiation().getFirstPaymentAmount().setCurrency("EUR");
		request.getData().getInitiation().getFirstPaymentAmount().setAmount("786");

		request.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		request.getData().getInitiation().getCreditorAccount().setSecondaryIdentification("Valid");

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setStatus(OBExternalConsentStatus1Code.REJECTED);

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.FAIL);
		when(utility.getMockedCutOffDtTm()).thenReturn("2019-12-31T00:00:00+00:00");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(utility.isValidSecIdentification(anyString())).thenReturn(false);
		Mockito.when(repository.save(any(CustomDStandingOrderConsentsPOSTResponse.class)))
				.thenThrow(new DataAccessResourceFailureException("Test"));
		adapter.processStandingOrderConsents(request, stageIdentifiers, params, OBExternalConsentStatus1Code.AUTHORISED,
				OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void testRetrieveStagedDomesticStandingOrdersConsents() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setInitiation(new OBDomesticStandingOrder1());
		response.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		response.getData().getInitiation().getFirstPaymentAmount().setAmount("9999");

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(repository.findOneByDataConsentId(anyString())).thenReturn(response);
		adapter.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, params);

	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveStagedDomesticStandingOrdersConsentsDataFailureException() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setInitiation(new OBDomesticStandingOrder1());
		response.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		response.getData().getInitiation().getFirstPaymentAmount().setAmount("9999");

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		when(repository.findOneByDataConsentId(anyString())).thenThrow(new DataAccessResourceFailureException("Test"));
		adapter.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, params);
		assertTrue(utility.isValidAmount(anyString(), anyString()));
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveStagedDomesticStandingOrdersConsentsSandBoxMockException() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setInitiation(new OBDomesticStandingOrder1());
		response.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		response.getData().getInitiation().getFirstPaymentAmount().setAmount("9999");

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		when(repository.findOneByDataConsentId(anyString())).thenReturn(response);
		adapter.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, params);
		assertTrue(utility.isValidAmount(anyString(), anyString()));
	}

	@After
	public void tearDown() {
		utility = null;
		reqAttributes = null;
		repository = null;
		adapter = null;
	}
}

package com.capgemini.psd2.account.direct.debits.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBDirectDebit1;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit1;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit1Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountDirectDebitsResponse;

public class AccountDirectDebitsRoutingAdapterTestMockData {

	public static PlatformAccountDirectDebitsResponse getMockDirectDebitResponse() {
		PlatformAccountDirectDebitsResponse platformAccountDirectDebitsResponse = new PlatformAccountDirectDebitsResponse();

		OBReadDirectDebit1Data directDebitData = new OBReadDirectDebit1Data();
		List<OBDirectDebit1> directDebit = new ArrayList();

		directDebitData.setDirectDebit(directDebit);

		OBReadDirectDebit1 obReadDirectDebit1 = new OBReadDirectDebit1();
		obReadDirectDebit1.setData(directDebitData);
		platformAccountDirectDebitsResponse.setoBReadDirectDebit1(obReadDirectDebit1);

		return platformAccountDirectDebitsResponse;
	}

}

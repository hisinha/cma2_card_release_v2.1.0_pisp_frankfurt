package com.capgemini.psd2.kms.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.AWSKMSClientBuilder;
@Configuration
@ConditionalOnProperty(prefix = "aws.kms", name = "enabled", havingValue = "true", matchIfMissing = true)
public class KMSConfiguration {

	@Configuration
	@ConditionalOnMissingBean(AWSKMS.class)
	static class KmsConfiguration {

		@Value("#{T(com.amazonaws.regions.Regions).fromName('${aws.region}')}")
		private Regions region;

		@Value("${aws.proxyHost:null}")
		private String proxyHost;

		@Value("${aws.proxyPort:null}")
		private String proxyPort;

		@Value("#{T(com.amazonaws.Protocol).valueOf('${aws.protocol:HTTPS}')}")
		private Protocol protocol;

		@Value("${aws.proxy:#{false}}")
		private boolean proxy;

		@Bean
		@Primary
		public AWSKMS kmsConfiguration() {
			ClientConfiguration clientConfiguration = new ClientConfiguration();
			if(proxy){
				clientConfiguration.setProxyHost(proxyHost);
				clientConfiguration.setProxyPort(Integer.valueOf(proxyPort));
				clientConfiguration.setProtocol(protocol);
			}
			return AWSKMSClientBuilder.standard()
					.withClientConfiguration(clientConfiguration)
					.withRegion(region).build();
		}

	}

}

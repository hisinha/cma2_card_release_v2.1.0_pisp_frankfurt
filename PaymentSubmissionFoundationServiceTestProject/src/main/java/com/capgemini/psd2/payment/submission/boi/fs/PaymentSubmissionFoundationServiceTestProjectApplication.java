package com.capgemini.psd2.payment.submission.boi.fs;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.PaymentSubmissionFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.constants.PaymentSubmissionFoundationServiceConstants;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPOSTRequest;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class PaymentSubmissionFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentSubmissionFoundationServiceTestProjectApplication.class, args);
	}
	
}

@RestController
@ResponseBody
class TestPaymentInsertUpdateFSAdapter {

	@Autowired
	PaymentSubmissionFoundationServiceAdapter adapter;
	
	@RequestMapping(value="/testPaymentSubmissionAdapter", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public PaymentSubmissionExecutionResponse getResponse(
			@RequestBody CustomPaymentSubmissionPOSTRequest pr,
			@RequestHeader(required = false,value="X-BOI-USER") String boiUser,
			@RequestHeader(required = false,value="X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false,value="X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false,value="X-CORRELATION-ID") String correlationID) {
		
		Map<String, String> params = new HashMap<>();
		params.put(PaymentSubmissionFoundationServiceConstants.USER_ID, boiUser);
		params.put(PaymentSubmissionFoundationServiceConstants.CHANNEL_ID, boiChannel);
		params.put(PaymentSubmissionFoundationServiceConstants.CORRELATION_ID, correlationID);
		params.put(PaymentSubmissionFoundationServiceConstants.PLATFORM_ID, boiPlatform);
		params.put(PSD2Constants.PAYMENT_VALIDATION_STATUS, "Pending");
		params.put(PSD2Constants.PAYMENT_CREATED_ON, new DateTime().toString());
		
		pr.setCreatedOn("2017-12-04T12:48:12");
	
		
		return adapter.executePaymentSubmission(pr, params);
		//return adapter.updateStagedPaymentSetup(pr, params);

	}
}
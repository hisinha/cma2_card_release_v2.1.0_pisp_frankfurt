package com.capgemini.psd2.restclient;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.config.OBConfig;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.model.InlineResponse2009;
import com.capgemini.psd2.model.OBThirdPartyProviders;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Component
public class ScimRestClient {
	private static final Logger LOGGER = LoggerFactory.getLogger(ScimRestClient.class);
	@Autowired
	private RestClientSync clientSync;
	@Autowired
	private OBConfig config;
	@Autowired
	private LoggerUtils loggerUtils;

	public String retrieveOBAccessToken(String jwtToken) {
		LOGGER.info("{\"Enter\":\"{}\"}", "com.capgemini.psd2.restclient.ScimRestClient.retrieveOBAccessToken()",
				loggerUtils.populateLoggerData("retrieveOBAccessToken()"));
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(config.getTokenUrl());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> inputBody = new LinkedMultiValueMap<>();
		Map<String, String> obAccessToken = null;
		String accessToken;
		inputBody.add("client_id", config.getClientId());
		inputBody.add("client_assertion_type", config.getClientAssertionType());
		inputBody.add("grant_type", config.getGrantType());
		inputBody.add("scope", config.getScope());
		inputBody.add("client_assertion", jwtToken);
		try {

			clientSync.buildSSLRequest(config.getTransportkeyAlias());
			obAccessToken = clientSync.callForPost(requestInfo, inputBody, Map.class, headers);
			accessToken = obAccessToken.get("access_token");
		} catch (PSD2Exception ex) {
			throw PSD2Exception.populatePSD2Exception(ex.getErrorInfo().getActualDetailErrorMessage(),
					ErrorCodeEnum.TECHNICAL_ERROR);
		} catch (Exception ex) {
			throw PSD2Exception.populatePSD2Exception(ex.getMessage(), ErrorCodeEnum.TECHNICAL_ERROR);
		}
		LOGGER.info("{\"Exit\":\"{}\"}", "com.capgemini.psd2.restclient.ScimRestClient.retrieveOBAccessToken()",
				loggerUtils.populateLoggerData("retrieveOBAccessToken()"));
		return accessToken;
	}

	public OBThirdPartyProviders retrieveTPPStatus(String tppOrgId, String accessToken) {
		LOGGER.info("{\"Enter\":\"{}\"}", "com.capgemini.psd2.restclient.ScimRestClient.retrieveTPPStatus()",
				loggerUtils.populateLoggerData("retrieveTPPStatus()"));
		OBThirdPartyProviders tppDetails = null;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Authorization", "Bearer " + accessToken);
		RequestInfo requestInfoforApiCall = new RequestInfo();
		requestInfoforApiCall.setUrl(config.getApiUrl()+"/"+ tppOrgId + "?attributes=" + config.getResponseattributes());
		tppDetails = clientSync.callForGetWithParams(requestInfoforApiCall, OBThirdPartyProviders.class, null,
				httpHeaders);
		LOGGER.info("{\"Exit\":\"{}\",\"TppDetails\":\"{}\"}",
				"com.capgemini.psd2.restclient.ScimRestClient.retrieveOBAccessToken()", tppDetails,
				loggerUtils.populateLoggerData("retrieveTPPStatus()"));
		return tppDetails;
	}
	public OBThirdPartyProviders getPassportingList(String accessToken,String id,String filter) {
		LOGGER.info("{\"Enter\":\"{}\"}", "com.capgemini.psd2.restclient.ScimRestClient.getPassportingList()",
				loggerUtils.populateLoggerData("getPassportingList()"));
		InlineResponse2009 passportingList = null;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Authorization", "Bearer " + accessToken);
		RequestInfo requestInfoforApiCall = new RequestInfo();
		
		requestInfoforApiCall.setUrl(config.getApiUrl()+"?"+filter+ "&attributes=" + config.getResponseattributes());
		
		passportingList = clientSync.callForGetWithParams(requestInfoforApiCall, InlineResponse2009.class, null,
				httpHeaders);
		LOGGER.info("{\"Exit\":\"{}\",\"TppDetails\":\"{}\"}",
				"com.capgemini.psd2.restclient.ScimRestClient.getPassportingList()", passportingList,
				loggerUtils.populateLoggerData("getPassportingList()"));
		if(passportingList.getTotalResults()==0) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NCA_ID_NOTFOUND);
		}
		return passportingList.getResources().get(0);
	}
}

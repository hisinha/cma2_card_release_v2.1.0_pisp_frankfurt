package com.capgemini.psd2.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * OrganisationPersonalAccountRoles
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OrganisationPersonalAccountRoles   {
  @JsonProperty("Role")
  private String role = null;

  @JsonProperty("UserName")
  private String userName = null;

  public OrganisationPersonalAccountRoles role(String role) {
    this.role = role;
    return this;
  }

   /**
   * Get role
   * @return role
  **/
  @ApiModelProperty(value = "")


  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public OrganisationPersonalAccountRoles userName(String userName) {
    this.userName = userName;
    return this;
  }

   /**
   * Get userName
   * @return userName
  **/
  @ApiModelProperty(value = "")


  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OrganisationPersonalAccountRoles organisationPersonalAccountRoles = (OrganisationPersonalAccountRoles) o;
    return Objects.equals(this.role, organisationPersonalAccountRoles.role) &&
        Objects.equals(this.userName, organisationPersonalAccountRoles.userName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(role, userName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OrganisationPersonalAccountRoles {\n");
    
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("    userName: ").append(toIndentedString(userName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


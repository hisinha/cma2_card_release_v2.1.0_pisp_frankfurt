package com.capgemini.psd2.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims   {
  @JsonProperty("RegisteredId")
  private String registeredId = null;

  @JsonProperty("RegisteredName")
  private String registeredName = null;

  @JsonProperty("RegistrationAuthorityId")
  private String registrationAuthorityId = null;

  public OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims registeredId(String registeredId) {
    this.registeredId = registeredId;
    return this;
  }

   /**
   * Get registeredId
   * @return registeredId
  **/
  @ApiModelProperty(value = "")


  public String getRegisteredId() {
    return registeredId;
  }

  public void setRegisteredId(String registeredId) {
    this.registeredId = registeredId;
  }

  public OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims registeredName(String registeredName) {
    this.registeredName = registeredName;
    return this;
  }

   /**
   * Get registeredName
   * @return registeredName
  **/
  @ApiModelProperty(value = "")


  public String getRegisteredName() {
    return registeredName;
  }

  public void setRegisteredName(String registeredName) {
    this.registeredName = registeredName;
  }

  public OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims registrationAuthorityId(String registrationAuthorityId) {
    this.registrationAuthorityId = registrationAuthorityId;
    return this;
  }

   /**
   * Get registrationAuthorityId
   * @return registrationAuthorityId
  **/
  @ApiModelProperty(value = "")


  public String getRegistrationAuthorityId() {
    return registrationAuthorityId;
  }

  public void setRegistrationAuthorityId(String registrationAuthorityId) {
    this.registrationAuthorityId = registrationAuthorityId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims obThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims = (OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims) o;
    return Objects.equals(this.registeredId, obThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims.registeredId) &&
        Objects.equals(this.registeredName, obThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims.registeredName) &&
        Objects.equals(this.registrationAuthorityId, obThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims.registrationAuthorityId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(registeredId, registeredName, registrationAuthorityId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims {\n");
    
    sb.append("    registeredId: ").append(toIndentedString(registeredId)).append("\n");
    sb.append("    registeredName: ").append(toIndentedString(registeredName)).append("\n");
    sb.append("    registrationAuthorityId: ").append(toIndentedString(registrationAuthorityId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


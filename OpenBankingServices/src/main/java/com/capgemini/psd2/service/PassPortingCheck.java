package com.capgemini.psd2.service;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.model.Authorisation;
import com.capgemini.psd2.model.OBThirdPartyProviders;
import com.capgemini.psd2.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations;

@Component
public class PassPortingCheck {
	private static final Logger LOGGER = LoggerFactory.getLogger(PassPortingCheck.class);
	@Autowired
	RequestHeaderAttributes requestHeader;

	public boolean passportingCheck(OBThirdPartyProviders oBThirdPartyProvidersLive,
			List<Authorisation> allowedMemberStateList, List<String> roles) {
		LOGGER.info("{\"Passporting check enabled: Allowed member state \":\"{}\",\" Requested Roles \":\"{}\"}",
				allowedMemberStateList, roles);
		boolean flag = true;
		for (int i = 0; i < allowedMemberStateList.size(); i++) {
			Authorisation temp = allowedMemberStateList.get(i);
			List<String> activeRolesList = roleListFromOB(oBThirdPartyProvidersLive, temp.getMember_state());
			if (!activeRolesList.containsAll(roles)) {
				LOGGER.error(
						"{\"Passporting check failed: Member state \":\"{}\",\" OB passporting service roles info \":\"{}\",\" Requested Roles \":\"{}\"}",
						temp.getMember_state(), activeRolesList, roles);
				flag = false;
				break;
			}
		}
		return flag;
	}

	public List<String> roleListFromOB(OBThirdPartyProviders oBThirdPartyProviders, String memberState) {
		return oBThirdPartyProviders.getUrnopenbankingcompetentauthorityclaims10().getAuthorisations().stream()
				.filter(filterRoles(memberState)).map(value -> value.getPsd2Role()).collect(Collectors.toList());
	}

	private static Predicate<? super OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> filterRoles(
			String memberState) {
		return value -> value.getMemberState().equals(memberState) && value.getActive().equals(true);
	}
}

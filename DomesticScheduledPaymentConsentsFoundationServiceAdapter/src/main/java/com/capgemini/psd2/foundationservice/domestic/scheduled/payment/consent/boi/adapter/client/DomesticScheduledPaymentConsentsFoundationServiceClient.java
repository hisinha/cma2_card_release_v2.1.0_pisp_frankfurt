
package com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import javax.annotation.PostConstruct;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class DomesticScheduledPaymentConsentsFoundationServiceClient {

	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;

    @Autowired
	private RestTemplate restTemplate;
	
	@PostConstruct
	private void init() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        converter.setObjectMapper(mapper);
        restTemplate.getMessageConverters().add(0,converter);
	}
	
	public ScheduledPaymentInstructionProposal restTransportForDomesticScheduledPaymentFoundationService(RequestInfo reqInfo,
			Class<ScheduledPaymentInstructionProposal> responseType, HttpHeaders headers) {
		return restClient.callForGet(reqInfo, responseType, headers);

	}

	public ScheduledPaymentInstructionProposal restTransportForDomesticPaymentFoundationServicePost(RequestInfo requestInfo,
			ScheduledPaymentInstructionProposal paymentInstructionProposalRequest, Class<ScheduledPaymentInstructionProposal> responseType,
			HttpHeaders httpHeaders) {
		
		return restClient.callForPost(requestInfo,paymentInstructionProposalRequest, responseType, httpHeaders);
	}

}

package com.capgemini.psd2.customer.account.profile.mock.foundationservice.service;

import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.PISP.DigitalUserProfile;

public  interface CustomerAccountInformationService {
	public DigitalUserProfile  retrieveCustomerAccountInformation(String id) throws Exception;

}

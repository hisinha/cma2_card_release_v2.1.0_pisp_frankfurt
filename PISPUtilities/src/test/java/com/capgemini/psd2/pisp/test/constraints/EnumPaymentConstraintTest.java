package com.capgemini.psd2.pisp.test.constraints;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.constraints.EnumPaymentConstraint;
@RunWith(SpringJUnit4ClassRunner.class)
public class EnumPaymentConstraintTest {
	
	@Test
	public void testEnumSetterPositive(){
		EnumPaymentConstraint.SORTCODEACCOUNTNUMBER.getCode();
		EnumPaymentConstraint.SORTCODEACCOUNTNUMBER.getStatus();
		EnumPaymentConstraint.ADDRESSLINE_LENGTH.getSize();
		EnumPaymentConstraint.ADDITIONAL_PARAMETERS.getValue();
	}

}

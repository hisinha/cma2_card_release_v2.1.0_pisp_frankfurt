package com.capgemini.psd2.pisp.domestic.payments.routing.test.adapter.impl;

import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domestic.payments.routing.adapter.impl.DomesticPaymentsRoutingAdapterImpl;
import com.capgemini.psd2.pisp.domestic.payments.routing.adapter.routing.DomesticPaymentsAdapterFactory;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticPaymentsRoutingAdapterImplTest {

	@Mock
	private LoggerUtils loggerUtils;
	
	@InjectMocks
	DomesticPaymentsRoutingAdapterImpl paymentSubmissionExecutionRoutingAdapterImpl;

	@Mock
	DomesticPaymentsAdapter paymentSubmissionExecutionAdapter;
	@Mock
	DomesticPaymentsAdapterFactory factory;
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	DomesticPaymentsAdapter adapter;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void retrieveStagedDomesticPaymentsTest() {
		Mockito.when(factory.getDomesticPaymentsAdapterInstance(anyString()))
				.thenReturn(paymentSubmissionExecutionAdapter);
		CustomPaymentStageIdentifiers customPaymentSetupInfo = new CustomPaymentStageIdentifiers();
		
		paymentSubmissionExecutionRoutingAdapterImpl.retrieveStagedDomesticPayments(customPaymentSetupInfo, new HashMap<>());
	}
	
	@Test
	public void processDomesticPaymentsTest() {
		Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap =new HashMap<>();
		
		Token token = new Token();
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "rete");
		
		token.setSeviceParams(seviceParams);
		
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
	
		//CustomPaymentStageIdentifiers customPaymentSetupInfo = new CustomPaymentStageIdentifiers();
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		Map<String, String> params = new HashMap<>();
		Mockito.when(factory.getDomesticPaymentsAdapterInstance(anyString())).thenReturn(adapter);
		paymentSubmissionExecutionRoutingAdapterImpl.processDomesticPayments(request, paymentStatusMap, params);
	}

	
	@After
	public void tearDown() throws Exception {
		factory = null;
		paymentSubmissionExecutionRoutingAdapterImpl = null;
		paymentSubmissionExecutionAdapter = null;
	}
}

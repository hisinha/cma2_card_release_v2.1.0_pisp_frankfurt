package com.capgemini.psd2.pisp.domestic.payments.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;

public interface DomesticPaymentSetupRetrieveFoundationRepository extends MongoRepository<CustomDPaymentConsentsPOSTResponse, String>{

	  public CustomDPaymentConsentsPOSTResponse findOneByDataConsentId(String consentId );	
}

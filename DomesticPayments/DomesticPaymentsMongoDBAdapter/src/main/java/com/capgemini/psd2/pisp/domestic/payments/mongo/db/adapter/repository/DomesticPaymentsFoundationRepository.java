package com.capgemini.psd2.pisp.domestic.payments.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.DPaymentsFoundationResource;

public interface DomesticPaymentsFoundationRepository extends MongoRepository<DPaymentsFoundationResource, String> {

	public DPaymentsFoundationResource findOneByDataDomesticPaymentId(String submissionId);

}

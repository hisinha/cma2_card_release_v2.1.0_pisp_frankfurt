package com.capgemini.psd2.pisp.domestic.payments.test.transformer.impl;

import static org.mockito.Matchers.anyObject;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.domestic.payments.transformer.impl.DomesticPaymentsTransformerImpl;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticPaymentsTransformerImplTest {
	@InjectMocks
	private DomesticPaymentsTransformerImpl domesticPaymentsResponseTransformerImpl;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PaymentsPlatformAdapter paymentsPlatformAdapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void updateMetaAndLinksTest() {
		PaymentDomesticSubmitPOST201Response submissionResponse = new PaymentDomesticSubmitPOST201Response();
		PaymentSetupPOSTResponseLinks links = new PaymentSetupPOSTResponseLinks();
		submissionResponse.setLinks(links);
		PaymentSetupPOSTResponseMeta meta = new PaymentSetupPOSTResponseMeta();
		submissionResponse.setMeta(meta);
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		submissionResponse.setData(data);
		String domesticPaymentId = "45435";
		submissionResponse.getData().setDomesticPaymentId(domesticPaymentId);
		String methodType = RequestMethod.POST.toString();
		domesticPaymentsResponseTransformerImpl.updateMetaAndLinks(submissionResponse, methodType);

	}

	@Test
	public void paymentSubmissionResponseTransformerTestPost() {
		PaymentDomesticSubmitPOST201Response paymentSubmissionResponse = new PaymentDomesticSubmitPOST201Response();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.POST.toString();
		PaymentsPlatformResource platResource = new PaymentsPlatformResource();
		platResource.setCreatedAt("");
		platResource.setStatus("AcceptedSettlementCompleted");
		platResource.setStatusUpdateDateTime("");
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		paymentSubmissionResponse.setData(data);
		paymentSubmissionResponse.getData().setCreationDateTime("");
		paymentSubmissionResponse.getData().setStatus(OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTCOMPLETED);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime("");
		OBDomestic1 initiation = new OBDomestic1();
		paymentSubmissionResponse.getData().setInitiation(initiation);
		paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().setSchemeName("PAN");
		paymentSubmissionResponse.getData().setMultiAuthorisation(new OBMultiAuthorisation1());
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("IBAN");
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setTppDebtorDetails(Boolean.FALSE.toString());
		paymentsPlatformResourceMap.put("submission", platResource);
		paymentsPlatformResourceMap.put("consent", paymentConsentsPlatformResource);
		domesticPaymentsResponseTransformerImpl.paymentSubmissionResponseTransformer(paymentSubmissionResponse,
				paymentsPlatformResourceMap, methodType);
	}

	@Test
	public void paymentSubmissionResponseTransformerTestPutCompleted() {
		PaymentDomesticSubmitPOST201Response paymentSubmissionResponse = new PaymentDomesticSubmitPOST201Response();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();
		PaymentsPlatformResource platResource = new PaymentsPlatformResource();
		platResource.setCreatedAt("");
		platResource.setStatus("AcceptedSettlementCompleted");
		platResource.setStatusUpdateDateTime("");
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		paymentSubmissionResponse.setData(data);
		paymentSubmissionResponse.getData().setCreationDateTime("");
		paymentSubmissionResponse.getData().setStatus(OBTransactionIndividualStatus1Code.PENDING);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime("");
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.AUTHORISED);
		paymentSubmissionResponse.getData().setMultiAuthorisation(multiAuth);
		OBDomestic1 initiation = new OBDomestic1();
		paymentSubmissionResponse.getData().setInitiation(initiation);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("IBAN");
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setTppDebtorDetails(Boolean.FALSE.toString());
		paymentsPlatformResourceMap.put("submission", platResource);
		paymentsPlatformResourceMap.put("consent", paymentConsentsPlatformResource);
		domesticPaymentsResponseTransformerImpl.paymentSubmissionResponseTransformer(paymentSubmissionResponse,
				paymentsPlatformResourceMap, methodType);
	}

	@Test
	public void paymentSubmissionResponseTransformerTestPutAuthorized() {
		PaymentDomesticSubmitPOST201Response paymentSubmissionResponse = new PaymentDomesticSubmitPOST201Response();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();
		PaymentsPlatformResource platformResource = new PaymentsPlatformResource();
		platformResource.setCreatedAt("");
		platformResource.setStatus("AcceptedSettlementCompleted");
		platformResource.setStatusUpdateDateTime("");
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		paymentSubmissionResponse.setData(data);
		paymentSubmissionResponse.getData().setCreationDateTime("");
		paymentSubmissionResponse.getData().setStatus(OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTCOMPLETED);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime("");
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.AUTHORISED);
		paymentSubmissionResponse.getData().setMultiAuthorisation(multiAuth);
		OBDomestic1 initiation = new OBDomestic1();
		paymentSubmissionResponse.getData().setInitiation(initiation);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("IBAN");
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setTppDebtorDetails("True");
		paymentConsentsPlatformResource.setTppDebtorNameDetails(null);
		paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		paymentsPlatformResourceMap.put("submission", platformResource);
		paymentsPlatformResourceMap.put("consent", paymentConsentsPlatformResource);
		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		Mockito.when(paymentsPlatformAdapter.updatePaymentsPlatformResource(anyObject()))
				.thenReturn(paymentsPlatformResource);
		domesticPaymentsResponseTransformerImpl.paymentSubmissionResponseTransformer(paymentSubmissionResponse,
				paymentsPlatformResourceMap, methodType);
	}

	@Test
	public void paymentSubmissionResponseTransformerTestPutRejected() {
		PaymentDomesticSubmitPOST201Response paymentSubmissionResponse = new PaymentDomesticSubmitPOST201Response();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();
		PaymentsPlatformResource platResource = new PaymentsPlatformResource();
		platResource.setCreatedAt("");
	//	platResource.setStatus("AcceptedSettlementCompleted");
		platResource.setStatus("Pending");
		platResource.setStatusUpdateDateTime("");
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		paymentSubmissionResponse.setData(data);
		paymentSubmissionResponse.getData().setCreationDateTime("");
		paymentSubmissionResponse.getData().setStatus(OBTransactionIndividualStatus1Code.REJECTED);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime("");
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		//multiAuth.setStatus(OBExternalStatus2Code.AUTHORISED);
		multiAuth.setStatus(OBExternalStatus2Code.REJECTED);
		paymentSubmissionResponse.getData().setMultiAuthorisation(multiAuth);
		OBDomestic1 initiation = new OBDomestic1();
		paymentSubmissionResponse.getData().setInitiation(initiation);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("IBAN");
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setTppDebtorDetails(Boolean.FALSE.toString());
		paymentsPlatformResourceMap.put("submission", platResource);
		paymentsPlatformResourceMap.put("consent", paymentConsentsPlatformResource);
		domesticPaymentsResponseTransformerImpl.paymentSubmissionResponseTransformer(paymentSubmissionResponse,
				paymentsPlatformResourceMap, methodType);
	}

	@Test
	public void paymentSubmissionResponseTransformerTestPutPending() {
		PaymentDomesticSubmitPOST201Response paymentSubmissionResponse = new PaymentDomesticSubmitPOST201Response();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();
		PaymentsPlatformResource platformResource = new PaymentsPlatformResource();
		platformResource.setCreatedAt("");
		platformResource.setStatus("Pending");
		platformResource.setStatusUpdateDateTime("");
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		paymentSubmissionResponse.setData(data);
		paymentSubmissionResponse.getData().setCreationDateTime("");
		paymentSubmissionResponse.getData().setStatus(OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTCOMPLETED);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime("");
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.AUTHORISED);
		paymentSubmissionResponse.getData().setMultiAuthorisation(multiAuth);
		OBDomestic1 initiation = new OBDomestic1();
		paymentSubmissionResponse.getData().setInitiation(initiation);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("IBAN");
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setTppDebtorDetails(Boolean.FALSE.toString());
		paymentsPlatformResourceMap.put("submission", platformResource);
		paymentsPlatformResourceMap.put("consent", paymentConsentsPlatformResource);
		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		Mockito.when(paymentsPlatformAdapter.updatePaymentsPlatformResource(anyObject()))
				.thenReturn(paymentsPlatformResource);
		domesticPaymentsResponseTransformerImpl.paymentSubmissionResponseTransformer(paymentSubmissionResponse,
				paymentsPlatformResourceMap, methodType);
	}

	@Test
	public void paymentSubmissionResponseTransformerTestRejected() {
		PaymentDomesticSubmitPOST201Response paymentSubmissionResponse = new PaymentDomesticSubmitPOST201Response();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();
		PaymentsPlatformResource platformResource = new PaymentsPlatformResource();
		platformResource.setCreatedAt("");
		platformResource.setStatus("Pending");
		platformResource.setStatusUpdateDateTime("");
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		paymentSubmissionResponse.setData(data);
		paymentSubmissionResponse.getData().setCreationDateTime("");
		paymentSubmissionResponse.getData().setStatus(OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTCOMPLETED);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime("");
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.REJECTED);
		paymentSubmissionResponse.getData().setMultiAuthorisation(multiAuth);
		OBDomestic1 initiation = new OBDomestic1();
		paymentSubmissionResponse.getData().setInitiation(initiation);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("IBAN");
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setTppDebtorDetails(Boolean.FALSE.toString());
		paymentsPlatformResourceMap.put("submission", platformResource);
		paymentsPlatformResourceMap.put("consent", paymentConsentsPlatformResource);
		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		Mockito.when(paymentsPlatformAdapter.updatePaymentsPlatformResource(anyObject()))
				.thenReturn(paymentsPlatformResource);
		domesticPaymentsResponseTransformerImpl.paymentSubmissionResponseTransformer(paymentSubmissionResponse,
				paymentsPlatformResourceMap, methodType);
	}

	@Test
	public void testPaymentSubmissionRequestTransformer() {
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		domesticPaymentsResponseTransformerImpl.paymentSubmissionRequestTransformer(request);
	}
}
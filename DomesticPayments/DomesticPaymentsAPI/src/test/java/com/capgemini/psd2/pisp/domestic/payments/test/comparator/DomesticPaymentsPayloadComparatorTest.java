package com.capgemini.psd2.pisp.domestic.payments.test.comparator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomestic1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domestic.payments.comparator.DomesticPaymentsPayloadComparator;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticPaymentsPayloadComparatorTest {

	@InjectMocks
	DomesticPaymentsPayloadComparator comparator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void compareTest() {
		CustomDPaymentConsentsPOSTResponse paymentResponse = new CustomDPaymentConsentsPOSTResponse();
		CustomDPaymentConsentsPOSTResponse adaptedPaymentResponse = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		data.setConsentId("1234");
		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);
		initiation.setLocalInstrument("111");
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("20.0");

		paymentResponse.setData(data);
		OBRisk1 risk = new OBRisk1();
		risk.setMerchantCategoryCode("testCC");
		paymentResponse.setRisk(risk);
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		adaptedPaymentResponse.setData(data1);
		OBRisk1 risk1 = new OBRisk1();
		adaptedPaymentResponse.setRisk(risk1);
		risk1.setMerchantCategoryCode("testCC");
		data1.setConsentId("1234");
		OBDomestic1 initiation1 = new OBDomestic1();
		data1.setInitiation(initiation1);
		initiation1.setLocalInstrument("111");
		initiation1.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("20.0");
		initiation1.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("20.0");

		comparator.compare(paymentResponse, adaptedPaymentResponse);
	}

	@Test
	public void comparePaymentDetails() {

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		response.setId("testid");
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		response.setData(data);

		data.setCreationDateTime("30102018");
		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("0.00");
		initiation.setInstructedAmount(instructedAmount);
		initiation.setLocalInstrument("9999");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setName("test");

		OBWriteDataDomesticConsent1 data1 = new OBWriteDataDomesticConsent1();
		request.setData(data1);
		OBDomestic1 initiation1 = new OBDomestic1();
		data1.setInitiation(initiation1);
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("dublin east");
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("james street");
		deliveryAddress.setCountrySubDivision("warner bros2");
		request.setRisk(risk);

		initiation1.setLocalInstrument("12345");
		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setName("test1");
		initiation1.setDebtorAccount(debtorAccount1);
		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		initiation1.setInstructedAmount(instructedAmount1);
		instructedAmount1.setAmount("0.00");
		PaymentConsentsPlatformResource PaymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		PaymentConsentsPlatformResource.setTppDebtorDetails("true");
		PaymentConsentsPlatformResource.setTppDebtorNameDetails("testdebtor");

		int result = comparator.comparePaymentDetails(response, request, PaymentConsentsPlatformResource);
		assertEquals(1, result);
	}

	@Test
	public void comparePaymentDetails1() {

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		response.setId("testid");
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		response.setData(data);
		data.setCreationDateTime("30102018");
		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("0.00");
		initiation.setInstructedAmount(instructedAmount);
		initiation.setLocalInstrument("9999");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setName("test");
		OBWriteDataDomestic1 data1 = new OBWriteDataDomestic1();
		request.setData(data1);
		OBDomestic1 initiation1 = new OBDomestic1();
		data1.setInitiation(initiation1);
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("testreference");
		remittanceInformation.setUnstructured("testunstructured");
		initiation1.setRemittanceInformation(remittanceInformation);
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		;
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("warner bros");
		addressLine.add("warner bros2");
		List<String> countrySubDivision = new ArrayList<>();
		deliveryAddress.setCountrySubDivision("warner bros2");
		countrySubDivision.add("Dublin west");
		request.setRisk(risk);

		initiation1.setLocalInstrument("12345");
		OBCashAccountDebtor3 debtorAccount1 = null;
		// debtorAccount1.setName("test1");
		initiation1.setDebtorAccount(debtorAccount1);
		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		initiation1.setInstructedAmount(instructedAmount1);
		instructedAmount1.setAmount("0.00");
		PaymentConsentsPlatformResource PaymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		PaymentConsentsPlatformResource.setTppDebtorDetails("false");
		PaymentConsentsPlatformResource.setTppDebtorNameDetails("testdebtor");

		int result = comparator.comparePaymentDetails(response, request, PaymentConsentsPlatformResource);
		assertEquals(1, result);

	}

	@Test
	public void validateDebtorDetailsTest() {
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBDomestic1 responseInitiation = new OBDomestic1();
		OBDomestic1 requestInitiation = new OBDomestic1();
		PaymentConsentsPlatformResource PaymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		PaymentConsentsPlatformResource.setTppDebtorDetails("false");
		requestInitiation.setDebtorAccount(debtorAccount);
		boolean result = comparator.validateDebtorDetails(responseInitiation, requestInitiation,
				PaymentConsentsPlatformResource);
		assertThat(result).isEqualTo(false);
		requestInitiation.setDebtorAccount(null);
		responseInitiation.setDebtorAccount(null);
		result = comparator.validateDebtorDetails(responseInitiation, requestInitiation,
				PaymentConsentsPlatformResource);
		assertThat(result).isEqualTo(true);
	}

	@Test
	public void comparetest_nullAddressLine1() {

		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();

		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);

		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("20.0");
		initiation.setInstructedAmount(instructedAmount);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		OBPostalAddress6 creditorPostalAddressR = new OBPostalAddress6();
		List<String> addressLineR = new ArrayList<String>();
		addressLineR.add("");
		creditorPostalAddressR.addressLine(addressLineR);
		initiation.setCreditorPostalAddress(creditorPostalAddressR);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("");

		request.setData(data);
		request.setRisk(risk);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		data1.setInitiation(initiation);

		response.setData(data1);
		response.setRisk(risk);
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void comparetest_nullRemittance() {

		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();

		OBDomestic1 initiation = new OBDomestic1();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		data.setInitiation(initiation);
		
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("20.0");
		initiation.setInstructedAmount(instructedAmount);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		remittanceInformation.setReference(null);
		remittanceInformation.setUnstructured(null);
		initiation.setRemittanceInformation(remittanceInformation);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		OBPostalAddress6 creditorPostalAddressR = new OBPostalAddress6();
		List<String> addressLineP = new ArrayList<String>();
		addressLineP.add("");
		creditorPostalAddressR.setAddressLine(addressLineP);
		initiation.setCreditorPostalAddress(creditorPostalAddressR);
		request.setData(data);
		request.setRisk(risk);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
	
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<String>();
		addressLine.add("");
		deliveryAddress.setAddressLine(addressLine);
		List<String> countrySubDivision = new ArrayList<>();
		deliveryAddress.setCountrySubDivision("warner bros2");
		countrySubDivision.add("Dublin west");

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		
		OBRisk1 risk1 = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		deliveryAddress1.setAddressLine(null);
		risk1.setDeliveryAddress(deliveryAddress1 );
		deliveryAddress1.setBuildingNumber("572");
		deliveryAddress1.setCountry("Ireland");
		deliveryAddress1.setAddressLine(null);
		deliveryAddress1.setCountrySubDivision("warner bros2");
		countrySubDivision.add("Dublin west");

		response.setData(data1);
		response.setRisk(risk1);
		OBDomestic1 initiation1 = new OBDomestic1();
		initiation1.setInstructedAmount(instructedAmount);

		OBPostalAddress6 creditorPostalAddressResponse = new OBPostalAddress6();
		creditorPostalAddressResponse.setAddressLine(null);
		initiation1.setCreditorPostalAddress(creditorPostalAddressResponse);
		
		data1.setInitiation(initiation1);
		initiation1.setInstructionIdentification("9999");
		initiation1.setRemittanceInformation(null);
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void comparetest_nullCountrySubDivision() {

		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();

		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("788");
		remittanceInformation.setUnstructured("546");
		initiation.setRemittanceInformation(remittanceInformation);
		// creditorAccount.setSchemeName(schemeName);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();

		debtorAccount.setName("Bob Clements");
		debtorAccount.setSchemeName("SortCodeAccountNumber");
		initiation.setDebtorAccount(debtorAccount);
		request.setData(data);
		request.setRisk(risk);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = null;
		deliveryAddress.setAddressLine(addressLine);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");
		String tppDebtorDetails = "True";
		paymentSetupPlatformResource.setTppDebtorDetails(tppDebtorDetails);
		String tppDebtorNameDetails = "TDebtorS";
		paymentSetupPlatformResource.setTppDebtorNameDetails(tppDebtorNameDetails);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		response.setData(data1);
		OBRisk1 risk1 = new OBRisk1();
		response.setRisk(risk1);
		OBDomestic1 initiation1 = new OBDomestic1();
		initiation1.setDebtorAccount(debtorAccount);

		data1.setInitiation(initiation1);
		initiation1.setInstructionIdentification("9999");
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void comparetest_nullAmount() {

		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();

		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("788");
		remittanceInformation.setUnstructured("546");
		initiation.setRemittanceInformation(remittanceInformation);
		// creditorAccount.setSchemeName(schemeName);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();

		debtorAccount.setName("Bob Clements");
		debtorAccount.setSchemeName("SortCodeAccountNumber");
		initiation.setDebtorAccount(debtorAccount);
		request.setData(data);
		request.setRisk(risk);
		// OBActiveOrHistoricCurrencyAndAmount amount = new
		// OBActiveOrHistoricCurrencyAndAmount();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("warner bros");
		addressLine.add("warner bros2");
		deliveryAddress.setCountrySubDivision("warner bros2");

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");
		String tppDebtorDetails = "True";
		paymentSetupPlatformResource.setTppDebtorDetails(tppDebtorDetails);
		String tppDebtorNameDetails = "TDebtorS";
		paymentSetupPlatformResource.setTppDebtorNameDetails(tppDebtorNameDetails);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		// OBWriteDataDomesticConsentResponse1 data1=new
		// OBWriteDataDomesticConsentResponse1();
		response.setData(data1);
		OBRisk1 risk1 = new OBRisk1();
		response.setRisk(risk1);
		OBDomestic1 initiation1 = new OBDomestic1();
		initiation1.setDebtorAccount(debtorAccount);

		data1.setInitiation(initiation1);
		initiation1.setInstructionIdentification("9999");
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	/*
	 * @Test public void comparetest_nullrequestDebtor() {
	 * 
	 * CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
	 * OBRisk1 risk = new OBRisk1(); OBWriteDataDomestic1 data = new
	 * OBWriteDataDomestic1();
	 * 
	 * OBDomestic1 initiation = new OBDomestic1(); data.setInitiation(initiation);
	 * 
	 * OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
	 * initiation.setCreditorAccount(creditorAccount); OBRemittanceInformation1
	 * remittanceInformation=new OBRemittanceInformation1();
	 * remittanceInformation.setReference("788");
	 * remittanceInformation.setUnstructured("546");
	 * initiation.setRemittanceInformation(remittanceInformation); //
	 * creditorAccount.setSchemeName(schemeName);
	 * creditorAccount.setIdentification("08080021325698");
	 * creditorAccount.setName("ACME Inc");
	 * creditorAccount.setSecondaryIdentification("0002");
	 * 
	 * OBCashAccountDebtor3 debtorAccount = null;
	 * 
	 * 
	 * debtorAccount.setName("Bob Clements");
	 * debtorAccount.setSchemeName("SortCodeAccountNumber");
	 * debtorAccount.setIdentification("08080021325698"); //
	 * debtorAccount.setSecondaryIdentification(secondaryIdentification);
	 * initiation.setDebtorAccount(debtorAccount); request.setData(data);
	 * request.setRisk(risk);
	 * 
	 * OBRisk1DeliveryAddress deliveryAddress=new OBRisk1DeliveryAddress();;
	 * risk.setDeliveryAddress(deliveryAddress);
	 * deliveryAddress.setBuildingNumber("572");
	 * deliveryAddress.setCountry("Ireland"); List<String> addressLine=new
	 * ArrayList<>(); deliveryAddress.setAddressLine(addressLine);
	 * //addressLine.add("warner bros"); //addressLine.add("warner bros2");
	 * List<String> countrySubDivision=new ArrayList<>();
	 * deliveryAddress.setCountrySubDivision(countrySubDivision);
	 * countrySubDivision.add("Dublin west");
	 * 
	 * 
	 * PaymentConsentsPlatformResource paymentSetupPlatformResource = new
	 * PaymentConsentsPlatformResource(); paymentSetupPlatformResource.setId("144");
	 * paymentSetupPlatformResource.setPaymentId("58923-001");
	 * paymentSetupPlatformResource.setPaymentConsentId("58923");
	 * paymentSetupPlatformResource.setSetupCmaVersion("3");
	 * 
	 * String tppDebtorDetails = "false";
	 * paymentSetupPlatformResource.setTppDebtorDetails(tppDebtorDetails); String
	 * tppDebtorNameDetails = "TDebtorS";
	 * paymentSetupPlatformResource.setTppDebtorNameDetails(tppDebtorNameDetails );
	 * 
	 * CustomDPaymentConsentsPOSTResponse response = new
	 * CustomDPaymentConsentsPOSTResponse(); OBWriteDataDomesticConsentResponse1
	 * data1 = new OBWriteDataDomesticConsentResponse1(); response.setData(data1);
	 * OBRisk1 risk1=new OBRisk1(); response.setRisk(risk1);
	 * 
	 * OBDomestic1 initiation1 = new OBDomestic1();
	 * data1.setInitiation(initiation1);
	 * initiation1.setInstructionIdentification("9999"); OBCashAccountDebtor3
	 * debtorAccount1=new OBCashAccountDebtor3();
	 * initiation1.setDebtorAccount(debtorAccount1); debtorAccount1.setName("PSD2");
	 * 
	 * comparator.comparePaymentDetails(response, request,
	 * paymentSetupPlatformResource); }
	 */
}

package com.capgemini.psd2.security.exceptions;

public enum SCAConsentErrorCodeEnum {

	INVALID_REQUEST("700","Invalid request","Invalid request","400"),
	INVALID_TOKEN("701","Invalid ID Token","Invalid ID Token","401"),
	TOKEN_EXPIRED("702","Access Expired","Access Expired","401"),
	VALIDATION_ERROR("707","Validation error found with the provided input","Validation Error found in the parameter","400"),
	SESSION_EXPIRED("731","Session has been invalidated now","Session has been invalidated now","400"),
	REPLAY_ATTACK_ERROR("732","Un-authorized request.","Un-authorized request.","401"),
	TECHNICAL_ERROR("733","Technical Error.","Technical Error.","500"),
	UN_AUTHORIZED_USER_REFRESH_TOKEN_FLOW("734","Un-authorized user for running renewal flow.","Un-authorized user for running renewal flow.","401");
	
	private String errorCode;
	private String errorMessage;
	private String detailErrorMessage;
	/** The status code. */
	private String statusCode;

	
	SCAConsentErrorCodeEnum(String errorCode,String errorMesssage,String detailErrorMessage, String statusCode){
		this.errorCode=errorCode;
		this.errorMessage=errorMesssage;
		this.detailErrorMessage = detailErrorMessage;
		this.statusCode = statusCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getDetailErrorMessage() {
		return detailErrorMessage;
	}
	
	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public String getStatusCode() {
		return statusCode;
	}		
}

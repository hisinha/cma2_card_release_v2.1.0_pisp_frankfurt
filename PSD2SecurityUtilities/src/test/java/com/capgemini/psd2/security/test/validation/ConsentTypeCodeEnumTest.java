/*package com.capgemini.psd2.security.test.validation;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.security.validation.ConsentTypeCodeEnum;

public class ConsentTypeCodeEnumTest {

	private  ConsentTypeCodeEnum consentTypeCodeEnum;
	
	@Before
	public void setUp() throws Exception {
		consentTypeCodeEnum = ConsentTypeCodeEnum.AccountRequestId;
	}
	@Test
	public void test(){
		String [] oauthScopes =new String[]{"accounts"};
		assertArrayEquals(oauthScopes, consentTypeCodeEnum.getOauthScopes());
	}
	
	@Test
	public void testToString(){
	  assertEquals(ConsentTypeCodeEnum.AccountRequestId.toString(), consentTypeCodeEnum.toString());
	}
	
	@Test
	public void testIsAISPFlow(){
	  assertTrue(ConsentTypeCodeEnum.isAISPFlow(ConsentTypeCodeEnum.AccountRequestId.name()));
	}
	
	@Test
	public void testIsPISPFlow(){
	  assertTrue(ConsentTypeCodeEnum.isPISPFlow(ConsentTypeCodeEnum.PaymentId.name()));
	}
	
	@Test
	public void testIsCISPFlow(){
	  assertTrue(ConsentTypeCodeEnum.isCISPFlow(ConsentTypeCodeEnum.FundsCheckRequestId.name()));
	}
	
	@Test
	public void testIsAISPFlowForRequestedScopes(){
		Set<String> scopes =  new HashSet<>();
		scopes.add("accounts");
		assertTrue(ConsentTypeCodeEnum.isAISPFlowForRequestedScopes(scopes));
	}
	
	@Test
	public void testIsPISPFlowForRequestedScopes(){
		Set<String> scopes =  new HashSet<>();
		scopes.add("payments");
		assertTrue(ConsentTypeCodeEnum.isPISPFlowForRequestedScopes(scopes));
	}
	
	@Test
	public void testConsentTypeCodeObj(){
		assertEquals(ConsentTypeCodeEnum.AccountRequestId.toString(),ConsentTypeCodeEnum.consentTypeCodeObj("AISP").toString());
	}
}
*/
/*package com.capgemini.psd2.security.auth.test.jwt;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.authentication.handlers.JwtAuthenticationProvider;
import com.capgemini.psd2.security.models.JwtAuthenticationToken;
import com.capgemini.psd2.security.models.RawAccessJwtToken;
import com.capgemini.psd2.security.models.UserContext;

@RunWith(SpringJUnit4ClassRunner.class)
public class JwtAuthenticationProviderTest {

	@InjectMocks
	private JwtAuthenticationProvider jwtAuthenticationProvider = new JwtAuthenticationProvider();
	
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testAuthenticate(){
		Authentication authentication = Mockito.mock(Authentication.class);
		requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		ReflectionTestUtils.setField(jwtAuthenticationProvider,"requestHeaderAttributes",requestHeaderAttributes);
		String token ="eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJib2kxMjMiLCJzY29wZXMiOlsiQ1VTVE9NRVJfUk9MRSJdLCJpc3MiOiJodHRwOi8vU2FhUy5jb20iLCJqdGkiOiI3NmRiN2JjZS05Y2RhLTQ5NDMtOGM2ZS01NTFmNGUzNWMxYzciLCJpYXQiOjE1MDAyOTcyODMsImV4cCI6MTUxNTg0OTI4M30.eEpXpsLaoUH70-a19xqCcCtvDUsZ3b8sQ3mH6ovmgXDNiBvGFIts819nU9dnLJt3IMbdsscL5a9IaRGHNu980Q";
		RawAccessJwtToken RawAccessJwtToken = new RawAccessJwtToken(token,requestHeaderAttributes);
		when(authentication.getCredentials()).thenReturn(RawAccessJwtToken);
		ReflectionTestUtils.setField(jwtAuthenticationProvider,"tokenSigningKey","0123456789abcdef");
		List<String> scopes = new ArrayList<>();
		scopes.add("CUSTOMER_ROLE");
		List<GrantedAuthority> authorities = scopes.stream()
                .map(authority -> new SimpleGrantedAuthority(authority))
                .collect(Collectors.toList());
		UserContext context = UserContext.create("boi123",authorities);
		assertEquals(new JwtAuthenticationToken(context, context.getAuthorities()).getAuthorities() ,jwtAuthenticationProvider.authenticate(authentication).getAuthorities());
	}
	
	@Test
	public void test(){
		jwtAuthenticationProvider.supports(Authentication.class);
	}
}
*/
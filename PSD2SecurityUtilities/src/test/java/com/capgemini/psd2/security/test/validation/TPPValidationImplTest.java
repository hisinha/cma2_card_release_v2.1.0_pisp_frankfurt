/*package com.capgemini.psd2.security.test.validation;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.validation.ConsentTypeCodeEnum;
import com.capgemini.psd2.security.validation.TPPValidationImpl;

public class TPPValidationImplTest {

	private TPPValidationImpl tppValidationImpl;

	@Before
	public void setUp() throws Exception {

		tppValidationImpl = new TPPValidationImpl();
	}

	@Test
	public void testValidateAndFindConsentType(){
		Map<String, String> params =new HashMap<>();
		params.put(ConsentTypeCodeEnum.AccountRequestId.name(),"123445");
		assertEquals(ConsentTypeCodeEnum.AccountRequestId.toString(),tppValidationImpl.validateAndFindConsentType(params));
	}
	@Test
	public void testFindConsentType(){
		Map<String, String> params =new HashMap<>();
		params.put(ConsentTypeCodeEnum.AccountRequestId.name(),"123445");
		assertEquals(ConsentTypeCodeEnum.AccountRequestId.toString(),tppValidationImpl.findConsentType(params).toString());
	}

	@Test(expected = PSD2SecurityException.class)
	public void testValidateAndFindConsentTypeNull(){
		Map<String, String> params =new HashMap<>();
		
		tppValidationImpl.validateAndFindConsentType(params);
	}
	
	@Test
	public void testCheckRequestType(){
		Map<String, String> params =new HashMap<>();
		params.put("AccountRequestId", "12345");
		tppValidationImpl.checkRequestType(params);
	}
}
*/
package com.capgemini.psd2.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.test.config.ConsentListITConfig;
import com.capgemini.psd2.test.data.Consent;
import com.capgemini.psd2.test.data.ConsentListResponse;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ConsentListITConfig.class)
@WebIntegrationTest
public class ConsentListIT {

	@Value("${app.edgeserverURL}")
	private String edgeserverURL;
	
	@Value("${app.accessToken}")
	private String accessToken;

	@Value("${app.invalidAccessToken}")
	private String invalidAccessToken;
	
	@Value("${app.userId}")
	private String userId;
	
	@Value("${app.invalidUserId}")
	private String invalidUserId;
	
	@Value("${app.status}")
	private String status;
	
	@Value("${app.invalidStatus}")
	private String invalidStatus;
	
	@Autowired
	protected RestClientSyncImpl restClientSync;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetConsentListSuccess() {
		String url = edgeserverURL + "consents/" + userId + "?status=" + status;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		ConsentListResponse response = restClientSync.callForGet(requestInfo, ConsentListResponse.class, headers);
		assertNotNull(response);
		List<Consent> consents = response.getData();
		assertTrue(!consents.isEmpty());
		for (Consent consent : consents) {
			assertNotNull(consent.getConsentId());
			assertNotNull(consent.getAccountRequestId());
			assertNotNull(consent.getConsentCreationDate());
			assertNotNull(consent.getPsuId());
			assertTrue(consent.getPsuId().equals(userId));
			assertNotNull(consent.getStatus());
			assertTrue(consent.getStatus().equals(status));			
			assertNotNull(consent.getTppLegalEntityName());
			assertNotNull(consent.getAccountDetails());
			assertNotNull(consent.getPermissions());
		}
		assertNotNull(response.getLinks());
		assertNotNull(response.getMeta());
		assertTrue(response.getMeta().getTotalPages().equals(1));
	}
	
	@Test
	public void testGetConsentListWithoutStatusSuccess() {
		String url = edgeserverURL + "consents/" + userId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		ConsentListResponse response = restClientSync.callForGet(requestInfo, ConsentListResponse.class, headers);
		assertNotNull(response);
		List<Consent> consents = response.getData();
		assertTrue(!consents.isEmpty());
		for (Consent consent : consents) {
			assertNotNull(consent.getConsentId());
			assertNotNull(consent.getAccountRequestId());
			assertNotNull(consent.getConsentCreationDate());
			assertNotNull(consent.getPsuId());
			assertTrue(consent.getPsuId().equals(userId));
			assertNotNull(consent.getTppLegalEntityName());
			assertNotNull(consent.getAccountDetails());
			assertNotNull(consent.getPermissions());
		}
		assertNotNull(response.getLinks());
		assertNotNull(response.getMeta());
		assertTrue(response.getMeta().getTotalPages().equals(1));
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetConsentListInvalidAccessToken() {
		String url = edgeserverURL + "consents/" + userId + "?status=" + status;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", invalidAccessToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		restClientSync.callForGet(requestInfo, ConsentListResponse.class, headers);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetConsentListWithoutAuthorization() {
		String url = edgeserverURL + "consents/" + userId + "?status=" + status;
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		restClientSync.callForGet(requestInfo, ConsentListResponse.class, headers);
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetConsentListWithoutFinacialId() {
		String url = edgeserverURL + "consents/" + userId + "?status=" + status;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		restClientSync.callForGet(requestInfo, ConsentListResponse.class, headers);		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetConsentListInvalidStatus() {
		String url = edgeserverURL + "consents/" + userId + "?status=" + invalidStatus;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		restClientSync.callForGet(requestInfo, ConsentListResponse.class, headers);		
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetConsentListInvalidUserId() {
		String url = edgeserverURL + "consents/" + invalidUserId + "?status=" + status;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		restClientSync.callForGet(requestInfo, ConsentListResponse.class, headers);		
	}
}

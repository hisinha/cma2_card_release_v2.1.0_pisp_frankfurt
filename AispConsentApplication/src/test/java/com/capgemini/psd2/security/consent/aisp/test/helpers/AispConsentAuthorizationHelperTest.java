package com.capgemini.psd2.security.consent.aisp.test.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification4;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentAuthorizationHelper;

@RunWith(SpringJUnit4ClassRunner.class)
public class AispConsentAuthorizationHelperTest {

	@InjectMocks
	AispConsentAuthorizationHelper aispHelper;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void populateAccountWithUnmaskedValuesTest() {
		Map<String, String> map = new HashMap<>();

		PSD2Account accountwithoutmask = new PSD2Account();
		List<OBCashAccount3> acct = new ArrayList<>();
		OBCashAccount3 account = new OBCashAccount3();
		account.setIdentification("12345");
		account.setSchemeName("IBAN");
		acct.add(account);
		OBBranchAndFinancialInstitutionIdentification4 servicer = new OBBranchAndFinancialInstitutionIdentification4();
		servicer.setIdentification("12345");
		accountwithoutmask.setServicer(servicer);
		accountwithoutmask.setAccount(acct);
		accountwithoutmask.setCurrency("EUR");
		accountwithoutmask.setNickname("John");
		accountwithoutmask.setAdditionalInformation(map);

		PSD2Account accountwithmask = new PSD2Account();
		OBCashAccount3 account1 = new OBCashAccount3();
		account1.setIdentification("12345");
		account1.setSchemeName("IBAN");
		OBBranchAndFinancialInstitutionIdentification4 servicer1 = new OBBranchAndFinancialInstitutionIdentification4();
		servicer1.setIdentification("12345");
		accountwithmask.setServicer(servicer);
		accountwithmask.setAccount(acct);
		accountwithmask.setCurrency("EUR");
		accountwithmask.setNickname("John");
		accountwithmask.setAdditionalInformation(map);

		aispHelper.populateAccountwithUnmaskedValues(accountwithmask, accountwithoutmask);
	}

	@Test
	public void testGetConsentSupportedSchemeMap() {
		aispHelper.getConsentSupportedSchemeMap();
	}

	@Test
	public void testPopulateAccountListFromAccountDetails() {
		AispConsent aispConsent = new AispConsent();
		List<AccountDetails> accountDetails = new ArrayList<>();
		AccountDetails adetails = new AccountDetails();
		adetails.setAccount(new OBCashAccount3());
		adetails.getAccount().setIdentification("BOI");
		adetails.setServicer(new OBBranchAndFinancialInstitutionIdentification4());
		adetails.getServicer().setIdentification("served");
		accountDetails.add(adetails);
		aispConsent.setAccountDetails(accountDetails);
		PSD2Account account = new PSD2Account();
		account.setCurrency("GBP");
		account.setNickname("acc");
		account.setAccountSubType(OBExternalAccountSubType1Code.CHARGECARD);
		account.setAccountId("456");
		List<OBCashAccount3> cashAccount = new ArrayList<>();;
		account.setAccount(cashAccount);
		account.setServicer(adetails.getServicer());

		aispHelper.populateAccountListFromAccountDetails(aispConsent);
	}
}

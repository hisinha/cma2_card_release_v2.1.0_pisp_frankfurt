package com.capgemini.psd2.integration.service;

public interface ResponseSigningService {
	
	public String signResponse(String responseBody);
}

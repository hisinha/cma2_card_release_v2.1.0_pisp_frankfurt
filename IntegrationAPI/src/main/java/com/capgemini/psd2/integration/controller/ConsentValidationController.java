package com.capgemini.psd2.integration.controller;

import java.time.ZonedDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.Consent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;

@RestController
public class ConsentValidationController {

	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	private PispConsentAdapter pispConsentAdapter;

	@Autowired
	private CispConsentAdapter cispConsentAdapter;

	@RequestMapping(value = "/validateconsent/{intentId}", method = RequestMethod.GET)
	public void validateConsent(@PathVariable String intentId, @RequestHeader("scope") String scope,
			@RequestHeader("client_id") String clientId) {
		String endDate;
		ZonedDateTime consentExpiry = null;
		ZonedDateTime currentDate = ZonedDateTime.now();
		
		if (clientId == null || clientId.trim().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CLIENT_ID_MISSING_IN_REQUEST);
		}
		if ((PSD2Constants.OPENID_ACCOUNTS.equals(scope)) || (PSD2Constants.ACCOUNTS.equals(scope))) {
			AispConsent aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(intentId,
					ConsentStatusEnum.AUTHORISED);
			validateConsent(clientId,aispConsent);
			endDate = aispConsent.getEndDate();

		} else if ((PSD2Constants.OPENID_PAYMENTS.equals(scope)) || (PSD2Constants.PAYMENTS.equals(scope))) {
			PispConsent pispConsent = pispConsentAdapter.retrieveConsentByPaymentId(intentId,
					ConsentStatusEnum.AUTHORISED);
			validateConsent(clientId,pispConsent);
			endDate = pispConsent.getEndDate();
					
		} else if ((PSD2Constants.OPENID_FUNDSCONFIRMATIONS.equals(scope)) || (PSD2Constants.FUNDSCONFIRMATIONS.equals(scope))) {
			CispConsent cispConsent = cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(intentId,ConsentStatusEnum.AUTHORISED);
			validateConsent(clientId,cispConsent);
			endDate = cispConsent.getEndDate();
		}
		else {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SCOPE_MISSING_IN_REQUEST);			
		}
		if (endDate != null) {
			consentExpiry = ZonedDateTime.parse(endDate);			
		}
		
		if (consentExpiry != null && consentExpiry.isBefore(currentDate)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_EXPIRED);
		}
	}
	
	public void validateConsent(String clientId,Consent consent) {
		if (consent == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_DATA_NOT_PRESENT);
		}
		
		if (!clientId.equalsIgnoreCase(consent.getTppCId())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
	}
	
}
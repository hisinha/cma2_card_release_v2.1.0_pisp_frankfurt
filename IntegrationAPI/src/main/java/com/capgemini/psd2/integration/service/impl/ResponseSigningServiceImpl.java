package com.capgemini.psd2.integration.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.jose4j.jwt.NumericDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.ResponseSigningService;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;
import com.nimbusds.jose.util.Base64URL;

@Service
@ConfigurationProperties("app.signature")
public class ResponseSigningServiceImpl implements ResponseSigningService{
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Value("${server.ssl.key-store}")
	private String keystorePath;

	@Value("${server.ssl.key-store-password}")
	private String keystorePassword;
	
	private KeyStore keyStore = null;

	private List<String> critData = new ArrayList<>();
	
	private Map<String, Map<String, String>> signingKeyDetails=new HashMap<>();

	public List<String> getCritData() {
		return critData;
	}
	
	public Map<String, Map<String, String>> getSigningKeyDetails() {
		return signingKeyDetails;
	}
	
	@PostConstruct
	public void populateKeyStore() {
		
		try {
	 		Resource resource = new ClassPathResource(keystorePath);
			InputStream in = resource.getInputStream();
			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStore.load(in, keystorePassword.toCharArray());
			Enumeration<String> aliases=keyStore.aliases();
			while(aliases.hasMoreElements())
				System.out.println(aliases.nextElement());
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}
	}

	@Override
	public String signResponse(String responseBody) {
		
		JWSObject jwsObject=null;
		JWSHeader header=null;
		PrivateKey key=null;
		JWSSigner signer=null;
		String token = null;
		try {
			System.out.println("tenantId is "+requestHeaderAttributes.getTenantId());
			System.out.println("-----------------------");
			System.out.println(signingKeyDetails.entrySet());
			header = populateJWSHeader();
			jwsObject=createJWSObject(header, responseBody);
			key = (PrivateKey) getPrivateKey();
			signer = new RSASSASigner(key);
			signer.getJCAContext().setProvider(BouncyCastleProviderSingleton.getInstance());
			jwsObject.sign(signer);
			token=jwsObject.serialize();
		} catch (JOSEException | IllegalStateException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}
		 
		String[] tokenParts=token.split("\\.");
		
		return tokenParts[0] + ".." + jwsObject.getSignature();
	}
	
	private JWSObject createJWSObject(JWSHeader header,String jsonResponseString) {
		
		Base64URL unencodedBody = new Base64URL(jsonResponseString);
		Payload payload = new Payload(unencodedBody);
		return new JWSObject(header, payload);
	}

	private String getSubjectFromCertificate() {
		X509Certificate certificate = null;
		try {
			certificate = (X509Certificate) keyStore.getCertificate(signingKeyDetails.get(requestHeaderAttributes.getTenantId()).get("alias"));
			return certificate.getSubjectDN().toString();
		} catch (KeyStoreException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}
	}

	private Key getPrivateKey() {
		try {
			Key key=keyStore.getKey(signingKeyDetails.get(requestHeaderAttributes.getTenantId()).get("alias"), keystorePassword.toCharArray());
			System.out.println("key details "+ key.hashCode());
			return key;
		} catch (UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}
	}

	private JWSHeader populateJWSHeader() {
		Set<String> critHeaders = new HashSet<>();
		critHeaders.addAll(critData);
		
		return new JWSHeader.Builder(JWSAlgorithm.parse(signingKeyDetails.get(requestHeaderAttributes.getTenantId()).get("signingAlgo"))).criticalParams(critHeaders)
				.customParam(PSD2Constants.JOSE_HEADER_B64, Boolean.FALSE)
				.customParam(PSD2Constants.JOSE_HEADER_ISSUER, getSubjectFromCertificate())
				.customParam(PSD2Constants.JOSE_HEADER_IAT, NumericDate.now().getValue())
				.type(JOSEObjectType.JOSE)
				.keyID(signingKeyDetails.get(requestHeaderAttributes.getTenantId()).get("kid"))
				.contentType(MediaType.APPLICATION_JSON_VALUE).build();
	}

}

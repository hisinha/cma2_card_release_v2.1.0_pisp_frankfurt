package com.capgemini.psd2.payment.prestage.validation.mock.foundationservice.service;

import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.payment.prestage.validation.mock.foundationservice.domain.PaymentInstruction;

public interface PaymentPreStageValidationService {

public ValidationPassed validatePaymentInstruction(PaymentInstruction paymentInstruction);
	
}

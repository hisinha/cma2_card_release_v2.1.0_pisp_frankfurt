package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBAccount2Basic;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.AccountStatementsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.client.AccountStatementsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.delegate.AccountStatementsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.Statements;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementsResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsFoundationServiceAdapterTest {


	/** The account statements file foundation service adapter. */
	@InjectMocks
	private AccountStatementsFoundationServiceAdapter accountStatementsFileFoundationServiceAdapter = new AccountStatementsFoundationServiceAdapter();


	/** The account statements file foundation service client. *//*
	 * 
	 */
	@Mock
	private AccountStatementsFoundationServiceClient accountStatementsFileFoundationServiceClient;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	/** The account statements file foundation service delegate. */
	@Mock
	private AccountStatementsFoundationServiceDelegate accountStatementsFileFoundationServiceDelegate;


	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test account statements file FS.
	 */



	@Test(expected=AdapterException.class)
	public void testAccountStatementFileFSIsNullAccountMapping()
	{
		Map<String , String> params = new HashMap<String ,String>();
		params.put("a", "b");
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(null, params);
	}	

	@Test(expected=AdapterException.class)
	public void testAccountStatementFileFSpsuIdIsNull()
	{
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId(null);
		Map<String , String> params = new HashMap<String ,String>();
		params.put("a", "b");
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
	}

	@Test(expected=AdapterException.class)
	public void testAccountStatementFileFSParamsIsNull()
	{
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accountMapping.setAccountDetails(accDetList);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, null);
	}


	@Test(expected=AdapterException.class)
	public void testAccountStatementFileFSforCurrentAccount()
	{
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		Map<String , String> params = new HashMap<String ,String>();
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
	}


	@Test(expected=AdapterException.class)
	public void testAccountStatementFileFSforCurrentAccountException()
	{
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		accDetList.add(null);
		accountMapping.setAccountDetails(null);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		Map<String , String> params = new HashMap<String ,String>();
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
	}



	@Test(expected=AdapterException.class)
	public void testAccountStatementFileFSforCreditCard()
	{
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		Map<String , String> params = new HashMap<String ,String>();
		AccountDetails accDet = new AccountDetails();
		OBAccount2 oba = new OBAccount2();
		OBCashAccount3 oBCashAccount3 = new OBCashAccount3();
		List<OBCashAccount3> account = new ArrayList<OBCashAccount3>();
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();

		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		oBCashAccount3.setIdentification("abcudeudfu");
		account.add(oBCashAccount3);
		oba.setAccount(account);
		accDet.setAccount(oBCashAccount3);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		params.put("channelId","channel123");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "CreditCard");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER,"accountNumber" );
		httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(requestInfo,
				accountMapping, params);
		String singleAccountStatementsCreditCardFileBaseURL="fuyfg";
		accountStatementsFileFoundationServiceDelegate.getFoundationServiceCardURLFile(params, singleAccountStatementsCreditCardFileBaseURL);
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
	}


	@Test(expected=AdapterException.class)
	public void testAccountStatementFileFSIsNullAccountMappingforretrieveAccountStatements()
	{
		Map<String , String> params = new HashMap<String ,String>();
		params.put("a", "b");
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(null, params);
	}	

	@Test(expected=AdapterException.class)
	public void testAccountStatementFileFSpsuIdIsNullforretrieveAccountStatements()
	{
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId(null);
		Map<String , String> params = new HashMap<String ,String>();
		params.put("a", "b");
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
	}
	
	@Test(expected=AdapterException.class)
	public void testAccountStatementFileFSforforretrieveAccountStatementsException()
	{
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		accDetList.add(null);
		accountMapping.setAccountDetails(null);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		Map<String , String> params = new HashMap<String ,String>();
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
	}

	@Test(expected=AdapterException.class)
	public void testAccountStatementFileFSParamsIsNullforretrieveAccountStatements()
	{
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accountMapping.setAccountDetails(accDetList);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, null);
	}
//	@Test
//	public void testAccountStatementFileFSforCreditCardforretrieveAccountStatements()
//	{
//		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//		AccountMapping accountMapping = new AccountMapping();
//		MultiValueMap<String, String> Queryparams = new LinkedMultiValueMap<String ,String>();
//		Map<String , String> params = new HashMap<String ,String>();
//		OBCashAccount3 oBCashAccount3 = new OBCashAccount3();
//		List<OBCashAccount3> account = new ArrayList<OBCashAccount3>();
//		AccountDetails accDet = new AccountDetails();
//		OBAccount2 oba = new OBAccount2();
//		StatementsResponse statements = new StatementsResponse();
//		RequestInfo requestInfo = new RequestInfo();
//		HttpHeaders httpHeaders = new HttpHeaders();
//
//		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
//		accDet.setAccountId("12345");
//		accDet.setAccountNSC("nsc1234");
//		accDet.setAccountNumber("acct1234");
//		oBCashAccount3.setIdentification("abcudeudfu");
//		account.add(oBCashAccount3);
//		oba.setAccount(account);
//		accDet.setAccount(oBCashAccount3);;
//		accDetList.add(accDet);
//		accountMapping.setAccountDetails(accDetList);
//		accountMapping.setTppCID("test");
//		accountMapping.setCorrelationId("test");
//		accountMapping.setPsuId("test");
//		Queryparams.add("channelId","channel123");
//		Queryparams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "CreditCard");
//		Queryparams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER,"accountNumber" );
//		httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(requestInfo,
//				accountMapping, params);
//		String singleAccountStatementsFileBaseStmtURL="/fuyfg";
//		String accountNSC = "nsc1234";
//		String accountNumber= "acct1234";
//		String baseURL = "/fuyfg";
//		accountStatementsFileFoundationServiceDelegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
//		statements = accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(requestInfo, StatementsResponse.class,Queryparams,httpHeaders);
//		accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(statements, params);
//		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
//	}
//	@Test
//	public void testAccountStatementFileFSforCreditCardforretrieveAccountStatementsCA()
//	{
//		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//		AccountMapping accountMapping = new AccountMapping();
//		MultiValueMap<String, String> Queryparams = new LinkedMultiValueMap<String ,String>();
//		Map<String , String> params = new HashMap<String ,String>();
//		OBCashAccount3 oBCashAccount3 = new OBCashAccount3();
//		List<OBCashAccount3> account = new ArrayList<OBCashAccount3>();
//		AccountDetails accDet = new AccountDetails();
//		OBAccount2 oba = new OBAccount2();
//		StatementsResponse statements = new StatementsResponse();
//		RequestInfo requestInfo = new RequestInfo();
//		HttpHeaders httpHeaders = new HttpHeaders();
//
//		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
//		accDet.setAccountId("12345");
//		accDet.setAccountNSC("nsc1234");
//		accDet.setAccountNumber("acct1234");
//		oBCashAccount3.setIdentification("abcudeudfu");
//		account.add(oBCashAccount3);
//		accDet.setAccount(oBCashAccount3);
//		accDetList.add(accDet);
//		accountMapping.setAccountDetails(accDetList);
//		accountMapping.setTppCID("test");
//		accountMapping.setCorrelationId("test");
//		accountMapping.setPsuId("test");
//		Queryparams.add("channelId","channel123");
//		Queryparams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "CurrentAccount");
//		Queryparams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER,"accountNumber" );
//		httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(requestInfo,
//				accountMapping, params);
//		String singleAccountStatementsFileBaseStmtURL="/fuyfg";
//		String accountNSC = "nsc1234";
//		String accountNumber= "acct1234";
//		String baseURL = "/fuyfg";
//		accountStatementsFileFoundationServiceDelegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
//		statements = accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(requestInfo,  StatementsResponse.class, Queryparams,httpHeaders);
//		accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(statements, params);
//		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
//	}
	@Test
	public void retrieveAccountStatementsByStatementIdTest()
	{
		AccountMapping accountMapping = new AccountMapping();
		Map<String , String> params = new HashMap<String ,String>();
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatementsByStatementId(accountMapping, params);	
	}
	
	@Test
	public void retrieveAccountTransactionsByStatementsId()
	{
		AccountMapping accountMapping = new AccountMapping();
		Map<String , String> params = new HashMap<String ,String>();
		accountStatementsFileFoundationServiceAdapter.retrieveAccountTransactionsByStatementsId(accountMapping, params);
	}
}

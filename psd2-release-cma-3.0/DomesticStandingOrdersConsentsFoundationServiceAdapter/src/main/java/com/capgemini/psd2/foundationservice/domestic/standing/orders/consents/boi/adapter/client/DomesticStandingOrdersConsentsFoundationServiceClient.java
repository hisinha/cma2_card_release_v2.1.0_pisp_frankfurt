package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import javax.annotation.PostConstruct;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class DomesticStandingOrdersConsentsFoundationServiceClient {
	
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@PostConstruct
	private void init() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        converter.setObjectMapper(mapper);
        restTemplate.getMessageConverters().add(0,converter);
	}
	
	public StandingOrderInstructionProposal restTransportForDomesticStandingOrdersServicePost(RequestInfo requestInfo,
			StandingOrderInstructionProposal standingInstructionProposalRequest, Class<StandingOrderInstructionProposal> responseType,
			HttpHeaders httpHeaders) {
		
		return restClient.callForPost(requestInfo, standingInstructionProposalRequest, responseType, httpHeaders);
	}
	
	public StandingOrderInstructionProposal restTransportForDomesticStandingOrderServiceGet(RequestInfo reqInfo,
			Class<StandingOrderInstructionProposal> responseType, HttpHeaders headers) {

		return restClient.callForGet(reqInfo, responseType, headers);

	}

}

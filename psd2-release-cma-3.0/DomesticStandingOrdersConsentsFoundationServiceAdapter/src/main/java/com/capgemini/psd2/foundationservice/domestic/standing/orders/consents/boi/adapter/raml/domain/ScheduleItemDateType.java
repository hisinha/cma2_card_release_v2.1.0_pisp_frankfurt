package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Defines if the schedule Item Date is the arrival date of the payment or execution date
 */
public enum ScheduleItemDateType {
  
  EXECUTION("Execution"),
  
  ARRIVAL("Arrival");

  private String value;

  ScheduleItemDateType(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ScheduleItemDateType fromValue(String text) {
    for (ScheduleItemDateType b : ScheduleItemDateType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}


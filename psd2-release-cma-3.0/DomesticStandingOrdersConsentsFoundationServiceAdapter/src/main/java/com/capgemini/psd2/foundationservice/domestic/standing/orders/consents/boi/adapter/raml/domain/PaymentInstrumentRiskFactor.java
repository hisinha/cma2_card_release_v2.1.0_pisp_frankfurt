package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Risk Factor associated to a payment instruction.
 */
@ApiModel(description = "Risk Factor associated to a payment instruction.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PaymentInstrumentRiskFactor   {
  @JsonProperty("paymentContextCode")
  private String paymentContextCode = null;

  @JsonProperty("merchantCategoryCode")
  private String merchantCategoryCode = null;

  @JsonProperty("merchantCustomerIdentification")
  private String merchantCustomerIdentification = null;

  @JsonProperty("counterPartyAddress")
  private Address counterPartyAddress = null;

  public PaymentInstrumentRiskFactor paymentContextCode(String paymentContextCode) {
    this.paymentContextCode = paymentContextCode;
    return this;
  }

  /**
   * Get paymentContextCode
   * @return paymentContextCode
  **/
  @ApiModelProperty(value = "")

@Pattern(regexp="[A-Za-z]{0,35}") 
  public String getPaymentContextCode() {
    return paymentContextCode;
  }

  public void setPaymentContextCode(String paymentContextCode) {
    this.paymentContextCode = paymentContextCode;
  }

  public PaymentInstrumentRiskFactor merchantCategoryCode(String merchantCategoryCode) {
    this.merchantCategoryCode = merchantCategoryCode;
    return this;
  }

  /**
   * Get merchantCategoryCode
   * @return merchantCategoryCode
  **/
  @ApiModelProperty(value = "")

@Size(min=3,max=4) 
  public String getMerchantCategoryCode() {
    return merchantCategoryCode;
  }

  public void setMerchantCategoryCode(String merchantCategoryCode) {
    this.merchantCategoryCode = merchantCategoryCode;
  }

  public PaymentInstrumentRiskFactor merchantCustomerIdentification(String merchantCustomerIdentification) {
    this.merchantCustomerIdentification = merchantCustomerIdentification;
    return this;
  }

  /**
   * Get merchantCustomerIdentification
   * @return merchantCustomerIdentification
  **/
  @ApiModelProperty(value = "")

@Size(min=0,max=32) 
  public String getMerchantCustomerIdentification() {
    return merchantCustomerIdentification;
  }

  public void setMerchantCustomerIdentification(String merchantCustomerIdentification) {
    this.merchantCustomerIdentification = merchantCustomerIdentification;
  }

  public PaymentInstrumentRiskFactor counterPartyAddress(Address counterPartyAddress) {
    this.counterPartyAddress = counterPartyAddress;
    return this;
  }

  /**
   * Get counterPartyAddress
   * @return counterPartyAddress
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Address getCounterPartyAddress() {
    return counterPartyAddress;
  }

  public void setCounterPartyAddress(Address counterPartyAddress) {
    this.counterPartyAddress = counterPartyAddress;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentInstrumentRiskFactor paymentInstrumentRiskFactor = (PaymentInstrumentRiskFactor) o;
    return Objects.equals(this.paymentContextCode, paymentInstrumentRiskFactor.paymentContextCode) &&
        Objects.equals(this.merchantCategoryCode, paymentInstrumentRiskFactor.merchantCategoryCode) &&
        Objects.equals(this.merchantCustomerIdentification, paymentInstrumentRiskFactor.merchantCustomerIdentification) &&
        Objects.equals(this.counterPartyAddress, paymentInstrumentRiskFactor.counterPartyAddress);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentContextCode, merchantCategoryCode, merchantCustomerIdentification, counterPartyAddress);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentInstrumentRiskFactor {\n");
    
    sb.append("    paymentContextCode: ").append(toIndentedString(paymentContextCode)).append("\n");
    sb.append("    merchantCategoryCode: ").append(toIndentedString(merchantCategoryCode)).append("\n");
    sb.append("    merchantCustomerIdentification: ").append(toIndentedString(merchantCustomerIdentification)).append("\n");
    sb.append("    counterPartyAddress: ").append(toIndentedString(counterPartyAddress)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


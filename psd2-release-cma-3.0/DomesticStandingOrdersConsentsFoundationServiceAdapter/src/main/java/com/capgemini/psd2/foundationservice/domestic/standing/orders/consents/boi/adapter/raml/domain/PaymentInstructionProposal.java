package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Payment Instruction Proposal ( aka Payment Order Consent ).
 */
@ApiModel(description = "Payment Instruction Proposal ( aka Payment Order Consent ).")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PaymentInstructionProposal   {
  @JsonProperty("paymentInstructionProposalId")
  private String paymentInstructionProposalId = null;

  @JsonProperty("proposalCreationDatetime")
  private OffsetDateTime proposalCreationDatetime = null;

  @JsonProperty("proposalStatus")
  private ProposalStatus proposalStatus = null;

  @JsonProperty("proposalStatusUpdateDatetime")
  private OffsetDateTime proposalStatusUpdateDatetime = null;

  @JsonProperty("authorisationType")
  private AuthorisationType authorisationType = null;

  @JsonProperty("authorisationDatetime")
  private OffsetDateTime authorisationDatetime = null;

  @JsonProperty("proposingParty")
  private Party proposingParty = null;

  @JsonProperty("proposingPartyPostalAddress")
  private PaymentInstructionPostalAddress proposingPartyPostalAddress = null;

  @JsonProperty("proposingPartyAccount")
  private ProposingPartyAccount proposingPartyAccount = null;

  @JsonProperty("authorisingParty")
  private Party authorisingParty = null;

  @JsonProperty("authorisingPartyAccount")
  private AuthorisingPartyAccount authorisingPartyAccount = null;

  @JsonProperty("paymentInstructionRiskFactorReference")
  private PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = null;

  @JsonProperty("paymentType")
  private PaymentType paymentType = null;

  @JsonProperty("charges")
  @Valid
  private List<PaymentInstructionCharge> charges = null;

  @JsonProperty("fraudSystemResponse")
  private FraudSystemResponse fraudSystemResponse = null;

  @JsonProperty("authorisingPartyReference")
  private String authorisingPartyReference = null;

  @JsonProperty("authorisingPartyUnstructuredReference")
  private String authorisingPartyUnstructuredReference = null;

  @JsonProperty("instructionReference")
  private String instructionReference = null;

  @JsonProperty("instructionEndToEndReference")
  private String instructionEndToEndReference = null;

  @JsonProperty("instructionLocalInstrument")
  private String instructionLocalInstrument = null;

  @JsonProperty("transactionCurrency")
  private Currency transactionCurrency = null;

  @JsonProperty("financialEventAmount")
  private FinancialEventAmount financialEventAmount = null;

  public PaymentInstructionProposal paymentInstructionProposalId(String paymentInstructionProposalId) {
    this.paymentInstructionProposalId = paymentInstructionProposalId;
    return this;
  }

  /**
   * The identifier of the Payment Instruction Proposal in the Banking System
   * @return paymentInstructionProposalId
  **/
  @ApiModelProperty(required = true, value = "The identifier of the Payment Instruction Proposal in the Banking System")
  @NotNull

@Size(min=0,max=35) 
  public String getPaymentInstructionProposalId() {
    return paymentInstructionProposalId;
  }

  public void setPaymentInstructionProposalId(String paymentInstructionProposalId) {
    this.paymentInstructionProposalId = paymentInstructionProposalId;
  }

  public PaymentInstructionProposal proposalCreationDatetime(OffsetDateTime proposalCreationDatetime) {
    this.proposalCreationDatetime = proposalCreationDatetime;
    return this;
  }

  /**
   * The risk profile associated
   * @return proposalCreationDatetime
  **/
  @ApiModelProperty(required = true, value = "The risk profile associated")
  @NotNull

  @Valid

  public OffsetDateTime getProposalCreationDatetime() {
    return proposalCreationDatetime;
  }

  public void setProposalCreationDatetime(OffsetDateTime proposalCreationDatetime) {
    this.proposalCreationDatetime = proposalCreationDatetime;
  }

  public PaymentInstructionProposal proposalStatus(ProposalStatus proposalStatus) {
    this.proposalStatus = proposalStatus;
    return this;
  }

  /**
   * Get proposalStatus
   * @return proposalStatus
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ProposalStatus getProposalStatus() {
    return proposalStatus;
  }

  public void setProposalStatus(ProposalStatus proposalStatus) {
    this.proposalStatus = proposalStatus;
  }

  public PaymentInstructionProposal proposalStatusUpdateDatetime(OffsetDateTime proposalStatusUpdateDatetime) {
    this.proposalStatusUpdateDatetime = proposalStatusUpdateDatetime;
    return this;
  }

  /**
   * Reflects the last time proposalStatus is updated.
   * @return proposalStatusUpdateDatetime
  **/
  @ApiModelProperty(value = "Reflects the last time proposalStatus is updated.")

  @Valid

  public OffsetDateTime getProposalStatusUpdateDatetime() {
    return proposalStatusUpdateDatetime;
  }

  public void setProposalStatusUpdateDatetime(OffsetDateTime proposalStatusUpdateDatetime) {
    this.proposalStatusUpdateDatetime = proposalStatusUpdateDatetime;
  }

  public PaymentInstructionProposal authorisationType(AuthorisationType authorisationType) {
    this.authorisationType = authorisationType;
    return this;
  }

  /**
   * Get authorisationType
   * @return authorisationType
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AuthorisationType getAuthorisationType() {
    return authorisationType;
  }

  public void setAuthorisationType(AuthorisationType authorisationType) {
    this.authorisationType = authorisationType;
  }

  public PaymentInstructionProposal authorisationDatetime(OffsetDateTime authorisationDatetime) {
    this.authorisationDatetime = authorisationDatetime;
    return this;
  }

  /**
   * Moment on which the proposal was authorised.
   * @return authorisationDatetime
  **/
  @ApiModelProperty(value = "Moment on which the proposal was authorised.")

  @Valid

  public OffsetDateTime getAuthorisationDatetime() {
    return authorisationDatetime;
  }

  public void setAuthorisationDatetime(OffsetDateTime authorisationDatetime) {
    this.authorisationDatetime = authorisationDatetime;
  }

  public PaymentInstructionProposal proposingParty(Party proposingParty) {
    this.proposingParty = proposingParty;
    return this;
  }

  /**
   * Get proposingParty
   * @return proposingParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Party getProposingParty() {
    return proposingParty;
  }

  public void setProposingParty(Party proposingParty) {
    this.proposingParty = proposingParty;
  }

  public PaymentInstructionProposal proposingPartyPostalAddress(PaymentInstructionPostalAddress proposingPartyPostalAddress) {
    this.proposingPartyPostalAddress = proposingPartyPostalAddress;
    return this;
  }

  /**
   * Get proposingPartyPostalAddress
   * @return proposingPartyPostalAddress
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PaymentInstructionPostalAddress getProposingPartyPostalAddress() {
    return proposingPartyPostalAddress;
  }

  public void setProposingPartyPostalAddress(PaymentInstructionPostalAddress proposingPartyPostalAddress) {
    this.proposingPartyPostalAddress = proposingPartyPostalAddress;
  }

  public PaymentInstructionProposal proposingPartyAccount(ProposingPartyAccount proposingPartyAccount) {
    this.proposingPartyAccount = proposingPartyAccount;
    return this;
  }

  /**
   * Get proposingPartyAccount
   * @return proposingPartyAccount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ProposingPartyAccount getProposingPartyAccount() {
    return proposingPartyAccount;
  }

  public void setProposingPartyAccount(ProposingPartyAccount proposingPartyAccount) {
    this.proposingPartyAccount = proposingPartyAccount;
  }

  public PaymentInstructionProposal authorisingParty(Party authorisingParty) {
    this.authorisingParty = authorisingParty;
    return this;
  }

  /**
   * Get authorisingParty
   * @return authorisingParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Party getAuthorisingParty() {
    return authorisingParty;
  }

  public void setAuthorisingParty(Party authorisingParty) {
    this.authorisingParty = authorisingParty;
  }

  public PaymentInstructionProposal authorisingPartyAccount(AuthorisingPartyAccount authorisingPartyAccount) {
    this.authorisingPartyAccount = authorisingPartyAccount;
    return this;
  }

  /**
   * Get authorisingPartyAccount
   * @return authorisingPartyAccount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public AuthorisingPartyAccount getAuthorisingPartyAccount() {
    return authorisingPartyAccount;
  }

  public void setAuthorisingPartyAccount(AuthorisingPartyAccount authorisingPartyAccount) {
    this.authorisingPartyAccount = authorisingPartyAccount;
  }

  public PaymentInstructionProposal paymentInstructionRiskFactorReference(PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference) {
    this.paymentInstructionRiskFactorReference = paymentInstructionRiskFactorReference;
    return this;
  }

  /**
   * Get paymentInstructionRiskFactorReference
   * @return paymentInstructionRiskFactorReference
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PaymentInstrumentRiskFactor getPaymentInstructionRiskFactorReference() {
    return paymentInstructionRiskFactorReference;
  }

  public void setPaymentInstructionRiskFactorReference(PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference) {
    this.paymentInstructionRiskFactorReference = paymentInstructionRiskFactorReference;
  }

  public PaymentInstructionProposal paymentType(PaymentType paymentType) {
    this.paymentType = paymentType;
    return this;
  }

  /**
   * Get paymentType
   * @return paymentType
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PaymentType getPaymentType() {
    return paymentType;
  }

  public void setPaymentType(PaymentType paymentType) {
    this.paymentType = paymentType;
  }

  public PaymentInstructionProposal charges(List<PaymentInstructionCharge> charges) {
    this.charges = charges;
    return this;
  }

  public PaymentInstructionProposal addChargesItem(PaymentInstructionCharge chargesItem) {
    if (this.charges == null) {
      this.charges = new ArrayList<PaymentInstructionCharge>();
    }
    this.charges.add(chargesItem);
    return this;
  }

  /**
   * Get charges
   * @return charges
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<PaymentInstructionCharge> getCharges() {
    return charges;
  }

  public void setCharges(List<PaymentInstructionCharge> charges) {
    this.charges = charges;
  }

  public PaymentInstructionProposal fraudSystemResponse(FraudSystemResponse fraudSystemResponse) {
    this.fraudSystemResponse = fraudSystemResponse;
    return this;
  }

  /**
   * Get fraudSystemResponse
   * @return fraudSystemResponse
  **/
  @ApiModelProperty(value = "")

  @Valid

  public FraudSystemResponse getFraudSystemResponse() {
    return fraudSystemResponse;
  }

  public void setFraudSystemResponse(FraudSystemResponse fraudSystemResponse) {
    this.fraudSystemResponse = fraudSystemResponse;
  }

  public PaymentInstructionProposal authorisingPartyReference(String authorisingPartyReference) {
    this.authorisingPartyReference = authorisingPartyReference;
    return this;
  }

  /**
   * Payer Reference.
   * @return authorisingPartyReference
  **/
  @ApiModelProperty(example = "FRESCO-101", required = true, value = "Payer Reference.")
  @NotNull

@Pattern(regexp="[A-Za-z0-9/?:().,'+ -]{0,35}") 
  public String getAuthorisingPartyReference() {
    return authorisingPartyReference;
  }

  public void setAuthorisingPartyReference(String authorisingPartyReference) {
    this.authorisingPartyReference = authorisingPartyReference;
  }

  public PaymentInstructionProposal authorisingPartyUnstructuredReference(String authorisingPartyUnstructuredReference) {
    this.authorisingPartyUnstructuredReference = authorisingPartyUnstructuredReference;
    return this;
  }

  /**
   * Payer unstructured reference.
   * @return authorisingPartyUnstructuredReference
  **/
  @ApiModelProperty(example = "Internal ops code 5120101", required = true, value = "Payer unstructured reference.")
  @NotNull

@Pattern(regexp="[A-Za-z0-9/?:().,'+ -]{0,140}") 
  public String getAuthorisingPartyUnstructuredReference() {
    return authorisingPartyUnstructuredReference;
  }

  public void setAuthorisingPartyUnstructuredReference(String authorisingPartyUnstructuredReference) {
    this.authorisingPartyUnstructuredReference = authorisingPartyUnstructuredReference;
  }

  public PaymentInstructionProposal instructionReference(String instructionReference) {
    this.instructionReference = instructionReference;
    return this;
  }

  /**
   * Payment Instruction Reference Identification.
   * @return instructionReference
  **/
  @ApiModelProperty(required = true, value = "Payment Instruction Reference Identification.")
  @NotNull

@Size(min=0,max=35) 
  public String getInstructionReference() {
    return instructionReference;
  }

  public void setInstructionReference(String instructionReference) {
    this.instructionReference = instructionReference;
  }

  public PaymentInstructionProposal instructionEndToEndReference(String instructionEndToEndReference) {
    this.instructionEndToEndReference = instructionEndToEndReference;
    return this;
  }

  /**
   * Payment Instruction E2E Unique Id.
   * @return instructionEndToEndReference
  **/
  @ApiModelProperty(required = true, value = "Payment Instruction E2E Unique Id.")
  @NotNull

@Size(min=0,max=35) 
  public String getInstructionEndToEndReference() {
    return instructionEndToEndReference;
  }

  public void setInstructionEndToEndReference(String instructionEndToEndReference) {
    this.instructionEndToEndReference = instructionEndToEndReference;
  }

  public PaymentInstructionProposal instructionLocalInstrument(String instructionLocalInstrument) {
    this.instructionLocalInstrument = instructionLocalInstrument;
    return this;
  }

  /**
   * Identifies the requested payment scheme. This element is used to specify a local instrument, local clearing option and/or further qualify the service or service level.
   * @return instructionLocalInstrument
  **/
  @ApiModelProperty(example = "localInstrument", value = "Identifies the requested payment scheme. This element is used to specify a local instrument, local clearing option and/or further qualify the service or service level.")

@Pattern(regexp="[A-Za-z]{0,35}") 
  public String getInstructionLocalInstrument() {
    return instructionLocalInstrument;
  }

  public void setInstructionLocalInstrument(String instructionLocalInstrument) {
    this.instructionLocalInstrument = instructionLocalInstrument;
  }

  public PaymentInstructionProposal transactionCurrency(Currency transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
    return this;
  }

  /**
   * Get transactionCurrency
   * @return transactionCurrency
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Currency getTransactionCurrency() {
    return transactionCurrency;
  }

  public void setTransactionCurrency(Currency transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
  }

  public PaymentInstructionProposal financialEventAmount(FinancialEventAmount financialEventAmount) {
    this.financialEventAmount = financialEventAmount;
    return this;
  }

  /**
   * Get financialEventAmount
   * @return financialEventAmount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public FinancialEventAmount getFinancialEventAmount() {
    return financialEventAmount;
  }

  public void setFinancialEventAmount(FinancialEventAmount financialEventAmount) {
    this.financialEventAmount = financialEventAmount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentInstructionProposal paymentInstructionProposal = (PaymentInstructionProposal) o;
    return Objects.equals(this.paymentInstructionProposalId, paymentInstructionProposal.paymentInstructionProposalId) &&
        Objects.equals(this.proposalCreationDatetime, paymentInstructionProposal.proposalCreationDatetime) &&
        Objects.equals(this.proposalStatus, paymentInstructionProposal.proposalStatus) &&
        Objects.equals(this.proposalStatusUpdateDatetime, paymentInstructionProposal.proposalStatusUpdateDatetime) &&
        Objects.equals(this.authorisationType, paymentInstructionProposal.authorisationType) &&
        Objects.equals(this.authorisationDatetime, paymentInstructionProposal.authorisationDatetime) &&
        Objects.equals(this.proposingParty, paymentInstructionProposal.proposingParty) &&
        Objects.equals(this.proposingPartyPostalAddress, paymentInstructionProposal.proposingPartyPostalAddress) &&
        Objects.equals(this.proposingPartyAccount, paymentInstructionProposal.proposingPartyAccount) &&
        Objects.equals(this.authorisingParty, paymentInstructionProposal.authorisingParty) &&
        Objects.equals(this.authorisingPartyAccount, paymentInstructionProposal.authorisingPartyAccount) &&
        Objects.equals(this.paymentInstructionRiskFactorReference, paymentInstructionProposal.paymentInstructionRiskFactorReference) &&
        Objects.equals(this.paymentType, paymentInstructionProposal.paymentType) &&
        Objects.equals(this.charges, paymentInstructionProposal.charges) &&
        Objects.equals(this.fraudSystemResponse, paymentInstructionProposal.fraudSystemResponse) &&
        Objects.equals(this.authorisingPartyReference, paymentInstructionProposal.authorisingPartyReference) &&
        Objects.equals(this.authorisingPartyUnstructuredReference, paymentInstructionProposal.authorisingPartyUnstructuredReference) &&
        Objects.equals(this.instructionReference, paymentInstructionProposal.instructionReference) &&
        Objects.equals(this.instructionEndToEndReference, paymentInstructionProposal.instructionEndToEndReference) &&
        Objects.equals(this.instructionLocalInstrument, paymentInstructionProposal.instructionLocalInstrument) &&
        Objects.equals(this.transactionCurrency, paymentInstructionProposal.transactionCurrency) &&
        Objects.equals(this.financialEventAmount, paymentInstructionProposal.financialEventAmount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentInstructionProposalId, proposalCreationDatetime, proposalStatus, proposalStatusUpdateDatetime, authorisationType, authorisationDatetime, proposingParty, proposingPartyPostalAddress, proposingPartyAccount, authorisingParty, authorisingPartyAccount, paymentInstructionRiskFactorReference, paymentType, charges, fraudSystemResponse, authorisingPartyReference, authorisingPartyUnstructuredReference, instructionReference, instructionEndToEndReference, instructionLocalInstrument, transactionCurrency, financialEventAmount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentInstructionProposal {\n");
    
    sb.append("    paymentInstructionProposalId: ").append(toIndentedString(paymentInstructionProposalId)).append("\n");
    sb.append("    proposalCreationDatetime: ").append(toIndentedString(proposalCreationDatetime)).append("\n");
    sb.append("    proposalStatus: ").append(toIndentedString(proposalStatus)).append("\n");
    sb.append("    proposalStatusUpdateDatetime: ").append(toIndentedString(proposalStatusUpdateDatetime)).append("\n");
    sb.append("    authorisationType: ").append(toIndentedString(authorisationType)).append("\n");
    sb.append("    authorisationDatetime: ").append(toIndentedString(authorisationDatetime)).append("\n");
    sb.append("    proposingParty: ").append(toIndentedString(proposingParty)).append("\n");
    sb.append("    proposingPartyPostalAddress: ").append(toIndentedString(proposingPartyPostalAddress)).append("\n");
    sb.append("    proposingPartyAccount: ").append(toIndentedString(proposingPartyAccount)).append("\n");
    sb.append("    authorisingParty: ").append(toIndentedString(authorisingParty)).append("\n");
    sb.append("    authorisingPartyAccount: ").append(toIndentedString(authorisingPartyAccount)).append("\n");
    sb.append("    paymentInstructionRiskFactorReference: ").append(toIndentedString(paymentInstructionRiskFactorReference)).append("\n");
    sb.append("    paymentType: ").append(toIndentedString(paymentType)).append("\n");
    sb.append("    charges: ").append(toIndentedString(charges)).append("\n");
    sb.append("    fraudSystemResponse: ").append(toIndentedString(fraudSystemResponse)).append("\n");
    sb.append("    authorisingPartyReference: ").append(toIndentedString(authorisingPartyReference)).append("\n");
    sb.append("    authorisingPartyUnstructuredReference: ").append(toIndentedString(authorisingPartyUnstructuredReference)).append("\n");
    sb.append("    instructionReference: ").append(toIndentedString(instructionReference)).append("\n");
    sb.append("    instructionEndToEndReference: ").append(toIndentedString(instructionEndToEndReference)).append("\n");
    sb.append("    instructionLocalInstrument: ").append(toIndentedString(instructionLocalInstrument)).append("\n");
    sb.append("    transactionCurrency: ").append(toIndentedString(transactionCurrency)).append("\n");
    sb.append("    financialEventAmount: ").append(toIndentedString(financialEventAmount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * DigitalUser11
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class DigitalUser11   {
  @JsonProperty("digitalUserIdentifier")
  private String digitalUserIdentifier = null;

  @JsonProperty("authenticationSystemCode")
  private AuthenticationSystemCode authenticationSystemCode = null;

  @JsonProperty("customerAuthenticationSession")
  @Valid
  private List<CustomerAuthenticationSession> customerAuthenticationSession = new ArrayList<CustomerAuthenticationSession>();

  public DigitalUser11 digitalUserIdentifier(String digitalUserIdentifier) {
    this.digitalUserIdentifier = digitalUserIdentifier;
    return this;
  }

  /**
   * Get digitalUserIdentifier
   * @return digitalUserIdentifier
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getDigitalUserIdentifier() {
    return digitalUserIdentifier;
  }

  public void setDigitalUserIdentifier(String digitalUserIdentifier) {
    this.digitalUserIdentifier = digitalUserIdentifier;
  }

  public DigitalUser11 authenticationSystemCode(AuthenticationSystemCode authenticationSystemCode) {
    this.authenticationSystemCode = authenticationSystemCode;
    return this;
  }

  /**
   * Get authenticationSystemCode
   * @return authenticationSystemCode
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AuthenticationSystemCode getAuthenticationSystemCode() {
    return authenticationSystemCode;
  }

  public void setAuthenticationSystemCode(AuthenticationSystemCode authenticationSystemCode) {
    this.authenticationSystemCode = authenticationSystemCode;
  }

  public DigitalUser11 customerAuthenticationSession(List<CustomerAuthenticationSession> customerAuthenticationSession) {
    this.customerAuthenticationSession = customerAuthenticationSession;
    return this;
  }

  public DigitalUser11 addCustomerAuthenticationSessionItem(CustomerAuthenticationSession customerAuthenticationSessionItem) {
    this.customerAuthenticationSession.add(customerAuthenticationSessionItem);
    return this;
  }

  /**
   * Get customerAuthenticationSession
   * @return customerAuthenticationSession
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public List<CustomerAuthenticationSession> getCustomerAuthenticationSession() {
    return customerAuthenticationSession;
  }

  public void setCustomerAuthenticationSession(List<CustomerAuthenticationSession> customerAuthenticationSession) {
    this.customerAuthenticationSession = customerAuthenticationSession;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DigitalUser11 digitalUser11 = (DigitalUser11) o;
    return Objects.equals(this.digitalUserIdentifier, digitalUser11.digitalUserIdentifier) &&
        Objects.equals(this.authenticationSystemCode, digitalUser11.authenticationSystemCode) &&
        Objects.equals(this.customerAuthenticationSession, digitalUser11.customerAuthenticationSession);
  }

  @Override
  public int hashCode() {
    return Objects.hash(digitalUserIdentifier, authenticationSystemCode, customerAuthenticationSession);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DigitalUser11 {\n");
    
    sb.append("    digitalUserIdentifier: ").append(toIndentedString(digitalUserIdentifier)).append("\n");
    sb.append("    authenticationSystemCode: ").append(toIndentedString(authenticationSystemCode)).append("\n");
    sb.append("    customerAuthenticationSession: ").append(toIndentedString(customerAuthenticationSession)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


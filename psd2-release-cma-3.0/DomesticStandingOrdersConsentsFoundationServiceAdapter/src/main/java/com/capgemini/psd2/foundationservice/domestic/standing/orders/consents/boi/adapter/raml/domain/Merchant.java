package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Merchant
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class Merchant   {
  @JsonProperty("merchantCategoryCode")
  private String merchantCategoryCode = null;

  @JsonProperty("merchantName")
  private String merchantName = null;

  @JsonProperty("merchantDescription")
  private String merchantDescription = null;

  public Merchant merchantCategoryCode(String merchantCategoryCode) {
    this.merchantCategoryCode = merchantCategoryCode;
    return this;
  }

  /**
   * Get merchantCategoryCode
   * @return merchantCategoryCode
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getMerchantCategoryCode() {
    return merchantCategoryCode;
  }

  public void setMerchantCategoryCode(String merchantCategoryCode) {
    this.merchantCategoryCode = merchantCategoryCode;
  }

  public Merchant merchantName(String merchantName) {
    this.merchantName = merchantName;
    return this;
  }

  /**
   * Get merchantName
   * @return merchantName
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getMerchantName() {
    return merchantName;
  }

  public void setMerchantName(String merchantName) {
    this.merchantName = merchantName;
  }

  public Merchant merchantDescription(String merchantDescription) {
    this.merchantDescription = merchantDescription;
    return this;
  }

  /**
   * Get merchantDescription
   * @return merchantDescription
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getMerchantDescription() {
    return merchantDescription;
  }

  public void setMerchantDescription(String merchantDescription) {
    this.merchantDescription = merchantDescription;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Merchant merchant = (Merchant) o;
    return Objects.equals(this.merchantCategoryCode, merchant.merchantCategoryCode) &&
        Objects.equals(this.merchantName, merchant.merchantName) &&
        Objects.equals(this.merchantDescription, merchant.merchantDescription);
  }

  @Override
  public int hashCode() {
    return Objects.hash(merchantCategoryCode, merchantName, merchantDescription);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Merchant {\n");
    
    sb.append("    merchantCategoryCode: ").append(toIndentedString(merchantCategoryCode)).append("\n");
    sb.append("    merchantName: ").append(toIndentedString(merchantName)).append("\n");
    sb.append("    merchantDescription: ").append(toIndentedString(merchantDescription)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


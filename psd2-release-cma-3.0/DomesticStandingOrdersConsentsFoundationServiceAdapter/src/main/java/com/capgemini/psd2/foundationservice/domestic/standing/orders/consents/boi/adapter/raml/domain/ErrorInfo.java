package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * ErrorInfo
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class ErrorInfo   {
  @JsonProperty("replayCount")
  private Integer replayCount = null;

  @JsonProperty("lastError")
  private String lastError = null;

  public ErrorInfo replayCount(Integer replayCount) {
    this.replayCount = replayCount;
    return this;
  }

  /**
   * Get replayCount
   * @return replayCount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getReplayCount() {
    return replayCount;
  }

  public void setReplayCount(Integer replayCount) {
    this.replayCount = replayCount;
  }

  public ErrorInfo lastError(String lastError) {
    this.lastError = lastError;
    return this;
  }

  /**
   * Get lastError
   * @return lastError
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getLastError() {
    return lastError;
  }

  public void setLastError(String lastError) {
    this.lastError = lastError;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErrorInfo errorInfo = (ErrorInfo) o;
    return Objects.equals(this.replayCount, errorInfo.replayCount) &&
        Objects.equals(this.lastError, errorInfo.lastError);
  }

  @Override
  public int hashCode() {
    return Objects.hash(replayCount, lastError);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorInfo {\n");
    
    sb.append("    replayCount: ").append(toIndentedString(replayCount)).append("\n");
    sb.append("    lastError: ").append(toIndentedString(lastError)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Savings Account object
 */
@ApiModel(description = "Savings Account object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class SavingsAccount   {
  @JsonProperty("parentNationalSortCodeNSCNumber")
  private String parentNationalSortCodeNSCNumber = null;

  @JsonProperty("internationalBankAccountNumberIBAN")
  private String internationalBankAccountNumberIBAN = null;

  @JsonProperty("swiftBankIdentifierCodeBIC")
  private String swiftBankIdentifierCodeBIC = null;

  public SavingsAccount parentNationalSortCodeNSCNumber(String parentNationalSortCodeNSCNumber) {
    this.parentNationalSortCodeNSCNumber = parentNationalSortCodeNSCNumber;
    return this;
  }

  /**
   * Parent NSC of the branch where account is held
   * @return parentNationalSortCodeNSCNumber
  **/
  @ApiModelProperty(example = "987654", required = true, value = "Parent NSC of the branch where account is held")
  @NotNull

@Pattern(regexp="[0-9]{6}") 
  public String getParentNationalSortCodeNSCNumber() {
    return parentNationalSortCodeNSCNumber;
  }

  public void setParentNationalSortCodeNSCNumber(String parentNationalSortCodeNSCNumber) {
    this.parentNationalSortCodeNSCNumber = parentNationalSortCodeNSCNumber;
  }

  public SavingsAccount internationalBankAccountNumberIBAN(String internationalBankAccountNumberIBAN) {
    this.internationalBankAccountNumberIBAN = internationalBankAccountNumberIBAN;
    return this;
  }

  /**
   * Get internationalBankAccountNumberIBAN
   * @return internationalBankAccountNumberIBAN
  **/
  @ApiModelProperty(example = "987654987654", required = true, value = "")
  @NotNull

@Pattern(regexp="[A-Z0-9]{12,34}") 
  public String getInternationalBankAccountNumberIBAN() {
    return internationalBankAccountNumberIBAN;
  }

  public void setInternationalBankAccountNumberIBAN(String internationalBankAccountNumberIBAN) {
    this.internationalBankAccountNumberIBAN = internationalBankAccountNumberIBAN;
  }

  public SavingsAccount swiftBankIdentifierCodeBIC(String swiftBankIdentifierCodeBIC) {
    this.swiftBankIdentifierCodeBIC = swiftBankIdentifierCodeBIC;
    return this;
  }

  /**
   * Get swiftBankIdentifierCodeBIC
   * @return swiftBankIdentifierCodeBIC
  **/
  @ApiModelProperty(example = "12345123451", required = true, value = "")
  @NotNull

@Pattern(regexp="^([A-Z0-9]{8}|[A-Z0-9]{11})$") 
  public String getSwiftBankIdentifierCodeBIC() {
    return swiftBankIdentifierCodeBIC;
  }

  public void setSwiftBankIdentifierCodeBIC(String swiftBankIdentifierCodeBIC) {
    this.swiftBankIdentifierCodeBIC = swiftBankIdentifierCodeBIC;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SavingsAccount savingsAccount = (SavingsAccount) o;
    return Objects.equals(this.parentNationalSortCodeNSCNumber, savingsAccount.parentNationalSortCodeNSCNumber) &&
        Objects.equals(this.internationalBankAccountNumberIBAN, savingsAccount.internationalBankAccountNumberIBAN) &&
        Objects.equals(this.swiftBankIdentifierCodeBIC, savingsAccount.swiftBankIdentifierCodeBIC);
  }

  @Override
  public int hashCode() {
    return Objects.hash(parentNationalSortCodeNSCNumber, internationalBankAccountNumberIBAN, swiftBankIdentifierCodeBIC);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SavingsAccount {\n");
    
    sb.append("    parentNationalSortCodeNSCNumber: ").append(toIndentedString(parentNationalSortCodeNSCNumber)).append("\n");
    sb.append("    internationalBankAccountNumberIBAN: ").append(toIndentedString(internationalBankAccountNumberIBAN)).append("\n");
    sb.append("    swiftBankIdentifierCodeBIC: ").append(toIndentedString(swiftBankIdentifierCodeBIC)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


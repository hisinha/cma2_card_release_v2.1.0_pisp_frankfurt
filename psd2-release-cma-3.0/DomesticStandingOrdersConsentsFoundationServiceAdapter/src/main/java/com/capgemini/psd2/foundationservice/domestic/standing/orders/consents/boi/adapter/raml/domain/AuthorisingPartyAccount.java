package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Basic details of the debtor account, to which a debit entry will be made as a result of the transaction.
 */
@ApiModel(description = "Basic details of the debtor account, to which a debit entry will be made as a result of the transaction.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class AuthorisingPartyAccount   {
  @JsonProperty("accountNumber")
  private String accountNumber = null;

  @JsonProperty("accountName")
  private String accountName = null;

  @JsonProperty("schemeName")
  private String schemeName = null;

  @JsonProperty("secondaryIdentification")
  private String secondaryIdentification = null;

  public AuthorisingPartyAccount accountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
    return this;
  }

  /**
   * Debtor BOI account number.
   * @return accountNumber
  **/
  @ApiModelProperty(required = true, value = "Debtor BOI account number.")
  @NotNull

@Pattern(regexp="[A-Za-z0-9/?:().,'+ -]{0,34}") 
  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public AuthorisingPartyAccount accountName(String accountName) {
    this.accountName = accountName;
    return this;
  }

  /**
   * Name of the debtor account.
   * @return accountName
  **/
  @ApiModelProperty(value = "Name of the debtor account.")

@Size(min=0,max=70) 
  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public AuthorisingPartyAccount schemeName(String schemeName) {
    this.schemeName = schemeName;
    return this;
  }

  /**
   * Name of the identification scheme, in a coded form as published in an external list.
   * @return schemeName
  **/
  @ApiModelProperty(required = true, value = "Name of the identification scheme, in a coded form as published in an external list.")
  @NotNull


  public String getSchemeName() {
    return schemeName;
  }

  public void setSchemeName(String schemeName) {
    this.schemeName = schemeName;
  }

  public AuthorisingPartyAccount secondaryIdentification(String secondaryIdentification) {
    this.secondaryIdentification = secondaryIdentification;
    return this;
  }

  /**
   * This is secondary identification of the account, as assigned by the account servicing institution. This can be used by building societies to additionally identify accounts with a roll number (in addition to a sort code and account number combination).
   * @return secondaryIdentification
  **/
  @ApiModelProperty(value = "This is secondary identification of the account, as assigned by the account servicing institution. This can be used by building societies to additionally identify accounts with a roll number (in addition to a sort code and account number combination).")


  public String getSecondaryIdentification() {
    return secondaryIdentification;
  }

  public void setSecondaryIdentification(String secondaryIdentification) {
    this.secondaryIdentification = secondaryIdentification;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AuthorisingPartyAccount authorisingPartyAccount = (AuthorisingPartyAccount) o;
    return Objects.equals(this.accountNumber, authorisingPartyAccount.accountNumber) &&
        Objects.equals(this.accountName, authorisingPartyAccount.accountName) &&
        Objects.equals(this.schemeName, authorisingPartyAccount.schemeName) &&
        Objects.equals(this.secondaryIdentification, authorisingPartyAccount.secondaryIdentification);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountNumber, accountName, schemeName, secondaryIdentification);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AuthorisingPartyAccount {\n");
    
    sb.append("    accountNumber: ").append(toIndentedString(accountNumber)).append("\n");
    sb.append("    accountName: ").append(toIndentedString(accountName)).append("\n");
    sb.append("    schemeName: ").append(toIndentedString(schemeName)).append("\n");
    sb.append("    secondaryIdentification: ").append(toIndentedString(secondaryIdentification)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


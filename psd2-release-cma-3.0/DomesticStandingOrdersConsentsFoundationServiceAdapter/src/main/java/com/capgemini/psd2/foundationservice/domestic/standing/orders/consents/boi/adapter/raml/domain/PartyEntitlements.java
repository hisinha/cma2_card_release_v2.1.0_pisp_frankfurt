package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Party Entitlements object
 */
@ApiModel(description = "Party Entitlements object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PartyEntitlements   {
  @JsonProperty("entitlements")
  @Valid
  private List<String> entitlements = new ArrayList<String>();

  public PartyEntitlements entitlements(List<String> entitlements) {
    this.entitlements = entitlements;
    return this;
  }

  public PartyEntitlements addEntitlementsItem(String entitlementsItem) {
    this.entitlements.add(entitlementsItem);
    return this;
  }

  /**
   * Get entitlements
   * @return entitlements
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public List<String> getEntitlements() {
    return entitlements;
  }

  public void setEntitlements(List<String> entitlements) {
    this.entitlements = entitlements;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PartyEntitlements partyEntitlements = (PartyEntitlements) o;
    return Objects.equals(this.entitlements, partyEntitlements.entitlements);
  }

  @Override
  public int hashCode() {
    return Objects.hash(entitlements);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PartyEntitlements {\n");
    
    sb.append("    entitlements: ").append(toIndentedString(entitlements)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


/*
 * Authentication Service API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Customer Authentication Session object
 */
@ApiModel(description = "Customer Authentication Session object")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-10-24T18:15:40.212+05:30")
public class CustomerAuthenticationSessionBaseType {
  @SerializedName("authenticationMethodCode")
  private AuthenticationMethodCodeEnum authenticationMethodCode = null;

  @SerializedName("secureAccessKeyUsed")
  private String secureAccessKeyUsed = null;

  @SerializedName("authenticationParameterText")
  private List<String> authenticationParameterText = null;

  public CustomerAuthenticationSessionBaseType authenticationMethodCode(AuthenticationMethodCodeEnum authenticationMethodCode) {
    this.authenticationMethodCode = authenticationMethodCode;
    return this;
  }

   /**
   * Get authenticationMethodCode
   * @return authenticationMethodCode
  **/
  @ApiModelProperty(required = true, value = "")
  public AuthenticationMethodCodeEnum getAuthenticationMethodCode() {
    return authenticationMethodCode;
  }

  public void setAuthenticationMethodCode(AuthenticationMethodCodeEnum authenticationMethodCode) {
    this.authenticationMethodCode = authenticationMethodCode;
  }

  public CustomerAuthenticationSessionBaseType secureAccessKeyUsed(String secureAccessKeyUsed) {
    this.secureAccessKeyUsed = secureAccessKeyUsed;
    return this;
  }

   /**
   * 
   * @return secureAccessKeyUsed
  **/
  @ApiModelProperty(required = true, value = "")
  public String getSecureAccessKeyUsed() {
    return secureAccessKeyUsed;
  }

  public void setSecureAccessKeyUsed(String secureAccessKeyUsed) {
    this.secureAccessKeyUsed = secureAccessKeyUsed;
  }

  public CustomerAuthenticationSessionBaseType authenticationParameterText(List<String> authenticationParameterText) {
    this.authenticationParameterText = authenticationParameterText;
    return this;
  }

  public CustomerAuthenticationSessionBaseType addAuthenticationParameterTextItem(String authenticationParameterTextItem) {
    if (this.authenticationParameterText == null) {
      this.authenticationParameterText = new ArrayList<String>();
    }
    this.authenticationParameterText.add(authenticationParameterTextItem);
    return this;
  }

   /**
   * 
   * @return authenticationParameterText
  **/
  @ApiModelProperty(value = "")
  public List<String> getAuthenticationParameterText() {
    return authenticationParameterText;
  }

  public void setAuthenticationParameterText(List<String> authenticationParameterText) {
    this.authenticationParameterText = authenticationParameterText;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CustomerAuthenticationSessionBaseType customerAuthenticationSessionBaseType = (CustomerAuthenticationSessionBaseType) o;
    return Objects.equals(this.authenticationMethodCode, customerAuthenticationSessionBaseType.authenticationMethodCode) &&
        Objects.equals(this.secureAccessKeyUsed, customerAuthenticationSessionBaseType.secureAccessKeyUsed) &&
        Objects.equals(this.authenticationParameterText, customerAuthenticationSessionBaseType.authenticationParameterText);
  }

  @Override
  public int hashCode() {
    return Objects.hash(authenticationMethodCode, secureAccessKeyUsed, authenticationParameterText);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CustomerAuthenticationSessionBaseType {\n");
    
    sb.append("    authenticationMethodCode: ").append(toIndentedString(authenticationMethodCode)).append("\n");
    sb.append("    secureAccessKeyUsed: ").append(toIndentedString(secureAccessKeyUsed)).append("\n");
    sb.append("    authenticationParameterText: ").append(toIndentedString(authenticationParameterText)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}


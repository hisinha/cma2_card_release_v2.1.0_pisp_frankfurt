
package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.security.AdapterAuthenticationException;
import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.client.AuthenticationApplicationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.delegate.AuthenticationApplicationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.domain.AuthenticationMethodCodeEnum;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.domain.AuthenticationRequest;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.domain.AuthenticationSystemCodeEnum;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.domain.CustomerAuthenticationSessionBaseType;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.domain.DigitalUserRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AuthenticationApplicationFoundationServiceAdapter implements AuthenticationAdapter {

	@Autowired
	private AdapterUtility adapterUtility;
	

	@Autowired
	private AuthenticationApplicationFoundationServiceDelegate authenticationApplicationFoundationServiceDelegate;

	@Autowired
	private AuthenticationApplicationFoundationServiceClient authenticationApplicationFoundationServiceClient;

	
	@Override
	public <T> T authenticate(T authentication, Map<String, String> params) {

		RequestInfo requestInfo = new RequestInfo();
		
		Authentication authenticationObject = (Authentication)authentication;

		String channelId = params.get(AdapterSecurityConstants.CHANNELID_HEADER);
		if (AuthenticationSystemCodeEnum.BOL.getValue().equalsIgnoreCase(channelId)){
			DigitalUserRequest request = this.createBOLRequest(authenticationObject, params);
			HttpHeaders httpHeaders = authenticationApplicationFoundationServiceDelegate.createRequestHeadersBOL(requestInfo, params);
			String finalURL = adapterUtility.retriveFoundationServiceURL(channelId);
			if(NullCheckUtils.isNullOrEmpty(finalURL)){
				throw AdapterAuthenticationException.populateAuthenticationFailedException("No valid foundation url found.",SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			requestInfo.setUrl(finalURL);
			String str = authenticationApplicationFoundationServiceClient.restTransportForAuthenticationApplication(requestInfo, request, String.class, httpHeaders);
			System.out.println("Auth response::: "+str);
		}
		else if(AuthenticationSystemCodeEnum.B365.getValue().equalsIgnoreCase(channelId)){
			AuthenticationRequest authenticationRequest = new AuthenticationRequest();

			HttpHeaders httpHeaders = authenticationApplicationFoundationServiceDelegate.createRequestHeadersB365(requestInfo, params);
			authenticationRequest.setUserName(authenticationObject.getPrincipal().toString());
			authenticationRequest.setPassword(authenticationObject.getCredentials().toString());
			
			String finalURL = adapterUtility.retriveFoundationServiceURL(channelId);
			if(NullCheckUtils.isNullOrEmpty(finalURL)){
				throw AdapterAuthenticationException.populateAuthenticationFailedException("No valid foundation url found.",SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			requestInfo.setUrl(finalURL);
			authenticationApplicationFoundationServiceClient.restTransportForAuthenticationApplication(requestInfo, authenticationRequest, String.class, httpHeaders);		  
		}
		return authentication;
	}
	
	private DigitalUserRequest createBOLRequest(Authentication authenticationObject, Map<String, String> params)
	{
		DigitalUser digitalUser = new DigitalUser();
		DigitalUserRequest request = new DigitalUserRequest();
		request.setDigitalUser(digitalUser);
		digitalUser.setDigitalUserIdentifier(String.valueOf(authenticationObject.getPrincipal()));
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCodeEnum.BOL);
		
		List<CustomerAuthenticationSessionBaseType> customerAuthenticationSession = new ArrayList<>();
		CustomerAuthenticationSessionBaseType customerAuthSession = new CustomerAuthenticationSessionBaseType();
		customerAuthSession.setAuthenticationMethodCode(AuthenticationMethodCodeEnum.ONE_TIME_PASSWORD);
		customerAuthSession.setSecureAccessKeyUsed(String.valueOf(authenticationObject.getCredentials()));
		
		List<String> authenticationParameterText = new ArrayList<>();
		authenticationParameterText.add("");
		authenticationParameterText.add("");
		customerAuthSession.setAuthenticationParameterText(authenticationParameterText);
		customerAuthenticationSession.add(customerAuthSession);
		digitalUser.setCustomerAuthenticationSession(customerAuthenticationSession);
		return request;
	}
	
}

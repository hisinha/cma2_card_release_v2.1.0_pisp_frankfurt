package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets authenticationMethodCode
 */
public enum AuthenticationMethodCode {
  
  ONE_TIME_PASSWORD("One Time Password"),
  
  PASSWORD("Password");

  private String value;

  AuthenticationMethodCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static AuthenticationMethodCode fromValue(String text) {
    for (AuthenticationMethodCode b : AuthenticationMethodCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}


package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Additional information about the response
 */
@ApiModel(description = "Additional information about the response")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class AdditionalInformation   {
  @JsonProperty("type")
  private Type1 type = null;

  @JsonProperty("details")
  private Object details = null;

  public AdditionalInformation type(Type1 type) {
    this.type = type;
    return this;
  }

  /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Type1 getType() {
    return type;
  }

  public void setType(Type1 type) {
    this.type = type;
  }

  public AdditionalInformation details(Object details) {
    this.details = details;
    return this;
  }

  /**
   * The additional information details as per the type defined
   * @return details
  **/
  @ApiModelProperty(required = true, value = "The additional information details as per the type defined")
  @NotNull


  public Object getDetails() {
    return details;
  }

  public void setDetails(Object details) {
    this.details = details;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AdditionalInformation additionalInformation = (AdditionalInformation) o;
    return Objects.equals(this.type, additionalInformation.type) &&
        Objects.equals(this.details, additionalInformation.details);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, details);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AdditionalInformation {\n");
    
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    details: ").append(toIndentedString(details)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


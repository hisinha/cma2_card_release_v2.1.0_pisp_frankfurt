package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;
import java.io.IOException;
import java.util.Objects;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Additional information about the response
 */
@ApiModel(description = "Additional information about the response")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-01-16T12:10:46.138+05:30")
public class CommonResponseAdditionalInformation {
  /**
   * The type of additional information. Text - Plain text containing additional information response, Error - Error message containing additional information, Exception - Exception stack trace containing additional information
   */
  @JsonAdapter(TypeEnum.Adapter.class)
  public enum TypeEnum {
    TEXT("Text"),
    
    ERROR("Error"),
    
    EXCEPTION("Exception");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<TypeEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final TypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public TypeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return TypeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("type")
  private TypeEnum type = null;

  public CommonResponseAdditionalInformation type(TypeEnum type) {
    this.type = type;
    return this;
  }

   /**
   * The type of additional information. Text - Plain text containing additional information response, Error - Error message containing additional information, Exception - Exception stack trace containing additional information
   * @return type
  **/
  @ApiModelProperty(required = true, value = "The type of additional information. Text - Plain text containing additional information response, Error - Error message containing additional information, Exception - Exception stack trace containing additional information")
  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommonResponseAdditionalInformation commonResponseAdditionalInformation = (CommonResponseAdditionalInformation) o;
    return Objects.equals(this.type, commonResponseAdditionalInformation.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommonResponseAdditionalInformation {\n");
    
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}


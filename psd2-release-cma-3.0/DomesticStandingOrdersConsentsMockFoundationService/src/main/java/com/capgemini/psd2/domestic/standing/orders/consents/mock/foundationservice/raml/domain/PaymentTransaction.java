package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Payment Transaction object
 */
@ApiModel(description = "Payment Transaction object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PaymentTransaction   {
  @JsonProperty("transactionCurrency")
  private Currency transactionCurrency = null;

  @JsonProperty("financialEventAmount")
  private FinancialEventAmount financialEventAmount = null;

  public PaymentTransaction transactionCurrency(Currency transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
    return this;
  }

  /**
   * Get transactionCurrency
   * @return transactionCurrency
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Currency getTransactionCurrency() {
    return transactionCurrency;
  }

  public void setTransactionCurrency(Currency transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
  }

  public PaymentTransaction financialEventAmount(FinancialEventAmount financialEventAmount) {
    this.financialEventAmount = financialEventAmount;
    return this;
  }

  /**
   * Get financialEventAmount
   * @return financialEventAmount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public FinancialEventAmount getFinancialEventAmount() {
    return financialEventAmount;
  }

  public void setFinancialEventAmount(FinancialEventAmount financialEventAmount) {
    this.financialEventAmount = financialEventAmount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentTransaction paymentTransaction = (PaymentTransaction) o;
    return Objects.equals(this.transactionCurrency, paymentTransaction.transactionCurrency) &&
        Objects.equals(this.financialEventAmount, paymentTransaction.financialEventAmount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transactionCurrency, financialEventAmount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentTransaction {\n");
    
    sb.append("    transactionCurrency: ").append(toIndentedString(transactionCurrency)).append("\n");
    sb.append("    financialEventAmount: ").append(toIndentedString(financialEventAmount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


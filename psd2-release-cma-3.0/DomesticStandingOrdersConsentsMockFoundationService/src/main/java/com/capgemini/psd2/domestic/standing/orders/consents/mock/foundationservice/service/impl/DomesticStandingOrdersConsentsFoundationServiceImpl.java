package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.constants.DomesticStandingOrdersConsentsFoundationConstants;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.AuthorisationType;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.ProposalStatus;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.repository.DomesticStandingOrdersConsentsFoundationRepository;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.service.DomesticStandingOrdersConsentsService;


@Service
public class DomesticStandingOrdersConsentsFoundationServiceImpl implements DomesticStandingOrdersConsentsService {

	@Autowired
	private DomesticStandingOrdersConsentsFoundationRepository repository;
	
	@Override
	public StandingOrderInstructionProposal retrieveAccountInformation(String paymentInstructionProposalId)
			throws Exception {

		StandingOrderInstructionProposal standingOrderInstructionProposal = repository
				.findByPaymentInstructionProposalId(paymentInstructionProposalId);

		if (null == standingOrderInstructionProposal) {
			throw new RecordNotFoundException(DomesticStandingOrdersConsentsFoundationConstants.RECORD_NOT_FOUND);
		}

		return standingOrderInstructionProposal;
	}

	@Override
	public StandingOrderInstructionProposal createDomesticStandingOrdersConsentsResource(
			StandingOrderInstructionProposal standingOrderInstructionProposalReq) throws Exception {
		
		String consentID = null;
		if (null == standingOrderInstructionProposalReq) {

			throw new RecordNotFoundException(DomesticStandingOrdersConsentsFoundationConstants.RECORD_NOT_FOUND);
		}
		if (!(standingOrderInstructionProposalReq.getReference())
				.equalsIgnoreCase(DomesticStandingOrdersConsentsFoundationConstants.REFERENCE)) {
			standingOrderInstructionProposalReq.setProposalStatus(ProposalStatus.AwaitingAuthorisation);
		} else {
			standingOrderInstructionProposalReq.setProposalStatus(ProposalStatus.Rejected);
		}
		consentID = UUID.randomUUID().toString();
		standingOrderInstructionProposalReq.setPaymentInstructionProposalId(consentID);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		standingOrderInstructionProposalReq.setProposalCreationDatetime(dateFormat.format(new Date()));
		standingOrderInstructionProposalReq.setProposalStatusUpdateDatetime(dateFormat.format(new Date()));
		repository.save(standingOrderInstructionProposalReq);
		return standingOrderInstructionProposalReq;
	}

}

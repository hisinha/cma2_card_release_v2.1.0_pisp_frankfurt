package com.capgemini.psd2.customer.account.profile.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.DigitalUserProfile;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.repository.CustomerAccountInformationRepository;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.service.CustomerAccountInformationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;

@Service
public class CustomerAccountInformationServiceImpl implements CustomerAccountInformationService {
	@Autowired
	CustomerAccountInformationRepository customerAccountInformationRepository;
	
	@Override
	public DigitalUserProfile retrieveCustomerAccountInformation(String id) throws Exception {
	
		DigitalUserProfile digitalUserProfile = customerAccountInformationRepository.findByDigitalUserProfileID(id);
		
		
		if(digitalUserProfile == null)
		{
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.NO_ACOUNT_FOUND_PPR_DUP);
		}
		return digitalUserProfile;

	}

}

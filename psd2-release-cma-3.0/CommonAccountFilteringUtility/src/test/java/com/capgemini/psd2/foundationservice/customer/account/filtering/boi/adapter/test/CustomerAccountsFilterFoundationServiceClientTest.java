/*package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.client.CustomerAccountsFilterFoundationServiceClient;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlement;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountsFilterFoundationServiceClientTest {

	@InjectMocks
	CustomerAccountsFilterFoundationServiceClient customerAccountsFilterFoundationServiceClient;
	
	@Mock
	RestClientSync restClient;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testRestTransportForCustomerAccountProfile(){
		
		DigitalUserProfile channelProfile=new DigitalUserProfile();
		Account accounts = new Account();
		accounts.setAccountNumber("123");
		accounts.setAccountName("boi");
		AccountEntitlement ace= new AccountEntitlement();
		ace.setAccount(accounts);
		List<AccountEntitlement> ae= new ArrayList<AccountEntitlement>();
		ae.add(ace);
		channelProfile.setAccountEntitlements(ae);
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(channelProfile);
		DigitalUserProfile accnts = customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(new RequestInfo(), DigitalUserProfile.class, new HttpHeaders());
		assertNotNull(accnts);
	}
	@Test
	public void testRestTransportForSingleAccountProfile(){
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.setAccount(accnt);
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(accounts);
		Accnts accnts = customerAccountsFilterFoundationServiceClient.restTransportForSingleAccountProfile(new RequestInfo(), Accounts.class, new HttpHeaders());
		assertNotNull(accnts);
	}
}
*/
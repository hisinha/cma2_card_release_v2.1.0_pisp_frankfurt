package com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.constants.AccountSchedulePaymentsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.delegate.AccountSchedulePaymentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.PartiesOnceOffPaymentScheduleScheduleItemsresponse;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.transformer.AccountSchedulePaymentsFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;

public class AccountSchedulePaymentsFoundationServiceDelegateTest {

	@InjectMocks
	AccountSchedulePaymentsFoundationServiceDelegate delegate;
	
	@Mock
	AccountSchedulePaymentsFoundationServiceTransformer accountSchedulePaymentsFSTransformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}
	
	@Test(expected = AdapterException.class)
	public void testgetFoundationServiceURL() {
		String accountNSC="456";
		String accountNumber="123";
		String UserId="boi";
		String baseURL="capg" ;
		String endUrl="success";
		delegate.getFoundationServiceURL(accountNSC, accountNumber, UserId, baseURL, endUrl);
		assertNotNull(delegate);
	}
	@Test(expected = AdapterException.class)
	public void testgetFoundationServiceURLuser() {
		String accountNSC="456";
		String accountNumber="123";
		String UserId=null;
		String baseURL="capg" ;
		String endUrl="success";
		delegate.getFoundationServiceURL(accountNSC, accountNumber, UserId, baseURL, endUrl);
		assertNotNull(delegate);
	}
	@Test(expected = AdapterException.class)
	public void testgetFoundationServiceURLBaseurl() {
		String accountNSC="456";
		String accountNumber="123";
		String UserId="boi";
		String baseURL=null ;
		String endUrl="success";
		delegate.getFoundationServiceURL(accountNSC, accountNumber, UserId, baseURL, endUrl);
		assertNotNull(delegate);
	}
	@Test(expected = AdapterException.class)
	public void testgetFoundationServiceURLendUrl() {
		String accountNSC="456";
		String accountNumber="123";
		String UserId="boi";
		String baseURL="capg" ;
		String endUrl=null;
		delegate.getFoundationServiceURL(accountNSC, accountNumber, UserId, baseURL, endUrl);
		assertNotNull(delegate);
	}
	@Test(expected = AdapterException.class)
	public void testgetFoundationServiceURLaccNo() {
		String accountNSC="456";
		String accountNumber=null;
		String UserId="boi";
		String baseURL="capg" ;
		String endUrl="success";
		delegate.getFoundationServiceURL(accountNSC, accountNumber, UserId, baseURL, endUrl);
		assertNotNull(delegate);
	}
	@Test(expected = AdapterException.class)
	public void testgetFoundationServiceURLaccNsc() {
		String accountNSC=null;
		String accountNumber="123";
		String UserId="boi";
		String baseURL="capg" ;
		String endUrl="success";
		delegate.getFoundationServiceURL(accountNSC, accountNumber, UserId, baseURL, endUrl);
		assertNotNull(delegate);
	}
	@Test
	public void testtransformResponseFromFDToAPI(){
		PartiesOnceOffPaymentScheduleScheduleItemsresponse response=new PartiesOnceOffPaymentScheduleScheduleItemsresponse();
		Map<String, String> params=new HashMap<>();
		delegate.transformResponseFromFDToAPI(response, params);
		assertNotNull(delegate);
	}
	
	@Test
	public void testcreateRequestHeaders(){
		AccountMapping accountMapping=new AccountMapping();
		accountMapping.setPsuId("1345");
		accountMapping.setTppCID("6789");
		accountMapping.setCorrelationId("456");
		ReflectionTestUtils.setField(delegate,"transactionReqHeader","transaction");
		ReflectionTestUtils.setField(delegate,"sourcesystem","PSD2API");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","correlation");
		ReflectionTestUtils.setField(delegate,"channelCodeReqHeader","channelId");
		Map<String, String> params=new HashMap<>();
		params.put(AccountSchedulePaymentsFoundationServiceConstants.CHANNEL_ID, "channelId");
		params.put(AccountSchedulePaymentsFoundationServiceConstants.ACCOUNT_ID, "accountId");
		params.put(AccountSchedulePaymentsFoundationServiceConstants.ACCOUNT_SUBTYPE, "account_subtype");
		params.put(AccountSchedulePaymentsFoundationServiceConstants.CURRENT_ACCOUNT, "CurrentAccount");
		params.put(AccountSchedulePaymentsFoundationServiceConstants.CREDIT_CARD, "CreditCard");
		params.put(AccountSchedulePaymentsFoundationServiceConstants.CHANNEL_ID, "channelId");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders=delegate.createRequestHeaders(new RequestInfo(), accountMapping, params);
		assertNotNull(httpHeaders);
	}
	
}

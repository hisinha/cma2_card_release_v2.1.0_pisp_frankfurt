package com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.delegate;

import java.text.ParseException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.transformer.DomesticScheduledPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;
@Component
public class DomesticScheduledPaymentConsentsFoundationServiceDelegate {

	@Autowired
	private DomesticScheduledPaymentConsentsFoundationServiceTransformer domesticPaymentConsentsFoundationServiceTransformer;
	
	@Value("${foundationService.transactionReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactionReqHeader;
	
	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationMuleReqHeader;
	
	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;
	
	@Value("${foundationService.sourcesystem:#{X-API-SOURCE-SYSTEM}}")
	private String sourcesystem;
	
	@Value("${foundationService.apiChannelCode:#{X-API-CHANNEL-CODE}}")
	private String apiChannelCode;
	
	@Value("${foundationService.domesticScheduledPaymentConsentBaseURL}")
	private String domesticScheduledPaymentConsentBaseURL;
	
	@Value("${foundationService.apiChannelCode:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partySourceId;
	
	@Value("${foundationService.domesticPaymentConsentSetupVersion}")
	private String domesticScheduledPaymentConsentSetupVersion;
	
	public HttpHeaders createPaymentRequestHeaders(RequestInfo requestInfo ,Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(transactionReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		httpHeaders.add(correlationMuleReqHeader,"");
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(sourcesystem,"PSD2API");
		httpHeaders.add(apiChannelCode, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		return httpHeaders;
	}
	
	public String getPaymentFoundationServiceURL(String version,String PaymentInstuctionProposalId){
		return domesticScheduledPaymentConsentBaseURL + "/" + "v" +domesticScheduledPaymentConsentSetupVersion + "/domestic/scheduled/payment-instruction-proposals" + "/" + PaymentInstuctionProposalId;
	}

	public HttpHeaders createPaymentRequestHeadersPost(RequestInfo requestInfo, Map<String, String> params) {
		
		HttpHeaders httpHeaders = new HttpHeaders();
		
		httpHeaders.add(apiChannelCode, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(transactionReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		httpHeaders.add(correlationMuleReqHeader,"");
		httpHeaders.add("partySourceId","" );
		httpHeaders.add(sourcesystem, "PSD2API");
		
		return httpHeaders;
	}

	public String postPaymentFoundationServiceURL(String paymentSetupVersion) {
		
		return domesticScheduledPaymentConsentBaseURL + "/" + "v"+domesticScheduledPaymentConsentSetupVersion   + "/domestic/scheduled/payment-instruction-proposals";
	}

	public ScheduledPaymentInstructionProposal transformDomesticConsentResponseFromAPIToFDForInsert(
			CustomDSPConsentsPOSTRequest paymentConsentsRequest, Map<String, String> params) {
		
		return domesticPaymentConsentsFoundationServiceTransformer.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest, params);
	}

/*	public GenericPaymentConsentResponse transformDomesticConsentRequestFromAPIToFDForInsert(
			CustomDSPConsentsPOSTRequest domesticScheduledPaymentRequest, Map<String, String> params) {
		
		return domesticPaymentConsentsFoundationServiceTransformer.transformDomesticConsentResponseFromFDToAPIForInsert(domesticScheduledPaymentRequest,params);
	}*/

}

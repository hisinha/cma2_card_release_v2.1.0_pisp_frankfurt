package com.capgemini.psd2.authentication.application.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.AuthenticationRequest;

public interface AuthenticationApplicationRepository extends MongoRepository<AuthenticationRequest , String> {

	public AuthenticationRequest findByUserNameAndPassword(String userName, String password) ;
	

}


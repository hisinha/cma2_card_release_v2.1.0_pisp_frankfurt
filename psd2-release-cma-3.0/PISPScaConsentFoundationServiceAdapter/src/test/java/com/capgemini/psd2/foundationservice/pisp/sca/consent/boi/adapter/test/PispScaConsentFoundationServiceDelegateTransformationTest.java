/*package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.delegate.PispScaConsentFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.transformer.PispScaConsentFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class PispScaConsentFoundationServiceDelegateTransformationTest {

	@InjectMocks
	private PispScaConsentFoundationServiceDelegate delegate;

	@Mock
	private PispScaConsentFoundationServiceTransformer pispScaConsentFoundationServiceTransformer;

	*//** The rest client. *//*
	@Mock
	private RestClientSyncImpl restClient;

	*//** The psd 2 validator. *//*
	@Mock
	private PSD2Validator psd2Validator;

	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	*//**
	 * Sets the up.
	 *//*
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	*//**
	 * Context loads.
	 *//*
	@Test
	public void contextLoads() {
	}

	@Test
	public void testPispScaConsentFoundationServiceDelegate() {

		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourceSystemReqHeader", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partysourceReqHeader", "partysource");
		ReflectionTestUtils.setField(delegate, "domesticPaymentConsentBaseURL", "jf");

		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params = new HashMap<>();

		params.put("X-API-SOURCE-SYSTEM", "fgrd");
		params.put("X-API-SOURCE-USER", "USER_ID");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", "PARTY_SOURCE_ID");
		params.put("X-API-TRANSACTION-ID", "TRANSACTION_ID");
		params.put("BOL", "BOL");

		delegate.createPispScaConsentRequestHeaders(requestInfo, params);
	}

	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrl() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.getPispScaConsentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrl1() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.getPispScaConsentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrl2() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = null;
		delegate.getPispScaConsentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}
	
	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForValidateNullcheck() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = null;
		delegate.validatePispScaConsentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}
	
	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrlForValidate() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.validatePispScaConsentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrlForPut() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.putPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	
	

}
*/
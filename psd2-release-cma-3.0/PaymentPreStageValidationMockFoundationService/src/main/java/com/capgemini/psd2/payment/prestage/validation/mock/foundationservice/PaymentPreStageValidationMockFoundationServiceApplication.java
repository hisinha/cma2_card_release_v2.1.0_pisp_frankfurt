package com.capgemini.psd2.payment.prestage.validation.mock.foundationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan(basePackages = { "com.capgemini.psd2" })
public class PaymentPreStageValidationMockFoundationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentPreStageValidationMockFoundationServiceApplication.class, args);
	}

}

/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.client.AccountInformationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.constants.AccountInformationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.delegate.AccountInformationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.CreditCardAccnt;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.AccountBaseType;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Card;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.CreditCardAccountInformationBaseType;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.transformer.AccountInformationFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

/**
 * The Class AccountInformationFoundationServiceDelegateTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationFoundationServiceDelegateTransformationTest {

	/** The delegate. */
	@InjectMocks
	private AccountInformationFoundationServiceDelegate delegate;

	/** The account information foundation service client. */
	@InjectMocks
	private AccountInformationFoundationServiceClient accountInformationFoundationServiceClient;

	/** The account information FS transformer. */
	@Mock
	private AccountInformationFoundationServiceTransformer accountInformationFSTransformer;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	/** The adapterUtility. */
	@Mock
	private AdapterUtility adapterUtility;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test rest transport for single account information.
	 */
	@Test
	public void testRestTransportForSingleAccountInformation() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		Accounts accounts = new Accounts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.setAccount(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");

		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-WORKSTATION", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
	}

	@Test
	public void testRestTransportForSingleAccountInformation2() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		Accounts accounts = new Accounts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.setAccount(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");

		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-WORKSTATION", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");

	}

	

	/**
	 * Test get foundation service URL.
	 */
	@Test
	public void testGetFoundationServiceURL() {

		String accountNSC = "number";
		String accountNumber = "accountNumber";
		String channel = "BOL";
		String version = "v1.0";
		String baseURL = "http://localhost:8082/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business";
		String finalURL = "http://localhost:8082/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/v1.0/accounts/number/accountNumber";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String testURL = delegate.getFoundationServiceURL(version,channel, accountNSC, accountNumber);
		assertEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	/**
	 * Test get foundation service with invalid URL.
	 */
	@Test
	public void testGetFoundationServiceWithInvalidURL() {

		String accountNSC = "number";
		String accountNumber = "accountNumber";
		String channel = "BOL";
		String version = "v1.0";
		String baseURL = "http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry";
		String finalURL = "http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business/number/accountNumber/account";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String testURL = delegate.getFoundationServiceURL(version,channel, accountNSC, accountNumber);

		assertNotEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	/**
	 * Test get foundation service with account NSC as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNSCAsNull() {

		String channel = "BOL";
		String version = "v1.0";
		String accountNSC = null;
		String accountNumber = "number";
		String baseURL = "http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business";

		//delegate.getFoundationServiceURL(baseURL, accountNSC, accountNumber);
		delegate.getFoundationServiceURL(version, channel, accountNSC, accountNumber);

		fail("Invalid account NSC");
	}
	@Test
	public void testGetFoundationServiceWithAccountIdAsNull() {

		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		Accounts accounts = new Accounts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.setAccount(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");

		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-WORKSTATION", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
	}

	/**
	 * Test get foundation service with account number as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNumberAsNull() {
		
		String channel = "BOL";
		String version = "v1.0";
		String accountNumber = null;
		String accountNSC = "nsc";
		String baseURL = "http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business";

		delegate.getFoundationServiceURL(version, channel, accountNSC, accountNumber);

		fail("Invalid Account Number");
	}

	/**
	 * Test get foundation service with base URL as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithBaseURLAsNull() {

		String version = "v1.0";
		String accountNSC = "nsc";
		String accountNumber = "number";
		String baseURL = "test";

		Mockito.when(adapterUtility.retriveFoundationServiceURL(anyObject())).thenReturn(null);
		delegate.getFoundationServiceURL(version,baseURL, accountNSC, accountNumber);

		fail("Invalid base URL");
	}
	/**
	 * Test get foundation service with base URL as null.
	 */

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithChannelIdAsNull() {
		
		String version = "v1.0";
		String accountNumber = null;
		String accountNSC = "nsc";
		String baseURL = "http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business";
		String channelId =null;
		delegate.getFoundationServiceURL(version,channelId, accountNSC, accountNumber);

		fail("Invalid Account Number");
	}
	/**
	 * Test create request headers actual.
	 */
	@Test
	public void testCreateRequestHeadersActual() {

		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "userInReqHeadeRaml", "x-api-source-user");
		ReflectionTestUtils.setField(delegate, "channelInReqHeaderRaml", "x-api-channel-Code");
		ReflectionTestUtils.setField(delegate, "platformInReqHeaderRaml", "x-api-source-system");
		ReflectionTestUtils.setField(delegate, "transactionReqHeaderRaml", "x-api-transaction-id");
		ReflectionTestUtils.setField(delegate, "correlationReqHeaderRaml", "x-api-correlation-id");
		Map<String, String> params = new HashMap<>();
		params.put(AccountInformationFoundationServiceConstants.CHANNEL_ID,"channel123");
		httpHeaders = delegate.createRequestHeaders(new RequestInfo(), accountMapping,params);

		assertNotNull(httpHeaders);
	}
	@Test
	public void testCreateCreditCardRequestHeaders() {

		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
	accountMapping.setCorrelationId("correlationId");
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "userInReqHeadeRaml", "x-api-source-user");
		ReflectionTestUtils.setField(delegate, "channelInReqHeaderRaml", "x-api-channel-Code");
		ReflectionTestUtils.setField(delegate, "platformInReqHeaderRaml", "x-api-source-system");
		ReflectionTestUtils.setField(delegate, "transactionReqHeaderRaml", "x-api-transaction-id");
		ReflectionTestUtils.setField(delegate, "correlationReqHeaderRaml", "x-api-correlation-id");
		ReflectionTestUtils.setField(delegate, "maskedPan", "X-MASKED-PAN");
		Map<String, String> params = new HashMap<>();
		params.put("channelId","channel123");
		httpHeaders = delegate.createCreditCardRequestHeaders(new RequestInfo(), accountMapping,params);

		assertNotNull(httpHeaders);
	}


/**
	 * Test get account from account list.
	 */
	@Test
	public void testGetAccountFromAccountList() {

		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.getAccount().add(acc);
		Accnt res = delegate.getAccountFromAccountList("acct1234", accounts);
		assertEquals("acct1234", res.getAccountNumber());

	}

	@Test
	public void testGetNullAccountFromAccountList() {

		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.getAccount().add(acc);
		Accnt res = delegate.getAccountFromAccountList("acct1", accounts);
		assertEquals(null, res);
	}

	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		
		Account accounts = new Account();
		Accnt acc = new Accnt();
		Card Card = new Card();	
		CreditCardAccountInformationBaseType ccAccntInfo = new CreditCardAccountInformationBaseType();
		CreditCardAccnt creditCardAccnt = new CreditCardAccnt();
		creditCardAccnt.setAccountSubType("CreditCard");
		Card.setMaskedCardPANNumber("XXXXXXXXXXXX4545");
		ccAccntInfo.setCard(Card);
		creditCardAccnt.setCreditCardNumber("XXXXXXXXXXXX4545");

		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		acc.setAccountSubType("CurrentAccount");
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Map<String, String> params=new HashMap<String, String>();
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,"CurrentAccount");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		Mockito.when(delegate.transformResponseFromFDToAPI(accounts, params)).thenReturn(new PlatformAccountInformationResponse());
		PlatformAccountInformationResponse res = delegate.transformResponseFromFDToAPI(accounts, params);
		assertNotNull(res);
	}


	@Test
	public void testGetFoundationServiceURL1() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "channelId");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("123");
		Map<String, String> foundationCustProfileUrl = new HashMap<>();
		foundationCustProfileUrl.put("channelId", "channelId");
		delegate.setFoundationCustProfileUrl(foundationCustProfileUrl);
		delegate.getFoundationCustProfileUrl();
		delegate.getFoundationServiceURL(accountMapping, params);
		String url = delegate.getFoundationServiceURL(accountMapping, params);
		assertEquals("null/123/accounts", url);

	}
	@Test
	public void testGetFoundationServiceRamlURL() {

		Map<String, String> params = new HashMap<>();
		params.put("channelId", "channelId");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("123");
		Map<String, String> foundationCustProfileUrl = new HashMap<>();
		foundationCustProfileUrl.put("channelId", "channelId");
		delegate.setFoundationCustProfileUrl(foundationCustProfileUrl);
		delegate.getFoundationCustProfileUrl();
		String url = delegate.getFoundationServiceRamlURL(accountMapping, params);
		assertEquals("nullCHANNELID/digitalusers/123/digital-user-profile", url);
	}

	@Test
	public void testGetCreditCardFoundationServiceURL() {

		String version = "v1.0";
		String identification = "identification";
		String accountNumber = "accountNumber";
		String channel = "B365";
		String baseURL = "https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.8/core-banking/abt/p/v1.0/accounts";
		String finalUrl=baseURL+ "/" + identification + "/" + accountNumber;
		Mockito.when(adapterUtility.retriveFoundationServiceRamlURL(any())).thenReturn(baseURL);
		String testUrl=delegate.getCreditCardFoundationServiceURL(version,identification, accountNumber, channel);
//		assertEquals( finalUrl,testUrl);
		assertNotEquals("Test for building URL failed.", finalUrl, testUrl);
		assertNotNull(testUrl);
	}
	@Test(expected = AdapterException.class)
	public void testGetCreditCardFoundationServiceURL1() {

		String version = "v1.0";
		String identification = "identification";
		String accountNumber = "accountNumber";
		String channel = null;
		String baseURL = "https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.8/core-banking/abt/p/v1.0/accounts";
		String finalUrl=baseURL+ "/" + identification + "/" + accountNumber;
		Mockito.when(adapterUtility.retriveFoundationServiceRamlURL(any())).thenReturn(baseURL);
		String testUrl=delegate.getCreditCardFoundationServiceURL(version,identification, accountNumber, channel);
//		assertEquals( finalUrl,testUrl);
		fail("Invalid Channel Id");
	}
	@Test(expected = AdapterException.class)
	public void testGetCreditCardFoundationServiceURL2() {

		String version = "v1.0";
		String identification = "identification";
		String accountNumber = null;
		String channel = "B365";
		String baseURL = "https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.8/core-banking/abt/p/v1.0/accounts";
		String finalUrl=baseURL+ "/" + identification + "/" + accountNumber;
		Mockito.when(adapterUtility.retriveFoundationServiceRamlURL(any())).thenReturn(baseURL);
		String testUrl=delegate.getCreditCardFoundationServiceURL(version,identification, accountNumber, channel);
//		assertEquals( finalUrl,testUrl);
		fail("Invalid Account Number");
	}
	@Test(expected = AdapterException.class)
	public void testGetCreditCardFoundationServiceURL3() {

		String version = "v1.0";
		String identification = null;
		String accountNumber = "accountNumber";
		String channel = "B365";
		String baseURL = "https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.8/core-banking/abt/p/v1.0/accounts";
		String finalUrl=baseURL+ "/" + identification + "/" + accountNumber;
		Mockito.when(adapterUtility.retriveFoundationServiceRamlURL(any())).thenReturn(baseURL);
		String testUrl=delegate.getCreditCardFoundationServiceURL(version,identification, accountNumber, channel);
//		assertEquals( finalUrl,testUrl);
		fail("Invalid Credit Card Number");
	}
	@Test(expected = AdapterException.class)
	public void testGetCreditCardFoundationServiceURL4() {

		String version = "v1.0";
		String identification = "asd";
		String accountNumber = "accountNumber";
		String channel = "B365";
		String baseURL = null;
		String finalUrl=baseURL+ "/" + identification + "/" + accountNumber;
		Mockito.when(adapterUtility.retriveFoundationServiceRamlURL(any())).thenReturn(baseURL);
		String testUrl=delegate.getCreditCardFoundationServiceURL(version,identification, accountNumber, channel);
//		assertEquals( finalUrl,testUrl);
		fail("Invalid Base Url");
	}
	@Test
	public void testRestTransportForMultipleAccountInformation() {
		DigitalUserProfile channelProfile=new DigitalUserProfile();
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(channelProfile);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.8/core-banking/abt/p/v1.0/accounts");
        HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-WORKSTATION", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");	DigitalUserProfile res = accountInformationFoundationServiceClient.restTransportForMultipleAccountInformation(requestInfo,
				DigitalUserProfile.class, httpHeaders);
		assertNotNull(res);
	}
	/**
	 * Test rest transport for CreditCardInformation.
	 */
	@Test
	public void testRestTransportForCreditCardInformation() {
		
		AccountBaseType accountBaseType=new AccountBaseType();
		
		Account accounts = new Account();

		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
		
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.8/core-banking/abt/p/v1.0/accounts");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-CORRELATION-ID", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		
		Account res = accountInformationFoundationServiceClient.restTransportForCreditCardInformation(requestInfo, Account.class, httpHeaders);
		
		assertNotNull(res);
	}
	


}



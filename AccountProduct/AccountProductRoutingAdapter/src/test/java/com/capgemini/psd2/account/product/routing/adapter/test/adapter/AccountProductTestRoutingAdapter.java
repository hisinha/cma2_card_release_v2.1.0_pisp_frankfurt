package com.capgemini.psd2.account.product.routing.adapter.test.adapter;

import java.util.Map;

import com.capgemini.psd2.account.product.routing.adapter.test.routing.AccountProductRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountProductTestRoutingAdapter implements AccountProductsAdapter
{

	@Override
	public PlatformAccountProductsResponse retrieveAccountProducts(AccountMapping accountMapping, Map<String, String> params) {
		return AccountProductRoutingAdapterTestMockData.getMockProductGETResponse();
	}

}

//package com.capgemini.psd2.foundationservice.account.product.boi.adapter.test;
///*******************************************************************************
// * CAPGEMINI CONFIDENTIAL
// * __________________
// * 
// * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
// *  
// * NOTICE:  All information contained herein is, and remains
// * the property of CAPGEMINI GROUP.
// * The intellectual and technical concepts contained herein
// * are proprietary to CAPGEMINI GROUP and may be covered
// * by patents, patents in process, and are protected by trade secret
// * or copyright law.
// * Dissemination of this information or reproduction of this material
// * is strictly forbidden unless prior written permission is obtained
// * from CAPGEMINI GROUP.
// *******************************************************************************/
//
//import static org.junit.Assert.assertEquals;
//import static org.mockito.Matchers.any;
//import static org.mockito.Matchers.anyObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.HttpHeaders;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import com.capgemini.psd2.adapter.exceptions.AdapterException;
//import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
//import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
//import com.capgemini.psd2.consent.domain.AccountDetails;
//import com.capgemini.psd2.consent.domain.AccountMapping;
//import com.capgemini.psd2.exceptions.ErrorCodeEnum;
//import com.capgemini.psd2.exceptions.ErrorInfo;
//import com.capgemini.psd2.exceptions.PSD2Exception;
//
//import com.capgemini.psd2.foundationservice.account.product.boi.adapter.AccountProductFoundationServiceAdapter;
//import com.capgemini.psd2.foundationservice.account.product.boi.adapter.client.AccountProductFoundationServiceClient;
//import com.capgemini.psd2.foundationservice.account.product.boi.adapter.delegate.AccountProductFoundationServiceDelegate;
//import com.capgemini.psd2.foundationservice.account.product.boi.adapter.raml.domain.Account;
//import com.capgemini.psd2.foundationservice.account.product.boi.adapter.raml.domain.Product;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountCurrency;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccountInformation;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.EntitlementsAccount;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyInformation;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType;
//import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//public class AccountProductFoundationServiceAdapterTest {
//
//	/** The account information foundation service adapter. */
//	@InjectMocks
//	private AccountProductFoundationServiceAdapter accountProductFoundationServiceAdapter;
//
//	/** The account information foundation service delegate. */
//	@Mock
//	private AccountProductFoundationServiceDelegate accountProductFoundationServiceDelegate;
//
//	/** The account information foundation service client. */
//	@Mock
//	private AccountProductFoundationServiceClient accountProductFoundationServiceClient;
//
//	/** The rest client. */
//	@Mock
//	private RestClientSyncImpl restClient;
//	@Mock
//	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
//	
//	@Mock
//	private AdapterFilterUtility adapterFilterUtility;
//	/**
//	 * Sets the up.
//	 */
//	@Before
//	public void setUp() {
//		MockitoAnnotations.initMocks(this);
//	}
//
//	/**
//	 * Context loads.
//	 */
//	@Test
//	public void contextLoads() {
//	}
//
//	/**
//	 * Test account information FS.
//	 */
//	@Test
//	public void testAccountProductFS1() {
//		Account acc = new Account();
//
//		Product product = new Product();
//
//		product.setProductName("test");
//		product.setProductDescription("BCA");
//		product.setProductCode("123");
//		
//
//		acc.setProduct(product);
//
//		AccountMapping accountMapping = new AccountMapping();
//		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//		AccountDetails accDet = new AccountDetails();
//		accDet.setAccountId("12345");
//		accDet.setAccountNSC("nsc1234");
//		accDet.setAccountNumber("acct1234");
//		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
//		accDetList.add(accDet);
//
//		accountMapping.setAccountDetails(accDetList);
//		accountMapping.setTppCID("test");
//		accountMapping.setPsuId("test");
//		accountMapping.setCorrelationId("test");
//
//		Mockito.when(accountProductFoundationServiceClient.restTransportForAccountProduct(any(), any(), any()))
//				.thenReturn(acc);
//		Mockito.when(accountProductFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
//				.thenReturn(new PlatformAccountProductsResponse());
//		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(product);
//		accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, new HashMap<String, String>());
//
//	}
//
//	@Test
//	public void testAccountProductFS2() {
//		Account acc = new Account();
//
//		Product product = new Product();
//		product.setProductCode("123");
//		
//		product.setProductName("test");
//		product.setProductDescription("BCA");
//
//		acc.setProduct(product);
//
//		AccountMapping accountMapping = new AccountMapping();
//		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//		AccountDetails accDet = new AccountDetails();
//		accDet.setAccountId("12345");
//		accDet.setAccountNSC("nsc1234");
//		accDet.setAccountNumber("acct1234");
//		accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
//		accDetList.add(accDet);
//
//		accountMapping.setAccountDetails(accDetList);
//		accountMapping.setTppCID("test");
//		accountMapping.setPsuId("test");
//		accountMapping.setCorrelationId("test");
//		HashMap<String,String> params =  new HashMap<String, String>();
//		
//
//		DigitalUser du = new DigitalUser();
//		du.setDigitalUserLockedOutIndicator(false);
//		DigitalUserProfile dup = new DigitalUserProfile();
//		dup.setDigitalUser(du);
//		PartyInformation pi = new PartyInformation();
//		pi.setPartyActiveIndicator(true);
//		dup.setPartyInformation(pi);
//		List<EntitlementsAccount> accountEntitlements = new ArrayList<EntitlementsAccount>();
//		EntitlementsAccount ea = new EntitlementsAccount();
//		List<String> ent = new ArrayList<>();
//		ent.add("Display Only Account");
//		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account account = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account();
//		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
//		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
//		account.setAccountNumber("24512786");
//		AccountCurrency currency = new AccountCurrency();
//		CurrentAccountInformation cuacinfo = new CurrentAccountInformation();
//		currency.setIsoAlphaCode("GBP");
//		account.setAccountCurrency(currency);
//		account.setRetailNonRetailCode(retail);
//		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
//		cuacinfo.setParentNationalSortCodeNSCNumber("902127");
//		cuacinfo.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
//		cuacinfo.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
//		account.setCurrentAccountInformation(cuacinfo);
//		ea.setAccount(account);
//		ea.setEntitlements(ent);
//		accountEntitlements.add(ea);
//		dup.setAccountEntitlements(accountEntitlements);
//
//		
//		Mockito.when(accountProductFoundationServiceClient.restTransportForAccountProduct(any(), any(), any()))
//				.thenReturn(acc);
//		Mockito.when(accountProductFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
//				.thenReturn(new PlatformAccountProductsResponse());
//		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(dup);
//
//		Mockito.when(adapterFilterUtility.prevalidateAccounts(dup, accountMapping)).thenReturn(accountMapping);
//		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(product);
//		accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, params);
//
//	}
//
//    @Test
//	public void testAccountProductFS3() {
//		Account acc = new Account();
//
//		Product product = new Product();
//
//		product.setProductName("test");
//		product.setProductDescription("BCA");
//
//		acc.setProduct(product);
//
//		AccountMapping accountMapping = new AccountMapping();
//		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//		AccountDetails accDet = new AccountDetails();
//		accDet.setAccountId("12345");
//		accDet.setAccountNSC("nsc1234");
//		accDet.setAccountNumber("acct1234");
//		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
//		accDetList.add(accDet);
//		HashMap<String,String> params =  new HashMap<String, String>();
//		accountMapping.setAccountDetails(accDetList);
//		accountMapping.setTppCID("test");
//		accountMapping.setPsuId("test");
//		accountMapping.setCorrelationId("test");
//		DigitalUser du = new DigitalUser();
//		du.setDigitalUserLockedOutIndicator(false);
//		DigitalUserProfile dup = new DigitalUserProfile();
//		dup.setDigitalUser(du);
//		PartyInformation pi = new PartyInformation();
//		pi.setPartyActiveIndicator(true);
//		dup.setPartyInformation(pi);
//		List<EntitlementsAccount> accountEntitlements = new ArrayList<EntitlementsAccount>();
//		EntitlementsAccount ea = new EntitlementsAccount();
//		List<String> ent = new ArrayList<>();
//		ent.add("Display Only Account");
//		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account account = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account();
//		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
//		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
//		account.setAccountNumber("24512786");
//		AccountCurrency currency = new AccountCurrency();
//		CurrentAccountInformation cuacinfo = new CurrentAccountInformation();
//		currency.setIsoAlphaCode("GBP");
//		account.setAccountCurrency(currency);
//		account.setRetailNonRetailCode(retail);
//		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
//		cuacinfo.setParentNationalSortCodeNSCNumber("902127");
//		cuacinfo.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
//		cuacinfo.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
//		account.setCurrentAccountInformation(cuacinfo);
//		ea.setAccount(account);
//		ea.setEntitlements(ent);
//		accountEntitlements.add(ea);
//		dup.setAccountEntitlements(accountEntitlements);
//
//		Mockito.when(accountProductFoundationServiceClient.restTransportForAccountProduct(any(), any(), any()))
//				.thenReturn(acc);
//		Mockito.when(accountProductFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
//				.thenReturn(new PlatformAccountProductsResponse());
//		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(dup);
//
//		Mockito.when(adapterFilterUtility.prevalidateAccounts(dup, accountMapping)).thenReturn(accountMapping);
//		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(product);
//		accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, params);
//
//	}
//
//	/**
//	 * Test account Product accnt mapping as null.
//	 */
//	@Test(expected = AdapterException.class)
//	public void testAccountProductAccntMappingAsNull() {
//
//		Mockito.when(
//				accountProductFoundationServiceDelegate.createRequestHeaders(anyObject(), anyObject(), anyObject()))
//				.thenReturn(new HttpHeaders());
//		accountProductFoundationServiceAdapter.retrieveAccountProducts(null, new HashMap<String, String>());
//	}
//
//	/**
//	 * Test account Product accnt mapping psu ID as null.
//	 */
//	@Test(expected = AdapterException.class)
//	public void testAccountProductAccntMappingPsuIdAsNull() {
//		AccountMapping accountMapping = new AccountMapping();
//		Mockito.when(
//				accountProductFoundationServiceDelegate.createRequestHeaders(anyObject(), anyObject(), anyObject()))
//				.thenReturn(new HttpHeaders());
//		accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, new HashMap<String, String>());
//	}
//
//	@Test(expected = AdapterException.class)
//	public void testParamsAsNull() {
//		AccountMapping accountMapping = new AccountMapping();
//		accountMapping.setPsuId("test");
//		accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, null);
//	}
//
//	/**
//	 * Test account Product as null.
//	 */
//	@Test
//	public void testAccountProductAsNull() {
//		AccountMapping accountMapping = new AccountMapping();
//		accountMapping.setPsuId("test");
//		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//		AccountDetails accDet = new AccountDetails();
//		accDet.setAccountId("12345");
//		accDet.setAccountNSC("nsc1234");
//		accDet.setAccountNumber("acct1234");
//		accDetList.add(accDet);
//		Product po = null;
//
//		accountMapping.setAccountDetails(accDetList);
//
//		Mockito.when(accountProductFoundationServiceClient.restTransportForAccountProduct(anyObject(), anyObject(),
//				anyObject())).thenReturn(null);
//		PlatformAccountProductsResponse platformAccountProductsResponse = accountProductFoundationServiceAdapter
//				.retrieveAccountProducts(accountMapping, new HashMap<String, String>());
//		assertEquals(platformAccountProductsResponse, po);
//	}
//
//	/**
//	 * Test account accnt details as null.
//	 */
//	@Test(expected = AdapterException.class)
//	public void testAccountDetailsAsNull() {
//		AccountMapping accountMapping = new AccountMapping();
//		accountMapping.setPsuId("test");
//
//		accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, new HashMap<String, String>());
//	}
//
//	@Test(expected = Exception.class)
//	public void testRestTransportForProduct() {
//		PlatformAccountProductsResponse platformAccountProductsResponse = null;
//		AccountMapping accountMapping = new AccountMapping();
//		accountMapping.setPsuId("test");
//		Mockito.when(accountProductFoundationServiceClient.restTransportForAccountProduct(anyObject(), anyObject(),
//				anyObject())).thenThrow(PSD2Exception.populatePSD2Exception("", ErrorCodeEnum.TECHNICAL_ERROR));
//		Mockito.when(accountProductFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
//				.thenReturn(platformAccountProductsResponse);
//		accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, new HashMap<String, String>());
//	}
//
//	@Test(expected = AdapterException.class)
//	public void testRestTransportForAccountProducts() {
//
//		AccountMapping accountMapping = new AccountMapping();
//		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//		AccountDetails accDet = new AccountDetails();
//		accDet.setAccountId("12345");
//		accDet.setAccountNSC("nsc1234");
//		accDet.setAccountNumber("acct1234");
//		accDetList.add(accDet);
//		PlatformAccountProductsResponse productGETResponse = null;
//
//		accountMapping.setAccountDetails(accDetList);
//		accountMapping.setTppCID("test");
//		accountMapping.setPsuId("test");
//		accountMapping.setCorrelationId("test");
//
//		ErrorInfo errorInfo = new ErrorInfo();
//		errorInfo.setErrorCode("571");
//		errorInfo.setErrorMessage("This request cannot be processed. No StandingOrder found");
//		Mockito.when(accountProductFoundationServiceClient.restTransportForAccountProduct(anyObject(), anyObject(),
//				anyObject())).thenThrow((new AdapterException("Adapter Exception", errorInfo)));
//		PlatformAccountProductsResponse getresponse = accountProductFoundationServiceAdapter
//				.retrieveAccountProducts(accountMapping, new HashMap<String, String>());
//		assertEquals(productGETResponse, getresponse);
//
//	}
//}

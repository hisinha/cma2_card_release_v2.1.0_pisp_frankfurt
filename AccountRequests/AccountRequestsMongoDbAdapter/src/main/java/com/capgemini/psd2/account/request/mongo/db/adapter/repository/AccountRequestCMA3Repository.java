package com.capgemini.psd2.account.request.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;

public interface AccountRequestCMA3Repository extends MongoRepository<OBReadConsentResponse1Data, String> {

	public OBReadConsentResponse1Data findByConsentIdAndCmaVersionIn(String accountId,List<String> cmaVersion);
	
}

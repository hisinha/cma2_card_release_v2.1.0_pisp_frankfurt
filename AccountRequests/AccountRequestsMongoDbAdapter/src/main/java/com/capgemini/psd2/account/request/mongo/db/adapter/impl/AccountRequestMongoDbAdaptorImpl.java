package com.capgemini.psd2.account.request.mongo.db.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestCMA3Repository;
import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestRepository;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadDataResponse1;
import com.capgemini.psd2.aisp.transformer.AccountRequestTransformer;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.DateUtilites;

public class AccountRequestMongoDbAdaptorImpl implements AccountRequestAdapter {

	@Autowired
	private AccountRequestCMA3Repository accountRequestRepository;

	@Autowired
	private AccountRequestRepository accountRequestRepositoryV2;

	@Autowired
	private AispConsentAdapterImpl aispConsentAdapter;

	@Autowired
	private AccountRequestTransformer accountRequestTransformer;
	
	@Autowired
	private CompatibleVersionList compatibleVersionList;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Override
	public OBReadConsentResponse1 createAccountRequestPOSTResponse(OBReadConsentResponse1Data data) {

		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data responseData;
		try {
			responseData = accountRequestRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}

		accountRequestPOSTResponse.setData(responseData);
		return accountRequestPOSTResponse;
	}

	@Override
	public OBReadConsentResponse1 getAccountRequestGETResponse(String consentId) {

		OBReadConsentResponse1 accountRequestGETResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = fetchData(consentId);

		String cid = reqHeaderAtrributes.getTppCID();
		if (null != cid && !cid.equals(data.getTppCID())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
		accountRequestGETResponse.setData(data);
		return accountRequestGETResponse;
	}

	@Override
	public OBReadConsentResponse1 updateAccountRequestResponse(String consentId, OBExternalRequestStatus1Code statusEnum) {

		OBReadConsentResponse1 updateAccountRequestResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = fetchData(consentId);
		
		if (data.getCmaVersion() != null)
			data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setStatus(statusEnum);
		try {
			data = accountRequestRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}
		updateAccountRequestResponse.setData(data);
		return updateAccountRequestResponse;
	}

	@Override
	public void removeAccountRequest(String consentId, String cid) {
		OBReadConsentResponse1Data data = fetchData(consentId);
		
		if (null != cid && !cid.equals(data.getTppCID())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
		
		if (data.getCmaVersion() != null)
			data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setStatus(OBExternalRequestStatus1Code.REVOKED);
		try {
			revokeConsentAndToken(consentId);
			accountRequestRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
	}

	private OBReadConsentResponse1Data fetchData(String consentId) {
		OBReadConsentResponse1Data data = null;
		OBReadDataResponse1 dataV2 = null;
		try {
			data = accountRequestRepository.findByConsentIdAndCmaVersionIn(consentId,compatibleVersionList.fetchVersionList());
			if(data!=null)
				System.out.println("FOUND...");
			else
				System.out.println("NOT FOUND...");
			if (data == null){
				dataV2 = accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(consentId,compatibleVersionList.fetchVersionList());
				if (dataV2 != null) {
					data = accountRequestTransformer.transformAccountInformation(dataV2, data, null);
				}
			}
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}

		if (null == data) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.NO_ACCOUNT_REQUEST_DATA_FOUND));
		}
		return data;
	}
	
	private void revokeConsentAndToken(String accountRequestId) {

		AispConsent aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(accountRequestId,
				ConsentStatusEnum.AUTHORISED);
		if (aispConsent == null)
			return;
		aispConsentAdapter.updateConsentStatus(aispConsent.getConsentId(), ConsentStatusEnum.REVOKED);
	}

}
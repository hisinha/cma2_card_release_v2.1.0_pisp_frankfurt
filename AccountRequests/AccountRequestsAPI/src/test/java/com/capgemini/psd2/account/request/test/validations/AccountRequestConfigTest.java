package com.capgemini.psd2.account.request.test.validations;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.capgemini.psd2.account.request.config.AccountRequestConfig;

public class AccountRequestConfigTest {
	
		
	@Test
	public void test() {
		
		AccountRequestConfig bypassTPPRoles = new AccountRequestConfig();
		List<String> internalInvocationRoles = new ArrayList<>();
		internalInvocationRoles.add("AISP");
		bypassTPPRoles.setInternalAPIInvocationRoles(internalInvocationRoles);
		assertTrue("AISP".equals(bypassTPPRoles.getInternalAPIInvocationRoles().get(0)));
		
	}
	

}

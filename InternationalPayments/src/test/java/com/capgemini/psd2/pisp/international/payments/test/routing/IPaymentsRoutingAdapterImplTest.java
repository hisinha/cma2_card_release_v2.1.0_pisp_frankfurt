package com.capgemini.psd2.pisp.international.payments.test.routing;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.InternationalPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.international.payments.routing.IPaymentsRoutingAdapterImpl;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.token.TppInformationTokenData;

@RunWith(SpringJUnit4ClassRunner.class)
public class IPaymentsRoutingAdapterImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private ApplicationContext applicationContext;
	@InjectMocks
	private IPaymentsRoutingAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testrouting() {
		CustomIPaymentsPOSTRequest requestBody = new CustomIPaymentsPOSTRequest();
		Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap = new HashMap<>();
		Map<String, String> params = new HashMap<>();

		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "wedec");

		Token token = new Token();
		token.setSeviceParams(seviceParams);

		reqHeaderAtrributes.setToken(token);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		ReflectionTestUtils.setField(adapter, "paymentSubmissionExecutionAdapter",
				"internationalPaymentsMongoDbAdapter");

		InternationalPaymentsAdapter value = new InternationalPaymentsAdapter() {

			@Override
			public PaymentInternationalSubmitPOST201Response retrieveStagedPaymentsResponse(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public PaymentInternationalSubmitPOST201Response processPayments(CustomIPaymentsPOSTRequest requestBody,
					Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
		};
		when(applicationContext.getBean(anyString())).thenReturn(value);

		adapter.processPayments(requestBody, paymentStatusMap, params);

	}

	@Test
	public void testretrieve() {
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> prams = null;

		InternationalPaymentsAdapter value = new InternationalPaymentsAdapter() {

			@Override
			public PaymentInternationalSubmitPOST201Response retrieveStagedPaymentsResponse(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public PaymentInternationalSubmitPOST201Response processPayments(CustomIPaymentsPOSTRequest requestBody,
					Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
		};
		when(applicationContext.getBean(anyString())).thenReturn(value);
		adapter.retrieveStagedPaymentsResponse(customPaymentStageIdentifiers, prams);
	}

	@After
	public void tearDown() throws Exception {
		reqHeaderAtrributes = null;
		adapter = null;
	}

}

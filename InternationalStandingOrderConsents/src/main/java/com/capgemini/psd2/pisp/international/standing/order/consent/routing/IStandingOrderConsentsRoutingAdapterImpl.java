package com.capgemini.psd2.pisp.international.standing.order.consent.routing;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("iStandingOrderConsentsStagingRoutingAdapter")
public class IStandingOrderConsentsRoutingAdapterImpl
		implements InternationalStandingOrdersPaymentStagingAdapter, ApplicationContextAware {

	private InternationalStandingOrdersPaymentStagingAdapter beanInstance;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.paymentSetupStagingAdapter}")
	private String beanName;

	/** The application context. */
	private ApplicationContext applicationContext;

	public InternationalStandingOrdersPaymentStagingAdapter getRoutingInstance(String adapterName) {
		if (beanInstance == null)
			beanInstance = (InternationalStandingOrdersPaymentStagingAdapter) applicationContext.getBean(adapterName);
		return beanInstance;

	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}

	@Override
	public CustomIStandingOrderConsentsPOSTResponse processStandingOrderConsents(
			CustomIStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return getRoutingInstance(beanName).processStandingOrderConsents(standingOrderConsentsRequest,
				customStageIdentifiers, addHeaderParams(params), successStatus, failureStatus);
	}

	@Override
	public CustomIStandingOrderConsentsPOSTResponse retrieveStagedInternationalStandingOrdersConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		return getRoutingInstance(beanName).retrieveStagedInternationalStandingOrdersConsents(
				customPaymentStageIdentifiers, addHeaderParams(params));
	}

	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		/*
		 * Below code will be used when update service will be called from payment
		 * submission API. For submission flow -> update, PSUId and ChannelName are
		 * mandatory fields for FS
		 */
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParam = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParam.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParam.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParam.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParam;
	}

}

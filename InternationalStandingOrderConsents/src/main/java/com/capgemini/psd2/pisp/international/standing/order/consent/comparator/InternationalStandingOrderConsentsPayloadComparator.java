package com.capgemini.psd2.pisp.international.standing.order.consent.comparator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBInternationalStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class InternationalStandingOrderConsentsPayloadComparator {

	public int compare(CustomIStandingOrderConsentsPOSTResponse response,
			CustomIStandingOrderConsentsPOSTResponse adaptedStandingOrderConsentsResponse) {
		int value = 1;

		if (Double.compare(Double.parseDouble(response.getData().getInitiation().getInstructedAmount().getAmount()),Double
				.parseDouble(
						adaptedStandingOrderConsentsResponse.getData().getInitiation().getInstructedAmount().getAmount())) == 0){
			adaptedStandingOrderConsentsResponse.getData().getInitiation().getInstructedAmount()
					.setAmount(response.getData().getInitiation().getInstructedAmount().getAmount());
		}

		if (response.getData().getInitiation().equals(adaptedStandingOrderConsentsResponse.getData().getInitiation())
				&& response.getRisk().equals(adaptedStandingOrderConsentsResponse.getRisk())) {
			value = 0;
		}

		OBAuthorisation1 responseAuth = response.getData().getAuthorisation();
		OBAuthorisation1 adaptedResponseAuth = adaptedStandingOrderConsentsResponse.getData().getAuthorisation();

		if (!NullCheckUtils.isNullOrEmpty(responseAuth) && !NullCheckUtils.isNullOrEmpty(adaptedResponseAuth)
				&& value != 1) {
			if (responseAuth.equals(adaptedResponseAuth))
				value = 0;
			else
				value = 1;
		}

		return value;
	}

	public int compareStandingOrderDetails(CustomIStandingOrderConsentsPOSTResponse response,
			CustomIStandingOrderConsentsPOSTRequest request,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		String strStandingOrderConsentsRequest = JSONUtilities.getJSONOutPutFromObject(request);
		CustomIStandingOrderConsentsPOSTResponse adaptedStandingOrdersConsentsResponse = JSONUtilities
				.getObjectFromJSONString(PispUtilities.getObjectMapper(), strStandingOrderConsentsRequest,
						CustomIStandingOrderConsentsPOSTResponse.class);

		if (!validateDebtorDetails(response.getData().getInitiation(),
				adaptedStandingOrdersConsentsResponse.getData().getInitiation(), paymentSetupPlatformResource))
			return 1;

		/*
		 * Date: 13-Feb-2018 Changes are made for CR-10
		 */

		checkAndModifyEmptyFields(adaptedStandingOrdersConsentsResponse, response);

		if (!NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation())
				&& (NullCheckUtils.isNullOrEmpty(adaptedStandingOrdersConsentsResponse.getData().getAuthorisation()))) {
			return 1;
		}

		if (NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation())
				&& !NullCheckUtils.isNullOrEmpty(adaptedStandingOrdersConsentsResponse.getData().getAuthorisation())) {
			return 1;
		}

		int returnValue = compare(response, adaptedStandingOrdersConsentsResponse);

		response.getRisk().setDeliveryAddress(request.getRisk().getDeliveryAddress());

		// End of CR-10
		return returnValue;

	}

	private boolean validateDebtorDetails(OBInternationalStandingOrder1 obInternational1,
			OBInternationalStandingOrder1 obInternational12,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {
			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be true, then
			 * name will participate in comparison else product will set foundation response
			 * DebtorAccount.Name as null so that it can be passed in comparison. Both
			 * fields would have the value as 'Null'.
			 */
			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
				obInternational1.getDebtorAccount().setName(null);

			return Objects.equals(obInternational1.getDebtorAccount(), obInternational12.getDebtorAccount());
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& obInternational12.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& obInternational1.getDebtorAccount() != null) {
			obInternational1.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;

	}

	private void checkAndModifyEmptyFields(CustomIStandingOrderConsentsPOSTResponse adaptedPaymentConsentsResponse,
			CustomIStandingOrderConsentsPOSTResponse response) {

		if (adaptedPaymentConsentsResponse.getRisk().getDeliveryAddress() != null) {
			checkAndModifyAddressLine(adaptedPaymentConsentsResponse.getRisk().getDeliveryAddress());
			checkAndModifyCountrySubDivision(adaptedPaymentConsentsResponse.getRisk().getDeliveryAddress());
		}

		if (adaptedPaymentConsentsResponse.getData().getInitiation().getCreditor().getPostalAddress() != null
				&& adaptedPaymentConsentsResponse.getData().getInitiation().getCreditor().getPostalAddress()
						.getAddressLine() != null) {
			checkAndModifyCreditorPostalAddressLine(
					adaptedPaymentConsentsResponse.getData().getInitiation().getCreditor().getPostalAddress(),
					response);
		}

	}

	private void checkAndModifyAddressLine(OBRisk1DeliveryAddress obRisk1DeliveryAddress) {

		// addressLine List to String
		if (obRisk1DeliveryAddress.getAddressLine() != null) {
			List<String> addressLineList = new ArrayList<>();
			for (String addressLine : obRisk1DeliveryAddress.getAddressLine()) {
				if (NullCheckUtils.isNullOrEmpty(addressLine))
					continue;
				addressLineList.add(addressLine);
			}
			if (addressLineList.isEmpty())
				addressLineList = null;

			obRisk1DeliveryAddress.setAddressLine(addressLineList);
		}
	}

	private void checkAndModifyCreditorPostalAddressLine(OBPostalAddress6 obPostalAddress6,
			CustomIStandingOrderConsentsPOSTResponse response) {
		List<String> addressLineList = new ArrayList<>();
		for (String addressLine : (obPostalAddress6.getAddressLine())) {
			if (NullCheckUtils.isNullOrEmpty(addressLine))
				continue;
			addressLineList.add(addressLine);
		}
		if (addressLineList.isEmpty()
				&& response.getData().getInitiation().getCreditor().getPostalAddress().getAddressLine() == null)
			addressLineList = null;

		obPostalAddress6.setAddressLine(addressLineList);
	}

	private void checkAndModifyCountrySubDivision(OBRisk1DeliveryAddress obRisk1DeliveryAddress) {
		// CountrySubDivision List to String
		if (obRisk1DeliveryAddress.getCountrySubDivision() != null) {
			obRisk1DeliveryAddress.setCountrySubDivision(obRisk1DeliveryAddress.getCountrySubDivision());

		}
	}

}

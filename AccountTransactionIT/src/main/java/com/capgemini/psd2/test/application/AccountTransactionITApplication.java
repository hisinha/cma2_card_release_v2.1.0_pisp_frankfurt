package com.capgemini.psd2.test.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })
public class AccountTransactionITApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountTransactionITApplication.class, args);
	}

}

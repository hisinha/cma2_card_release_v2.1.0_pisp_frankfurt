package com.capgemini.psd2.test;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.aisp.domain.Data3Transaction;
import com.capgemini.psd2.aisp.domain.Data3Transaction.CreditDebitIndicatorEnum;
import com.capgemini.psd2.aisp.domain.Data3Transaction.StatusEnum;
import com.capgemini.psd2.aisp.domain.Data5CreditLine.TypeEnum;
import com.capgemini.psd2.config.AccountTransactionITConfig;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AccountTransactionITConfig.class)
@WebIntegrationTest
public class AccountTransactionIT {

	@Value("${app.edgeserverURL}")
	private String edgeserverURL;
	@Value("${app.accountTransactionURL}")
	private String accountTransactionURL;

	@Value("${app.refToken}")
	private String refToken;
	@Value("${app.invalidRefToken}")
	private String invalidRefToken;
	@Value("${app.withoutPermissionToken}")
	private String withoutPermissionToken;
	@Value("${app.expiredToken}")
	private String expiredToken;
	@Value("${app.TokenWithDebitPermission}")
	private String TokenWithDebitPermission;
	@Value("${app.TokenWithCreditPermission}")
	private String TokenWithCreditPermission;
	@Value("${app.TokenWithBasicPermission}")
	private String TokenWithBasicPermission;

	@Value("${app.accountId1}")
	private String accountId1;
	@Value("${app.invalidAccountId}")
	private String invalidAccountId;
	@Value("${app.OutOfRangeAccountId}")
	private String OutOfRangeAccountId;
	@Value("${app.specialCharAccountId}")
	private String specialCharAccountId;
	@Value("${app.noDataAccId}")
	private String noDataAccId;
	@Value("${app.fsDataErrorAccId}")
	private String fsDataErrorAccId;
	@Value("${app.accountIdDR}")
	private String accountIdDR;
	@Value("${app.accountIdCR}")
	private String accountIdCR;
	@Value("${app.basicPermissionAccId}")
	private String basicPermissionAccId;

	@Value("${app.fromDateParamName}")
	private String fromDateParamName;
	@Value("${app.toDateParamName}")
	private String toDateParamName;

	@Autowired
	protected RestClientSyncImpl restClientSync;

	@Autowired
	protected RestTemplate restTemplate;

	@Before
	public void setUp() throws Exception {
	}

	// AccountID is not present in DB, //401 UNAUTHORIZED
	@Test(expected = HttpClientErrorException.class)
	public void NoAccIdInDBTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + invalidAccountId + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
			assertTrue(e.getResponseBodyAsString().contains("No permissions given for requested account"));
			throw e;
		}
	}

	// AccountID is greater than 40chars, //400 Bad Request
	@Test(expected = HttpClientErrorException.class)
	public void GreaterThan40AccIdTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + OutOfRangeAccountId + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
			throw e;
		}
	}

	// AccountID is greater than 40chars, //400 Bad Request
	@Test(expected = HttpClientErrorException.class)
	public void SpecialCharAccIdTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + specialCharAccountId + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
			throw e;
		}
	}

	// POST request, //405 Not Allowed
	@Test(expected = HttpClientErrorException.class)
	public void PostRequestTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.POST, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.METHOD_NOT_ALLOWED, e.getStatusCode());
			throw e;
		}
	}

	// Dates in param are not in ISO dateTime, //400 Bad Request
	@Test(expected = HttpClientErrorException.class)
	public void DatesNonISoTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/?"
				+ fromDateParamName + "=2017-07-29" + "/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
			throw e;
		}
	}

	// Param from date is future date, //400 Bad Request
	@Test(expected = HttpClientErrorException.class)
	public void FutureParamFromDateTest() {
		String futureDate = LocalDateTime.now().plusDays(1).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/?"
				+ fromDateParamName + "=" + futureDate;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
			throw e;
		}
	}

	// param from date is after param to date, //400 Bad Request
	@Test(expected = HttpClientErrorException.class)
	public void ParamFromAfterParamToDateTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/?"
				+ fromDateParamName + "=2017-04-13T00:00:00.000&" + toDateParamName + "=2016-04-12T00:00:00.000";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
			throw e;
		}
	}

	// No transactions found for given nsc and acc num in db. //404 Not Found
	@Test(expected = HttpClientErrorException.class)
	public void NoDataFoundInFSTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + noDataAccId + "/transactions/?"
				+ fromDateParamName + "=2016-04-12T00:00:00.000&" + toDateParamName + "=2017-04-13T00:00:00.000";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
			throw e;
		}
	}

	// Auth code is absent //401 UNAUTHORIZED
	@Test(expected = HttpClientErrorException.class)
	public void NoAuthTokenGivenTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.UNAUTHORIZED,e.getStatusCode());
			throw e;
		}
	}

	// Auth code is not valid //401 UNAUTHORIZED
	@Test(expected = PSD2Exception.class)
	public void InvalidAuthTokenGivenTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", invalidRefToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, AccountTransactionsGETResponse.class, headers);
		} catch (PSD2Exception e) {
			assertNotNull(e.getErrorInfo().getErrorMessage());
			assertEquals(HttpStatus.UNAUTHORIZED.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}

	// Expired Authorization code is sent in the request
	@Test(expected = PSD2Exception.class)
	public void ExpiredAuthorizationTokenTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", expiredToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, AccountTransactionsGETResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.UNAUTHORIZED.toString(), e.getErrorInfo().getStatusCode());
			throw e;
		}
	}

	// InvalidPermission : ReadBalances //403 FORBIDDEN
	@Test(expected = PSD2Exception.class)
	public void InvalidPermissionTokenTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", withoutPermissionToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, AccountTransactionsGETResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.UNAUTHORIZED.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}

	// fapi-financial-id mandatory header Test //400 BAD_REQUEST
	@Test(expected = HttpClientErrorException.class)
	public void FapiMandatoryHeaderMissingTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertTrue(e.getResponseBodyAsString().contains("x-fapi-financial-id is missing in headers."));
			assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
			throw e;
		}
	}

	/**
	 * An optional field Accept sent with the value as application/json. If
	 * optional field Accept sent and its values is other than application/json.
	 */
	@Test(expected = HttpClientErrorException.class)
	public void AcceptFieldInHeaderTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("Accept", "application/json");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, AccountTransactionsGETResponse.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		try {
			headers.remove("Accept");
			headers.add("Accept", "text");
			HttpEntity<?> requestEntity2 = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.POST, requestEntity2, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.NOT_ACCEPTABLE, e.getStatusCode());
			assertTrue(response.getHeaders().get("Content-Type").toString().contains(("application/json")));
			throw e;
		}
	}

	/**
	 * In successful response, x-fapi-interaction-id must come into header.
	 * Value of x-fapi-interaction-id in response is always same if
	 * x-fapi-interaction-id sent in the request. If the value of
	 * x-fapi-interaction-id is absent or blank in the request then its value
	 * must sent by the ASASP.
	 */
	@Test
	public void XfapiInteractionIdTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-fapi-interaction-id", "xyz123");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);

		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, AccountTransactionsGETResponse.class);
		assertTrue(response.getHeaders().get("x-fapi-interaction-id").contains("xyz123"));

		headers.remove("x-fapi-interaction-id");
		HttpEntity<?> requestEntity2 = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response2 = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity2, AccountTransactionsGETResponse.class);
		assertNotNull(response2.getHeaders().get("x-fapi-interaction-id"));
		assertTrue(!(response2.getHeaders().get("x-fapi-interaction-id").contains("xyz123")));
	}

	// Response Header Fields Test
	/**
	 * In successful response, Content-type header should be populate with value
	 * as application/json
	 */
	@Test
	public void ResponseHeaderFieldsTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, AccountTransactionsGETResponse.class);
		assertNotNull(response.getHeaders().get("Content-Type"));
		assertTrue(response.getHeaders().get("Content-Type").toString().contains(("application/json")));
	}

	@Test
	public void ResponseFieldsTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, AccountTransactionsGETResponse.class);
		assertNotNull(response.getBody().getData());
		assertNotNull(response.getBody().getLinks());
		assertNotNull(response.getBody().getMeta());
		assertEquals(accountId1, response.getBody().getData().getTransaction().get(0).getAccountId());
		assertNotNull(response.getBody().getData().getTransaction().get(0).getAmount().getAmount());
		assertNotNull(response.getBody().getData().getTransaction().get(0).getAmount().getCurrency());
		assertTrue(Pattern.matches("[0-9.]{1,19}", response.getBody().getData().getTransaction().get(0).getAmount().getAmount()));
		assertNotNull(response.getBody().getData().getTransaction().get(0).getBalance());
		assertNotNull(response.getBody().getData().getTransaction().get(0).getBalance().getAmount());
		assertNotNull(response.getBody().getData().getTransaction().get(0).getBalance().getCreditDebitIndicator());
		assertNotNull(response.getBody().getData().getTransaction().get(0).getBalance().getType());
		assertNotNull(response.getBody().getData().getTransaction().get(0).getBalance().getAmount().getAmount());
		assertNotNull(response.getBody().getData().getTransaction().get(0).getBalance().getAmount().getCurrency());
		assertTrue(Pattern.matches("[0-9.]{1,19}",
				response.getBody().getData().getTransaction().get(0).getBalance().getAmount().getAmount()));
		Boolean flag = Boolean.FALSE;
		for (CreditDebitIndicatorEnum creditDebitIndicatorEnum : CreditDebitIndicatorEnum.values()) {
			if (creditDebitIndicatorEnum.toString()
					.equals(response.getBody().getData().getTransaction().get(0).getBalance().getCreditDebitIndicator().toString())) {
				assertEquals(creditDebitIndicatorEnum.toString(),
						response.getBody().getData().getTransaction().get(0).getBalance().getCreditDebitIndicator().toString());
				flag = Boolean.TRUE;
				break;
			}
		}
		assertEquals(Boolean.TRUE, flag);
		flag = Boolean.FALSE;
		for (TypeEnum typeEnum : TypeEnum.values()) {
			if (typeEnum.toString().equals(response.getBody().getData().getTransaction().get(0).getBalance().getType().toString())) {
				assertEquals(typeEnum.toString(),
						response.getBody().getData().getTransaction().get(0).getBalance().getType().toString());
				flag = Boolean.TRUE;
				break;
			}
		}
		assertEquals(Boolean.TRUE, flag);
		if (response.getBody().getData().getTransaction().get(0).getBankTransactionCode() != null) {
			assertNotNull(response.getBody().getData().getTransaction().get(0).getBankTransactionCode());
			assertNotNull(response.getBody().getData().getTransaction().get(0).getBankTransactionCode().getCode());
			assertNotNull(response.getBody().getData().getTransaction().get(0).getBankTransactionCode().getSubCode());
		}
		flag = Boolean.FALSE;
		for (CreditDebitIndicatorEnum creditDebitIndicatorEnum : CreditDebitIndicatorEnum.values()) {
			if (creditDebitIndicatorEnum.toString()
					.equals(response.getBody().getData().getTransaction().get(0).getCreditDebitIndicator().toString())) {
				assertEquals(creditDebitIndicatorEnum.toString(),
						response.getBody().getData().getTransaction().get(0).getCreditDebitIndicator().toString());
				flag = Boolean.TRUE;
				break;
			}
		}
		assertEquals(Boolean.TRUE, flag);
		if (response.getBody().getData().getTransaction().get(0).getMerchantDetails() != null) {
			assertNotNull(response.getBody().getData().getTransaction().get(0).getMerchantDetails());
			assertTrue(Pattern.matches("[a-zA-Z]{3,4}",
					response.getBody().getData().getTransaction().get(0).getMerchantDetails().getMerchantCategoryCode()));
			assertTrue(response.getBody().getData().getTransaction().get(0).getMerchantDetails().getMerchantName().length() <= 350);
		}

		if (response.getBody().getData().getTransaction().get(0).getProprietaryBankTransactionCode() != null) {
			assertNotNull(response.getBody().getData().getTransaction().get(0).getProprietaryBankTransactionCode());
			assertTrue(
					response.getBody().getData().getTransaction().get(0).getProprietaryBankTransactionCode().getCode().length() <= 35);
			assertTrue(
					response.getBody().getData().getTransaction().get(0).getProprietaryBankTransactionCode().getIssuer().length() <= 35);
		}

		flag = Boolean.FALSE;
		for (StatusEnum statusEnum : StatusEnum.values()) {
			if (statusEnum.toString().equals(response.getBody().getData().getTransaction().get(0).getStatus().toString())) {
				assertEquals(statusEnum.toString(), response.getBody().getData().getTransaction().get(0).getStatus().toString());
				flag = Boolean.TRUE;
				break;
			}
		}
		assertEquals(Boolean.TRUE, flag);
		if (response.getBody().getData().getTransaction().get(0).getTransactionId() != null)
			assertTrue(response.getBody().getData().getTransaction().get(0).getTransactionId().length() <= 40);

		if (response.getBody().getData().getTransaction().get(0).getTransactionReference() != null)
			assertTrue(response.getBody().getData().getTransaction().get(0).getTransactionReference().length() <= 35);

		assertNotNull(response.getBody().getLinks());
		assertNotNull(response.getBody().getLinks().getSelf());
		assertNotNull(response.getBody().getMeta());

		try {
			LocalDateTime.parse(response.getBody().getData().getTransaction().get(0).getBookingDateTime(),
					DateTimeFormatter.ISO_DATE_TIME);
		} catch (Exception e) {
			fail("Response Date should be in ISO Format");
		}

	}

	// When FS data not compliant with CMA specifications.
	@Test(expected = HttpServerErrorException.class)
	public void FSInternalServerErrorTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + fsDataErrorAccId + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpServerErrorException e) {
			// assertTrue(e.getResponseBodyAsString().contains("x-fapi-financial-id
			// is missing in headers."));
			assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatusCode());
			assertTrue(e.getResponseBodyAsString().contains("Validation error found with the provided ouput"));
			throw e;
		}
	}

	// Credit Permission provided in token, displaying only CR transactions.
	//@Test
	public void CreditFilterTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountIdCR + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", TokenWithCreditPermission);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, AccountTransactionsGETResponse.class);
		response.getBody().getData().getTransaction().stream().forEach((x -> {
			assertEquals(CreditDebitIndicatorEnum.CREDIT, x.getCreditDebitIndicator());
		}));
	}

	// Debit Permission provided in token, displaying only DR transactions.
	@Test
	public void DebitFilterTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountIdDR + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", TokenWithDebitPermission);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, AccountTransactionsGETResponse.class);
		response.getBody().getData().getTransaction().stream().forEach((x -> {
			assertEquals(CreditDebitIndicatorEnum.DEBIT, x.getCreditDebitIndicator());
		}));
	}

	// Both Credit Debit Permissions provided in token, displaying all CR and DR transactions. 
	@Test
	public void CreditDebitBothFilterTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, AccountTransactionsGETResponse.class);
		List<CreditDebitIndicatorEnum> indicatorList = response.getBody().getData().getTransaction().stream()
				.map(Data3Transaction::getCreditDebitIndicator).collect(Collectors.toList());
		assertTrue(indicatorList.contains(CreditDebitIndicatorEnum.DEBIT));
		assertTrue(indicatorList.contains(CreditDebitIndicatorEnum.CREDIT));
	}

	// Detail Permission token displaying all detail fields.
	@Test
	public void DetailPermissionFieldsTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, AccountTransactionsGETResponse.class);
		if (response.getBody().getData().getTransaction().get(0).getMerchantDetails() != null)
			assertNotNull(response.getBody().getData().getTransaction().get(0).getMerchantDetails());
		assertNotNull(response.getBody().getData().getTransaction().get(0).getTransactionInformation());
		assertNotNull(response.getBody().getData().getTransaction().get(0).getBalance());
	}

	// Basic Permission token displaying only basic fields.
	@Test
	public void BasicPermissionsFieldsTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + basicPermissionAccId + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", TokenWithBasicPermission);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, AccountTransactionsGETResponse.class);
		assertNull(response.getBody().getData().getTransaction().get(0).getMerchantDetails());
		assertNull(response.getBody().getData().getTransaction().get(0).getTransactionInformation());
		assertNull(response.getBody().getData().getTransaction().get(0).getBalance());
	}

	// Masking on field TransactionInformation 
	@Test
	public void MaskedTransactionInformationTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, AccountTransactionsGETResponse.class);
		assertEquals("SidXXXXXXXXXXXXXXXXXXXXsdf", response.getBody().getData().getTransaction().get(0).getTransactionInformation());
	}
	
	
	// Pagination Test with all links
	@Test
	public void PaginationTest() {
		String url = edgeserverURL + accountTransactionURL + "/accounts/" + accountId1 + "/transactions/?pg=2";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountTransactionsGETResponse> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, AccountTransactionsGETResponse.class);
		assertNotNull(response.getBody().getLinks().getSelf());
		assertNotNull(response.getBody().getLinks().getNext());
	}

}

package com.capgemini.psd2.revoke.consent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.revoke.consent.service.RevokeConsentService;

@RestController
public class RevokeConsentController {

	@Autowired
	private RevokeConsentService revokeRequestService;

	@RequestMapping(value = {"/revoke-consent/{ConsentId}"}, method = RequestMethod.DELETE, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<String> deleteAccountRequest(@PathVariable("ConsentId") String consentId, @RequestParam(value = "tenantId", required = true)String tenantId) {
		revokeRequestService.removeConsentRequest(consentId, tenantId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT.toString(), HttpStatus.NO_CONTENT);
	}
}

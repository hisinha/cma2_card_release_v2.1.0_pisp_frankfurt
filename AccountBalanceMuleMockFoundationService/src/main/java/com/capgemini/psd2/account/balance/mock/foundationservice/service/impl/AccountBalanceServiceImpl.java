/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.balance.mock.foundationservice.domain.Balanceresponse;
import com.capgemini.psd2.account.balance.mock.foundationservice.domain.CreditcardsBalanceresponse;
import com.capgemini.psd2.account.balance.mock.foundationservice.repository.AccountBalanceRepository;
import com.capgemini.psd2.account.balance.mock.foundationservice.repository.AccountCardBalanceRepository;
import com.capgemini.psd2.account.balance.mock.foundationservice.service.AccountBalanceService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;

/**
 * The Class AccountBalanceServiceImpl.
 */
@Service
public class AccountBalanceServiceImpl implements AccountBalanceService {

	/** The repository. */
	@Autowired
	private AccountBalanceRepository repository;
	
	@Autowired
	private
	AccountCardBalanceRepository ccrepository;

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.account.balance.mock.foundationservice.service.AccountBalanceService#retrieveAccountBalance(java.lang.String, java.lang.String)
	 */

	@Override
	public Balanceresponse reteriveAccountsBalance(String accountId) throws Exception {
		Balanceresponse accnt = repository.findByAccountId(accountId);
		if (accnt == null) {
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.NO_ACOUNT_FOUND_PAC_BAL);
		}
	
		return accnt;
	}
	
	@Override
	public CreditcardsBalanceresponse reteriveCreditCardBalance(String accountId) throws Exception {
		CreditcardsBalanceresponse accnt = ccrepository.findByAccountId(accountId);
		if (accnt == null) {
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PAC_BAL);
		}
	
		return accnt;
	}


}

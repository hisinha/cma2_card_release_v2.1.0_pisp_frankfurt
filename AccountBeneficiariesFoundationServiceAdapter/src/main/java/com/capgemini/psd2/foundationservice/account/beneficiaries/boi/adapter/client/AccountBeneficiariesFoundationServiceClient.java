
package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.client;

import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;

public interface AccountBeneficiariesFoundationServiceClient {

	public PartiesPaymentBeneficiariesresponse restTransportForAccountBeneficiaryFS(RequestInfo requestInfo,
			Class<PartiesPaymentBeneficiariesresponse> beneficiaries,
			HttpHeaders httpHeaders);

}

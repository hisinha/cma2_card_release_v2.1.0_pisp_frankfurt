server:
  port: 9087
spring:
  application:
    name: AccountBeneficiariesFoundationServiceAdapter    
foundationService:
  timeZone: Europe/Dublin
  payloadLog: true
  maskPayloadLog: true
  maskBeneficiariesResponse: true
  apiId: AccountBeneficiariesFoundationServiceAdapter
  accountBeneficiaryBaseURL: http://localhost:9087/fs-abt-service/services/user
  userInReqHeader: X-BOI-USER
  channelInReqHeader: X-BOI-CHANNEL
  platformInReqHeader: X-BOI-PLATFORM
  correlationReqHeader: X-CORRELATION-ID
  consentFlowType: AISP
  platform: PSD2API
  accountFiltering: 
   permission: 
      AISP:
       - V
       - A
      CISP: 
       - V
       - A
      PISP:
       - X
       - A
   accountType:
      AISP:
       - Current Account
      CISP: 
       - Current Account
      PISP:
       - Current Account
   jurisdiction:
      AISP:
        - NORTHERN_IRELAND.SchemeName=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.Identification=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.ServicerSchemeName=
        - NORTHERN_IRELAND.ServicerIdentification=
        - GREAT_BRITAIN.SchemeName=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.Identification=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.ServicerSchemeName=
        - GREAT_BRITAIN.ServicerIdentification=
      CISP: 
        - NORTHERN_IRELAND.SchemeName=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.Identification=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.ServicerSchemeName=
        - NORTHERN_IRELAND.ServicerIdentification=
        - GREAT_BRITAIN.SchemeName=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.Identification=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.ServicerSchemeName=
        - GREAT_BRITAIN.ServicerIdentification=
      PISP: 
        - NORTHERN_IRELAND.SchemeName=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.Identification=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.ServicerSchemeName=
        - NORTHERN_IRELAND.ServicerIdentification=
        - GREAT_BRITAIN.SchemeName=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.Identification=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.ServicerSchemeName=
        - GREAT_BRITAIN.ServicerIdentification=
  errormap: 
  	FS_ESBE_001: BAD_REQUEST
    FS_ESBE_002: AUTHENTICATION_FAILURE_ERROR
    FS_ESBE_003: NO_BENEFICIARY_FOUND
    FS_ESBE_004: TECHNICAL_ERROR
    FS_CAP_001:  BAD_REQUEST
    FS_CAP_002:  AUTHENTICATION_FAILURE_ERROR
    FS_CAP_003:  ACCOUNT_DETAILS_NOT_FOUND
    FS_CAP_004:  TECHNICAL_ERROR
    FS_CAP_005:  ACCOUNT_DETAILS_NOT_FOUND
    FS_CAP_006:  USER_IS_BLOCKED
  foundationCustProfileUrl:
    BOL: http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/profile
    B365: http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/profile
    PO: http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/profile
    AA: http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/profile
app:
  platform: platform
  rules:
   response:
    restTransportForAccountBeneficiaryFS: 
     $.beneficiary|*|.accountNumber: '[0-9a-zA-z](?=\w{3}),X'
     $.beneficiary|*|.nsc: '(?<=\w{3})[0-9a-zA-z],X'
     $.beneficiary|*|.iban: '\d{8},XXXXXXXX'
package com.capgemini.psd2.fraudsystem.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.customer.account.list.routing.adapter.impl.CustomerAccountListRoutingAdapter;
import com.capgemini.psd2.fraudsystem.adapter.FraudSystemAdapter;
import com.capgemini.psd2.fraudsystem.routing.adapter.impl.FraudSystemRoutingAdapter;

@Configuration
public class FraudSystemsConfig {

	@Bean(name = "CustomerAccountListFraudSystemAdapter")
	public CustomerAccountListAdapter customeAccountListAdapter() {
		return new CustomerAccountListRoutingAdapter();
	}
	
	@Bean(name="FraudSystemAdapter")
	public FraudSystemAdapter fraudSystemAdapter() {
		return new FraudSystemRoutingAdapter();
	}

}

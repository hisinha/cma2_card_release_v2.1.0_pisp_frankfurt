package com.capgemini.psd2.fraudsystem.test.constants;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.junit.Test;

import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;

public class FraudSystemConstantsTest {

	@Test
	public void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
	  Constructor<FraudSystemConstants> constructor = FraudSystemConstants.class.getDeclaredConstructor();
	  assertTrue(Modifier.isPrivate(constructor.getModifiers()));
	}
}

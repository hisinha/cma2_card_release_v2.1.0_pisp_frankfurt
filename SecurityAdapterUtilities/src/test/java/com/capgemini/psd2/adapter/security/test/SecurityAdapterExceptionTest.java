package com.capgemini.psd2.adapter.security.test;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterException;

/**
 * The Class SecurityAdapterExceptionTest.
 */
public class SecurityAdapterExceptionTest {
		
		/**
		 * Test populate PSD 2 Security exception.
		 */
		@Test
		public void testPopulatePSD2SecurityException(){
			SecurityAdapterErrorCodeEnum errorCodeEnum= SecurityAdapterErrorCodeEnum.AUTHENTICATION_FAILURE_ERROR_LOGIN;
			assertEquals("Authentication failed" ,SecurityAdapterException.populatePSD2SecurityException(errorCodeEnum).getMessage());
		}
		
		/**
		 * Test populate PSD 2 Security exception with detail message.
		 */
		@Test
		public void testPopulatePSD2SecurityExceptionWithDetailMessage(){
			SecurityAdapterErrorCodeEnum errorCodeEnum= SecurityAdapterErrorCodeEnum.NO_VALID_BRAND_ID;
			assertEquals("No valid Brand id provided in request." ,SecurityAdapterException.populatePSD2SecurityException("No valid Brand id provided in request.",errorCodeEnum).getMessage());
		}

}


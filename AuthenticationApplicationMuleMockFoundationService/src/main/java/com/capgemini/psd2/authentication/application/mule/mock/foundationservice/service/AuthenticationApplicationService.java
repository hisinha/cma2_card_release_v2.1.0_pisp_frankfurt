package com.capgemini.psd2.authentication.application.mule.mock.foundationservice.service;

import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.AuthenticationRequest;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.DigitalUserRequest;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.LoginResponse;

public interface AuthenticationApplicationService {

	public LoginResponse retrieveBOLUserCredentials(DigitalUserRequest digitalId);
	
	public AuthenticationRequest retrieveB365UserCredentials(String userName, String password);

}

package com.capgemini.psd2.authentication.application.mule.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.DigitalUserRequest;

public interface DigitalUserRequestApplicationRepository extends MongoRepository<DigitalUserRequest , String>{
	
	public DigitalUserRequest findByDigitalUserDigitalUserIdentifier(String digitalUserIdentifier);

}

package com.capgemini.psd2.authentication.application.mule.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.DigitalUserRequest;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.LoginResponse;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.service.AuthenticationApplicationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;


@RestController
public class AuthenticationApplicationController {

	@Autowired
	private AuthenticationApplicationService authenticationApplicationService;

	/*BOL*/
    @RequestMapping(value = "/mocking/api/v1/links/934d3dd3-c7d7-4ed1-94f1-89e149eb1023/it-boi/p/authentication/v1.0/digital-user/authentication", method = RequestMethod.POST,consumes= "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<LoginResponse> authenticateBOLUserLogin(@RequestBody DigitalUserRequest digitalUser,                 
                 @RequestHeader(required = false, value = "x-api-source-user") String boiUser,
                 @RequestHeader(required = false, value = "x-api-source-system") String boiPlatform,
                 @RequestHeader(required = false, value = "x-api-correlation-id") String correlationID,
                 @RequestHeader(required = false, value = "x-api-transaction-id") String transanctionID) {

          if (NullCheckUtils.isNullOrEmpty(boiPlatform)) {                   
                 throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPR_AUT);
          }
          
          if ( NullCheckUtils.isNullOrEmpty(digitalUser.getDigitalUser().getCustomerAuthenticationSession())) {
                 throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPR_AUT);
          }
          
          LoginResponse loginResponse = authenticationApplicationService.retrieveBOLUserCredentials(digitalUser);
          System.out.println("loginResponse ::: "+loginResponse.toString());

          return new ResponseEntity<>(loginResponse, HttpStatus.OK);
    }

}

package com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain;

import javax.xml.bind.annotation.XmlTransient;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class DigitalUserRequest {

	private DigitalUser digitalUser;

	

	@Id
	@XmlTransient
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public DigitalUser getDigitalUser() {
		return digitalUser;
	}

	public void setDigitalUser(DigitalUser digitalUser) {
		this.digitalUser = digitalUser;
	}

}

package com.capgemini.psd2.authentication.application.mule.mock.foundationservice.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.AuthenticationMethodCode;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.AuthenticationRequest;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.CustomerAuthenticationSession;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.DigitalUser;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.DigitalUserRequest;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.DigitalUserSession;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.LoginResponse;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.PersonBasicInformation;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.repository.AuthenticationApplicationRepository;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.repository.DigitalUserRequestApplicationRepository;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.service.AuthenticationApplicationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;


@Service
public class AuthenticationApplicationServiceImpl implements AuthenticationApplicationService {

	@Autowired
	private AuthenticationApplicationRepository repository;
	
	@Autowired 
	private DigitalUserRequestApplicationRepository durepository;

	@Autowired
	private ValidationUtility validationUtility;

	@Override
    public LoginResponse retrieveBOLUserCredentials(DigitalUserRequest digitalUserReq) {
          
           //validationUtility.validateUserLogin(digitalUserReq.getDigitalId());
          System.out.println("Entering retrieve BOL User Credentials"); 
			validationUtility.validateBOLUser(digitalUserReq.getDigitalUser().getDigitalUserIdentifier());
		
          DigitalUserRequest searchDigitalUser = null;
          LoginResponse loginResponse = null;            
          searchDigitalUser = durepository.findByDigitalUserDigitalUserIdentifier(digitalUserReq.getDigitalUser().getDigitalUserIdentifier());
           System.out.println("Entering retrieve BOL User Credentials" +searchDigitalUser.toString());
          if (NullCheckUtils.isNullOrEmpty(searchDigitalUser)) {
                 loginResponse = new LoginResponse();
                 loginResponse.setDigitalUser(digitalUserReq.getDigitalUser());
                 DigitalUserSession digitalUserSession = new DigitalUserSession();
                 digitalUserSession.setSessionInitiationFailureIndicator(Boolean.TRUE);
                 digitalUserSession.setSessionInitiationFailureReasonCode("User not found");
                 loginResponse.setDigitalUserSession(digitalUserSession);
          }            
          else {
                 loginResponse = new LoginResponse();
                 loginResponse.setDigitalUser(digitalUserReq.getDigitalUser());
                 DigitalUserSession digitalUserSession = new DigitalUserSession();
                 String password = digitalUserReq.getDigitalUser().getCustomerAuthenticationSession().get(0).getSecureAccessKeyUsed();
                 if (password.equals(searchDigitalUser.getDigitalUser().getCustomerAuthenticationSession().get(0).getSecureAccessKeyUsed())) {
                        digitalUserSession.setSessionInitiationFailureIndicator(Boolean.FALSE);
                        digitalUserSession.setSessionStartDateTime(new Date());
                 }else{
                        digitalUserSession.setSessionInitiationFailureIndicator(Boolean.TRUE);
                        digitalUserSession.setSessionInitiationFailureReasonCode("User not validated successfully");
                 }                   
                 loginResponse.setDigitalUserSession(digitalUserSession);
          }
          return loginResponse;
    }

	@Override
	public AuthenticationRequest retrieveB365UserCredentials(String userName, String password) {

		validationUtility.validateMockBusinessValidations(userName);
		AuthenticationRequest authenticationRequest = null;
		try{
			 authenticationRequest = repository.findByUserNameAndPassword(userName, password);
		}catch(Exception ex){
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.INTERNAL_SERVER_ERROR);
		}

		if (authenticationRequest == null) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_LOGIN_AUTH);
		}

		return authenticationRequest;
	}

		

}

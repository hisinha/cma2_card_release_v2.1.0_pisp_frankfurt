package com.capgemini.psd2.funds.confirmation.mock.foundationservice.domain;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AvailableFundsAvailableFundsForWithdrawalAmount
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-07T14:38:09.445+05:30")

public class AvailableFundsAvailableFundsForWithdrawalAmount   {
  @JsonProperty("transactionCurrency")
  private BigDecimal transactionCurrency = null;

  public AvailableFundsAvailableFundsForWithdrawalAmount transactionCurrency(BigDecimal transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
    return this;
  }

  /**
   * The amount of funds that is available for withdrawal.
   * @return transactionCurrency
  **/
  @ApiModelProperty(required = true, value = "The amount of funds that is available for withdrawal.")
  @NotNull

  @Valid

  public BigDecimal getTransactionCurrency() {
    return transactionCurrency;
  }

  public void setTransactionCurrency(BigDecimal transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AvailableFundsAvailableFundsForWithdrawalAmount availableFundsAvailableFundsForWithdrawalAmount = (AvailableFundsAvailableFundsForWithdrawalAmount) o;
    return Objects.equals(this.transactionCurrency, availableFundsAvailableFundsForWithdrawalAmount.transactionCurrency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transactionCurrency);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AvailableFundsAvailableFundsForWithdrawalAmount {\n");
    
    sb.append("    transactionCurrency: ").append(toIndentedString(transactionCurrency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


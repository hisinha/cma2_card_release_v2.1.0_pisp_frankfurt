package com.capgemini.psd2.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.enums.ValidationType;
import com.capgemini.tpp.ssa.model.SSAModel;

@RunWith(SpringJUnit4ClassRunner.class)
public class HeaderValidatorTest {
	Set<ValidationStrategy> headerstrategies = null;

	SSAModel ssaModel = null;
	
	SSAModel ssaModel1 = null;

	@InjectMocks
	private HeaderValidationContext headerValidationContext;

	private ValidationType validationType;

	
	@Mock
	RequestHeaderAttributes headerAttributes;

	@Before
	public void setUp() {
		headerstrategies = new LinkedHashSet<ValidationStrategy>(EnumSet.allOf(HeaderValidationStrategy.class));
		ssaModel = new SSAModel();
		ssaModel1 = new SSAModel();
		ssaModel.setIss("OpenBanking Ltd");
		ssaModel.setOrg_id("0015800000jfQ9aAAE");
		ssaModel1.setOrg_id("000jfQ9aAAE");
		ssaModel.setSoftware_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_client_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_roles(new ArrayList<String>(Arrays.asList("AISP", "PISP")));
		ssaModel.setSoftware_redirect_uris(new ArrayList<String>(Arrays.asList("https://test.com")));
		headerValidationContext = new HeaderValidationContext(headerstrategies);
		headerAttributes=new RequestHeaderAttributes();
		headerAttributes.setX_ssl_client_cn("4cpi8P4dkTk200Inv1cPvO");
		headerAttributes.setX_ssl_client_ou("0015800000jfQ9aAAE");
		headerAttributes.setChannelId("OB_EXISTING_CERT");
		ReflectionTestUtils.setField(headerValidationContext, "headerAttributes", headerAttributes);
		
	}
	
	@Test
	public void testValidationForAllValid(){
		headerValidationContext.execute(ssaModel);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForOrgMismatch(){
		Set<ValidationStrategy> hstrategies1=new LinkedHashSet<ValidationStrategy>();
		hstrategies1.add(HeaderValidationStrategy.ORG_ID_OU);
		HeaderValidationContext cnxt=new HeaderValidationContext(hstrategies1);
		SSAModel ssaModel=new SSAModel();
		ssaModel.setOrg_id("131231231");
		ssaModel.setSoftware_id("11111111");
		ReflectionTestUtils.setField(cnxt, "headerAttributes", headerAttributes);
		cnxt.execute(ssaModel);
	}
	
	@Test
	public void testValidationForOrgmatch(){
		Set<ValidationStrategy> hstrategies1=new LinkedHashSet<ValidationStrategy>();
		hstrategies1.add(HeaderValidationStrategy.ORG_ID_OU);
		HeaderValidationContext cnxt=new HeaderValidationContext(hstrategies1);
		SSAModel ssaModel=new SSAModel();
		ssaModel.setOrg_id("0015800000jfQ9aAAE");
		ssaModel.setSoftware_id("11111111");
		ReflectionTestUtils.setField(cnxt, "headerAttributes", headerAttributes);
		cnxt.execute(ssaModel);
	}
	
	
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForCnMismatch(){
		Set<ValidationStrategy> hstrategies1=new LinkedHashSet<ValidationStrategy>();
		hstrategies1.add(HeaderValidationStrategy.SOFTWARE_ID_CN);
		HeaderValidationContext cnxt=new HeaderValidationContext(hstrategies1);
		SSAModel ssaModel=new SSAModel();
		ssaModel.setSoftware_id("11111111");
		ssaModel.setOrg_id("131231231");
		ReflectionTestUtils.setField(cnxt, "headerAttributes", headerAttributes);
		cnxt.execute(ssaModel);
	}
	
	@Test
	public void testValidationForCnmatch(){
		Set<ValidationStrategy> hstrategies1=new LinkedHashSet<ValidationStrategy>();
		hstrategies1.add(HeaderValidationStrategy.SOFTWARE_ID_CN);
		HeaderValidationContext cnxt=new HeaderValidationContext(hstrategies1);
		SSAModel ssaModel=new SSAModel();
		ssaModel.setSoftware_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setOrg_id("131231231");
		ReflectionTestUtils.setField(cnxt, "headerAttributes", headerAttributes);
		cnxt.execute(ssaModel);
	}
	
	@Test
	public void testValidationForUnsupportedOperation() {
		ValidationStrategy headerValidation = HeaderValidationStrategy.UNSUPPORTED;
		headerValidation.validate(null, null);
	}
	
	@Test(expected = UnsupportedOperationException.class)
	public void testValidationforUnsupportedOperation() {
		ValidationStrategy headerValidation = HeaderValidationStrategy.UNSUPPORTED;
		headerValidation.validate(null, null,null,null);
	}
	
	
}

package com.capgemini.psd2.aspect;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.LoggerAttribute;

@RunWith(SpringJUnit4ClassRunner.class)
public class DynamicAspectUtilsTest {
	
	@Mock
	private LoggerAttribute loggerAttribute;
	
	@InjectMocks
	private DynamicAspectUtils dynamicAspectUtils;


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void dynamicAspectUtilsTest() throws Exception {
		
		Mockito.when(loggerAttribute.getOperationName()).thenReturn("exceptionOccured()");
		
		Exception e = new Exception("error");
		dynamicAspectUtils.exceptionPayload(loggerAttribute, e, "error");
	}
	
	@After
	public void tearDown() throws Exception {
	}

}

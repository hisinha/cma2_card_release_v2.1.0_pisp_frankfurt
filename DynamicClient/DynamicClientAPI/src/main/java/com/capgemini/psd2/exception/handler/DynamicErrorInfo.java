package com.capgemini.psd2.exception.handler;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DynamicErrorInfo extends ErrorInfo{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("error_desciprtion")
	private String errorDesciprtion;

	public String getErrorDesciprtion() {
		return errorDesciprtion;
	}
	public void setErrorDesciprtion(String errorDesciprtion) {
		this.errorDesciprtion = errorDesciprtion;
	}
	
}

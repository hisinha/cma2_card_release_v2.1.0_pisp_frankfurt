package com.capgemini.psd2.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;

@ControllerAdvice
public class DynamicClientExeptionHandler extends ResponseEntityExceptionHandler{
	
	 /**
	 * Handle invalid request.
	 *
	 * @param e the e
	 * @param request the request
	 * @return the response entity
	 */
	@ExceptionHandler({Exception.class })
	protected ResponseEntity<Object> handleInvalidRequest(Exception e, WebRequest request) {
		ErrorInfo errorInfo = null;
		DynamicErrorInfo dynerrorInfo = null;
		HttpStatus status = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		if (e != null && e instanceof ClientRegistrationException) {
			errorInfo = ((PSD2Exception) e).getErrorInfo();
			dynerrorInfo = new DynamicErrorInfo();
			dynerrorInfo.setErrorMessage(errorInfo.getErrorMessage());
			dynerrorInfo.setErrorCode(errorInfo.getErrorCode());
			dynerrorInfo.setErrorDesciprtion(
					ClientRegistrationErrorCodeEnum.fromString(errorInfo.getErrorCode()).getDetailErrorMessage());
			status = HttpStatus.valueOf(Integer.parseInt(errorInfo.getStatusCode()));
		} else {
			ClientRegistrationErrorCodeEnum info = ClientRegistrationErrorCodeEnum.UNAPPROVED_SSA_ERROR;
			dynerrorInfo = new DynamicErrorInfo();
			dynerrorInfo.setErrorMessage(info.getErrorMessage());
			dynerrorInfo.setErrorCode(info.getErrorCode());
			dynerrorInfo.setErrorDesciprtion(info.getDetailErrorMessage());
			status = HttpStatus.valueOf(Integer.parseInt(info.getStatusCode()));
		}
		return handleExceptionInternal(e, dynerrorInfo, headers, status, request);
	}

}

package com.capgemini.psd2.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.capgemini.psd2.config.WellKnownConfig;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.DCRUtil;
import com.capgemini.psd2.validator.enums.CertificateTypes;
import com.capgemini.psd2.validator.enums.Roles;
import com.capgemini.psd2.validator.enums.ValidationType;
import com.capgemini.tpp.registration.model.RegistrationRequest;
import com.capgemini.tpp.ssa.model.SSAModel;
/**
 * 
 * @author nagoswam
 *
 */
public enum PayloadValidationStrategy implements ValidationStrategy {

	ISSUER(ValidationType.ISSUER) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes > void validate(T t, S s,
				Q q,K k) {
			if (!t.getIss().equals(s.getSoftware_id())) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.ISSUER_MISMATCH);
			}
		}
	},
	AUDIENCE(ValidationType.AUDIENCE) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (!t.getAud().equals(q.getIssuer(k.getTenantId()))) {
				throw ClientRegistrationException.populatePortalException(ClientRegistrationErrorCodeEnum.AUD_MISMATCH);
			}
		}
	},
	REDIRECT_URIS(ValidationType.REDIRECT_URIS) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig, K extends RequestHeaderAttributes> void validate(
				T t, S s, Q q, K k) {
			if (null == s.getSoftware_redirect_uris() || s.getSoftware_redirect_uris().isEmpty()) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.REDIRECT_URI_IS_MANDATORY);
			} else {
				if (!s.getSoftware_redirect_uris().containsAll(t.getRedirect_uris())) {
					throw ClientRegistrationException
							.populatePortalException(ClientRegistrationErrorCodeEnum.REDIRECT_URIS_MISMATCH);
				}
				String regex = "^(https)://(?!localhost)[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
				if (!s.getSoftware_redirect_uris().stream().allMatch(x -> x.matches(regex))) {
					throw ClientRegistrationException
							.populatePortalException(ClientRegistrationErrorCodeEnum.INVALID_REDIRECT_URIS);
				}
			}
		}
	},
	TOKEN_ENDPOINTS_AUTH_METHODS(ValidationType.TOKEN_ENDPOINTS_AUTH_METHODS) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (!q.getToken_endpoint_auth_methods_supported().contains(t.getToken_endpoint_auth_method())) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.AUTH_METHODS_NOT_SUPPORTED);
			}

		}
	},
	GRANT_TYPES(ValidationType.GRANT_TYPES) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (!q.getGrant_types_supported().containsAll(t.getGrant_types())) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.GRANT_TYPES_NOT_SUPPORTED);
			}

		}
	},
	RESPONSE_TYPES(ValidationType.RESPONSE_TYPES) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (!CollectionUtils.isEmpty(t.getResponse_types())
					&& !q.getResponse_types_supported().containsAll(t.getResponse_types())) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.RESPONSE_TYPES_NOT_SUPPORTED);
			}
		}
	},
	SOFTWARE_ID(ValidationType.SOFTWARE_ID) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (t.getSoftware_id() != null && !s.getSoftware_id().equals(t.getSoftware_id())) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.SOFTWARE_ID_MISMATCH);
			}
		}

	},
	SCOPES(ValidationType.SCOPES) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (t.getScope() != null/* && !CertificateTypes.NON_OB_EIDAS_CERT.name().equals(q.getChannelid())*/) {
				String[] scopes = StringUtils.split(t.getScope());
				List<String> scopeList = Arrays.asList(scopes);
				if (CollectionUtils.isEmpty(scopeList) || !q.getScopes().containsAll(scopeList)
						|| !"openid".equals(scopes[0])) {
					throw ClientRegistrationException
							.populatePortalException(ClientRegistrationErrorCodeEnum.SCOPES_NOT_SUPPORTED);
				}
				scopes = Arrays.stream(Roles.values()).map(Enum::name)
						.filter(m -> scopeList.stream().anyMatch(x -> x.equalsIgnoreCase(Roles.valueOf(m).getRole())))
						.toArray(String[]::new);
				List<String> scopeList1 = Arrays.asList(scopes);
				if (scopeList1.isEmpty()) {
					throw ClientRegistrationException
							.populatePortalException(ClientRegistrationErrorCodeEnum.SCOPES_NOT_SUPPORTED);
				}
				if (null != s.getSoftware_roles() && !s.getSoftware_roles().containsAll(scopeList1)){
					
					throw ClientRegistrationException
					.populatePortalException(ClientRegistrationErrorCodeEnum.SCOPES_NOT_SUPPORTED);
				}
				//set the subset of roles between SSA roles and registration request wrapper
				s.setSoftware_roles(new ArrayList<String>(scopeList1));
			}
		}
	},
	APPLICATION_TYPE(ValidationType.APPLICATION_TYPE) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (!"WEB".equals(t.getApplication_type())) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.APP_TYPE_NOT_SUPPORTED);
			}

		}
	},
	ID_TOKEN_SIGNING_ALG(ValidationType.ID_TOKEN_SIGNING_ALG) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (!q.getId_token_signing_alg_values_supported().contains(t.getId_token_signed_response_alg())) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.ID_TOKEN_ALG_NOT_SUPPORTED);
			}

		}
	},
	REQUEST_OBJECT_SIGNING_ALG(ValidationType.REQUEST_OBJECT_SIGNING_ALG) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (!q.getRequest_object_signing_alg_values_supported().contains(t.getRequest_object_signing_alg())) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.REQUEST_OBJECT_ALG_NOT_SUPPORTED);
			}

		}
	},
	TLS_CLIENT_AUTH_DN(ValidationType.TLS_CLIENT_AUTH_DN) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (CertificateTypes.OB_EXISTING_CERT.name().equals(k.getChannelId())) {
				if (!k.getX_ssl_client_cn()
						.equals(DCRUtil.extractPattren(t.getTls_client_auth_dn(), PSD2Constants.CN))) {
					throw ClientRegistrationException
							.populatePortalException(ClientRegistrationErrorCodeEnum.TLS_CLIENT_AUTH_DN);
				}
			} else 
				if (!k.getX_ssl_client_ncaid()
						.equals(DCRUtil.extractPattren(t.getTls_client_auth_dn(), PSD2Constants.NCA_ID_IDENTIFIER))) {
					throw ClientRegistrationException
							.populatePortalException(ClientRegistrationErrorCodeEnum.TLS_CLIENT_AUTH_DN);
			}

		}
	},
	UNSUPPORTED(ValidationType.UNSUPPORTED) {
		public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			/*
			 * Unsupported Validation Type
			 */
		}
	};

	private ValidationType validationType;

	private PayloadValidationStrategy(ValidationType validationType) {
		this.validationType = validationType;
	}

	public ValidationType getValidationType() {
		return validationType;
	}

	public <T extends RequestHeaderAttributes, S extends SSAModel> void validate(T t, S s) {
		throw new UnsupportedOperationException();
	}
}

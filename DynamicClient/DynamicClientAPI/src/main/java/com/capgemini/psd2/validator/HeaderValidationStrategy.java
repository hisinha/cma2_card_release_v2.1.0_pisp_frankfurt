package com.capgemini.psd2.validator;

import com.capgemini.psd2.config.WellKnownConfig;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.enums.CertificateTypes;
import com.capgemini.psd2.validator.enums.ValidationType;
import com.capgemini.tpp.registration.model.RegistrationRequest;
import com.capgemini.tpp.ssa.model.SSAModel;

/**
 * 
 * @author nagoswam
 *
 */
public enum HeaderValidationStrategy implements ValidationStrategy {

	SOFTWARE_ID_CN(ValidationType.SOFTWARE_ID_CN) {
		public <T extends RequestHeaderAttributes, S extends SSAModel> void validate(T t, S s) {
			if (CertificateTypes.OB_EXISTING_CERT.name().equals(t.getChannelId())
					&& !t.getX_ssl_client_cn().equals(s.getSoftware_id())) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.SSA_CERTIFICATE_MISMATCH_CN);
			}
		}
	},
	ORG_ID_OU(ValidationType.ORG_ID_OU) {
		public <T extends RequestHeaderAttributes, S extends SSAModel> void validate(T t, S s) {
			if (CertificateTypes.OB_EXISTING_CERT.name().equals(t.getChannelId())
					&& !t.getX_ssl_client_ou().equals(s.getOrg_id())) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.SSA_CERTIFICATE_MISMATCH_OU);
			}
		}
	}/*,
	NCAID_SSA(ValidationType.NCAID_SSA) {
		public <T extends RequestHeaderAttributes, S extends SSAModel> void validate(T t, S s) {
			if ((CertificateTypes.OB_EIDAS_CERT.name().equals(t.getChannelId())
					&& (null == t.getX_ssl_client_ncaid() || !t.getX_ssl_client_ncaid()
							.contains(s.getOrganisation_competent_authority_claims().getAuthority_id() + "-"
									+ s.getOrganisation_competent_authority_claims().getRegistration_id())))
					|| (CertificateTypes.NON_OB_EIDAS_CERT.name().equals(t.getChannelId())
							&& (null == t.getX_ssl_client_ncaid()
									|| !t.getX_ssl_client_ncaid().equals(s.getOrg_id())))) {
				throw ClientRegistrationException
						.populatePortalException(ClientRegistrationErrorCodeEnum.SSA_CERTIFICATE_MISMATCH_NCAID);
			} 
				 * else if (CertificateTypes.NON_OB_EIDAS_CERT.name().equals(t.
				 * getChannelId()) &&
				 * !t.getX_ssl_client_ncaid().equals(s.getOrg_id())) { throw
				 * ClientRegistrationException
				 * .populatePortalException(ClientRegistrationErrorCodeEnum.
				 * SSA_CERTIFICATE_MISMATCH_NCAID); }
				 
		}
	}*/,
	UNSUPPORTED(ValidationType.UNSUPPORTED) {
		public <T extends RequestHeaderAttributes, S extends SSAModel> void validate(T t, S s) {
			/*
			 * Unsupported Validation Type
			 */
		}
	};

	private ValidationType validationType;

	private HeaderValidationStrategy(ValidationType validationType) {
		this.validationType = validationType;
	}

	public ValidationType getValidationType() {
		return validationType;
	}

	public <T extends RegistrationRequest, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s, Q q,K k) {
		throw new UnsupportedOperationException();
	}
}

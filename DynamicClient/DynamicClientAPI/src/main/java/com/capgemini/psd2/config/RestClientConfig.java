package com.capgemini.psd2.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.capgemini.psd2.exception.handler.DynamicClientRestClientHandler;
import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@Configuration
public class RestClientConfig {
	
	
	@Bean(name = "tppPortalRestClient")
	public RestClientSync restClientSyncImpl() {
		return new RestClientSyncImpl(exceptionHandlImpl());
	}

	@Bean
	public ExceptionHandler exceptionHandlImpl() {
		return new DynamicClientRestClientHandler();
	}
}

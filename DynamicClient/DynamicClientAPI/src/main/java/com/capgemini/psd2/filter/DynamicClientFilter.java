package com.capgemini.psd2.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;

public class DynamicClientFilter extends OncePerRequestFilter{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DynamicClientFilter.class);
	
	/** The Constant DO_FILTER_INTERNAL_STRING. */
	private static final String DO_FILTER_INTERNAL_STRING = "com.capgemini.psd2.DynamicClientFilter.doFilterInternal()";

	/** The Constant doFilter. */
	private static final String DO_FILTER = "doFilterInternal";
	
	@Autowired
	RequestHeaderAttributes requestHeaderAttributes;
	
	@Autowired
	private LoggerUtils loggerUtils;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		requestHeaderAttributes.setMethodType(request.getMethod());
		requestHeaderAttributes.setCorrelationId(GenerateUniqueIdUtilities.getUniqueId().toString());
		requestHeaderAttributes.setX_ssl_client_dn(request.getHeader("DN"));
		requestHeaderAttributes.setX_ssl_client_cn(request.getHeader("DN"));
		requestHeaderAttributes.setX_ssl_client_ou(request.getHeader("OU"));
		requestHeaderAttributes.setX_ssl_client_cert(request.getHeader("X-SSL-Cert"));
		requestHeaderAttributes.setTenantId(request.getHeader("tenantid"));
		requestHeaderAttributes.setX_ssl_client_ncaid(request.getHeader("X-SSL-NCA-ID"));
		filterChain.doFilter(request, response);
		LOG.info("{\"Exit\":\"{}\",\"{}\"}", DO_FILTER_INTERNAL_STRING, loggerUtils.populateLoggerData(DO_FILTER));

	}

}

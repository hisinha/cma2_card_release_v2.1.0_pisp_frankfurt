package com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.constants.ValidatePaymentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.transformer.ValidatePaymentFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;

@RunWith(SpringJUnit4ClassRunner.class)
public class ValidatePaymentFoundationServiceTransformerTest {

	@InjectMocks
	private ValidatePaymentFoundationServiceTransformer transformer;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	@Test
	public void transformPaymentSetupPOSTResponseTest1(){
		
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		 Map<String, String> params = new HashMap<String, String>();
	    PaymentSetupResponse data = new PaymentSetupResponse();
	    
	    FraudServiceResponse fraudServiceResponse = new FraudServiceResponse();
	    fraudServiceResponse.setDecisionType("Decision");
	    
	    PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
	    initiation.setInstructionIdentification("ANSM023");
	    initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
	    
	    DebtorAccount debtorAccount = new DebtorAccount();
	    debtorAccount.setIdentification("01234567123654");
	    debtorAccount.setName("Andrea Smith");
	    debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
	    
	    DebtorAgent debtorAgent = new DebtorAgent();
	    debtorAgent.setIdentification("SC112800");
	    debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);        
	    
	    CreditorAccount creditorAccount = new CreditorAccount();
	    creditorAccount.setIdentification("21325698123654");
	    creditorAccount.setName("Bob Clements");
	    creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
	    
	    CreditorAgent creditorAgent = new CreditorAgent();
	    creditorAgent.setIdentification("080800");// In FS side it is expected to be Integer 
	    creditorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI);
	    
	    PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
	    instructedAmount.setAmount("20.00");
	    instructedAmount.setCurrency("GBP");
	    
	    RemittanceInformation remittanceInformation = new RemittanceInformation();
	    remittanceInformation.setReference("FRESCO-037");
	    remittanceInformation.setUnstructured("Internal ops code 5120103");
	    
	    RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
	    List<String> addressLine = new ArrayList<>();
	    addressLine.add("test1");
	    addressLine.add("test2");
	    List<String> countrySubDivision = new ArrayList<>();
	    countrySubDivision.add("test1");
	    countrySubDivision.add("test2");
	    
	    riskDeliveryAddress.setAddressLine(addressLine);
	    riskDeliveryAddress.setCountrySubDivision(countrySubDivision);
	    
	    initiation.setCreditorAccount(creditorAccount);
	    initiation.setCreditorAgent(creditorAgent);
	    initiation.setDebtorAccount(debtorAccount);
	    initiation.setDebtorAgent(debtorAgent);
	    initiation.setInstructedAmount(instructedAmount);
	    initiation.setRemittanceInformation(remittanceInformation);

	    Risk risk = new Risk();
	    risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
	    risk.setDeliveryAddress(riskDeliveryAddress);
	    
	    data.setInitiation(initiation);
	    paymentSetupPOSTResponse.setData(data);
	    paymentSetupPOSTResponse.setRisk(risk);
	    paymentSetupPOSTResponse.setFraudnetResponse(fraudServiceResponse);

	    params.put(ValidatePaymentFoundationServiceConstants.USER_ID, "testUser");
	    params.put(ValidatePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
	    params.put(ValidatePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
	    params.put(ValidatePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
	    
	    PaymentInstruction instruction = new PaymentInstruction();
	    instruction = transformer.transformPaymentSetupPOSTResponse(paymentSetupPOSTResponse, params);    		
	    assertNotNull(instruction);
	}
	
	
	@Test
	public void transformPaymentSetupPOSTRequestTest2(){
		
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		 Map<String, String> params = new HashMap<String, String>();
	    PaymentSetupResponse data = new PaymentSetupResponse();
	    
	    PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();

		
	    initiation.setInstructionIdentification("ANSM023");
	    initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
	    
	    DebtorAccount debtorAccount = new DebtorAccount();
	    debtorAccount.setIdentification("01234567");
	    debtorAccount.setName("Andrea Smith");
	    debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);

	    CreditorAccount creditorAccount = new CreditorAccount();
	    creditorAccount.setIdentification("21325698");
	    creditorAccount.setName("Bob Clements");
	    creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
	    
	    PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
	    instructedAmount.setAmount("20.00");
	    instructedAmount.setCurrency("GBP");
	    
	    RemittanceInformation remittanceInformation = new RemittanceInformation();
	    remittanceInformation.setReference("FRESCO-037");
	    remittanceInformation.setUnstructured("Internal ops code 5120103");
	    
	    RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
	    List<String> addressLine = new ArrayList<>();
	    addressLine.add("test1");
	    addressLine.add("test2");
    
	    initiation.setCreditorAccount(creditorAccount);
	    initiation.setDebtorAccount(debtorAccount);
	    initiation.setInstructedAmount(instructedAmount);
	    initiation.setRemittanceInformation(remittanceInformation);

	    Risk risk = new Risk();
	    risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
	    risk.setDeliveryAddress(riskDeliveryAddress);
	    
	    data.setInitiation(initiation);
	    paymentSetupPOSTResponse.setData(data);
	    paymentSetupPOSTResponse.setRisk(risk);

	    params.put(ValidatePaymentFoundationServiceConstants.USER_ID, "testUser");
	    params.put(ValidatePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
	    params.put(ValidatePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
	    params.put(ValidatePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
	    
	    PaymentInstruction instruction = new PaymentInstruction();    
	    instruction = transformer.transformPaymentSetupPOSTResponse(paymentSetupPOSTResponse, params);
	    assertNotNull(instruction);
	}
	
	
	@Test
	public void transformValidatePreStagePaymentResponseTest1(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		validationPassed.setSuccessCode("testCode");
		validationPassed.setSuccessMessage("testMessage");
		
		PaymentSetupValidationResponse paymentSetupPreValidationResponse = new PaymentSetupValidationResponse();
		paymentSetupPreValidationResponse = transformer.transformValidatePaymentResponse(validationPassed);
		assertNotNull(paymentSetupPreValidationResponse);
	}
	
	@Test(expected = AdapterException.class)
	public void transformValidatePreStagePaymentResponseTest2(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		transformer.transformValidatePaymentResponse(validationPassed);
	}
	
	@Test(expected = AdapterException.class)
	public void transformValidatePreStagePaymentResponseTestValidaionPassedNull(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		validationPassed.setSuccessCode("test");
		validationPassed.setSuccessMessage("test");
		PaymentSetupValidationResponse paymentSetupPreValidationResponse = transformer.transformValidatePaymentResponse(null);
		assertNotNull(paymentSetupPreValidationResponse);
	}
}


package com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.utility;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.transformer.ValidatePaymentFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class ValidatePaymentFoundationServiceUtility {

	@Autowired
	private ValidatePaymentFoundationServiceTransformer validatePaymentFSTransformer;
	
	
	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	@Value("${foundationService.platformInReqHeader:#{X-BOI-PLATFORM}}")
	private String platformInReqHeader;

	@Value("${foundationService.correlationReqHeader:#{X-CORRELATION-ID}}")
	private String correlationReqHeader;

	/** The platform. */
	@Value("${foundationService.platform}")
	private String platform;

	@Value("${foundationService.clientIdReqHeader_Payments:#{client_id}}")
	private String clientIdReqHeader_Payments;

	@Value("${foundationService.clientSecretReqHeader_Payments:#{client_secret}}")
	private String clientSecretReqHeader_Payments;

	@Value("${foundationService.clientId_Payments}")
	private String clientIdReqHeaderValue;

	@Value("${foundationService.clientSecret_Payments}")
	private String clientSecretReqHeaderValue;
	

	public PaymentInstruction transformRequestFromAPIToFS(CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse, Map<String, String> params) {
		return validatePaymentFSTransformer.transformPaymentSetupPOSTResponse(paymentSetupPOSTResponse, params);
	}

	public PaymentSetupValidationResponse transformResponseFromFSToAPI(ValidationPassed validationPassed) {
		return validatePaymentFSTransformer.transformValidatePaymentResponse(validationPassed);
	}

	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, PaymentSetupPOSTResponse paymentSetupPOSTResponse, Map<String, String> params) {

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(channelInReqHeader, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		String platformId = params.get(PSD2Constants.PLATFORM_IN_REQ_HEADER);
		httpHeaders.add(platformInReqHeader, NullCheckUtils.isNullOrEmpty(platformId) ? platform : platformId);
		httpHeaders.add(correlationReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		httpHeaders.add(clientIdReqHeader_Payments, clientIdReqHeaderValue);
		httpHeaders.add(clientSecretReqHeader_Payments, clientSecretReqHeaderValue);
		return httpHeaders;
	}

}

package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.DSPConsentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticScheduledPaymentStagingAdapter;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("dScheduledPaymentConsentsStagingMongoDbAdapter")
public class DSPConsentsStagingMongoDbAdapterImpl implements DomesticScheduledPaymentStagingAdapter {

	@Autowired
	private DSPConsentsFoundationRepository dScheduledPaymentConsentsBankRepository;

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private RequestHeaderAttributes reqAttributes;
	
	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;

	@Override
	public GenericPaymentConsentResponse processDomesticScheduledPaymentConsents(
			CustomDSPConsentsPOSTRequest customRequest, CustomPaymentStageIdentifiers customStageIdentifiers,
			Map<String, String> params, OBExternalConsentStatus1Code successStatus,
			OBExternalConsentStatus1Code failureStatus) {

		String initiationAmount = customRequest.getData().getInitiation().getInstructedAmount().getAmount();
		if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_POST_MOCKING)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}

		// Sandbox Secondary Identification Validation
		String secondaryIdentification = customRequest.getData().getInitiation().getCreditorAccount()
				.getSecondaryIdentification();
		if (utility.isValidSecIdentification(secondaryIdentification)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTSECONDARYIDENTIFIER,
							ErrorMapKeys.ACCOUNT_SECONDARYIDENTIFIER_INVALID));
		}

		GenericPaymentConsentResponse genericPaymentsResponse = populateMockedResponse(customRequest);
		populateAndSaveDbResource(customRequest, genericPaymentsResponse, successStatus, failureStatus);

		return genericPaymentsResponse;
	}

	@Override
	public CustomDSPConsentsPOSTResponse retrieveStagedDomesticScheduledPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		try {
			String paymentConsentId = customPaymentStageIdentifiers.getPaymentConsentId();
			CustomDSPConsentsPOSTResponse paymentBankResource = null;

			paymentBankResource = dScheduledPaymentConsentsBankRepository.findOneByDataConsentId(paymentConsentId);

			if (paymentBankResource == null || paymentBankResource.getData() == null
					|| paymentBankResource.getData().getInitiation() == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));

			String initiationAmount = paymentBankResource.getData().getInitiation().getInstructedAmount().getAmount();

			if (reqAttributes.getMethodType().equals("GET")) {
				if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING)) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
				}
			}

			paymentBankResource.setId(null);

			return paymentBankResource;

		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	private GenericPaymentConsentResponse populateMockedResponse(CustomDSPConsentsPOSTRequest customRequest) {

		GenericPaymentConsentResponse genericPaymentConsentsResponse = new GenericPaymentConsentResponse();

		String currency = customRequest.getData().getInitiation().getInstructedAmount().getCurrency();
		String initiationAmount = customRequest.getData().getInitiation().getInstructedAmount().getAmount();

		String consentId = null;
		String expectedExecutionDateTime = utility.getMockedExpectedExecDtTm();
		String expectedSettlementDateTime = utility.getMockedExpectedSettlementDtTm();
		String cutOffDateTime = utility.getMockedCutOffDtTm();
		
		ProcessConsentStatusEnum processConsentStatusEnum = utility.getMockedConsentProcessExecStatus(currency,
				initiationAmount);

		if (processConsentStatusEnum == ProcessConsentStatusEnum.PASS
				|| processConsentStatusEnum == ProcessConsentStatusEnum.FAIL)
			consentId = UUID.randomUUID().toString();

		genericPaymentConsentsResponse.setConsentId(consentId);
		
		// removing charges block as this is not returned for BOI on domestic payment APIs
		if(!isSandboxEnabled){
			List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, null);
			genericPaymentConsentsResponse.setCharges(charges);
		}
		
		genericPaymentConsentsResponse.setExpectedExecutionDateTime(expectedExecutionDateTime);
		genericPaymentConsentsResponse.setExpectedSettlementDateTime(expectedSettlementDateTime);
		genericPaymentConsentsResponse.setCutOffDateTime(cutOffDateTime);
		genericPaymentConsentsResponse.setProcessConsentStatusEnum(processConsentStatusEnum);
		return genericPaymentConsentsResponse;
	}

	private void populateAndSaveDbResource(CustomDSPConsentsPOSTRequest customRequest,
			GenericPaymentConsentResponse genericPaymentsResponse, OBExternalConsentStatus1Code successStatus,
			OBExternalConsentStatus1Code failureStatus) {

		String request = JSONUtilities.getJSONOutPutFromObject(customRequest);
		CustomDSPConsentsPOSTResponse paymentConsentsFoundationResponse = JSONUtilities
				.getObjectFromJSONString(PispUtilities.getObjectMapper(), request, CustomDSPConsentsPOSTResponse.class);

		paymentConsentsFoundationResponse.getData().setCreationDateTime(customRequest.getCreatedOn());
		paymentConsentsFoundationResponse.getData().setConsentId(genericPaymentsResponse.getConsentId());
		paymentConsentsFoundationResponse.getData().setCharges(genericPaymentsResponse.getCharges());
		paymentConsentsFoundationResponse.getData().setCutOffDateTime(genericPaymentsResponse.getCutOffDateTime());
		paymentConsentsFoundationResponse.getData()
				.setExpectedExecutionDateTime(genericPaymentsResponse.getExpectedExecutionDateTime());
		paymentConsentsFoundationResponse.getData()
				.setExpectedSettlementDateTime(genericPaymentsResponse.getExpectedSettlementDateTime());
		paymentConsentsFoundationResponse
				.setConsentPorcessStatus(genericPaymentsResponse.getProcessConsentStatusEnum());

		if (genericPaymentsResponse.getConsentId() != null) {
			OBExternalConsentStatus1Code status = calculateCMAStatus(
					genericPaymentsResponse.getProcessConsentStatusEnum(), successStatus, failureStatus);
			paymentConsentsFoundationResponse.getData().setStatus(status);
			paymentConsentsFoundationResponse.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}
		try {
			dScheduledPaymentConsentsBankRepository.save(paymentConsentsFoundationResponse);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	private OBExternalConsentStatus1Code calculateCMAStatus(ProcessConsentStatusEnum processConsentStatusEnum,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return (processConsentStatusEnum == ProcessConsentStatusEnum.PASS) ? successStatus : failureStatus;
	}
}
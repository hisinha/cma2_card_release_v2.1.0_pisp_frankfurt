package com.capgemini.psd2.adapter.frequency.utility;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;


@Component
public class FrequencyUtil {
	
	//convert frequency from CMA to Process Layer, For Example: IntrvlWkDay:01:01 to Weekly
	public String CmaToProcessLayer(String interval, Date paymentStartDate){
		String frequency="";
		int dayOfWeek=0;
		String intervalLastTwo=null;
		String intervalFrequency=null;
		int iDayOfWeek=0;
		int dayOfMonth=0;
		
		try {
		//seperate the interval part from frequency
		intervalFrequency=interval.substring(0,interval.length()-3);
		//seperate the date part from frequency
		intervalLastTwo=interval.substring(interval.length()-2);
		iDayOfWeek=Integer.parseInt(intervalLastTwo);
		
		//evaluate day of Month and day of week from paymentStartDate
		Instant instant = paymentStartDate.toInstant();
		ZoneId defaultZoneId = ZoneId.systemDefault();
		LocalDate localDate = instant.atZone(defaultZoneId).toLocalDate();
		dayOfMonth=localDate.getDayOfMonth();
		dayOfWeek = localDate.getDayOfWeek().getValue();
		}catch(Exception e){
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY);
		}
		
		switch (intervalFrequency) {
		case StandingOrderFrequencyConstant.IntervalWeekDay:
			if (dayOfWeek==iDayOfWeek)
				 frequency = StandingOrderFrequencyConstant.WEEKLY;
			else
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY_INVALID_DATE);
			break;			
		case StandingOrderFrequencyConstant.IntervalBiWeekday:
			if (dayOfWeek==iDayOfWeek)
				 frequency = StandingOrderFrequencyConstant.FORTNIGHTLY;
			else
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY_INVALID_DATE);
				break;				
		case StandingOrderFrequencyConstant.IntervalMonthDay:
			if(dayOfMonth==iDayOfWeek)
				frequency = StandingOrderFrequencyConstant.MONTHLY;
			else
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY_INVALID_DATE);
				break;
		case StandingOrderFrequencyConstant.IntervalAnnualday:
			if(dayOfMonth==iDayOfWeek)
				frequency=StandingOrderFrequencyConstant.YEARLY;
			else
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY_INVALID_DATE);
				break;
		default :
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY);
		}
		return frequency;
		}
	
	//convert frequency from Process Layer to CMA, For Example: Weekly to IntrvlWkDay:01:01
	public String processLayerToCma(String intervalFS,Date paymentStartDate) {
		String interval = "";
		int dayofweek = 0;
		int dayOfMonth=0;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(paymentStartDate);
		
		try {
		if (intervalFS.toString().contains(StandingOrderFrequencyConstant.MONTHLY)
				|| intervalFS.toString().contains(StandingOrderFrequencyConstant.YEARLY)) {
			dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		} else if (intervalFS.toString().contains(StandingOrderFrequencyConstant.WEEKLY)
				|| intervalFS.toString().contains(StandingOrderFrequencyConstant.FORTNIGHTLY)) {
			Instant instant = paymentStartDate.toInstant();
			ZoneId defaultZoneId = ZoneId.systemDefault();
			LocalDate localDate = instant.atZone(defaultZoneId).toLocalDate();
			dayofweek = localDate.getDayOfWeek().getValue();
		}
		
		switch (intervalFS) {
		case StandingOrderFrequencyConstant.WEEKLY:
			interval = StandingOrderFrequencyConstant.IntervalWeekDayOutput + dayofweek;
			break;
		case StandingOrderFrequencyConstant.FORTNIGHTLY:
			interval = StandingOrderFrequencyConstant.IntervalBiWeekdayOutput + dayofweek;
			break;
		case StandingOrderFrequencyConstant.MONTHLY:
			if (dayOfMonth >=1 && dayOfMonth <= 9)
				interval = StandingOrderFrequencyConstant.IntervalMonthDayOutput + dayOfMonth;
			else if(dayOfMonth >=10 && dayOfMonth <= 31)
				interval = StandingOrderFrequencyConstant.IntervalMonthDayOutput_1 + dayOfMonth;
			break;
		case StandingOrderFrequencyConstant.YEARLY:
			if (dayOfMonth >=1 && dayOfMonth <= 9)
				interval = StandingOrderFrequencyConstant.IntervalAnnualdayOutput + dayOfMonth;
			else if(dayOfMonth >=10 && dayOfMonth <= 31)
				interval = StandingOrderFrequencyConstant.IntervalAnnualdayOutput_1 + dayOfMonth;
			break;
		default :
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY);
		}
		return interval;
		}catch (Exception e) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY);
	}
	
	}
}	

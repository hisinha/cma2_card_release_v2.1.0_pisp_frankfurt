package com.capgemini.psd2.adapter.exceptions;

import org.springframework.http.HttpStatus;

public enum AdapterOBErrorCodeEnum {

	OBIE_TECHNICAL_ERROR("500 Internal Server Error","UK.OBIE.UnexpectedError", HttpStatus.INTERNAL_SERVER_ERROR, "Technical Error. Please try again later", "There was a problem processing your request. Please try again later"),
	
	OBIE_BAD_REQUEST("400 Bad Request","UK.OBIE.Resource.NotFound", HttpStatus.BAD_REQUEST, "Account Not Found", "Account Not Found"),
	
	OBIE_FORBIDDEN("403 Forbidden","UK.OBIE.Resource.Forbidden",HttpStatus.FORBIDDEN,"Access to requested resource denied","Forbidden"),
	
	OBIE_BAD_REQUEST_PISP("400 Bad Request","UK.OBIE.Resource.NotFound", HttpStatus.BAD_REQUEST, "Resource Not Found", "Resource Not Found"),
	UKIE_BAD_REQUEST_PISP1("400 Bad Request","IE.BOI.PaymentRejected", HttpStatus.BAD_REQUEST, "Payment Setup request has failed", "Payment Setup request has failed"),
	
	UKIE_BAD_REQUEST_PISP2("400 Bad Request","IE.BOI.PaymentRejected", HttpStatus.BAD_REQUEST, "Payment Execution request has failed", "Payment Execution request has failed");
	
	private String code;
	
	private String obErrorCode;
	
	private HttpStatus httpStatusCode;
	
	private String generalErrorMessage;

	private String specificErrorMessage;
	
	public String getCode() {
		return code;
	}

	public String getObErrorCode() {
		return obErrorCode;
	}

	public HttpStatus getHttpStatusCode() {
		return httpStatusCode;
	}

	public String getGeneralErrorMessage() {
		return generalErrorMessage;
	}

	public String getSpecificErrorMessage() {
		return specificErrorMessage;
	}

	private AdapterOBErrorCodeEnum(String code, String obErrorCode, HttpStatus httpStatusCode, String generalErrorMessage, String specificErrorMessage) {
		this.code = code;
		this.obErrorCode = obErrorCode;
		this.httpStatusCode = httpStatusCode;
		this.generalErrorMessage = generalErrorMessage;
		this.specificErrorMessage = specificErrorMessage;
	}
	
}

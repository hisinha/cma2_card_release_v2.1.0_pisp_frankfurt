package com.capgemini.psd2.adapter.datetime.utility.test;

import java.io.File;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.CustomDateSerializer;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomDateSerializerTest {
	@InjectMocks
	private CustomDateSerializer customDateSerializer;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testSerialize(){
		try { 
			XMLGregorianCalendar XMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
			JsonFactory factory = new JsonFactory();
			JsonGenerator generator = factory.createGenerator(new File("test.json"), JsonEncoding.UTF8);
			customDateSerializer.serialize(XMLGregorianCalendar, generator, null);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}

package com.capgemini.psd2.adapter.datetime.utility.test;

import static org.junit.Assert.assertNotNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;

@RunWith(SpringJUnit4ClassRunner.class)
public class TimeZoneDateTimeAdapterTest {
	
	
	@InjectMocks
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test/*(expected = AdapterException.class)*/
	public void testParseNewDateTimeFS(){
//		Date date = new SimpleDateFormat().parse("2007-10-11'T'23:30:26");
		Date date = new Date();
		ReflectionTestUtils.setField(timeZoneDateTimeAdapter, "timeZone", "Europe/Dublin");
		Date resultDate = timeZoneDateTimeAdapter.parseNewDateTimeFS(date);
		assertNotNull(resultDate);
		resultDate = timeZoneDateTimeAdapter.parseNewDateTimeFS(date);
	}
	
	@Test(expected = AdapterException.class)
	public void testParseDateFs(){
		String dateTime = "2007-10-11";
		Date date = timeZoneDateTimeAdapter.parseDateFS(dateTime);
		assertNotNull(date);
		timeZoneDateTimeAdapter.parseDateFS("test");
	}
	@Test(expected = AdapterException.class)
	public void testParseDateTimeFS(){
		String dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());
		Date date = timeZoneDateTimeAdapter.parseDateTimeFS(dateFormat);
		assertNotNull(date);
		
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		date = timeZoneDateTimeAdapter.parseDateTimeFS(dateFormat);
	}
	
	@Test
	public void testParseDateTimeCMA(){
		Date date = new Date();
		ReflectionTestUtils.setField(timeZoneDateTimeAdapter, "timeZone", "Europe/Dublin");
		String result = timeZoneDateTimeAdapter.parseDateTimeCMA(date);
		assertNotNull(result);
	}
	
	@Test
	public void testParseDateCMA() throws DatatypeConfigurationException{
		ReflectionTestUtils.setField(timeZoneDateTimeAdapter, "timeZone", "Europe/Dublin");
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(new Date());
		XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		String result = timeZoneDateTimeAdapter.parseDateCMA(xmlGregorianCalendar);
		assertNotNull(result);
	}
	
	@Test
	public void testparseDateTimeCMA() throws DatatypeConfigurationException{
		ReflectionTestUtils.setField(timeZoneDateTimeAdapter, "timeZone", "Europe/Dublin");
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(new Date());
		XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		String result = timeZoneDateTimeAdapter.parseDateTimeCMA(xmlGregorianCalendar);
		assertNotNull(result);
	}

}

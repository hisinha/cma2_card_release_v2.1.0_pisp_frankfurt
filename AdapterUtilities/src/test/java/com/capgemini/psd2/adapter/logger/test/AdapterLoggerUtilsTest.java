package com.capgemini.psd2.adapter.logger.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.logger.AdapterLoggerUtils;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.logger.LoggerUtils;

public class AdapterLoggerUtilsTest {

	@InjectMocks
	private AdapterLoggerUtils adapterLoggerUtils;

	@Mock
	private LoggerUtils loggerUtils;
	
	private LoggerAttribute mockLoggerAttribute = new LoggerAttribute();
	
	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		mockLoggerAttribute.setOperationName("doInternal");
		mockLoggerAttribute.setFinancialId("1234");
		mockLoggerAttribute.setApiId("accountinformationv1");
		ReflectionTestUtils.setField(adapterLoggerUtils,"foundationServiceApiId","foundationServiceApiId");
		ReflectionTestUtils.setField(adapterLoggerUtils,"loggerAttribute",mockLoggerAttribute);
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(mockLoggerAttribute);
	}

	@Test
	public void populateLoggerStartDataTest() {
		LoggerAttribute loggerAttribute = adapterLoggerUtils.populateLoggerStartData("doInternal");
		assertEquals(mockLoggerAttribute.getOperationName(), loggerAttribute.getOperationName());
		assertNotNull(loggerAttribute.getUpstream_start_time());
		assertNull(loggerAttribute.getUpstream_end_time());
		assertNull(loggerAttribute.getUpstream_request_payload());
	}

	@Test
	public void populateLoggerEndDate() {
		LoggerAttribute loggerAttribute = adapterLoggerUtils.populateLoggerEndData("doInternal");
		assertEquals(mockLoggerAttribute.getOperationName(), loggerAttribute.getOperationName());
		assertNotNull(loggerAttribute.getUpstream_end_time());
		assertNull(loggerAttribute.getUpstream_response_payload());
	}
}

package com.capgemini.psd2.adapter.aspect.test;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.aspect.PSD2AdapterAspect;
import com.capgemini.psd2.adapter.logger.AdapterLoggerUtils;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.mask.DataMask;
public class PSD2AdapterAspectTest {
	
	/** The data mask. */
	@Mock
	private DataMask dataMask;
	
	/** The logger attribute. */
	@Mock
	private LoggerAttribute loggerAttribute;

	/** The logger utils. */
	@Mock
	private AdapterLoggerUtils loggerUtils;

	@Mock
	private JoinPoint joinPoint;
		
	/** The signature. */
	@Mock
	private MethodSignature signature;
	
	/** The proceeding join point. */
	@Mock
	private ProceedingJoinPoint proceedingJoinPoint;
	
	/** The aspect. */
	@InjectMocks
	private PSD2AdapterAspect aspect = new PSD2AdapterAspect();
	
	/**
	 * Before.
	 */
	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		
		when(loggerUtils.populateLoggerStartData(anyString())).thenReturn(loggerAttribute);
		when(loggerUtils.populateLoggerEndData(anyString())).thenReturn(loggerAttribute);
		when(proceedingJoinPoint.getSignature()).thenReturn(signature);
		when(signature.getName()).thenReturn("retrieveAccountBalance");
		when(signature.getDeclaringTypeName()).thenReturn("retrieveAccountBalance");
		when(proceedingJoinPoint.getArgs()).thenReturn(new Object[1]);
		when(joinPoint.getSignature()).thenReturn(signature);
		when(signature.getDeclaringType()).thenReturn(String.class);
	}
	
	@Test
	public void arroundLoggerAdviceClientTest(){
		//doReturn(new Object()).when().methodPayloadAdvice(proceedingJoinPoint);
		ReflectionTestUtils.setField(aspect, "payloadLog", true);
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", true);
		ReflectionTestUtils.setField(aspect, "maskPayload", true);
		aspect.arroundLoggerAdviceClient(proceedingJoinPoint);
		
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", false);
		aspect.arroundLoggerAdviceClient(proceedingJoinPoint);
		
		ReflectionTestUtils.setField(aspect, "payloadLog", false);
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", false);
		ReflectionTestUtils.setField(aspect, "maskPayload", false);
		aspect.arroundLoggerAdviceClient(proceedingJoinPoint);
	}
	
	
	@Test
	public void arroundLoggerAdviceClientThrowableExceptionTest(){
		ReflectionTestUtils.setField(aspect, "payloadLog", false);
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", false);
		ReflectionTestUtils.setField(aspect, "maskPayload", false);
		aspect.arroundLoggerAdviceClient(proceedingJoinPoint);
	}
	
}

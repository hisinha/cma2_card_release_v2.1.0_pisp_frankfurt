package com.capgemini.psd2.adapter.datetime.utility.test;

import static org.junit.Assert.assertNotNull;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.DateAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
public class DateAdapterTest {

	@InjectMocks
	private DateAdapter dateAdapter;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testMarshal() throws Exception {
		String result = dateAdapter.marshal(new Date());
		assertNotNull(result);
	}
	
	@Test
	public void testUnmarshal() throws Exception{
		String dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		Date date = dateAdapter.unmarshal(dateFormat);
		assertNotNull(date);
	}
}

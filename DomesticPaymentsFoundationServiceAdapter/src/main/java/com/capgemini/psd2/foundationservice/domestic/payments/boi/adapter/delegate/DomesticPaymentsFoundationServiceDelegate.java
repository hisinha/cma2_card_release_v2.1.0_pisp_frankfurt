package com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionProposalComposite;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.transformer.DomesticPaymentsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;

import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticPaymentsFoundationServiceDelegate {

	@Autowired
	private DomesticPaymentsFoundationServiceTransformer domesticPaymentsFoundationServiceTransformer;
	
	@Value("${foundationService.transactioReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactioReqHeader;
	
	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationMuleReqHeader;
	
	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;
	
	@Value("${foundationService.sourcesystem:#{X-API-SOURCE-SYSTEM}}")
	private String sourcesystem;
	
	@Value("${foundationService.apiChannelCode:#{X-API-CHANNEL-CODE}}")
	private String apiChannelCode;
	
	@Value("${foundationService.partySourceId:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partySourceId;
	
	@Value("${foundationService.systemApiVersion:#{X-SYSTEM-API-VERSION}}")
	private String systemApiVersion;
	
	@Value("${foundationService.domesticPaymentSubmissionSetupVersion}")
	private String domesticPaymentSubmissionSetupVersion;
	
	@Value("${foundationService.channelBrand:#{X-API-CHANNEL-BRAND}}")
	private String channelBrand;
	
	
	public HttpHeaders createPaymentRequestHeaders(CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(sourcesystem, "PSD2API");
        
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
			httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		}
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER))) {
			httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		}
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.PARTY_IDENTIFIER))) {
			httpHeaders.add(partySourceId, params.get(PSD2Constants.PARTY_IDENTIFIER));
			
		}	
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
			httpHeaders.add(apiChannelCode, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).toUpperCase());
		}
		if (customStageIdentifiers.getPaymentSetupVersion().equalsIgnoreCase("1.0")) {
			httpHeaders.add(systemApiVersion, "1.1");
		} else {
			httpHeaders.add(systemApiVersion, "3.0");
		}
		
		// Code to be used after Product changes
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
			
		if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
			httpHeaders.add(channelBrand, BrandCode3.NIGB.toString());
		}else if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
			httpHeaders.add(channelBrand, BrandCode3.ROI.toString());
		}
	}
		return httpHeaders;
	}
	
	public String getPaymentFoundationServiceURL(String domesticPaymentBaseURL,String domesticPaymentId){
		return domesticPaymentBaseURL + "/" + "v" + domesticPaymentSubmissionSetupVersion+ "/" + "domestic/payment-instructions" + "/" +  domesticPaymentId;
	}


	public HttpHeaders createPaymentRequestHeadersPost(Map<String, String> params) {
		
		HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.add(sourcesystem, "PSD2API");
			httpHeaders.add(systemApiVersion, "3.0");

			if (!NullCheckUtils
					.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
				httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
			}
			if (!NullCheckUtils
					.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER))) {
				httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
			}	
			if (!NullCheckUtils
					.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
				httpHeaders.add(apiChannelCode, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).toUpperCase());
			}
			if (!NullCheckUtils
					.isNullOrEmpty(params.get(PSD2Constants.PARTY_IDENTIFIER))) {
				httpHeaders.add(partySourceId, params.get(PSD2Constants.PARTY_IDENTIFIER));
			}
			// Code to be used after Product changes
			if (!NullCheckUtils
					.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
				
			if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
				httpHeaders.add(channelBrand, BrandCode3.NIGB.toString());
			}else if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
				httpHeaders.add(channelBrand, BrandCode3.ROI.toString());
			}
		}
		
		return httpHeaders;
	}

	public String postPaymentFoundationServiceURL(String domesticPaymentSubmissionBaseURL) {
		
		return domesticPaymentSubmissionBaseURL+ "/" + "v" + domesticPaymentSubmissionSetupVersion + "/" + "domestic/payment-instructions";
	}

	public PaymentInstructionProposalComposite transformDomesticSubmissionResponseFromAPIToFDForInsert(
			CustomDPaymentsPOSTRequest domesticPaymentsRequest, Map<String, String> params) {
		
		return domesticPaymentsFoundationServiceTransformer.transformDomesticSubmissionResponseFromAPIToFDForInsert(domesticPaymentsRequest,params);
	}

	public PaymentDomesticSubmitPOST201Response transformDomesticConsentResponseFromFDToAPIForInsert(
			PaymentInstructionProposalComposite paymentInstructionProposalCompositeResponse) {
		
		return domesticPaymentsFoundationServiceTransformer.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposalCompositeResponse);
	}

	

}

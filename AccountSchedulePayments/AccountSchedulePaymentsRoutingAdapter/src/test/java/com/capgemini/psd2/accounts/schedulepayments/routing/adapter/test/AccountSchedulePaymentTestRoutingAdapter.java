package com.capgemini.psd2.accounts.schedulepayments.routing.adapter.test;

import java.util.Map;

import com.capgemini.psd2.accounts.schedulepayments.routing.adapter.test.mock.data.AccountSchedulePaymentMockData;
import com.capgemini.psd2.aisp.adapter.AccountSchedulePaymentsAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * Unit test for simple App.
 */

public class AccountSchedulePaymentTestRoutingAdapter implements AccountSchedulePaymentsAdapter{

	@Override
	public PlatformAccountSchedulePaymentsResponse retrieveAccountSchedulePayments(AccountMapping accountMapping,
			Map<String, String> params) {
		return AccountSchedulePaymentMockData.getMockSchedulePaymentGETResponse();
	}

}

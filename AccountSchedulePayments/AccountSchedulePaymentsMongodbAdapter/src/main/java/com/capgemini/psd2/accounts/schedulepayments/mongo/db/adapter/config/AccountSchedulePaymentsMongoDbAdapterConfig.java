package com.capgemini.psd2.accounts.schedulepayments.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.accounts.schedulepayments.mongo.db.adapter.impl.AccountSchedulePaymentsMongoDbAdapterImpl;
import com.capgemini.psd2.aisp.adapter.AccountSchedulePaymentsAdapter;

@Configuration
public class AccountSchedulePaymentsMongoDbAdapterConfig 
{
	@Bean(name="accountSchedulePaymentsMongoDbAdapter")
	public AccountSchedulePaymentsAdapter mongoDBAdapter(){
		return new AccountSchedulePaymentsMongoDbAdapterImpl();
	}
}

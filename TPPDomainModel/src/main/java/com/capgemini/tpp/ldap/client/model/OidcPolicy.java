package com.capgemini.tpp.ldap.client.model;

import java.util.List;

public class OidcPolicy {

	private String idTokenSigningAlgorithm;
	private PolicyGroup policyGroup;
	private String pingAccessLogoutCapable;
	private List<Object> logoutUris;

	public String getIdTokenSigningAlgorithm() {
		return this.idTokenSigningAlgorithm;
	}

	public void setIdTokenSigningAlgorithm(String idTokenSigningAlgorithm) {
		this.idTokenSigningAlgorithm = idTokenSigningAlgorithm;
	}

	public PolicyGroup getPolicyGroup() {
		return this.policyGroup;
	}

	public void setPolicyGroup(PolicyGroup policyGroup) {
		this.policyGroup = policyGroup;
	}

	private String grantAccessSessionRevocationApi;

	public String getGrantAccessSessionRevocationApi() {
		return this.grantAccessSessionRevocationApi;
	}

	public void setGrantAccessSessionRevocationApi(String grantAccessSessionRevocationApi) {
		this.grantAccessSessionRevocationApi = grantAccessSessionRevocationApi;
	}

	public String getPingAccessLogoutCapable() {
		return this.pingAccessLogoutCapable;
	}

	public void setPingAccessLogoutCapable(String pingAccessLogoutCapable) {
		this.pingAccessLogoutCapable = pingAccessLogoutCapable;
	}

	public List<Object> getLogoutUris() {
		return logoutUris;
	}

	public void setLogoutUris(List<Object> logoutUris) {
		this.logoutUris = logoutUris;
	}

}
package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * An OAuth2 Client
 */
@ApiModel(description = "An OAuth2 Client")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OAuth2Client   {
  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("url")
  private String url = null;

  @JsonProperty("iconUrl")
  private String iconUrl = null;

  @JsonProperty("emailAddress")
  private String emailAddress = null;

  public OAuth2Client name(String name) {
    this.name = name;
    return this;
  }

   /**
   * The name of the OAuth2 Client
   * @return name
  **/
  @ApiModelProperty(value = "The name of the OAuth2 Client")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OAuth2Client description(String description) {
    this.description = description;
    return this;
  }

   /**
   * The description of the OAuth2 Client
   * @return description
  **/
  @ApiModelProperty(value = "The description of the OAuth2 Client")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public OAuth2Client url(String url) {
    this.url = url;
    return this;
  }

   /**
   * The URL for the OAuth2 Client
   * @return url
  **/
  @ApiModelProperty(value = "The URL for the OAuth2 Client")


  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public OAuth2Client iconUrl(String iconUrl) {
    this.iconUrl = iconUrl;
    return this;
  }

   /**
   * The icon URL for this OAuth2 Client
   * @return iconUrl
  **/
  @ApiModelProperty(value = "The icon URL for this OAuth2 Client")


  public String getIconUrl() {
    return iconUrl;
  }

  public void setIconUrl(String iconUrl) {
    this.iconUrl = iconUrl;
  }

  public OAuth2Client emailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
    return this;
  }

   /**
   * The contact email for this OAuth2 Client
   * @return emailAddress
  **/
  @ApiModelProperty(value = "The contact email for this OAuth2 Client")


  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OAuth2Client oauth2Client = (OAuth2Client) o;
    return Objects.equals(this.name, oauth2Client.name) &&
        Objects.equals(this.description, oauth2Client.description) &&
        Objects.equals(this.url, oauth2Client.url) &&
        Objects.equals(this.iconUrl, oauth2Client.iconUrl) &&
        Objects.equals(this.emailAddress, oauth2Client.emailAddress);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, description, url, iconUrl, emailAddress);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OAuth2Client {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    iconUrl: ").append(toIndentedString(iconUrl)).append("\n");
    sb.append("    emailAddress: ").append(toIndentedString(emailAddress)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


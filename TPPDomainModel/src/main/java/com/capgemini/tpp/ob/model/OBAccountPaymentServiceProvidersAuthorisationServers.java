package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OBAccountPaymentServiceProvidersAuthorisationServers
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OBAccountPaymentServiceProvidersAuthorisationServers   {
  @JsonProperty("AutoRegistrationSupported")
  private Boolean autoRegistrationSupported = null;

  @JsonProperty("BaseApiDNSUri")
  private String baseApiDNSUri = null;

  @JsonProperty("ClientRegistrationUri")
  private String clientRegistrationUri = null;

  @JsonProperty("CustomerFriendlyDescription")
  private String customerFriendlyDescription = null;

  @JsonProperty("CustomerFriendlyLogoUri")
  private String customerFriendlyLogoUri = null;

  @JsonProperty("CustomerFriendlyName")
  private String customerFriendlyName = null;

  @JsonProperty("DeveloperPortalUri")
  private String developerPortalUri = null;

  @JsonProperty("Id")
  private String id = null;

  @JsonProperty("OpenIDConfigEndPointUri")
  private String openIDConfigEndPointUri = null;

  @JsonProperty("PayloadSigningCertLocation")
  private String payloadSigningCertLocation = null;

  @JsonProperty("TermsOfService")
  private String termsOfService = null;

  public OBAccountPaymentServiceProvidersAuthorisationServers autoRegistrationSupported(Boolean autoRegistrationSupported) {
    this.autoRegistrationSupported = autoRegistrationSupported;
    return this;
  }

   /**
   * Get autoRegistrationSupported
   * @return autoRegistrationSupported
  **/
  @ApiModelProperty(value = "")


  public Boolean getAutoRegistrationSupported() {
    return autoRegistrationSupported;
  }

  public void setAutoRegistrationSupported(Boolean autoRegistrationSupported) {
    this.autoRegistrationSupported = autoRegistrationSupported;
  }

  public OBAccountPaymentServiceProvidersAuthorisationServers baseApiDNSUri(String baseApiDNSUri) {
    this.baseApiDNSUri = baseApiDNSUri;
    return this;
  }

   /**
   * Get baseApiDNSUri
   * @return baseApiDNSUri
  **/
  @ApiModelProperty(value = "")


  public String getBaseApiDNSUri() {
    return baseApiDNSUri;
  }

  public void setBaseApiDNSUri(String baseApiDNSUri) {
    this.baseApiDNSUri = baseApiDNSUri;
  }

  public OBAccountPaymentServiceProvidersAuthorisationServers clientRegistrationUri(String clientRegistrationUri) {
    this.clientRegistrationUri = clientRegistrationUri;
    return this;
  }

   /**
   * The registration endpoint for TPP onboarding to ASPSPs.
   * @return clientRegistrationUri
  **/
  @ApiModelProperty(value = "The registration endpoint for TPP onboarding to ASPSPs.")


  public String getClientRegistrationUri() {
    return clientRegistrationUri;
  }

  public void setClientRegistrationUri(String clientRegistrationUri) {
    this.clientRegistrationUri = clientRegistrationUri;
  }

  public OBAccountPaymentServiceProvidersAuthorisationServers customerFriendlyDescription(String customerFriendlyDescription) {
    this.customerFriendlyDescription = customerFriendlyDescription;
    return this;
  }

   /**
   * Get customerFriendlyDescription
   * @return customerFriendlyDescription
  **/
  @ApiModelProperty(value = "")


  public String getCustomerFriendlyDescription() {
    return customerFriendlyDescription;
  }

  public void setCustomerFriendlyDescription(String customerFriendlyDescription) {
    this.customerFriendlyDescription = customerFriendlyDescription;
  }

  public OBAccountPaymentServiceProvidersAuthorisationServers customerFriendlyLogoUri(String customerFriendlyLogoUri) {
    this.customerFriendlyLogoUri = customerFriendlyLogoUri;
    return this;
  }

   /**
   * An image location where TPP's if they choose too, can retrieve a certified logo for the given authorisation server. Preferably as a scalable vector graphic. Customer work stream to define the ratios. Preferably delivered as a scalable vector graphic.
   * @return customerFriendlyLogoUri
  **/
  @ApiModelProperty(value = "An image location where TPP's if they choose too, can retrieve a certified logo for the given authorisation server. Preferably as a scalable vector graphic. Customer work stream to define the ratios. Preferably delivered as a scalable vector graphic.")


  public String getCustomerFriendlyLogoUri() {
    return customerFriendlyLogoUri;
  }

  public void setCustomerFriendlyLogoUri(String customerFriendlyLogoUri) {
    this.customerFriendlyLogoUri = customerFriendlyLogoUri;
  }

  public OBAccountPaymentServiceProvidersAuthorisationServers customerFriendlyName(String customerFriendlyName) {
    this.customerFriendlyName = customerFriendlyName;
    return this;
  }

   /**
   * Get customerFriendlyName
   * @return customerFriendlyName
  **/
  @ApiModelProperty(value = "")


  public String getCustomerFriendlyName() {
    return customerFriendlyName;
  }

  public void setCustomerFriendlyName(String customerFriendlyName) {
    this.customerFriendlyName = customerFriendlyName;
  }

  public OBAccountPaymentServiceProvidersAuthorisationServers developerPortalUri(String developerPortalUri) {
    this.developerPortalUri = developerPortalUri;
    return this;
  }

   /**
   * Get developerPortalUri
   * @return developerPortalUri
  **/
  @ApiModelProperty(value = "")


  public String getDeveloperPortalUri() {
    return developerPortalUri;
  }

  public void setDeveloperPortalUri(String developerPortalUri) {
    this.developerPortalUri = developerPortalUri;
  }

  public OBAccountPaymentServiceProvidersAuthorisationServers id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Unique Scheme Wide Authorisation Server id
   * @return id
  **/
  @ApiModelProperty(value = "Unique Scheme Wide Authorisation Server id")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public OBAccountPaymentServiceProvidersAuthorisationServers openIDConfigEndPointUri(String openIDConfigEndPointUri) {
    this.openIDConfigEndPointUri = openIDConfigEndPointUri;
    return this;
  }

   /**
   * Get openIDConfigEndPointUri
   * @return openIDConfigEndPointUri
  **/
  @ApiModelProperty(value = "")


  public String getOpenIDConfigEndPointUri() {
    return openIDConfigEndPointUri;
  }

  public void setOpenIDConfigEndPointUri(String openIDConfigEndPointUri) {
    this.openIDConfigEndPointUri = openIDConfigEndPointUri;
  }

  public OBAccountPaymentServiceProvidersAuthorisationServers payloadSigningCertLocation(String payloadSigningCertLocation) {
    this.payloadSigningCertLocation = payloadSigningCertLocation;
    return this;
  }

   /**
   * The location which is used for signing certificates
   * @return payloadSigningCertLocation
  **/
  @ApiModelProperty(value = "The location which is used for signing certificates")


  public String getPayloadSigningCertLocation() {
    return payloadSigningCertLocation;
  }

  public void setPayloadSigningCertLocation(String payloadSigningCertLocation) {
    this.payloadSigningCertLocation = payloadSigningCertLocation;
  }

  public OBAccountPaymentServiceProvidersAuthorisationServers termsOfService(String termsOfService) {
    this.termsOfService = termsOfService;
    return this;
  }

   /**
   * Get termsOfService
   * @return termsOfService
  **/
  @ApiModelProperty(value = "")


  public String getTermsOfService() {
    return termsOfService;
  }

  public void setTermsOfService(String termsOfService) {
    this.termsOfService = termsOfService;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBAccountPaymentServiceProvidersAuthorisationServers obAccountPaymentServiceProvidersAuthorisationServers = (OBAccountPaymentServiceProvidersAuthorisationServers) o;
    return Objects.equals(this.autoRegistrationSupported, obAccountPaymentServiceProvidersAuthorisationServers.autoRegistrationSupported) &&
        Objects.equals(this.baseApiDNSUri, obAccountPaymentServiceProvidersAuthorisationServers.baseApiDNSUri) &&
        Objects.equals(this.clientRegistrationUri, obAccountPaymentServiceProvidersAuthorisationServers.clientRegistrationUri) &&
        Objects.equals(this.customerFriendlyDescription, obAccountPaymentServiceProvidersAuthorisationServers.customerFriendlyDescription) &&
        Objects.equals(this.customerFriendlyLogoUri, obAccountPaymentServiceProvidersAuthorisationServers.customerFriendlyLogoUri) &&
        Objects.equals(this.customerFriendlyName, obAccountPaymentServiceProvidersAuthorisationServers.customerFriendlyName) &&
        Objects.equals(this.developerPortalUri, obAccountPaymentServiceProvidersAuthorisationServers.developerPortalUri) &&
        Objects.equals(this.id, obAccountPaymentServiceProvidersAuthorisationServers.id) &&
        Objects.equals(this.openIDConfigEndPointUri, obAccountPaymentServiceProvidersAuthorisationServers.openIDConfigEndPointUri) &&
        Objects.equals(this.payloadSigningCertLocation, obAccountPaymentServiceProvidersAuthorisationServers.payloadSigningCertLocation) &&
        Objects.equals(this.termsOfService, obAccountPaymentServiceProvidersAuthorisationServers.termsOfService);
  }

  @Override
  public int hashCode() {
    return Objects.hash(autoRegistrationSupported, baseApiDNSUri, clientRegistrationUri, customerFriendlyDescription, customerFriendlyLogoUri, customerFriendlyName, developerPortalUri, id, openIDConfigEndPointUri, payloadSigningCertLocation, termsOfService);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBAccountPaymentServiceProvidersAuthorisationServers {\n");
    
    sb.append("    autoRegistrationSupported: ").append(toIndentedString(autoRegistrationSupported)).append("\n");
    sb.append("    baseApiDNSUri: ").append(toIndentedString(baseApiDNSUri)).append("\n");
    sb.append("    clientRegistrationUri: ").append(toIndentedString(clientRegistrationUri)).append("\n");
    sb.append("    customerFriendlyDescription: ").append(toIndentedString(customerFriendlyDescription)).append("\n");
    sb.append("    customerFriendlyLogoUri: ").append(toIndentedString(customerFriendlyLogoUri)).append("\n");
    sb.append("    customerFriendlyName: ").append(toIndentedString(customerFriendlyName)).append("\n");
    sb.append("    developerPortalUri: ").append(toIndentedString(developerPortalUri)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    openIDConfigEndPointUri: ").append(toIndentedString(openIDConfigEndPointUri)).append("\n");
    sb.append("    payloadSigningCertLocation: ").append(toIndentedString(payloadSigningCertLocation)).append("\n");
    sb.append("    termsOfService: ").append(toIndentedString(termsOfService)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


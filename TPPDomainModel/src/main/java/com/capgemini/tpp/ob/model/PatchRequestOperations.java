package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PatchRequestOperations
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class PatchRequestOperations   {
  /**
   * Indicates the operation to perform and MAY be one of \"add\", \"remove\", or \"replace\".
   */
  public enum OpEnum {
    ADD("add"),
    
    REMOVE("remove"),
    
    REPLACE("replace");

    private String value;

    OpEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static OpEnum fromValue(String text) {
      for (OpEnum b : OpEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("op")
  private OpEnum op = null;

  @JsonProperty("path")
  private String path = null;

  public PatchRequestOperations op(OpEnum op) {
    this.op = op;
    return this;
  }

   /**
   * Indicates the operation to perform and MAY be one of \"add\", \"remove\", or \"replace\".
   * @return op
  **/
  @ApiModelProperty(value = "Indicates the operation to perform and MAY be one of \"add\", \"remove\", or \"replace\".")


  public OpEnum getOp() {
    return op;
  }

  public void setOp(OpEnum op) {
    this.op = op;
  }

  public PatchRequestOperations path(String path) {
    this.path = path;
    return this;
  }

   /**
   * Contains an attribute path describing the target of the operation. Optional for \"add\" and \"replace\" and is required for \"remove\" operations.
   * @return path
  **/
  @ApiModelProperty(value = "Contains an attribute path describing the target of the operation. Optional for \"add\" and \"replace\" and is required for \"remove\" operations.")


  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PatchRequestOperations patchRequestOperations = (PatchRequestOperations) o;
    return Objects.equals(this.op, patchRequestOperations.op) &&
        Objects.equals(this.path, patchRequestOperations.path);
  }

  @Override
  public int hashCode() {
    return Objects.hash(op, path);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PatchRequestOperations {\n");
    
    sb.append("    op: ").append(toIndentedString(op)).append("\n");
    sb.append("    path: ").append(toIndentedString(path)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


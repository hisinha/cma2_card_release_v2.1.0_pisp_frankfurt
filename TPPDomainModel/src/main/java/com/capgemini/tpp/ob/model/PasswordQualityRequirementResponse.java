package com.capgemini.tpp.ob.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The password quality requirements for a particular user
 */
@ApiModel(description = "The password quality requirements for a particular user")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class PasswordQualityRequirementResponse   {
  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    PASSWORDQUALITYREQUIREMENT("urn:pingidentity:schemas:2.0:PasswordQualityRequirement");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = new ArrayList<SchemasEnum>();

  @JsonProperty("currentPasswordRequired")
  private Boolean currentPasswordRequired = null;

  @JsonProperty("mustChangePassword")
  private Boolean mustChangePassword = null;

  @JsonProperty("secondsUntilPasswordExpiration")
  private Integer secondsUntilPasswordExpiration = null;

  @JsonProperty("passwordRequirements")
  private List<PasswordQualityRequirementResponsePasswordRequirements> passwordRequirements = null;

  public PasswordQualityRequirementResponse schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public PasswordQualityRequirementResponse addSchemasItem(SchemasEnum schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")
  @NotNull


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public PasswordQualityRequirementResponse currentPasswordRequired(Boolean currentPasswordRequired) {
    this.currentPasswordRequired = currentPasswordRequired;
    return this;
  }

   /**
   * True if the user's password is required for password updates.
   * @return currentPasswordRequired
  **/
  @ApiModelProperty(value = "True if the user's password is required for password updates.")


  public Boolean getCurrentPasswordRequired() {
    return currentPasswordRequired;
  }

  public void setCurrentPasswordRequired(Boolean currentPasswordRequired) {
    this.currentPasswordRequired = currentPasswordRequired;
  }

  public PasswordQualityRequirementResponse mustChangePassword(Boolean mustChangePassword) {
    this.mustChangePassword = mustChangePassword;
    return this;
  }

   /**
   * True if the user's password must be changed.
   * @return mustChangePassword
  **/
  @ApiModelProperty(value = "True if the user's password must be changed.")


  public Boolean getMustChangePassword() {
    return mustChangePassword;
  }

  public void setMustChangePassword(Boolean mustChangePassword) {
    this.mustChangePassword = mustChangePassword;
  }

  public PasswordQualityRequirementResponse secondsUntilPasswordExpiration(Integer secondsUntilPasswordExpiration) {
    this.secondsUntilPasswordExpiration = secondsUntilPasswordExpiration;
    return this;
  }

   /**
   * The number of seconds before the password will expire.
   * @return secondsUntilPasswordExpiration
  **/
  @ApiModelProperty(value = "The number of seconds before the password will expire.")


  public Integer getSecondsUntilPasswordExpiration() {
    return secondsUntilPasswordExpiration;
  }

  public void setSecondsUntilPasswordExpiration(Integer secondsUntilPasswordExpiration) {
    this.secondsUntilPasswordExpiration = secondsUntilPasswordExpiration;
  }

  public PasswordQualityRequirementResponse passwordRequirements(List<PasswordQualityRequirementResponsePasswordRequirements> passwordRequirements) {
    this.passwordRequirements = passwordRequirements;
    return this;
  }

  public PasswordQualityRequirementResponse addPasswordRequirementsItem(PasswordQualityRequirementResponsePasswordRequirements passwordRequirementsItem) {
    if (this.passwordRequirements == null) {
      this.passwordRequirements = new ArrayList<PasswordQualityRequirementResponsePasswordRequirements>();
    }
    this.passwordRequirements.add(passwordRequirementsItem);
    return this;
  }

   /**
   * A list of password requirements that must be met.
   * @return passwordRequirements
  **/
  @ApiModelProperty(value = "A list of password requirements that must be met.")

  @Valid

  public List<PasswordQualityRequirementResponsePasswordRequirements> getPasswordRequirements() {
    return passwordRequirements;
  }

  public void setPasswordRequirements(List<PasswordQualityRequirementResponsePasswordRequirements> passwordRequirements) {
    this.passwordRequirements = passwordRequirements;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PasswordQualityRequirementResponse passwordQualityRequirementResponse = (PasswordQualityRequirementResponse) o;
    return Objects.equals(this.schemas, passwordQualityRequirementResponse.schemas) &&
        Objects.equals(this.currentPasswordRequired, passwordQualityRequirementResponse.currentPasswordRequired) &&
        Objects.equals(this.mustChangePassword, passwordQualityRequirementResponse.mustChangePassword) &&
        Objects.equals(this.secondsUntilPasswordExpiration, passwordQualityRequirementResponse.secondsUntilPasswordExpiration) &&
        Objects.equals(this.passwordRequirements, passwordQualityRequirementResponse.passwordRequirements);
  }

  @Override
  public int hashCode() {
    return Objects.hash(schemas, currentPasswordRequired, mustChangePassword, secondsUntilPasswordExpiration, passwordRequirements);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PasswordQualityRequirementResponse {\n");
    
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    currentPasswordRequired: ").append(toIndentedString(currentPasswordRequired)).append("\n");
    sb.append("    mustChangePassword: ").append(toIndentedString(mustChangePassword)).append("\n");
    sb.append("    secondsUntilPasswordExpiration: ").append(toIndentedString(secondsUntilPasswordExpiration)).append("\n");
    sb.append("    passwordRequirements: ").append(toIndentedString(passwordRequirements)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


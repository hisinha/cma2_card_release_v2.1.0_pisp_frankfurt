package com.capgemini.tpp.dtos;

import java.util.ArrayList;
import java.util.List;

public class GrantScopes {

	private List<String> granttypes = new ArrayList<>();	
	private List<String> scopes = new ArrayList<>(); 
	
	public List<String> getGranttypes() {
		return granttypes;
	}
	public List<String> getScopes() {
		return scopes;
	}
	public void setGranttypes(List<String> granttypes) {
		this.granttypes = granttypes;
	}
	public void setScopes(List<String> scopes) {
		this.scopes = scopes;
	}
	
}

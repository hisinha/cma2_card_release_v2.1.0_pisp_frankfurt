package com.capgemini.tpp.dtos;

public class DeleteApplicationStage {

	DeleteApplicationStageEnum stage;

	public DeleteApplicationStageEnum getStage() {
		return stage;
	}

	public void setStage(DeleteApplicationStageEnum stage) {
		this.stage = stage;
	}
	
}

package com.capgemini.tpp.dtos;

public enum AddApplicationStageEnum {

	PF_APP_REG_SUCCESS(1,"Ping Federate Application Registeration Call SuccessFully Executed"),
	MULE_APP_REG_SUCCESS(2,"Mule TPP Application Registeration Call SuccessFully Executed"),
	PD_TPP_ORG_REG_SUCCESS(3,"Ping Directory TPP Organisation details Sucessfully Added"),
	PD_TPP_APP_ASS_SUCCESS(4,"Ping Directory TPP Application Association with TPP Successfully Executed");
	
	private int stageId;
	private String stageDescription;
	
	public int getStageId() {
		return stageId;
	}

	public String getStageDescription() {
		return stageDescription;
	}

	AddApplicationStageEnum(int stageId,String stageDescription){
		this.stageId = stageId;
		this.stageDescription = stageDescription;
	}
	
}

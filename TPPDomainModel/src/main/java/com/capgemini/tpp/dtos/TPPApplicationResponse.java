package com.capgemini.tpp.dtos;

import java.util.List;

import com.capgemini.tpp.ldap.client.model.TPPApplication;

public class TPPApplicationResponse{

	private List<TPPApplication> tppAppList;

	public List<TPPApplication> getTppAppList() {
		return tppAppList;
	}

	public void setTppAppList(List<TPPApplication> tppAppList) {
		this.tppAppList = tppAppList;
	}

}

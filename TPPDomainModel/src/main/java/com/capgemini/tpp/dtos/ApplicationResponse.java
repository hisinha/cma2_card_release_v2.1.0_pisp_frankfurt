package com.capgemini.tpp.dtos;

public class ApplicationResponse{
	
	private String tppApplicationName;

	public String getTppApplicationName() {
		return tppApplicationName;
	}

	public void setTppApplicationName(String tppApplicationName) {
		this.tppApplicationName = tppApplicationName;
	}
	
}

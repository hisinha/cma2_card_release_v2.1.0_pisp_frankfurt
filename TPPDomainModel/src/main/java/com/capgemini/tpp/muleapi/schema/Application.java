package com.capgemini.tpp.muleapi.schema;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "audit", "masterOrganizationId", "id", "name", "description", "coreServicesId", "url",
		"grantTypes", "redirectUri", "clientId", "clientSecret", "owner", "email", "owners" })
public class Application{

	@JsonProperty("audit")
	private Audit audit;
	@JsonProperty("masterOrganizationId")
	private String masterOrganizationId;
	@JsonProperty("id")
	private int id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("coreServicesId")
	private String coreServicesId;
	@JsonProperty("url")
	private String url;
	@JsonProperty("grantTypes")
	private List<Object> grantTypes = null;
	@JsonProperty("redirectUri")
	private List<Object> redirectUri = null;
	@JsonProperty("clientId")
	private String clientId;
	@JsonProperty("clientSecret")
	private String clientSecret;
	@JsonProperty("owner")
	private String owner;
	@JsonProperty("email")
	private String email;
	@JsonProperty("owners")
	private List<Owner> owners = null;

	private static final long serialVersionUID = -7927429510898251650L;

	@JsonProperty("audit")
	public Audit getAudit() {
		return audit;
	}

	@JsonProperty("audit")
	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	@JsonProperty("masterOrganizationId")
	public String getMasterOrganizationId() {
		return masterOrganizationId;
	}

	@JsonProperty("masterOrganizationId")
	public void setMasterOrganizationId(String masterOrganizationId) {
		this.masterOrganizationId = masterOrganizationId;
	}

	@JsonProperty("id")
	public int getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(int id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("coreServicesId")
	public String getCoreServicesId() {
		return coreServicesId;
	}

	@JsonProperty("coreServicesId")
	public void setCoreServicesId(String coreServicesId) {
		this.coreServicesId = coreServicesId;
	}

	@JsonProperty("url")
	public String getUrl() {
		return url;
	}

	@JsonProperty("url")
	public void setUrl(String url) {
		this.url = url;
	}

	@JsonProperty("grantTypes")
	public List<Object> getGrantTypes() {
		return grantTypes;
	}

	@JsonProperty("grantTypes")
	public void setGrantTypes(List<Object> grantTypes) {
		this.grantTypes = grantTypes;
	}

	@JsonProperty("redirectUri")
	public List<Object> getRedirectUri() {
		return redirectUri;
	}

	@JsonProperty("redirectUri")
	public void setRedirectUri(List<Object> redirectUri) {
		this.redirectUri = redirectUri;
	}

	@JsonProperty("clientId")
	public String getClientId() {
		return clientId;
	}

	@JsonProperty("clientId")
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@JsonProperty("clientSecret")
	public String getClientSecret() {
		return clientSecret;
	}

	@JsonProperty("clientSecret")
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	@JsonProperty("owner")
	public String getOwner() {
		return owner;
	}

	@JsonProperty("owner")
	public void setOwner(String owner) {
		this.owner = owner;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("owners")
	public List<Owner> getOwners() {
		return owners;
	}

	@JsonProperty("owners")
	public void setOwners(List<Owner> owners) {
		this.owners = owners;
	}

}
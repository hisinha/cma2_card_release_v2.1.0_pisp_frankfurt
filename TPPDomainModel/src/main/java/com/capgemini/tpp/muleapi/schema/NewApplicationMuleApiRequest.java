package com.capgemini.tpp.muleapi.schema;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "redirectUri", "apiEndpoints", "name", "description", "url" , "clientId" , "clientSecret"})
public class NewApplicationMuleApiRequest {

	@JsonProperty("redirectUri")
	private List<String> redirectUri = null;
	@JsonProperty("apiEndpoints")
	private boolean apiEndpoints;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("url")
	private String url;

	@JsonProperty("clientId")
	private String clientId;
	@JsonProperty("clientSecret")
	private String clientSecret;
	
	@JsonProperty("redirectUri")
	public List<String> getRedirectUri() {
		return redirectUri;
	}

	@JsonProperty("redirectUri")
	public void setRedirectUri(List<String> redirectUri) {
		this.redirectUri = redirectUri;
	}


	@JsonProperty("apiEndpoints")
	public boolean isApiEndpoints() {
		return apiEndpoints;
	}

	@JsonProperty("apiEndpoints")
	public void setApiEndpoints(boolean apiEndpoints) {
		this.apiEndpoints = apiEndpoints;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("url")
	public String getUrl() {
		return url;
	}

	@JsonProperty("url")
	public void setUrl(String url) {
		this.url = url;
	}

	@JsonProperty("clientId")
	public String getClientId() {
		return clientId;
	}

	@JsonProperty("clientId")
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@JsonProperty("clientSecret")
	public String getClientSecret() {
	return clientSecret;
	}

	@JsonProperty("clientSecret")
	public void setClientSecret(String clientSecret) {
	this.clientSecret = clientSecret;
	}
}	
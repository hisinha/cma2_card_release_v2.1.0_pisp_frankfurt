package com.capgemini.tpp.muleapi.schema;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/*{
	  "redirectUri": [],
	  "apiEndpoints": false,
	  "name": "app_test",
	  "description": "App for test",
	  "url": "http://www.app-test.com",
	  "coreServicesId": "9314239e3bff4cdf9ae3000a3eebd1e8",
	  "clientId": "9314239e3bff4cdf9ae3000a3eebd1e8",
	  "clientSecret": "b7845fa4e9ec42828980B9F5A62C3FCC",
	  "audit": {
	    "created": {
	      "date": "2016-08-02T16:20:44.420Z"
	    },
	    "updated": {}
	  },
	  "masterOrganizationId": "f0c9b011-980e-4928-9430-e60e3a97c043",
	  "id": 21265
	}*/
public class NewApplicationMuleApiResponse {

	private List<String> redirectUri;
	private String apiEndpoints;
	private String name;
	private String description;
	private String url;

	private String coreServicesId;
	private String clientId;
	private String clientSecret;
	private Audit audit;
	private String masterOrganizationId;
	private String id;
	@JsonIgnore
	private List<Owner> owners = null;

	/**
	 * @return the redirectUri
	 */
	public List<String> getRedirectUri() {
		return redirectUri;
	}

	/**
	 * @param redirectUri
	 *            the redirectUri to set
	 */
	public void setRedirectUri(List<String> redirectUri) {
		this.redirectUri = redirectUri;
	}

	/**
	 * @return the apiEndpoints
	 */
	public String getApiEndpoints() {
		return apiEndpoints;
	}

	/**
	 * @param apiEndpoints
	 *            the apiEndpoints to set
	 */
	public void setApiEndpoints(String apiEndpoints) {
		this.apiEndpoints = apiEndpoints;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the coreServicesId
	 */
	public String getCoreServicesId() {
		return coreServicesId;
	}

	/**
	 * @param coreServicesId the coreServicesId to set
	 */
	public void setCoreServicesId(String coreServicesId) {
		this.coreServicesId = coreServicesId;
	}

	/**
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	/**
	 * @return the clientSecret
	 */
	public String getClientSecret() {
		return clientSecret;
	}

	/**
	 * @param clientSecret the clientSecret to set
	 */
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	/**
	 * @return the audit
	 */
	public Audit getAudit() {
		return audit;
	}

	/**
	 * @param audit the audit to set
	 */
	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	/**
	 * @return the masterOrganizationId
	 */
	public String getMasterOrganizationId() {
		return masterOrganizationId;
	}

	/**
	 * @param masterOrganizationId the masterOrganizationId to set
	 */
	public void setMasterOrganizationId(String masterOrganizationId) {
		this.masterOrganizationId = masterOrganizationId;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonIgnore
	@JsonProperty("owners")
	public List<Owner> getOwners() {
		return owners;
	}

	@JsonIgnore
	@JsonProperty("owners")
	public void setOwners(List<Owner> owners) {
		this.owners = owners;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NewApplicationMuleApiResponse [getRedirectUri()=");
		builder.append(getRedirectUri());
		builder.append(", getApiEndpoints()=");
		builder.append(getApiEndpoints());
		builder.append(", getName()=");
		builder.append(getName());
		builder.append(", getDescription()=");
		builder.append(getDescription());
		builder.append(", getUrl()=");
		builder.append(getUrl());
		builder.append(", getCoreServicesId()=");
		builder.append(getCoreServicesId());
		builder.append(", getClientId()=");
		builder.append(getClientId());
		builder.append(", getClientSecret()=");
		builder.append(getClientSecret());
		builder.append(", getAudit()=");
		builder.append(getAudit());
		builder.append(", getMasterOrganizationId()=");
		builder.append(getMasterOrganizationId());
		builder.append(", getId()=");
		builder.append(getId());
		builder.append("]");
		return builder.toString();
	}

	

}

package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResetSecretResponse implements Serializable {

	private static final long serialVersionUID = -4110981162911699844L;
	
	@JsonIgnore
    @JsonProperty(value = "clientSecret")
	String clientSecret;
	

	@JsonIgnore
    @JsonProperty(value = "clientSecret")
	public String getClientSecret() {
		return clientSecret;
	}

	@JsonProperty("clientSecret")
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}


}

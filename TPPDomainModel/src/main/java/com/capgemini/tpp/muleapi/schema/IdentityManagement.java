
package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "service_provider",
    "saml"
})
public class IdentityManagement implements Serializable
{

    @JsonProperty("type")
    private Type type;
    @JsonProperty("service_provider")
    private ServiceProvider serviceProvider;
    @JsonProperty("saml")
    private Saml saml;
    private static final long serialVersionUID = 6166098518516942153L;

    @JsonProperty("type")
    public Type getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(Type type) {
        this.type = type;
    }

    @JsonProperty("service_provider")
    public ServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    @JsonProperty("service_provider")
    public void setServiceProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    @JsonProperty("saml")
    public Saml getSaml() {
        return saml;
    }

    @JsonProperty("saml")
    public void setSaml(Saml saml) {
        this.saml = saml;
    }

}

package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "audit", "id", "apiId" })
public class ApiVersion implements Serializable {

	@JsonProperty("audit")
	private Audit audit;
	@JsonProperty("id")
	private int id;
	@JsonProperty("apiId")
	private int apiId;
	
	private static final long serialVersionUID = 7837240194881087267L;

	@JsonProperty("audit")
	public Audit getAudit() {
		return audit;
	}

	@JsonProperty("audit")
	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	

	@JsonProperty("id")
	public int getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(int id) {
		this.id = id;
	}

	

	@JsonProperty("apiId")
	public int getApiId() {
		return apiId;
	}

	@JsonProperty("apiId")
	public void setApiId(int apiId) {
		this.apiId = apiId;
	}

	

}

package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "apiVisual",
    "mozart",
    "api"
})
public class DesignCenter implements Serializable
{

    @JsonProperty("apiVisual")
    private Boolean apiVisual;
    @JsonProperty("mozart")
    private Boolean mozart;
    @JsonProperty("api")
    private Boolean api;
    private static final long serialVersionUID = 4990450085403427386L;

    @JsonProperty("apiVisual")
    public Boolean getApiVisual() {
        return apiVisual;
    }

    @JsonProperty("apiVisual")
    public void setApiVisual(Boolean apiVisual) {
        this.apiVisual = apiVisual;
    }

    @JsonProperty("mozart")
    public Boolean getMozart() {
        return mozart;
    }

    @JsonProperty("mozart")
    public void setMozart(Boolean mozart) {
        this.mozart = mozart;
    }

    @JsonProperty("api")
    public Boolean getApi() {
        return api;
    }

    @JsonProperty("api")
    public void setApi(Boolean api) {
        this.api = api;
    }

}

package com.capgemini.tpp.dtos;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.tpp.dtos.ThirdPartyProvidersDetails;
import com.capgemini.tpp.ob.model.OrganisationEmailAddresses;
import com.capgemini.tpp.ob.model.OrganisationPhoneNumbers;

@RunWith(SpringJUnit4ClassRunner.class)
public class ThirdPartyProvidersDetailsTest {
	
	private ThirdPartyProvidersDetails thirdPartyProvidersDetails;
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
		thirdPartyProvidersDetails = new ThirdPartyProvidersDetails();
	}
	@Test
	public void test() {
		thirdPartyProvidersDetails.setOrganizationName("organizationName");
		List<OrganisationEmailAddresses> tppEmailIDs = new ArrayList<>();
		OrganisationEmailAddresses emailId = new OrganisationEmailAddresses();
		emailId.setName("Email");
		emailId.setValue("http://localhost:80/test");
		tppEmailIDs.add(emailId);
		List<String> emailList = new ArrayList<>();
		emailList.add(tppEmailIDs.get(0).getValue());
		thirdPartyProvidersDetails.setTppEmailID(emailList);
		thirdPartyProvidersDetails.setTppMemberStateCompetentAuthority("tppMemberStateCompetentAuthority");
		thirdPartyProvidersDetails.setTppOrganizationId("tppOrganizationId");
		List<OrganisationPhoneNumbers> tppPhoneNumbers = new ArrayList<>();
		OrganisationPhoneNumbers tppPhoneNumber = new OrganisationPhoneNumbers();
		tppPhoneNumber.setName("Mobile");
		tppPhoneNumber.setValue("23482394932");
		tppPhoneNumbers.add(tppPhoneNumber );
		List<String> phonenumbers=new ArrayList<>();
		phonenumbers.add(tppPhoneNumbers.get(0).getValue());
		thirdPartyProvidersDetails.setTppPhoneNumber(phonenumbers);
		thirdPartyProvidersDetails.setTppRegistrationID("tppRegistrationID");
		List<String> tppRoles = new ArrayList<>();
		String tppRole = "AISP";
		tppRoles.add(tppRole );
		thirdPartyProvidersDetails.setTppRoles(tppRoles);
		assertEquals("organizationName",thirdPartyProvidersDetails.getOrganizationName());
		assertEquals("http://localhost:80/test", thirdPartyProvidersDetails.getTppEmailID().get(0));
		assertEquals("tppMemberStateCompetentAuthority", thirdPartyProvidersDetails.getTppMemberStateCompetentAuthority());
		assertEquals("tppOrganizationId", thirdPartyProvidersDetails.getTppOrganizationId());
		assertEquals("23482394932", thirdPartyProvidersDetails.getTppPhoneNumber().get(0));
		assertEquals("tppRegistrationID", thirdPartyProvidersDetails.getTppRegistrationID());
		assertEquals("AISP", thirdPartyProvidersDetails.getTppRoles().get(0));
		
	}
	@After
	public void tearDown() throws Exception {
		thirdPartyProvidersDetails = null;

	}

}

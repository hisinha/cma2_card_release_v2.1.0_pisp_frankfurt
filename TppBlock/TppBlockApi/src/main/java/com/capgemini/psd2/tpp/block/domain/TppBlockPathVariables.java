package com.capgemini.psd2.tpp.block.domain;

public class TppBlockPathVariables {

	private String tppId;

	public String getTppId() {
		return tppId;
	}

	public void setTppId(String tppId) {
		this.tppId = tppId;
	}

}

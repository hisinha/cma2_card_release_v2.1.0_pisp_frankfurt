package com.capgemini.psd2.pisp.file.payment.routing.adapter.routing;

import com.capgemini.psd2.pisp.adapter.FilePaymentsAdapter;

@FunctionalInterface
public interface FPaymentsAdapterFactory {

	public FilePaymentsAdapter getFilePaymentsStagingInstance(String coreSystemName);
}

package com.capgemini.psd2.pisp.file.payments.routing.test.adapter.routing;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.adapter.FilePaymentsAdapter;
import com.capgemini.psd2.pisp.file.payment.routing.adapter.routing.FPaymentsCoreSystemAdapterFactory;
import com.capgemini.psd2.pisp.file.payments.routing.adapter.impl.FilePaymentsRoutingAdapterImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class FPaymentsCoreSystemAdapterFactoryTest {

	@InjectMocks
	FPaymentsCoreSystemAdapterFactory factory;

	@Mock
	ApplicationContext applicationContext;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetFilePaymentSetupStagingInstance() {
		FilePaymentsRoutingAdapterImpl adapter = new FilePaymentsRoutingAdapterImpl();
		when(applicationContext.getBean(anyString())).thenReturn(adapter);
		FilePaymentsAdapter fAdapter =  factory.getFilePaymentsStagingInstance("test");
		assertNotNull(fAdapter);
	}

	@Test
	public void testSetApplicationContext() {
		factory.setApplicationContext(new ApplicationContextMock());
	}

}

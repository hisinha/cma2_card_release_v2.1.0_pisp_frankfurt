package com.capgemini.psd2.pisp.file.payments.test.controller;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomestic1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFile1;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.file.payment.test.mock.data.FilePaymentPOSTRequestMockData;
import com.capgemini.psd2.pisp.file.payments.controller.FilePaymentsController;
import com.capgemini.psd2.pisp.file.payments.service.FilePaymentsService;
//import com.capgemini.psd2.pisp.file.payments.test.mock.data.FilePaymentConsentPOSTRequestResponseMockData;
import com.capgemini.psd2.pisp.validation.adapter.FilePaymentValidator;

@RunWith(SpringJUnit4ClassRunner.class)
public class FilePaymentsControllerTest {

	private MockMvc mockMvc;

	@Mock
	private FilePaymentsService filePaymentsService;

	@Mock
	private FilePaymentValidator filePaymentValidator;

	@InjectMocks
	private FilePaymentsController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testSubmitFilePaymentResource() throws Exception {
		CustomFilePaymentsPOSTRequest request = new CustomFilePaymentsPOSTRequest();
		PaymentFileSubmitPOST201Response response = new PaymentFileSubmitPOST201Response();

		boolean r = true;
		when(filePaymentsService.createFilePaymentsResource(anyObject(), eq(r))).thenReturn(response);

		this.mockMvc.perform(post("/file-payments").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(FilePaymentPOSTRequestMockData.asJsonString(request))).andExpect(status().isCreated());
	}

	/*@Test
	public void testFindFilePaymentResourceStatus() throws Exception {
		CustomFilePaymentsPOSTRequest request = new CustomFilePaymentsPOSTRequest();
		PaymentFileSubmitPOST201Response response = new PaymentFileSubmitPOST201Response();
		OBWriteDataFile1 data = new OBWriteDataFile1();
		request.setData(data);
		
		when(filePaymentsService.findStatusFilePaymentsResource(anyString())).thenReturn(response);

		this.mockMvc.perform(
				post("/file-payments/{filePaymentId}", 123214).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(FilePaymentPOSTRequestMockData.asJsonString(request)))
				.andExpect(status().isOk());
	}*/

	@Test
	public void testdownloadFilePaymentReport() throws Exception {

		PlatformFilePaymentsFileResponse response = new PlatformFilePaymentsFileResponse();
		response.setFileName("transaction.xml");
		response.setFileType(MediaType.APPLICATION_XML_VALUE);
		response.setFileByteArray("hi".getBytes());

		when(filePaymentValidator.validateFPaymentsDownloadRequest(anyObject())).thenReturn(true);
		PlatformFilePaymentsFileResponse fileResponse = new PlatformFilePaymentsFileResponse();
		fileResponse.setFileName("transaction.xml");
		fileResponse.setFileType(MediaType.APPLICATION_XML_VALUE);
		fileResponse.setFileByteArray("hi".getBytes());

		when(filePaymentsService.downloadTransactionFileByConsentId(anyString())).thenReturn(fileResponse);

		this.mockMvc.perform(get("/file-payments/{paymentId}/report-file", 2934872).content("hi".getBytes()))
				.andExpect(status().isOk());

	}
}

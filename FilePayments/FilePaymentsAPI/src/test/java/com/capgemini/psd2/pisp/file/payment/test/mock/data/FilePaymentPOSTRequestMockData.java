package com.capgemini.psd2.pisp.file.payment.test.mock.data;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FilePaymentPOSTRequestMockData {

	public static CustomFilePaymentSetupPOSTRequest getPaymentPOSTRequest() {
		return new CustomFilePaymentSetupPOSTRequest();
		
	}
	
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}


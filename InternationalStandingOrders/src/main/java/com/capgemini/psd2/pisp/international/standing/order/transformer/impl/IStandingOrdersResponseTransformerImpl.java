package com.capgemini.psd2.pisp.international.standing.order.transformer.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionTransformer;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Lazy
@Component("internationalStandingOrderTransformer")
public class IStandingOrdersResponseTransformerImpl implements PaymentSubmissionTransformer<CustomIStandingOrderPOSTResponse, CustomIStandingOrderPOSTRequest> {

	@Autowired
	private PispDateUtility pispDateUtility;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PaymentsPlatformAdapter paymentsPlatformAdapter;
	
	@Override
	public CustomIStandingOrderPOSTResponse paymentSubmissionResponseTransformer(
			CustomIStandingOrderPOSTResponse paymentSubmissionResponse, Map<String, Object> paymentsPlatformResourceMap,
			String methodType) {

		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);
		PaymentConsentsPlatformResource paymentConentsPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.CONSENT);

		/* Adding Prefix uk.obie. in creditor scheme in response */
		String creditorAccountScheme = paymentSubmissionResponse.getData().getInitiation().getCreditorAccount()
				.getSchemeName();
		if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(creditorAccountScheme)) {
			paymentSubmissionResponse.getData().getInitiation().getCreditorAccount()
					.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(creditorAccountScheme));
		}

		/* Adding Prefix uk.obie. in debtor scheme in response */
		if (!NullCheckUtils.isNullOrEmpty(paymentSubmissionResponse.getData().getInitiation().getDebtorAccount())) {
			String debtorAccountScheme = paymentSubmissionResponse.getData().getInitiation().getDebtorAccount()
					.getSchemeName();
			if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(debtorAccountScheme)) {
				paymentSubmissionResponse.getData().getInitiation().getDebtorAccount()
						.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(debtorAccountScheme));
			}
		}

		if (methodType.equals(RequestMethod.POST.toString())) {
			paymentSubmissionResponse.getData().setCreationDateTime(paymentsPlatformResource.getCreatedAt());
			paymentSubmissionResponse.getData()
					.setStatus(OBExternalStatus1Code.valueOf(paymentsPlatformResource.getStatus().toUpperCase()));
			paymentSubmissionResponse.getData()
					.setStatusUpdateDateTime(paymentsPlatformResource.getStatusUpdateDateTime());
			if (paymentConentsPlatformResource.getTppDebtorDetails().equalsIgnoreCase(Boolean.FALSE.toString())) {
				paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(null);
			}

		} else {
			paymentSubmissionResponse.getData().setCreationDateTime(paymentsPlatformResource.getCreatedAt());
			OBExternalStatus1Code status = OBExternalStatus1Code
					.valueOf(paymentsPlatformResource.getStatus().toUpperCase());
			String statusUpdateDateTime = paymentsPlatformResource.getStatusUpdateDateTime();

			if (paymentsPlatformResource.getStatus().equals(OBExternalStatus1Code.INITIATIONPENDING.toString())) {
				if (paymentSubmissionResponse.getData().getStatus() == OBExternalStatus1Code.INITIATIONCOMPLETED) {
					status = paymentSubmissionResponse.getData().getStatus();
					statusUpdateDateTime = paymentSubmissionResponse.getData().getStatusUpdateDateTime();
					updatePaymentsResource(paymentsPlatformResource, status);
				}

				else if (!NullCheckUtils.isNullOrEmpty(paymentSubmissionResponse.getData().getMultiAuthorisation())) {
					if (paymentSubmissionResponse.getData().getMultiAuthorisation()
							.getStatus() == OBExternalStatus2Code.AUTHORISED) {
						status = OBExternalStatus1Code.INITIATIONCOMPLETED;
						statusUpdateDateTime = PispUtilities.getCurrentDateInISOFormat();
						updatePaymentsResource(paymentsPlatformResource, status);
					} else if (paymentSubmissionResponse.getData().getMultiAuthorisation()
							.getStatus() == OBExternalStatus2Code.REJECTED) {
						status = OBExternalStatus1Code.INITIATIONFAILED;
						statusUpdateDateTime = PispUtilities.getCurrentDateInISOFormat();
						updatePaymentsResource(paymentsPlatformResource, status);
					}
				}
			}
			paymentSubmissionResponse.getData().setStatus(status);
			paymentSubmissionResponse.getData().setStatusUpdateDateTime(statusUpdateDateTime);
		}

		updateMetaAndLinks(paymentSubmissionResponse, methodType);
		return paymentSubmissionResponse;
	}

	private CustomIStandingOrderPOSTResponse updateMetaAndLinks(CustomIStandingOrderPOSTResponse submissionResponse, String methodType) {
		if (submissionResponse.getLinks() == null)
			submissionResponse.setLinks(new PaymentSetupPOSTResponseLinks());
		submissionResponse.getLinks()
				.setSelf(PispUtilities.populateLinks(submissionResponse.getData().getInternationalStandingOrderId(),
						methodType, reqHeaderAtrributes.getSelfUrl()));

		if (submissionResponse.getMeta() == null)
			submissionResponse.setMeta(new PaymentSetupPOSTResponseMeta());
		return submissionResponse;
	}

	private void updatePaymentsResource(PaymentsPlatformResource paymentsPlatformResource,
			OBExternalStatus1Code status) {
		paymentsPlatformResource.setStatus(status.toString());
		paymentsPlatformAdapter.updatePaymentsPlatformResource(paymentsPlatformResource);
	}

	@Override
	public CustomIStandingOrderPOSTRequest paymentSubmissionRequestTransformer(
			CustomIStandingOrderPOSTRequest paymentSubmissionRequest) {
		paymentSubmissionRequest.getData().getInitiation()
		.setFirstPaymentDateTime(pispDateUtility.transformDateTimeInRequest(
				paymentSubmissionRequest.getData().getInitiation().getFirstPaymentDateTime()));
paymentSubmissionRequest.getData().getInitiation()
		.setFinalPaymentDateTime(pispDateUtility.transformDateTimeInRequest(
				paymentSubmissionRequest.getData().getInitiation().getFinalPaymentDateTime()));
		return paymentSubmissionRequest;
	}

}

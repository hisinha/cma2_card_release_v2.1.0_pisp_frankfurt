package com.capgemini.psd2.pisp.international.standing.order.comparator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBInternationalStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class InternationalStandingOrdersPayloadComparator {

	public int comparePaymentDetails(CustomIStandingOrderConsentsPOSTResponse response,
			CustomIStandingOrderPOSTRequest request, PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		CustomIStandingOrderConsentsPOSTResponse paymentSetupResponse = null;
		CustomIStandingOrderConsentsPOSTResponse tempPaymentSetupResponse = null;
		CustomIStandingOrderConsentsPOSTResponse adaptedPaymentResponse = null;
		CustomIStandingOrderPOSTRequest paymentSubmissionRequest = null;
		int returnValue = 1;

		if (response instanceof CustomIStandingOrderConsentsPOSTResponse) {
			paymentSetupResponse = (CustomIStandingOrderConsentsPOSTResponse) response;
			String creationDateTime = paymentSetupResponse.getData().getCreationDateTime();
			paymentSetupResponse.getData().setCreationDateTime(null);
			String strPaymentResponse = JSONUtilities.getJSONOutPutFromObject(paymentSetupResponse);
			tempPaymentSetupResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentResponse, CustomIStandingOrderConsentsPOSTResponse.class);
			paymentSetupResponse.getData().setCreationDateTime(creationDateTime);
		}

		if (request instanceof CustomIStandingOrderPOSTRequest) {
			paymentSubmissionRequest = (CustomIStandingOrderPOSTRequest) request;
			String strSubmissionRequest = JSONUtilities.getJSONOutPutFromObject(paymentSubmissionRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strSubmissionRequest, CustomIStandingOrderConsentsPOSTResponse.class);
		}

		if (tempPaymentSetupResponse != null && adaptedPaymentResponse != null) {

			if (!validateDebtorDetails(tempPaymentSetupResponse.getData().getInitiation(),
					adaptedPaymentResponse.getData().getInitiation(), paymentSetupPlatformResource))
				return 1;
			compareAmount(tempPaymentSetupResponse.getData().getInitiation().getInstructedAmount(),
					adaptedPaymentResponse.getData().getInitiation().getInstructedAmount());

			/*
			 * Date: 13-Feb-2018 Changes are made for CR-10
			 */
			checkAndModifyEmptyFields(adaptedPaymentResponse, response);
			// End of CR-10

			returnValue = compare(tempPaymentSetupResponse, adaptedPaymentResponse);
		}
		return returnValue;

	}

	private void compareAmount(OBDomestic1InstructedAmount resAmount, OBDomestic1InstructedAmount reqAmount) {
		if (resAmount != null && reqAmount != null) {
			if (Float.compare(Float.parseFloat(resAmount.getAmount()), Float.parseFloat(reqAmount.getAmount())) == 0)
				resAmount.setAmount(reqAmount.getAmount());
		}
	}

	private int compare(CustomIStandingOrderConsentsPOSTResponse tempPaymentSetupResponse,
			CustomIStandingOrderConsentsPOSTResponse paymentResponse) {

		int value = 1;

		if (Float.parseFloat(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount()) == Float
				.parseFloat(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount())) {
			paymentResponse.getData().getInitiation().getInstructedAmount()
					.setAmount(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount())
				&& !NullCheckUtils
						.isNullOrEmpty(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount()))
			if (Float.parseFloat(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount()) == Float
					.parseFloat(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount())) {
				paymentResponse.getData().getInitiation().getInstructedAmount()
						.setAmount(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount());
			}

		/*
		 * if (!NullCheckUtils.isNullOrEmpty(paymentResponse.getData().
		 * getInitiation().getFinalPaymentAmount()) && !NullCheckUtils
		 * .isNullOrEmpty(paymentResponse.getData().getInitiation().
		 * getFinalPaymentAmount())) if
		 * (Float.parseFloat(paymentResponse.getData().getInitiation().
		 * getFinalPaymentAmount().getAmount()) == Float
		 * .parseFloat(paymentResponse.getData().getInitiation().
		 * getFinalPaymentAmount().getAmount())) {
		 * paymentResponse.getData().getInitiation().getFinalPaymentAmount()
		 * .setAmount(paymentResponse.getData().getInitiation().
		 * getFinalPaymentAmount().getAmount()); }
		 */

		if (paymentResponse.getData().getInitiation().equals(paymentResponse.getData().getInitiation())
				&& paymentResponse.getRisk().equals(paymentResponse.getRisk()))
			value = 0;
		return value;
	}

	private void checkAndModifyEmptyFields(CustomIStandingOrderConsentsPOSTResponse adaptedPaymentResponse,
			CustomIStandingOrderConsentsPOSTResponse response) {

		if (adaptedPaymentResponse.getRisk().getDeliveryAddress() != null) {
			checkAndModifyAddressLine(adaptedPaymentResponse.getRisk().getDeliveryAddress());
			checkAndModifyCountrySubDivision(adaptedPaymentResponse.getRisk().getDeliveryAddress());
		}

		if (adaptedPaymentResponse.getData().getInitiation().getCreditor().getPostalAddress() != null
				&& adaptedPaymentResponse.getData().getInitiation().getCreditor().getPostalAddress()
						.getAddressLine() != null) {
			checkAndModifyCreditorPostalAddressLine(
					adaptedPaymentResponse.getData().getInitiation().getCreditor().getPostalAddress(), response);
		}

	}

	private void checkAndModifyCountrySubDivision(OBRisk1DeliveryAddress deliveryAddress) {
		if (deliveryAddress.getCountrySubDivision() != null) {

			deliveryAddress.setCountrySubDivision(deliveryAddress.getCountrySubDivision());

		}
	}

	private void checkAndModifyAddressLine(OBRisk1DeliveryAddress deliveryAddress) {

		if (deliveryAddress.getAddressLine() != null) {
			List<String> addressLineList = new ArrayList<>();
			for (String addressLine : deliveryAddress.getAddressLine()) {
				if (NullCheckUtils.isNullOrEmpty(addressLine))
					continue;
				addressLineList.add(addressLine);
			}
			if (addressLineList.isEmpty())
				addressLineList = null;

			deliveryAddress.setAddressLine(addressLineList);

		}
	}

	private void checkAndModifyCreditorPostalAddressLine(OBPostalAddress6 obPostalAddress6,
			CustomIStandingOrderConsentsPOSTResponse response) {
		List<String> addressLineList = new ArrayList<>();
		for (String addressLine : (obPostalAddress6.getAddressLine())) {
			if (NullCheckUtils.isNullOrEmpty(addressLine))
				continue;
			addressLineList.add(addressLine);
		}
		if (addressLineList.isEmpty()
				&& response.getData().getInitiation().getCreditor().getPostalAddress().getAddressLine() == null)
			addressLineList = null;

		obPostalAddress6.setAddressLine(addressLineList);
	}

	private boolean validateDebtorDetails(OBInternationalStandingOrder1 responseInitiation,
			OBInternationalStandingOrder1 requestInitiation,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {

			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be set
			 * foundation response DebtorAccount.Name as null so that it can be passed in
			 * comparison. Both fields would have the value as 'Null'.
			 */
			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
				responseInitiation.getDebtorAccount().setName(null);
			return Objects.equals(responseInitiation.getDebtorAccount(), requestInitiation.getDebtorAccount());
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& requestInitiation.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& responseInitiation.getDebtorAccount() != null) {
			responseInitiation.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;

	}

}

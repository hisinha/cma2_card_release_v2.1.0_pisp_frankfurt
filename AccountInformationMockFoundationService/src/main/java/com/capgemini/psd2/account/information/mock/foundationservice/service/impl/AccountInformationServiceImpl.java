/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.information.mock.foundationservice.domain.Accnt;
import com.capgemini.psd2.account.information.mock.foundationservice.domain.Accounts;
import com.capgemini.psd2.account.information.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.account.information.mock.foundationservice.repository.AccountInformationRepository;
import com.capgemini.psd2.account.information.mock.foundationservice.service.AccountInformationService;

/**
 * The Class AccountInformationServiceImpl.
 */
@Service
public class AccountInformationServiceImpl implements AccountInformationService {

	/** The repository. */
	@Autowired
	private AccountInformationRepository repository;

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.account.information.mock.foundationservice.service.AccountInformationService#retrieveAccountInformation(java.lang.String, java.lang.String)
	 */
	@Override
	public Accounts retrieveAccountInformation(String accountNsc, String accountNumber) throws Exception  {

		Accnt accnt = repository.findByAccountNSCAndAccountNumber(accountNsc, accountNumber);
		
		if (accnt == null) {
			throw new RecordNotFoundException("Not Found");
		}
		
		Accounts account = new Accounts();
		account.setAccount(accnt);

		return account;

	}

}
